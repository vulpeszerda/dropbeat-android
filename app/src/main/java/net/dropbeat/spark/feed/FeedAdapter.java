package net.dropbeat.spark.feed;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.play.PlayerContext;

import org.w3c.dom.Text;

/**
 * Created by vulpes on 15. 5. 1..
 */
public abstract class FeedAdapter extends ArrayAdapter<Track> {

    private TrackClickListener mListener;
    private String mPlaylistId;
    private int mLayoutId;

    protected abstract void updateView(ViewHolder holder, Track track,
                                       int position);

    public FeedAdapter(Context context, int layoutId) {
        super(context);
        mLayoutId = layoutId;
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(mLayoutId, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }
        final Track track = getItem(position);

        ImageButton menuBtn = viewHolder.getMenuBtn();
        View hoverView = viewHolder.getThumbHover();

        View.OnClickListener listener =new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, track, position);
                }
            }
        };

        row.setOnClickListener(listener);
        menuBtn.setOnClickListener(listener);

        boolean isPlaying;
        isPlaying = PlayerContext.playState == PlayState.LOADING ||
                PlayerContext.playState == PlayState.PLAYING ||
                PlayerContext.playState == PlayState.PAUSED;
        isPlaying &= PlayerContext.currTrack != null &&
                PlayerContext.currTrack.getId().equals(track.getId());
        isPlaying &= PlayerContext.playlistId != null &&
                mPlaylistId != null &&
                PlayerContext.playlistId.equals(mPlaylistId);

        if (isPlaying) {
            viewHolder.getTitle().setTypeface(null, Typeface.BOLD);
            hoverView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.getTitle().setTypeface(null, Typeface.NORMAL);
            hoverView.setVisibility(View.GONE);
        }

        updateView(viewHolder, track, position);

        return row;
    }

    public void setPlaylistId(String playlistId) {
        mPlaylistId = playlistId;
    }

    public void setTrackClickListener(TrackClickListener listener) {
        mListener = listener;
    }

    public class ViewHolder {
        private TextView mTitleView;
        private TextView mArtistView;
        private TextView mDescView;
        private TextView mGenreView;
        private ImageButton mMenuBtn;
        private ImageView mThumbView;
        private View mThumbHoverView;
        private TextView mRankView;
        private TextView mChanneView;

        public ViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.thumbnail);
            mThumbHoverView = view.findViewById(R.id.thumbnail_hover);
            mTitleView = (TextView) view.findViewById(R.id.title);
            mArtistView = (TextView) view.findViewById(R.id.artist);
            mDescView = (TextView) view.findViewById(R.id.description);
            mGenreView = (TextView) view.findViewById(R.id.genre);
            mMenuBtn = (ImageButton) view.findViewById(R.id.menu_button);
            mRankView = (TextView) view.findViewById(R.id.rank);
            mChanneView = (TextView) view.findViewById(R.id.channel_name);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public TextView getArtist() {
            return mArtistView;
        }

        public TextView getDescView() {
            return mDescView;
        }

        public TextView getGenreView() {
            return mGenreView;
        }

        public TextView getRankView() {
            return mRankView;
        }

        public TextView getChannelView() {
            return mChanneView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public View getThumbHover() {
            return mThumbHoverView;
        }

        public ImageButton getMenuBtn() {
            return mMenuBtn;
        }
    }
}
