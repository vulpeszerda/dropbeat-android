package net.dropbeat.spark.feed;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.Nullable;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.UserTrack;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by vulpes on 15. 10. 8..
 */
public class FeedNewUploadFragment extends BaseFeedFragment {

    public static FeedNewUploadFragment newInstance(
            ArrayList<FilterOption> options) {

        FeedNewUploadFragment fragment = new FeedNewUploadFragment();
        Bundle args = new Bundle();
        args.putSerializable(PARAM_OPTIONS, options);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mOptionTitle.setText("ORDER BY:");
        mImageOptions = new DisplayImageOptions.Builder()
            .showImageOnLoading(new ColorDrawable(0x00000000))
//                .displayer(new FadeInBitmapDisplayer(300))
            .cacheOnDisk(true)
            .cacheInMemory(true)
            .showImageOnFail(R.drawable.default_cover_big)
            .build();
    }

    @Override
    protected List<Track> parseResponse(JSONObject response) throws JSONException {
        if (!response.optBoolean("success")) {
            return null;
        }

        List<Track> tracks = new ArrayList<>();

        JSONArray data = response.getJSONArray("data");

        for (int i = 0; i < data.length(); i++) {
            JSONObject trackObj = data.getJSONObject(i);
            Track track = UserTrack.fromJsonObject(trackObj);
            if (track != null) {
                tracks.add(track);
            }
        }

        return tracks;
    }

    @Override
    protected String getSectionKey() {
        return FeedFragment.KEY_PAGE_NEW_UPLOAD;
    }

    @Override
    protected void requestToApi(FilterOption option, int pageIdx,
                                boolean forceRefresh,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
        Requests.getStreamNewUpload(getBaseActivity(), option.key, pageIdx,
                listener, errorListener);
    }

    @Override
    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {
        holder.getTitle().setText(track.getTrackName());
        mImageLoader.displayImage(track.getHqThunbnailUrl(),
                holder.getThumb(), mImageOptions);

        if (!TextUtils.isEmpty(track.getUserName())) {
            holder.getArtist().setText(track.getUserName());
            holder.getArtist().setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(track.getDesc())) {
            String desc = "Uploaded on " + new SimpleDateFormat("MMM d, yyyy",
                    Locale.ENGLISH).format(track.getCreatedAt());
            holder.getDescView().setText(desc);
            holder.getDescView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected boolean hasPagenation() {
        if (mCurrentOption != null && mCurrentOption.getKey() != "popular") {
            return true;
        }
        return false;
    }

    @Override
    protected String getPlaylistName() {
        String name = "New Uploads";
        if (mCurrentOption != null) {
            name += " - "  + mCurrentOption.name;
        }
        return name;
    }

    @Override
    protected int getTrackLayoutId() {
        return R.layout.feed_item_big;
    }
}
