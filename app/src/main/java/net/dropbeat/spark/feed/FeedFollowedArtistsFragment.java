package net.dropbeat.spark.feed;

import android.accounts.Account;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.dropbeat.spark.NavigationDrawerActivity;
import net.dropbeat.spark.NavigationDrawerFragment;
import net.dropbeat.spark.NavigationItem;
import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.model.Drop;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vulpes on 15. 8. 12..
 */
public class FeedFollowedArtistsFragment extends BaseFeedFragment {

    private boolean mIsFollowingLoading = false;
    private View mFollowingView;
    private View mNeedSigninView;

    public static FeedFollowedArtistsFragment newInstance(ArrayList<FilterOption>
                                                             options) {
        FeedFollowedArtistsFragment fragment = new FeedFollowedArtistsFragment();
        Bundle args =  new Bundle();
        args.putSerializable(PARAM_OPTIONS, options);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mOptionTitle.setText("ORDER BY:");

        mImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(new ColorDrawable(0x00000000))
//                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_cover_big)
                .build();

        if (mFollowingView != null && mListView.getHeaderViewsCount() == 0) {
            addHeaderView(mFollowingView);
        }

        if (DbAccount.getUser(getBaseActivity()) != null) {
            loadFollowingInfos();
        } else {
            if (mNeedSigninView == null) {
                mNeedSigninView = mLayoutInflater.inflate(R.layout.followed_artist_header, null, false);
                View emptyView = mNeedSigninView.findViewById(R.id.empty);
                emptyView.setVisibility(View.VISIBLE);

                View nonemptyView = mNeedSigninView.findViewById(R.id.nonempty);
                nonemptyView.setVisibility(View.GONE);

                View signinBtn = mNeedSigninView.findViewById(R.id.search_artist_button);

                signinBtn.setOnClickListener(this);
            }
            addHeaderView(mNeedSigninView);
            if (mFollowingView != null) {
                removeHeaderView(mFollowingView);
                mFollowingView = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DbAccount.getUser(getBaseActivity()) != null) {
            if (!mIsFollowingLoading) {
                loadFollowingInfos();
            }
        } else {
            if (mFollowingView != null) {
                removeHeaderView(mFollowingView);
                mFollowingView = null;
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.search_artist_button:
            case R.id.search_more_artist_button:
                if (DbAccount.getUser(getBaseActivity()) == null) {
                    Intent intent = AuthActivity.createIntent(getBaseActivity(),
                            true);
                    startActivity(intent);
                    return;
                }
                int menuType = NavigationDrawerFragment.MenuType.SEARCH;
                NavigationDrawerFragment.changeMenu(getBaseActivity(), menuType);
                break;
        }
    }

    @Override
    protected List<Track> parseResponse(JSONObject response) throws JSONException {
        if (!response.optBoolean("success")) {
            return null;
        }
        List<Track> tracks = new ArrayList<>();

        JSONArray data = response.getJSONArray("data");
        Gson gson = DbGson.getInstance();

        Type listType = new TypeToken<ArrayList<FollowedArtistTrack>>() {}.getType();
        List<FollowedArtistTrack> trendingTracks = gson.fromJson(data.toString(),
                listType);

        for(FollowedArtistTrack t : trendingTracks) {
            tracks.add(t.toTrack());
        }

        return tracks;
    }

    @Override
    protected String getSectionKey() {
        return FeedFragment.KEY_PAGE_FOLLOWED_ARTISTS;
    }

    @Override
    protected void requestToApi(FilterOption option, int pageIdx,
                                boolean forceRefresh,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {

        Requests.getStreamFollowing(getBaseActivity(), option.key, pageIdx,
                false, listener, errorListener);
    }

    @Override
    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {
        holder.getTitle().setText(track.getTitle());
        mImageLoader.displayImage(track.getHqThunbnailUrl(),
                holder.getThumb(), mImageOptions);

        if (!TextUtils.isEmpty(track.getUserName())) {
            holder.getArtist().setText(track.getUserName());
            holder.getArtist().setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(track.getDesc())) {
            holder.getDescView().setText(track.getDesc());
            holder.getDescView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected boolean hasPagenation() {
        return true;
    }

    @Override
    protected String getPlaylistName() {
        String name = "Followed Artists";
        if (mCurrentOption != null) {
            name += " - " + mCurrentOption.name;
        }
        return name;
    }

    @Override
    protected int getTrackLayoutId() {
        return R.layout.feed_item_big;
    }

    private void loadFollowingInfos() {
        if (mIsFollowingLoading) {
            return;
        }
        mIsFollowingLoading = true;
        RobustResponseListener<JSONObject> listener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsFollowingLoading = false;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mIsFollowingLoading = false;
                if (!response.optBoolean("success")) {
                    Toast.makeText(getBaseActivity(),
                            "Failed to load following info.", Toast.LENGTH_SHORT);
                }
                try {
                    JSONArray followingsArray = response.getJSONArray("data");
                    int count = followingsArray.length();
                    updateFollowingInfoView(count);
                } catch(JSONException e) {
                    Toast.makeText(getBaseActivity(),
                            "Failed to load following info.", Toast.LENGTH_SHORT);
                }
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mIsFollowingLoading = false;
                Toast.makeText(getBaseActivity(), "Failed to load following " +
                        "info. " + msg, Toast.LENGTH_SHORT);
            }

            @Override
            public void onSkipped(VolleyError error) {
                mIsFollowingLoading = false;
            }
        };

        Requests.getFollowings(getBaseActivity(),
                DbAccount.getUser(getBaseActivity()).getUid(), listener, errorListener);
    }

    private void updateFollowingInfoView(int count) {
        if (mFollowingView == null) {
            mFollowingView = mLayoutInflater.inflate(R.layout
                    .followed_artist_header, null, false);
            addHeaderView(mFollowingView);
        }

        if (mNeedSigninView != null) {
            removeHeaderView(mNeedSigninView);
            mNeedSigninView = null;
        }

        View emptyView = mFollowingView.findViewById(R.id.empty);
        View nonEmptyView = mFollowingView.findViewById(R.id
                .nonempty);
        TextView followingInfoView = (TextView) mFollowingView
                .findViewById(R.id.following_info);
        Button searchArtistBtn = (Button) mFollowingView
                .findViewById(R.id.search_artist_button);
        Button searchMoreArtistBtn = (Button) mFollowingView
                .findViewById(R.id.search_more_artist_button);

        searchArtistBtn.setOnClickListener(this);
        searchMoreArtistBtn.setOnClickListener(this);

        if (count == 0) {
            emptyView.setVisibility(View.VISIBLE);
            nonEmptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            followingInfoView.setText("You are following " +
                    count + "artist" + (count > 1 ? "s" : ""));
            nonEmptyView.setVisibility(View.VISIBLE);
        }
    }

    private static class FollowedArtistTrack {
        String artistName;
        String releaseDate;
        String title;
        String id;
        String type;
        Drop drop;

        public Track toTrack() {

            String desc = null;
            if (!TextUtils.isEmpty(releaseDate) && releaseDate.length() >= 10) {
                String dateStr = releaseDate.substring(0, 10);
                SimpleDateFormat remoteFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date releaseDate = remoteFormat.parse(dateStr);
                    desc = "Released on " + new SimpleDateFormat("MMM d, yyyy",
                            Locale.ENGLISH).format(releaseDate);
                } catch(ParseException e) {

                }
            }

            String thumbnailUrl = null;
            String hqThumbnailUrl = null;

            if (TextUtils.isEmpty(thumbnailUrl) && type.equals("youtube")) {
                thumbnailUrl = "http://img.youtube.com/vi/" + id +
                        "/mqdefault.jpg";
            }
            if (TextUtils.isEmpty(hqThumbnailUrl) && type.equals("youtube")) {
                hqThumbnailUrl = "http://img.youtube.com/vi/" + id +
                        "/hqdefault.jpg";
            }

            Track track = new Track();
            track.setTitle(title);
            track.setId(id);
            track.setType(type);
            track.setDesc(desc);
            track.setDrop(drop);
            track.setUserName(artistName);
            track.setThumbnailUrl(thumbnailUrl);
            track.setHqThumbnailUrl(hqThumbnailUrl);
            return track;
        }
    }
}
