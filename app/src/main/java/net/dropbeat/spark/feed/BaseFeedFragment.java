package net.dropbeat.spark.feed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonSyntaxException;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.addon.EndlessScrollListener;
import net.dropbeat.spark.addon.Nullable;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.playlist.PlaylistFragment;
import net.dropbeat.spark.view.MaterialProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 12..
 */
public abstract class BaseFeedFragment extends BaseFragment implements
        TrackClickListener, OnFilterOptionChangeListener, View.OnClickListener {

    public static final String PARAM_OPTIONS = "param_options";

    @InjectView(R.id.progress)
    protected MaterialProgressBar mProgressBar;

    @InjectView(R.id.failure)
    protected View mFailureView;

    @InjectView(R.id.failure_msg)
    protected TextView mFailureMsgView;

    @InjectView(android.R.id.list)
    protected ListView mListView;

    @InjectView(R.id.retry_button)
    protected Button mRetryBtn;

    @Nullable
    @InjectView(R.id.option_frame)
    protected View mOptionFrame;

    @Nullable
    @InjectView(R.id.option_title)
    protected TextView mOptionTitle;

    @Nullable
    @InjectView(R.id.option_select)
    protected Spinner mOptionSelect;

    protected FilterOption mCurrentOption;
    protected FeedAdapter mAdapter;
    protected int mNextPageIdx = 0;
    protected List<FilterOption> mOptions;
    protected LayoutInflater mLayoutInflater;
    protected ImageLoader mImageLoader;
    protected DisplayImageOptions mImageOptions;
    protected boolean mDidInitialLoad = false;

    private LinearLayout mHeaderContainerView;
    private LinearLayout mFooterContainerView;

    private boolean mIsInitialized;
    private int mPrevPageIdx = -1;
    private FilterOption mPrevOption = null;
    private boolean mIsLoading = false;
    private OptionAdapter mOptionAdapter;


    private BroadcastReceiver mPlayStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isAdded() && (PlayerContext.playState == PlayState.LOADING ||
                    PlayerContext.playState == PlayState.STOPPED) &&
                    mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    protected abstract List<Track> parseResponse(JSONObject response) throws JSONException;
    protected abstract String getSectionKey();
    protected abstract void requestToApi(FilterOption option, int pageIdx,
                                         boolean forceRefresh,
                                         Response.Listener<JSONObject> listener,
                                         Response.ErrorListener errorListener);
    protected abstract boolean hasPagenation();
    protected abstract String getPlaylistName();
    protected abstract int getTrackLayoutId();


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        manager.unregisterReceiver(mPlayStateChangeReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        IntentFilter filter =
                new IntentFilter(PlayerService.ACTION_NOTIFY_PLAYSTATE_CHANGE);
        manager.registerReceiver(mPlayStateChangeReceiver, filter);
        if (mAdapter != null && mDidInitialLoad) {
            mAdapter.notifyDataSetChanged();
            refresh();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_contents, container,
                false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean isInitialLoad = !mIsInitialized;
        mLayoutInflater = LayoutInflater.from(getBaseActivity());

        if (!mIsInitialized) {
            mIsInitialized = true;

            DbApplication app = (DbApplication) getBaseActivity()
                    .getApplication();
            mImageLoader = app.getDefaultImageLoader();
            mImageOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.default_cover_big)
//                .displayer(new FadeInBitmapDisplayer(300))
                    .cacheOnDisk(true)
                    .cacheInMemory(true)
                    .showImageOnFail(R.drawable.default_thumb)
                    .build();

            mAdapter = createAdapter();
            mAdapter.setTrackClickListener(this);



            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);

            // set header container
            mHeaderContainerView = new LinearLayout(getActivity());
            mHeaderContainerView.setOrientation(LinearLayout.VERTICAL);
            mHeaderContainerView.setLayoutParams(layoutParams);

            // set header container
            mFooterContainerView = new LinearLayout(getActivity());
            mFooterContainerView.setOrientation(LinearLayout.VERTICAL);
            mFooterContainerView.setLayoutParams(layoutParams);

            Bundle args = getArguments();
            if (hasFilterOptions()) {
                mOptions = (List<FilterOption>) args.getSerializable
                    (PARAM_OPTIONS);
                if (mOptions != null && mOptions.size() > 0) {
                    mCurrentOption = mOptions.get(0);
                    mOptionAdapter = new OptionAdapter(getBaseActivity());
                    mOptionAdapter.addAll(mOptions);
                }
            }
            mNextPageIdx = 0;

            mAdapter.setPlaylistId(getPlaylistId());
        }

        updateHeader(mHeaderContainerView);
        updateFooter(mFooterContainerView);

        mListView.addHeaderView(mHeaderContainerView);
        mListView.addFooterView(mFooterContainerView);
        mListView.setAdapter(mAdapter);

        if (hasFilterOptions()) {
            mOptionSelect.setAdapter(mOptionAdapter);
            mOptionSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    FilterOption option = mOptionAdapter.getItem(position);
                    onFilterOptionChanged(option);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mOptionFrame.setVisibility(View.VISIBLE);
        } else if (mOptionFrame != null) {
            mOptionFrame.setVisibility(View.GONE);
        }

        mListView.setOnScrollListener(new EndlessScrollListener() {

            @Override
            public void onLoadMore(String page, int totalItemsCount) {
                if (!mIsLoading && page != null &&
                        Integer.parseInt(page) > 0 &&
                        mAdapter.getCount() > 0 &&
                        hasPagenation()) {
                    loadResources(mCurrentOption, Integer.parseInt(page));
                }
            }

            @Override
            public String getNextPage() {
                return String.valueOf(mNextPageIdx);
            }
        });

        mRetryBtn.setOnClickListener(this);

        if (isInitialLoad) {
            refresh();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry_button:
                if (mNextPageIdx == 0) {
                    refresh();
                } else {
                    mPrevOption = null;
                    mPrevPageIdx = -1;
                    loadResources(mCurrentOption, mNextPageIdx);
                }
                break;
        }
    }

    @Override
    public void onClick(View view, final Track track, int position) {
        switch(view.getId()) {
            case R.id.track:
                ArrayList<Track> tracks = new ArrayList<>(mAdapter.getItems());
                Playlist playlist = new Playlist(getPlaylistId(),
                        getPlaylistName(), tracks);
                PlayerContext.externalPlaylist = playlist;
                PlayerService.doPlay(getBaseActivity(), track, playlist.getId());
                break;
            case R.id.menu_button:
                PopupMenu menu = new PopupMenu(getBaseActivity(), view);
                menu.getMenuInflater().inflate(R.menu.menu_addable_track, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onTrackPopupMenuClicked(track, item);
                    }
                });
                menu.show();
                break;
        }
    }

    @Override
    public void onFilterOptionChanged(FilterOption option) {
        if (mCurrentOption == option) {
            return;
        }
        mCurrentOption = option;
        mNextPageIdx = 0;
        mAdapter.clear();
        mAdapter.setPlaylistId(getPlaylistId());
        mAdapter.notifyDataSetChanged();
        refresh();
    }

    protected boolean hasFilterOptions() {
        return true;
    }

    protected boolean onTrackPopupMenuClicked(Track track, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                PlaylistFragment.addTrack(getBaseActivity(), track,
                        getSectionKey());
                return true;
            case R.id.action_share:
                track.share(getBaseActivity());
                return true;
        }
        return false;
    }

    protected void addHeaderView(View view) {
        if (mHeaderContainerView.findViewById(view.getId()) != null) {
            return;
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mHeaderContainerView.addView(view, params);
    }

    protected void removeHeaderView(View view) {
        mHeaderContainerView.removeView(view);
    }

    protected void addFooterView(View view) {
        if (mFooterContainerView.findViewById(view.getId()) != null) {
            return;
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mFooterContainerView.addView(view, params);
    }

    protected void removeFooterView(View view) {
        mFooterContainerView.removeView(view);
    }

    protected void refresh() {
        mNextPageIdx = 0;
        mPrevOption = null;
        mPrevPageIdx = -1;
        loadResources(mCurrentOption, mNextPageIdx, true);



        String action = getSectionKey();
        if (mCurrentOption != null) {
            action += "_" + mCurrentOption.getName().toLowerCase()
                    .replace(" ", "_");
        }

        DbApplication app = (DbApplication) getBaseActivity().getApplication();
        Tracker tracker = app.getAnalyticsTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("load_feed")
                .setAction(action)
                .setLabel("feed")
                .setValue(1).build());
    }

    protected String getPlaylistId() {
        String key = getSectionKey();
        if (mCurrentOption != null && !TextUtils.isEmpty(mCurrentOption.key)) {
            key += "_" + mCurrentOption.key;
        }
        return key;
    }

    protected void updateHeader(View headerView) {

    }

    protected void updateFooter(View footerView) {

    }

    protected FeedAdapter createAdapter() {
        return new FeedAdapter(getBaseActivity(), getTrackLayoutId()) {
            @Override
            protected void updateView(ViewHolder holder, Track track,
                                      int position) {
                updateFeedTrackView(holder, track, position);
            }
        };
    }

    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {
        TextView titleView = holder.getTitle();
        titleView.setText(track.getTitle());

        mImageLoader.displayImage(track.getThumbnailUrl(), holder.getThumb(),
                mImageOptions);
    }

    protected void loadResources(FilterOption option, int pageIdx) {
        loadResources(option, pageIdx, false);
    }

    protected void loadResources(FilterOption option, int pageIdx,
                                 final boolean forceRefresh) {
        if (!forceRefresh &&
                (mNextPageIdx < 0 ||
                        (mPrevOption != null && (
                                (option == null && mPrevOption == null) ||
                                (option.key == null && mPrevOption.key == null) ||
                                option.key.equals(mPrevOption.key))
                                && pageIdx == mPrevPageIdx))) {
            return;
        }

        if (mIsLoading) {
            return;
        }

        mIsLoading = true;
        mPrevOption = option;
        mPrevPageIdx = pageIdx;

        showProgressIndicator(forceRefresh);
        hideFailure();

        RobustResponseListener<JSONObject> listener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsLoading = false;
                mDidInitialLoad = true;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mIsLoading = false;
                mDidInitialLoad = true;

                hideProgressIndicator();
                List<Track> tracks;
                try {
                    tracks = parseResponse(response);
                } catch(JSONException|JsonSyntaxException e) {
                    tracks = null;
                }
                if (tracks == null) {
                    String errorMsg = "Failed to fetch feed.\n";
                    showFailure(errorMsg);
                    return;
                }
                if (forceRefresh) {
                    mAdapter.clear();
                }
                if (!hasPagenation() || tracks.size() == 0) {
                    mNextPageIdx = -1;
                } else {
                    mNextPageIdx += 1;
                }
                for (Track track:tracks) {
                    mAdapter.add(track);
                }
                mAdapter.notifyDataSetChanged();

                if (PlayerContext.externalPlaylist != null && PlayerContext
                        .externalPlaylist.getId().equals(getPlaylistId())) {

                    Playlist playlist = PlayerContext.externalPlaylist;
                    playlist.addAll(new ArrayList<>(mAdapter.getItems()), true);

                    if (PlayerContext.playlistId != null &&
                            PlayerContext.playlistId.equals(getPlaylistId()) &&
                            PlayerContext.currTrack != null) {

                        Track currTrack = PlayerContext.currTrack;
                        List<Track> allTracks = playlist.getTracks();
                        for (int i = 0; i < allTracks.size(); i++) {
                            if (allTracks.get(i).getId().equals(currTrack
                                    .getId())) {
                                PlayerContext.trackIdx = i;
                                break;
                            }
                        }
                    }
                }
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mDidInitialLoad = true;
                mIsLoading = false;
                hideProgressIndicator();

                String errorMsg = "Failed to fetch feed.\n";
                if (TextUtils.isEmpty(msg)) {
                    errorMsg += msg;
                }
                showFailure(errorMsg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                mDidInitialLoad = true;
                mIsLoading = false;
            }
        };

        requestToApi(option, pageIdx, forceRefresh, listener, errorListener);
    }

    private void showFailure(String msg) {
        if (mAdapter.getCount() == 0) {
            mFailureView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            mFailureMsgView.setText(msg);
        } else {
            View failureView = mLayoutInflater.inflate(R.layout
                    .feed_footer_failure, null, false);
            TextView msgView = (TextView)failureView.findViewById(R.id
                    .failure_msg);
            msgView.setText(msg);

            Button retryBtn = (Button)failureView.findViewById(R.id
                    .retry_button);
            retryBtn.setOnClickListener(this);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            mFooterContainerView.addView(failureView, params);
        }
    }

    private void hideFailure() {
        mListView.setVisibility(View.VISIBLE);
        mFailureView.setVisibility(View.GONE);
        View footerFailureView = mFooterContainerView.findViewById(R.id
                .footer_failure);
        if (footerFailureView != null) {
            mFooterContainerView.removeView(footerFailureView);
        }
    }

    private void showProgressIndicator(boolean forceRefresh) {
        if (forceRefresh || mAdapter.getCount() == 0) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            View progressView = mLayoutInflater.inflate(R.layout
                    .loading_footer, null, false);

            mFooterContainerView.removeAllViews();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            mFooterContainerView.addView(progressView, params);
        }
    }

    private void hideProgressIndicator() {
        mProgressBar.setVisibility(View.GONE);
        View progressView = mFooterContainerView.findViewById(R.id.loading_progress_frame);
        if (progressView != null) {
            mFooterContainerView.removeView(progressView);
        }
    }

    protected static class OptionAdapter extends ArrayAdapter<FilterOption> {

        public OptionAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = mInflater.inflate(R.layout.genre_item, parent, false);
            }
            ((TextView)row.findViewById(R.id.text)).setText(getItem(position).name);
            return row;
        }
    }

    public static class FilterOption implements Serializable{
        String key;
        String name;
        public FilterOption(String key, String name) {
            this.key = key;
            this.name = name;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }
    }

}
