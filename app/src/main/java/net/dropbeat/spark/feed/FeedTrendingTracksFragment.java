package net.dropbeat.spark.feed;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.dropbeat.spark.R;
import net.dropbeat.spark.model.Drop;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vulpes on 15. 8. 12..
 */
public class FeedTrendingTracksFragment extends BaseFeedFragment {

    public static FeedTrendingTracksFragment newInstance(ArrayList<FilterOption>
                                                                 options) {
        FeedTrendingTracksFragment fragment = new FeedTrendingTracksFragment();
        Bundle args =  new Bundle();
        args.putSerializable(PARAM_OPTIONS, options);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(new ColorDrawable(0x00000000))
//                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_cover_big)
                .build();
    }

    @Override
    protected List<Track> parseResponse(JSONObject response) throws JSONException{
        if (!response.optBoolean("success")) {
            return null;
        }
        List<Track> tracks = new ArrayList<>();

        JSONArray data = response.getJSONArray("data");
        Gson gson = DbGson.getInstance();

        if (mCurrentOption.key == null) {
            Type listType = new TypeToken<ArrayList<TrendingTrack>>() {}.getType();
            List<TrendingTrack> trendingTracks = gson.fromJson(data.toString(),
                    listType);

            for(TrendingTrack t : trendingTracks) {
                tracks.add(t.toTrack());
            }

        } else {
            Type listType = new TypeToken<ArrayList<TrendingSubGenreTrack>>() {}.getType();
            List<TrendingSubGenreTrack> trendingTracks = gson.fromJson(data.toString(),
                    listType);
            for(TrendingSubGenreTrack t : trendingTracks) {
                tracks.add(t.toTrack());
            }
        }
        return tracks;
    }

    @Override
    protected String getSectionKey() {
        return FeedFragment.KEY_PAGE_TRENDING_TRACKS;
    }

    @Override
    protected void requestToApi(FilterOption option, int pageIdx,
                                boolean forceRefresh,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {

        Requests.getStreamTrending(getBaseActivity(), option.key, pageIdx,
                listener, errorListener);
    }

    @Override
    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {

        holder.getTitle().setText(track.getTrackName());
        mImageLoader.displayImage(track.getHqThunbnailUrl(), holder.getThumb(),
                mImageOptions);

        if (!TextUtils.isEmpty(track.getUserName())) {
            holder.getArtist().setText(track.getUserName());
            holder.getArtist().setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(track.getDesc())) {
            holder.getDescView().setText(track.getDesc());
            holder.getDescView().setVisibility(View.VISIBLE);
        }
        holder.getRankView().setVisibility(View.VISIBLE);
        holder.getRankView().setText(String.valueOf(position + 1));
    }

    @Override
    protected boolean hasPagenation() {
        return true;
    }

    @Override
    protected String getPlaylistName() {
        String name = "Popular Now";
        if (mCurrentOption != null) {
            name += " - " + mCurrentOption.name;
        }
        return name;
    }

    @Override
    protected int getTrackLayoutId() {
        return R.layout.feed_item_big;
    }

    private static class TrendingTrack {
        String artistName;
        String id;
        String snippet;
        String trackName;
        String type;
        Drop drop;

        public Track toTrack() {
            String thumbnailUrl = null;
            String hqThumbnailUrl = null;

            if (type.equals("youtube")) {
                thumbnailUrl = "http://img.youtube.com/vi/" + id +
                                "/mqdefault.jpg";
                hqThumbnailUrl = "http://img.youtube.com/vi/" + id +
                        "/hqdefault.jpg";
            }

            Track track = new Track();
            track.setTitle(artistName + " - " + trackName);
            track.setId(id);
            track.setType(type);
            track.setDesc(snippet);
            track.setDrop(drop);
            track.setUserName(artistName);
            track.setTrackName(trackName);
            track.setThumbnailUrl(thumbnailUrl);
            track.setHqThumbnailUrl(hqThumbnailUrl);
            return track;
        }
    }

    private static class TrendingSubGenreTrack {
        String artistName;
        String label;
        String mixType;
        String released;
        String thumbnail;
        String trackName;
        String youtubeUid;
        Drop drop;

        public Track toTrack() {
            String title = artistName + " - " + trackName;
            if (!TextUtils.isEmpty(mixType)) {
                title += " (" + mixType + ")";
            }

            String desc = null;
            if (!TextUtils.isEmpty(released) && released.length() >= 10) {
                String dateStr = released.substring(0, 10);
                SimpleDateFormat remoteFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date releaseDate = remoteFormat.parse(dateStr);
                    desc = "Released on " + new SimpleDateFormat("MMM d, yyyy",
                            Locale.ENGLISH).format(releaseDate);
                } catch(ParseException e) {

                }
            }

            String thumbnailUrl = thumbnail;
            String hqThumbnailUrl = thumbnail;

            if (TextUtils.isEmpty(thumbnailUrl)) {
                thumbnailUrl = "http://img.youtube.com/vi/" + youtubeUid +
                        "/mqdefault.jpg";
            }
            if (TextUtils.isEmpty(hqThumbnailUrl)) {
                hqThumbnailUrl = "http://img.youtube.com/vi/" + youtubeUid +
                        "/hqdefault.jpg";
            }

            Track track = new Track();
            track.setTitle(title);
            track.setId(youtubeUid);
            track.setType("youtube");
            track.setDesc(desc);
            track.setDrop(drop);
            track.setUserName(artistName);
            track.setTrackName(trackName);
            track.setThumbnailUrl(thumbnailUrl);
            track.setHqThumbnailUrl(hqThumbnailUrl);
            return track;
        }
    }
}
