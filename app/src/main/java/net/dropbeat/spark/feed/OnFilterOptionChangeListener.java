package net.dropbeat.spark.feed;

/**
 * Created by vulpes on 15. 8. 12..
 */
public interface OnFilterOptionChangeListener {
    public void onFilterOptionChanged(BaseFeedFragment.FilterOption option);
}
