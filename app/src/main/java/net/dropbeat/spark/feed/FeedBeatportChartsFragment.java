package net.dropbeat.spark.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.dropbeat.spark.R;
import net.dropbeat.spark.model.Drop;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vulpes on 15. 8. 12..
 */
public class FeedBeatportChartsFragment extends BaseFeedFragment {

    public static FeedBeatportChartsFragment newInstance(ArrayList<FilterOption>
                                                             options) {
        FeedBeatportChartsFragment fragment = new FeedBeatportChartsFragment();
        Bundle args =  new Bundle();
        args.putSerializable(PARAM_OPTIONS, options);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected List<Track> parseResponse(JSONObject response) throws JSONException {
        if (!response.optBoolean("success")) {
            return null;
        }
        List<Track> tracks = new ArrayList<>();

        JSONArray data = response.getJSONArray("data");
        Gson gson = DbGson.getInstance();

        Type listType = new TypeToken<ArrayList<BeatportTrack>>() {}.getType();
        List<BeatportTrack> trendingTracks = gson.fromJson(data.toString(),
                listType);

        for(BeatportTrack t : trendingTracks) {
            tracks.add(t.toTrack());
        }

        return tracks;
    }

    @Override
    protected String getSectionKey() {
        return FeedFragment.KEY_PAGE_BEATPORT_CHART;
    }

    @Override
    protected void requestToApi(FilterOption option, int pageIdx,
                                boolean forceRefresh,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {

        Requests.getChartList(getBaseActivity(), option.key, listener, errorListener);
    }

    @Override
    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {

        super.updateFeedTrackView(holder, track, position);

        holder.getTitle().setText(track.getTrackName());

        if (!TextUtils.isEmpty(track.getUserName())) {
            holder.getArtist().setText(track.getUserName());
            holder.getArtist().setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(track.getGenre())) {
            holder.getGenreView().setText(track.getGenre());
            holder.getGenreView().setVisibility(View.VISIBLE);
        }

        holder.getRankView().setVisibility(View.VISIBLE);
        holder.getRankView().setText(String.valueOf(position + 1));
    }

    @Override
    protected boolean hasPagenation() {
        return false;
    }

    @Override
    protected String getPlaylistName() {
        String name = "Daily Charts";
        if (mCurrentOption != null) {
            name += " - " + mCurrentOption.name;
        }
        return name;
    }

    @Override
    protected int getTrackLayoutId() {
        return R.layout.feed_item;
    }

    private static class BeatportTrack {
        String artistName;
        String released;
        String mixType;
        String thumbnail;
        String trackName;
        String youtubeUid;
        String label;
        String genre;
        Drop drop;

        public Track toTrack() {
            String trackName = this.trackName;
            if (mixType != null) {
                trackName += " (" + mixType + ")";
            }

            String title = artistName + " - " + trackName;

            String desc = null;
            if (!TextUtils.isEmpty(released) && released.length() >= 10) {
                String dateStr = released.substring(0, 10);
                SimpleDateFormat remoteFormat = new SimpleDateFormat(
                        "yyyy-MM-dd");
                try {
                    Date releaseDate = remoteFormat.parse(dateStr);
                    desc = "Released on " + new SimpleDateFormat("MMM d, yyyy",
                            Locale.ENGLISH).format(releaseDate);
                } catch(ParseException e) {

                }
            }

            String thumbnailUrl = thumbnail;
            String hqThumbnailUrl = thumbnail;

            if (TextUtils.isEmpty(thumbnailUrl)) {
                thumbnailUrl = "http://img.youtube.com/vi/" + youtubeUid +
                        "/mqdefault.jpg";
            }
            if (TextUtils.isEmpty(hqThumbnailUrl)) {
                hqThumbnailUrl = "http://img.youtube.com/vi/" + youtubeUid +
                        "/hqdefault.jpg";
            }

            Track track = new Track();
            track.setTitle(title);
            track.setId(youtubeUid);
            track.setType("youtube");
            track.setDesc(desc);
            track.setDrop(drop);
            track.setUserName(artistName);
            track.setTrackName(trackName);
            track.setGenre(genre);
            track.setThumbnailUrl(thumbnailUrl);
            track.setHqThumbnailUrl(hqThumbnailUrl);
            return track;
        }
    }
}
