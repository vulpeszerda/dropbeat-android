package net.dropbeat.spark.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.view.MaterialProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 5. 1..
 */
public class FeedFragment extends BaseFragment implements View.OnClickListener{

    private static class TabMenu {
        String name;
        String key;
        public TabMenu(String key, String name) {
            this.name = name;
            this.key = key;
        }
    }

    public static final String KEY_PAGE_NEW_UPLOAD = "new_upload";
    public static final String KEY_PAGE_TRENDING_TRACKS = "trending";
    public static final String KEY_PAGE_FOLLOWED_ARTISTS = "following";
    public static final String KEY_PAGE_BEATPORT_CHART = "beatport_chart";
    public static final String KEY_PAGE_NEW_RELEASE = "new_release";

    private static final TabMenu[] TAB_MENUS = {
            new TabMenu(KEY_PAGE_TRENDING_TRACKS, "Popular Now"),
            new TabMenu(KEY_PAGE_NEW_UPLOAD, "New Uploads"),
            new TabMenu(KEY_PAGE_FOLLOWED_ARTISTS, "Following Tracks"),
            new TabMenu(KEY_PAGE_NEW_RELEASE, "New Releases"),
            new TabMenu(KEY_PAGE_BEATPORT_CHART, "Daily Charts")
    };

    @InjectView(R.id.genre_progress)
    private MaterialProgressBar mProgressBar;

    @InjectView(R.id.pager)
    private ViewPager mPager;

    @InjectView(R.id.tabview)
    private TabLayout mTabLayout;

    @InjectView(R.id.feed_contents)
    private View mContentsView;

    @InjectView(R.id.failure)
    private View mFailureView;

    @InjectView(R.id.failure_msg)
    private TextView mFailureMsgView;

    @InjectView(R.id.retry_button)
    private Button mRetryBtn;

    private boolean mIsInitialized;
    private FeedPagerAdapter mPagerAdapter;
    private Map<String, ArrayList<BaseFeedFragment.FilterOption>> mGenres = new
            HashMap<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Runnable init = new Runnable() {
            @Override
            public void run() {
                mPagerAdapter = new FeedPagerAdapter(getChildFragmentManager(), mGenres);
                mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
                mPager.setAdapter(mPagerAdapter);
                mTabLayout.setupWithViewPager(mPager);
                mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                mPager.setOffscreenPageLimit(TAB_MENUS.length);
            }
        };
        if (!mIsInitialized) {
            mIsInitialized = true;

            hideFailure();
            hideContents();

            loadGenres(init);
        } else {
            hideFailure();
            showContents();
            init.run();
        }
        mRetryBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.retry_button:
                loadGenres(new Runnable() {
                    @Override
                    public void run() {
                        mPagerAdapter = new FeedPagerAdapter(getChildFragmentManager(), mGenres);
                        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
                        mPager.setAdapter(mPagerAdapter);
                        mTabLayout.setupWithViewPager(mPager);
                        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                        mPager.setOffscreenPageLimit(TAB_MENUS.length);
                    }
                });
                break;
        }
    }

    private void loadGenres(final Runnable callback) {
        mProgressBar.setVisibility(View.VISIBLE);
        hideFailure();

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mProgressBar.setVisibility(View.GONE);
                if (!response.optBoolean("success")) {
                    String errorMsg = "Failed to load feed data.";
                    showFailure(errorMsg);
                    return;
                }

                try {
                    mGenres = parseGenreResponse(response);

                    callback.run();
                    showContents();
                } catch (JSONException e) {
                    String errorMsg = "Failed to load feed data.";
                    showFailure(errorMsg);
                }
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mProgressBar.setVisibility(View.GONE);
                String errorMsg = "Failed to load feed data.";
                if (!TextUtils.isEmpty(msg)) {
                    errorMsg += "\n" + msg;
                }
                showFailure(errorMsg);
                hideContents();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.getFeedGenre(getBaseActivity(), listener, errorListener);
    }

    private void showFailure(String msg) {
        mFailureView.setVisibility(View.VISIBLE);
        mFailureMsgView.setText(msg);
    }

    private void hideFailure() {
        mFailureView.setVisibility(View.GONE);
    }

    private void showContents() {
        mContentsView.setVisibility(View.VISIBLE);
    }

    private void hideContents() {
        mContentsView.setVisibility(View.GONE);
    }

    private Map<String, ArrayList<BaseFeedFragment.FilterOption>> parseGenreResponse(JSONObject response)
            throws JSONException{

        Map<String, ArrayList<BaseFeedFragment.FilterOption>> genreMap = new
                HashMap<>();

        JSONArray defaultGenres = response.optJSONArray("default");
        ArrayList<BaseFeedFragment.FilterOption> genres = new
                ArrayList<>();

        for (int i = 0; i < defaultGenres.length(); i++) {
            JSONObject genreObj = defaultGenres.getJSONObject(i);
            String key = genreObj.optString("key");
            if (TextUtils.isEmpty(key)) {
                key = genreObj.getString("id");
            }
            String name = genreObj.getString("name");
            genres.add(new BaseFeedFragment.FilterOption(key,
                    name));
        }

        ArrayList<BaseFeedFragment.FilterOption> newReleaseGenres =
                new ArrayList<>(genres);
        newReleaseGenres.add(0, new BaseFeedFragment.FilterOption(null, "ALL"));
        genreMap.put(KEY_PAGE_NEW_RELEASE, newReleaseGenres);


        ArrayList<BaseFeedFragment.FilterOption> bpGenres = new ArrayList<>();
        bpGenres.add(0, new BaseFeedFragment.FilterOption("top100", "TOP100"));
        for (BaseFeedFragment.FilterOption genre : genres) {
            bpGenres.add(new BaseFeedFragment.FilterOption(genre
                    .name.toLowerCase(), genre.name));
        }
        genreMap.put(KEY_PAGE_BEATPORT_CHART, bpGenres);


        JSONArray trendingGenres = response.optJSONArray("trending");
        genres = new ArrayList<>();

        genres.add(new BaseFeedFragment.FilterOption(null, "NOW TRENDING"));

        for (int i = 0; i < trendingGenres.length(); i++) {
            JSONObject genreObj = trendingGenres.getJSONObject(i);
            String key = genreObj.optString("key");
            if (key == null) {
                genreObj.getString("id");
            }
            String name = genreObj.getString("name");
            genres.add(new BaseFeedFragment.FilterOption(key,
                    name));
        }

        genreMap.put(KEY_PAGE_TRENDING_TRACKS, genres);

        genres = new ArrayList();
        genres.add(new BaseFeedFragment.FilterOption("shuffle", "SHUFFLE"));
        genres.add(new BaseFeedFragment.FilterOption("recent", "RECENT"));
        genreMap.put(KEY_PAGE_FOLLOWED_ARTISTS, genres);

        genres = new ArrayList();
        genres.add(new BaseFeedFragment.FilterOption("popular", "POPULAR"));
        genres.add(new BaseFeedFragment.FilterOption("recent", "RECENT"));
        genreMap.put(KEY_PAGE_NEW_UPLOAD, genres);

        return genreMap;
    }

    private static class FeedPagerAdapter extends FragmentPagerAdapter {

        private Map<String, ArrayList<BaseFeedFragment.FilterOption>> mOptions;

        public FeedPagerAdapter(FragmentManager fm, Map<String,
                ArrayList<BaseFeedFragment.FilterOption>> options) {
            super(fm);
            mOptions = options;
        }

        @Override
        public Fragment getItem(int i) {
            TabMenu menu = TAB_MENUS[i];
            if (menu.key.equals(KEY_PAGE_TRENDING_TRACKS)) {
                return FeedTrendingTracksFragment.newInstance(mOptions.get(menu.key));
            } else if (menu.key.equals(KEY_PAGE_FOLLOWED_ARTISTS)) {
                return FeedFollowedArtistsFragment.newInstance(mOptions.get(menu.key));
            } else if (menu.key.equals(KEY_PAGE_NEW_RELEASE)) {
                return FeedNewReleaseFragment.newInstance(mOptions.get(menu.key));
            } else if (menu.key.equals(KEY_PAGE_BEATPORT_CHART)) {
                return FeedBeatportChartsFragment.newInstance(mOptions.get(menu.key));
            } else if (menu.key.equals(KEY_PAGE_NEW_UPLOAD)) {
                return FeedNewUploadFragment.newInstance(mOptions.get(menu.key));
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            TabMenu menu = TAB_MENUS[position];
            return menu.name;
        }

        @Override
        public int getCount() {
            return TAB_MENUS.length;
        }
    }


}
