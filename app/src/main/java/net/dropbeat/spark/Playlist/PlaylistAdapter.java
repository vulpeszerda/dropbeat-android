package net.dropbeat.spark.playlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.play.PlayerContext;

/**
 * Created by vulpes on 15. 4. 30..
 */
public class PlaylistAdapter extends ArrayAdapter<Track>{

    private static final int LAYOUT_ID = R.layout.playlist_item;
    private TrackClickListener mListener;
    private String mPlaylistId;

    public PlaylistAdapter(Context context) {
        super(context);
    }

    public void setTrackClickListener(TrackClickListener listener) {
        mListener = listener;
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        final Track track = getItem(position);
        ViewHolder viewHolder;
        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        TextView titleView = viewHolder.getTitle();
        titleView.setText(track.getTitle());

        ImageView iconView = viewHolder.getIcon();
        if (isSelected(position)) {
            iconView.setImageResource(R.drawable.btn_volume_max);
            if (PlayerContext.playState == PlayState.LOADING ||
                    PlayerContext.playState == PlayState.PLAYING) {
                titleView.setSelected(true);
                titleView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            } else {
                titleView.setSelected(false);
                titleView.setEllipsize(TextUtils.TruncateAt.END);
            }
            titleView.setTextColor(getContext().getResources().getColor
                    (android.R.color.white));
        } else {
            titleView.setEllipsize(TextUtils.TruncateAt.END);
            titleView.setSelected(false);
            titleView.setTextColor(getContext().getResources().getColor
                    (R.color.text_light_gray));
            iconView.setImageResource(R.drawable.ic_music_gray);
        }

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, track, position);
                }
            }
        });

        ImageButton menuBtn = viewHolder.getMenuBtn();
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, track, position);
                }
            }
        });
        return row;
    }

    private boolean isSelected(int position) {
        Track track = getItem(position);
        boolean isPlaying;
        isPlaying = PlayerContext.playState != PlayState.STOPPED;
        isPlaying &= PlayerContext.currTrack != null &&
                PlayerContext.currTrack.getId().equals(track.getId());
        isPlaying &= PlayerContext.trackIdx > -1;
        isPlaying &= PlayerContext.playlistId != null &&
                mPlaylistId != null &&
                mPlaylistId.equals(PlayerContext.playlistId);

        return isPlaying;
    }

    public void setPlaylistId(String playlistId) {
        mPlaylistId = playlistId;
    }

    private class ViewHolder {
        private TextView mTitleView;
        private ImageButton mMenuBtn;
        private ImageView mIconView;

        public ViewHolder(View view) {
            mIconView = (ImageView) view.findViewById(R.id.icon);
            mTitleView = (TextView) view.findViewById(R.id.music_title);
            mMenuBtn = (ImageButton) view.findViewById(R.id.menu_button);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageView getIcon() {
            return mIconView;
        }

        public ImageButton getMenuBtn() {
            return mMenuBtn;
        }
    }
}
