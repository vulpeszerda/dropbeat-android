package net.dropbeat.spark.playlist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.BaseDialogFragment;
import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.constants.ShuffleState;
import net.dropbeat.spark.model.ModelParser;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.utils.ViewUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 4. 28..
 */
public class PlaylistFragment extends BaseFragment implements
        View.OnClickListener, TrackClickListener,
        net.dropbeat.spark.playlist.PlaylistSelectAdapter.OnElementClickListener,
        SeekBar.OnSeekBarChangeListener, BaseDialogFragment.DialogResultHandler{


    public static void addTrack(Context context, Track track, String section) {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (context);
        Intent intent = new Intent(ACTION_ADD_TRACK);
        intent.putExtra(PARAM_TRACK, track);
        intent.putExtra(PARAM_SECTION, section);
        manager.sendBroadcast(intent);
    }

    private static interface OnPlaylistRefreshed {
        void onRefreshed(Playlist playlist);
    }

    private static final String ACTION_ADD_TRACK =
            "net.dropbeat.spark.ADD_TRACK";
    private static final String PARAM_TRACK = "track";
    private static final String PARAM_SECTION = "section";

    private static final int REQUEST_CODE__NEW_PLAYLIST = 1;
    private static final int REQUEST_CODE__PLAYLIST_NAME_CHANGE = 2;

    @InjectView(R.id.loading_progress)
    private View mPlaylistLoadProgress;

    @InjectView(R.id.close_button)
    private ImageButton mCloseBtn;

    @InjectView(R.id.share_playlist_button)
    private ImageButton mSharePlaylistBtn;

    @InjectView(android.R.id.list)
    private ListView mPlaylist;

    @InjectView(R.id.playlist_title_container)
    private View mPlaylistTitleContainerView;

    @InjectView(R.id.playlist_title)
    private TextView mPlaylistTitleView;

    @InjectView(R.id.play_button)
    private ImageButton mPlayBtn;

    @InjectView(R.id.pause_button)
    private ImageButton mPauseBtn;

    @InjectView(R.id.backward_button)
    private ImageView mBackwardBtn;

    @InjectView(R.id.forward_button)
    private ImageView mForwardBtn;

    @InjectView(R.id.shuffle_button)
    private ImageView mShuffleBtn;

    @InjectView(R.id.player_status)
    private TextView mPlayerStatus;

    @InjectView(R.id.player_title)
    private TextView mPlayerTitle;

    @InjectView(R.id.progress_container)
    private View mProgressContainer;

    @InjectView(R.id.progress)
    private SeekBar mProgressBar;

    @InjectView(R.id.curr_progress_text)
    private TextView mCurrProgressTextView;

    @InjectView(R.id.total_progress_text)
    private TextView mTotalProgressTextView;

    @InjectView(R.id.repeat_button)
    private ImageButton mRepeatBtn;

    @InjectView(R.id.share_button)
    private ImageButton mShareBtn;

    @InjectView(R.id.volume_button)
    private ImageButton mVolumeBtn;

    @InjectView(R.id.play_loader_wheel)
    private ProgressBar mPlayLoadingView;

    @InjectView(R.id.playlist_select_list)
    private ListView mPlaylistSelectList;

    private net.dropbeat.spark.playlist.PlaylistAdapter mAdapter;
    private net.dropbeat.spark.playlist.PlaylistSelectAdapter mPlaylistSelectAdapter;

    private ImageButton mFooterPauseBtn;
    private ImageButton mFooterPlayBtn;
    private TextView mFooterPlayerTitleView;
    private TextView mFooterPlayerStatusView;
    private SeekBar mFooterPlayerProgress;
    private View mFooterPlayerProgressWrapper;
    private ProgressBar mFooterPlayLoadingView;

    private String mCurrPlaylistId;
    private boolean mIsFragmentInitialized = false;
    private boolean mIsPlaylistSelect;
    private boolean mIsInitialized = false;

    private LinearLayout mSelectHeaderContainerView;
    private View mSelectHeaderView;


    private BroadcastReceiver mAuthChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isAdded()) {
                return;
            }
            if (mIsFragmentInitialized && intent.getAction().equals(DbAccount
                    .ACTION_SIGNIN)) {
                addSelectHeader();
                loadPlaylists(null, true);
            }
        }
    };

    private BroadcastReceiver mAddPlaylistReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isAdded()) {
                return;
            }
            Track track = (Track)intent.getSerializableExtra(PARAM_TRACK);
            String section = intent.getStringExtra(PARAM_SECTION);
            addTrackToPlaylist(track, section);
        }
    };

    private BroadcastReceiver mPlayerStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isAdded()) {
                return;
            }
            String action = intent.getAction();
            if (action.equals(PlayerService.ACTION_NOTIFY_PLAYSTATE_CHANGE)) {
                updateShareBtn();
                updatePlayBtn();
                if (PlayerContext.playlistId != null &&
                        mCurrPlaylistId != null &&
                        mCurrPlaylistId.equals(PlayerContext.playlistId) &&
                                mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
                if (PlayerContext.playState == PlayState.LOADING ||
                        PlayerContext.playState == PlayState.STOPPED) {
                    updateProgress();
                }
            } else if (action.equals(PlayerService
                    .ACTION_NOTIFY_PROGRESS_CHANGE)) {
                updateProgress();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        IntentFilter authChangeFilter = new IntentFilter(DbAccount
                .ACTION_SIGNIN);
        authChangeFilter.addAction(DbAccount.ACTION_SIGNOUT_SYNC);
        manager.registerReceiver(mAuthChangeReceiver, authChangeFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        manager.unregisterReceiver(mAuthChangeReceiver);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public boolean onBackPressed() {
        if (mIsPlaylistSelect) {
            hidePlaylistSelect();
            return true;
        }
        if (isVisible()) {
            FragmentManager manager = getBaseActivity()
                    .getSupportFragmentManager();
            manager.beginTransaction().hide(this).commit();
            return true;
        }
        return super.onBackPressed();
    }

    private void updatePlayCtrls() {
        updatePlayBtn();
        updateProgress();
        updateRepeatView();
        updateShuffleView();
        updateVolume();
        updateShareBtn();
    }

    private void updateShareBtn() {
        mShareBtn.setEnabled(PlayerContext.currTrack != null);
    }

    private void updateShuffleView() {
        int state = PlayerContext.shuffleState;
        switch (state) {
            case ShuffleState.SHUFFLE:
                mShuffleBtn.setImageResource(R.drawable.btn_shuffle_normal);
                break;
            case ShuffleState.NOT_SHUFFLE:
                mShuffleBtn.setImageResource(R.drawable.btn_shuffle_disabled);
                break;
        }
    }

    private void updateRepeatView() {
        switch (PlayerContext.repeatState) {
            case RepeatState.NOT_REPEAT:
                mRepeatBtn.setImageResource(R.drawable.btn_repeat_disabled);
                mForwardBtn.setEnabled(true);
                mBackwardBtn.setEnabled(true);
                break;
            case RepeatState.REPEAT_ONE:
                mRepeatBtn.setImageResource(R.drawable.btn_repeat_one);
                mForwardBtn.setEnabled(false);
                mBackwardBtn.setEnabled(false);
                break;
            case RepeatState.REPEAT_PLAYLIST:
                mRepeatBtn.setImageResource(R.drawable.btn_repeat_normal);
                mForwardBtn.setEnabled(true);
                mBackwardBtn.setEnabled(true);
                break;
        }
        updateForwardBtn();
        updateBackwardBtn();
        PlayerService.doUpdateRepeatState(getBaseActivity());
    }

    private void updateForwardBtn() {
        if (PlayerContext.pickNextTrack() == null ||
                PlayerContext.repeatState == RepeatState.REPEAT_ONE) {
            mForwardBtn.setEnabled(false);
            mForwardBtn.setImageResource(R.drawable.btn_forward_disabled);
        } else {
            mForwardBtn.setEnabled(true);
            mForwardBtn.setImageResource(R.drawable.btn_forward_normal);
        }
    }

    private void updateBackwardBtn() {
        if (PlayerContext.pickPrevTrack() == null ||
                PlayerContext.repeatState == RepeatState.REPEAT_ONE) {
            mBackwardBtn.setEnabled(false);
            mBackwardBtn.setImageResource(R.drawable.btn_backward_disabled);
        } else {
            mBackwardBtn.setEnabled(true);
            mBackwardBtn.setImageResource(R.drawable.btn_backward_normal);
        }
    }

    private void updatePlayBtn() {
        if (PlayerContext.currTrack != null) {
            mPlayerTitle.setText(PlayerContext.currTrack.getTitle());
            mFooterPlayerTitleView.setText(PlayerContext.currTrack.getTitle());
        }
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }

        switch (PlayerContext.playState) {
            case PlayState.LOADING:
                mProgressBar.setEnabled(false);
                mFooterPlayerProgress.setEnabled(false);

                mPlayBtn.setVisibility(View.GONE);
                mPauseBtn.setVisibility(View.GONE);
                mPlayLoadingView.setVisibility(View.VISIBLE);

                mFooterPlayBtn.setVisibility(View.GONE);
                mFooterPauseBtn.setVisibility(View.GONE);
                mFooterPlayLoadingView.setVisibility(View.VISIBLE);

                mPlayerStatus.setText(R.string.player_status__loading);
                mFooterPlayerStatusView.setText(R.string.player_status__loading);

                if (PlayerContext.currTrack != null) {
                    mPlayerTitle.setSelected(true);
                    mFooterPlayerTitleView.setSelected(true);

                    mPlayerTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    mFooterPlayerTitleView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                }

                updateForwardBtn();
                updateBackwardBtn();
                break;

            case PlayState.PLAYING:
                mProgressBar.setEnabled(true);
                mFooterPlayerProgress.setEnabled(true);

                mPlayBtn.setVisibility(View.GONE);
                mPauseBtn.setVisibility(View.VISIBLE);
                mPlayLoadingView.setVisibility(View.GONE);

                mFooterPlayBtn.setVisibility(View.GONE);
                mFooterPauseBtn.setVisibility(View.VISIBLE);
                mFooterPlayLoadingView.setVisibility(View.GONE);

                mPlayerStatus.setText(R.string.player_status__playing);
                mFooterPlayerStatusView.setText(R.string.player_status__playing);

                if (PlayerContext.currTrack != null) {
                    mPlayerTitle.setSelected(true);
                    mFooterPlayerTitleView.setSelected(true);

                    mPlayerTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    mFooterPlayerTitleView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                }
                break;

            case PlayState.STOPPED:
                mProgressBar.setEnabled(false);
                mFooterPlayerProgress.setEnabled(false);

                mPlayBtn.setVisibility(View.VISIBLE);
                mPauseBtn.setVisibility(View.GONE);
                mPlayLoadingView.setVisibility(View.GONE);


                mFooterPlayBtn.setVisibility(View.VISIBLE);
                mFooterPauseBtn.setVisibility(View.GONE);
                mFooterPlayLoadingView.setVisibility(View.GONE);

                mPlayerStatus.setText(R.string.player_status__ready);
                mFooterPlayerStatusView.setText(R.string.player_status__ready);

                mPlayerTitle.setText(R.string.player_default_title);
                mFooterPlayerTitleView.setText(R.string.player_default_title);

                if (PlayerContext.currTrack != null) {
                    mPlayerTitle.setSelected(false);
                    mFooterPlayerTitleView.setSelected(false);

                    mPlayerTitle.setEllipsize(TextUtils.TruncateAt.END);
                    mFooterPlayerTitleView.setEllipsize(TextUtils.TruncateAt.END);
                }

                updateForwardBtn();
                updateBackwardBtn();
                break;

            case PlayState.PAUSED:
                mProgressBar.setEnabled(false);
                mFooterPlayerProgress.setEnabled(false);

                mPlayBtn.setVisibility(View.VISIBLE);
                mPauseBtn.setVisibility(View.GONE);
                mPlayLoadingView.setVisibility(View.GONE);


                mFooterPlayBtn.setVisibility(View.VISIBLE);
                mFooterPauseBtn.setVisibility(View.GONE);
                mFooterPlayLoadingView.setVisibility(View.GONE);

                mPlayerStatus.setText(R.string.player_status__paused);
                mFooterPlayerStatusView.setText(R.string.player_status__paused);

                if (PlayerContext.currTrack != null) {
                    mPlayerTitle.setSelected(false);
                    mFooterPlayerTitleView.setSelected(false);

                    mPlayerTitle.setEllipsize(TextUtils.TruncateAt.END);
                    mFooterPlayerTitleView.setEllipsize(TextUtils.TruncateAt.END);
                }
                break;
        }
    }

    private void updateProgress() {
        float currTime = PlayerContext.currPlayTime;
        float totalTime = PlayerContext.totalPlayTime;

        if (currTime > 0 && totalTime > 0) {
            int sec = (int)(totalTime / 1000);
            String time = sec / 60 >= 10 ? (sec / 60) + ":" :
                    "0" + (sec / 60) + ":";
            time += (sec % 60) >= 10 ? (sec % 60) : "0" + (sec % 60);
            mTotalProgressTextView.setText(time);

            sec = (int)(currTime / 1000);
            time = sec / 60 >= 10 ? (sec / 60) + ":" :
                    "0" + (sec / 60) + ":";
            time += (sec % 60) >= 10 ? (sec % 60) : "0" + (sec % 60);
            mCurrProgressTextView.setText(time);

            int progress = (int)(currTime * 100 / totalTime);
            mProgressBar.setProgress(progress);
            mFooterPlayerProgress.setProgress(progress);
        } else {
            mTotalProgressTextView.setText("00:00");
            mCurrProgressTextView.setText("00:00");
            mProgressBar.setProgress(0);
            mFooterPlayerProgress.setProgress(0);
        }
    }

    private void updateVolume() {
        AudioManager audio = (AudioManager) getBaseActivity()
                .getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int percent = currentVolume * 100 / maxVolume;

        int btnImageId = R.drawable.btn_volume_mute;
        if (percent > 60) {
            btnImageId = R.drawable.btn_volume_max;
        } else if (percent > 30) {
            btnImageId = R.drawable.btn_volume_mid;
        } else if (percent > 0) {
            btnImageId = R.drawable.btn_volume_min;
        }
        mVolumeBtn.setImageResource(btnImageId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCloseBtn.setOnClickListener(this);
        BaseActivity activity = getBaseActivity();
        mFooterPauseBtn = (ImageButton) activity.findViewById(R.id.pause_button);
        mFooterPlayLoadingView = (ProgressBar) activity.findViewById(R.id
                .play_loader_wheel);
        mFooterPlayBtn = (ImageButton) activity.findViewById(R.id.play_button);
        mFooterPlayerTitleView = (TextView) activity.findViewById(R.id.player_title);
        mFooterPlayerStatusView = (TextView) activity.findViewById(R.id.player_status);
        mFooterPlayerProgress = (SeekBar) activity.findViewById(R.id
                .progress);
        mFooterPlayerProgressWrapper = activity.findViewById(R.id
                .progress_wrapper);

        mFooterPlayerProgressWrapper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mFooterPlayerProgress.onTouchEvent(event);
                return true;
            }
        });

        if (!mIsInitialized) {
            mIsInitialized = true;
            mPlaylistSelectAdapter = new net.dropbeat.spark.playlist.PlaylistSelectAdapter
                    (getBaseActivity());
            mPlaylistSelectAdapter.setListener(this);

            // set header container
            mSelectHeaderContainerView = new LinearLayout(getActivity());
            mSelectHeaderContainerView.setOrientation(LinearLayout.VERTICAL);

            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);

            mSelectHeaderContainerView.setLayoutParams(layoutParams);

            mAdapter = new net.dropbeat.spark.playlist.PlaylistAdapter(getBaseActivity());
            mAdapter.setTrackClickListener(this);
            loadPlaylists(null, true);

        } else {
            if (mCurrPlaylistId != null) {
                selectPlaylist(PlayerContext.getPlaylist(mCurrPlaylistId));
            }
        }

        if (mSelectHeaderContainerView != null) {
            mPlaylistSelectList.addHeaderView(mSelectHeaderContainerView);
            if (DbAccount.getUser(getBaseActivity()) != null) {
                addSelectHeader();
            }
        }

        mPlaylist.setAdapter(mAdapter);
        mPlaylistSelectList.setAdapter(mPlaylistSelectAdapter);

        mRepeatBtn.setOnClickListener(this);
        mShuffleBtn.setOnClickListener(this);

        mFooterPlayBtn.setOnClickListener(this);
        mPlayBtn.setOnClickListener(this);

        mFooterPauseBtn.setOnClickListener(this);
        mPauseBtn.setOnClickListener(this);

        mForwardBtn.setOnClickListener(this);
        mBackwardBtn.setOnClickListener(this);

        mProgressBar.setOnSeekBarChangeListener(this);
        mProgressBar.setEnabled(false);
        mFooterPlayerProgress.setOnSeekBarChangeListener(this);
        mFooterPlayerProgress.setEnabled(false);

        mPlaylistTitleContainerView.setOnClickListener(this);
        mPlaylistTitleContainerView.setPadding(0, 0,
                ViewUtils.dpToPx(getBaseActivity(), 32), 0);

        mShareBtn.setEnabled(PlayerContext.currTrack != null);
        mShareBtn.setOnClickListener(this);

        if (DbAccount.getUser(getBaseActivity()) != null) {
            mSharePlaylistBtn.setVisibility(View.VISIBLE);
        } else {
            mSharePlaylistBtn.setVisibility(View.GONE);
        }
        mSharePlaylistBtn.setOnClickListener(this);

        // set view as current playcontext

        mProgressBar.setMax(100);
        mFooterPlayerProgress.setMax(100);


        updatePlayCtrls();

        mIsFragmentInitialized = true;
    }

    private synchronized void addSelectHeader() {
        if (mSelectHeaderContainerView == null || mSelectHeaderView != null) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(getBaseActivity());
        mSelectHeaderView = inflater.inflate(
                R.layout.playlist_select_header, null, false);
        mSelectHeaderView.setOnClickListener(this);
        mSelectHeaderContainerView.addView(mSelectHeaderView);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_playlist, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(getBaseActivity());


        IntentFilter playerFiler = new IntentFilter(PlayerService
                .ACTION_NOTIFY_PLAYSTATE_CHANGE);
        playerFiler.addAction(PlayerService.ACTION_NOTIFY_PROGRESS_CHANGE);
        manager.registerReceiver(mPlayerStateReceiver, playerFiler);
        manager.registerReceiver(mAddPlaylistReceiver,
                new IntentFilter(ACTION_ADD_TRACK));

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        updatePlayCtrls();
        if (DbAccount.getUser(getBaseActivity()) != null) {
            mSharePlaylistBtn.setVisibility(View.VISIBLE);
        } else {
            mSharePlaylistBtn.setVisibility(View.GONE);
        }
    }


    @Override
    public void onVisibilityChanged(boolean isVisible) {
        super.onVisibilityChanged(isVisible);
        BaseActivity parent = getBaseActivity();
        if (parent != null && isVisible && mIsInitialized &&
                DbAccount.getUser(parent) != null) {
            loadPlaylists(mCurrPlaylistId, false);
        }
    }



    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(getBaseActivity());
        manager.unregisterReceiver(mPlayerStateReceiver);
        manager.unregisterReceiver(mAddPlaylistReceiver);
    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(getBaseActivity()).edit();
        switch (v.getId()) {
            case R.id.close_button:
                if (mIsPlaylistSelect) {
                    hidePlaylistSelect();
                } else {
                    FragmentManager manager =
                            getBaseActivity().getSupportFragmentManager();
                    manager.beginTransaction().hide(this).commit();
                }
                break;
            case R.id.shuffle_button:
                PlayerContext.changeshuffleState();
                editor.putInt("shuffle_state", PlayerContext.shuffleState);
                editor.commit();
                updateShuffleView();
                break;
            case R.id.repeat_button:
                PlayerContext.changeRepeatState();
                editor.putInt("repeat_state", PlayerContext.repeatState);
                editor.commit();
                updateRepeatView();
                break;
            case R.id.play_button:
                doPlay(null, PlayerContext.playlistId);
                break;
            case R.id.pause_button:
                doPause();
                break;
            case R.id.forward_button:
                doForward();
                break;
            case R.id.backward_button:
                doBackward();
                break;
            case R.id.playlist_title_container:
                if (mIsPlaylistSelect) {
                    hidePlaylistSelect();
                } else {
                    showPlaylistSelect();
                }
                break;
            case R.id.volume_button:
                break;
            case R.id.share_button:
                if (PlayerContext.currTrack != null) {
                    PlayerContext.currTrack.share(getBaseActivity());
                }
                break;
            case R.id.create_new_playlist_button:
                showNewPlaylistDialog();
                break;
            case R.id.share_playlist_button:
                if (DbAccount.getUser(getBaseActivity()) == null ||
                        mCurrPlaylistId == null) {
                    return;
                }
                Playlist found = null;
                for (Playlist playlist : PlayerContext.getCopiedPlaylists()) {
                    if (playlist.getId().equals(mCurrPlaylistId)) {
                        found = playlist;
                        break;
                    }
                }
                if (found != null) {
                    found.share(getBaseActivity());
                }
                break;
        }
    }

    @Override
    public void onClick(View view, final Track track, int position) {
        switch(view.getId()) {
            case R.id.track:
                doPlay(track, mCurrPlaylistId);
                break;
            case R.id.menu_button:
                PopupMenu menu = new PopupMenu(getBaseActivity(), view);

                if (DbAccount.getUser(getBaseActivity()) != null) {
                    menu.getMenuInflater().inflate(R.menu.menu_playlist_track, menu.getMenu());
                } else {
                    menu.getMenuInflater().inflate(R.menu
                            .menu_playlist_track_nonauth, menu.getMenu());
                }
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onTrackPopupMenuClicked(track, item);
                    }
                });
                menu.show();
                break;
        }
    }

   private boolean onTrackPopupMenuClicked(Track track, MenuItem item) {
       switch(item.getItemId()) {
           case R.id.action_delete:
               removeTrackFromPlaylist(track);
               return true;
           case R.id.action_share:
               track.share(getBaseActivity());
               return true;
       }
       return false;
   }

    @Override
    public void onClick(View view, final Playlist playlist, int position) {
        switch (view.getId()) {
            case R.id.playlist_item:
                selectPlaylist(playlist, true);
                break;
            case R.id.menu_button:
                PopupMenu menu = new PopupMenu(getBaseActivity(), view);

                if (DbAccount.getUser(getBaseActivity()) != null) {
                    menu.getMenuInflater().inflate(
                            R.menu.menu_playlist_select_playlist,
                            menu.getMenu());
                } else {
                    menu.getMenuInflater().inflate(R.menu
                            .menu_playlist_select_playlist, menu.getMenu());
                }
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onPlaylistPopupMenuClicked(playlist, item);
                    }
                });
                menu.show();
                break;
        }
    }

    private boolean onPlaylistPopupMenuClicked(Playlist playlist, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_rename:
                showPlaylistNameChangeDialog(playlist);
                return true;
            case R.id.action_delete:
                showPlaylistDeleteConfirmDialog(playlist);
                return true;
            case R.id.action_share:
                playlist.share(getBaseActivity());
                return true;
        }
        return false;
    }

    private void loadPlaylists(final String playlistId, boolean hideListView) {
        loadPlaylistsWithCallback(new Runnable() {
            @Override
            public void run() {
                List<Playlist> playlists = PlayerContext.getCopiedPlaylists();
                if (playlists.size() > 0) {
                    Playlist found = null;
                    if (playlistId != null) {
                        for (Playlist playlist : playlists) {
                            if (playlist.getId().equals(playlistId)) {
                                found = playlist;
                                break;
                            }
                        }
                    }

                    if (found == null) {
                        selectPlaylist(playlists.get(0), true);
                    } else {
                        selectPlaylist(found, true);
                    }
                } else {
                    selectPlaylist(null, true);
                }
            }
        }, hideListView);
    }

    private void loadPlaylistsWithCallback(final Runnable onAfterLoadPlaylists,
                                           boolean hideListView) {
        showPlaylistLoadProgress(hideListView);

        RobustResponseListener<JSONObject> authListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                if (!response.optBoolean("success")) {
                    return;
                }

                List<Playlist> playlists = ModelParser.parsePlaylists(response);
                if (playlists == null) {
                    return;
                }
                playlists = Lists.reverse(playlists);
                PlayerContext.resetPlaylists(playlists);
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hideAll(getBaseActivity());
                hidePlaylistLoadProgress();
                if (!response.optBoolean("success")) {
                    Toast.makeText(getBaseActivity().getApplicationContext(),
                            "Failed to fetch playlists (" + response.optString
                                    ("error")+ ")", Toast.LENGTH_LONG).show();
                    return;
                }

                List<Playlist> playlists = ModelParser.parsePlaylists(response);
                if (playlists == null) {
                    playlists = new ArrayList<>();
                }
                playlists = Lists.reverse(playlists);
                PlayerContext.resetPlaylists(playlists);

                mPlaylistSelectAdapter.clear();
                mPlaylistSelectAdapter.addAll(playlists);
                mPlaylistSelectAdapter.notifyDataSetChanged();

                if (playlists.size() == 0) {
                    Toast.makeText(getBaseActivity().getApplicationContext(),
                            "Failed to fetch playlists (0 playlist)",
                            Toast.LENGTH_LONG).show();
                }
                if (onAfterLoadPlaylists != null) {
                    onAfterLoadPlaylists.run();
                }
            }
        };

        RobustResponseListener<JSONObject> nonauthListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (!response.optBoolean("success")) {
                            return;
                        }

                        Playlist playlist = ModelParser.parsePlaylist(response);
                        if (playlist == null) {
                            return;
                        }
                        List<Playlist> playlists = new ArrayList<>();
                        playlists.add(playlist);
                        PlayerContext.resetPlaylists(playlists);
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        ProgressDialogFragment.hideAll(getBaseActivity());
                        hidePlaylistLoadProgress();
                        if (!response.optBoolean("success")) {
                            Toast.makeText(getBaseActivity().getApplicationContext(),
                                    "Failed to fetch playlists (" + response.optString
                                            ("error")+ ")", Toast.LENGTH_LONG).show();
                            return;
                        }

                        Playlist playlist = ModelParser.parsePlaylist(response);
                        List<Playlist> playlists = new ArrayList<>();
                        if (playlist == null) {
                            Toast.makeText(getBaseActivity().getApplicationContext(),
                                    "Failed to fetch playlists (0 playlist)",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            playlists.add(playlist);
                        }
                        PlayerContext.resetPlaylists(playlists);

                        mPlaylistSelectAdapter.clear();
                        mPlaylistSelectAdapter.add(playlist);
                        mPlaylistSelectAdapter.notifyDataSetChanged();

                        if (onAfterLoadPlaylists != null) {
                            onAfterLoadPlaylists.run();
                        }
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hideAll(getBaseActivity());
                hidePlaylistLoadProgress();
                Toast.makeText(getBaseActivity().getApplicationContext(),
                        "Failed to fetch playlists (" + msg + ")",
                        Toast.LENGTH_LONG).show();
                return;
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        DbAccount account = DbAccount.getUser(getBaseActivity());
        if (account != null) {
            Requests.fetchPlaylistList(getBaseActivity(), authListener, errorListener);
        } else {
            Requests.fetchInitialPlaylist(getBaseActivity(), nonauthListener,
                    errorListener);
        }
    }

    private void showPlaylistLoadProgress(boolean hidePlaylist) {
        if (getBaseActivity() == null || isDetached()) {
            return;
        }
        mPlaylistLoadProgress.setVisibility(View.VISIBLE);
        if (hidePlaylist) {
            mPlaylist.setVisibility(View.GONE);
        }
        mPlaylistSelectList.setVisibility(View.GONE);
    }

    private void hidePlaylistLoadProgress() {
        if (getBaseActivity() == null || isDetached()) {
            return;
        }
        mPlaylistLoadProgress.setVisibility(View.GONE);
        if (mIsPlaylistSelect) {
            mPlaylistSelectList.setVisibility(View.VISIBLE);
            mPlaylist.setVisibility(View.GONE);
        } else {
            mPlaylistSelectList.setVisibility(View.GONE);
            mPlaylist.setVisibility(View.VISIBLE);
        }
    }

    private void selectPlaylist(Playlist playlist) {
        selectPlaylist(playlist, false);
    }

    private void selectPlaylist(Playlist playlist, final boolean byUser) {
        hidePlaylistSelect();
        if (playlist == null) {
            if (byUser) {
                Toast.makeText(getBaseActivity().getApplicationContext(),
                        "Failed to find proper playlist",
                        Toast.LENGTH_LONG).show();
            }
            return;
        }
        mPlaylistSelectAdapter.setSelctedId(playlist.getId());
        mPlaylistSelectAdapter.notifyDataSetChanged();
        mCurrPlaylistId = playlist.getId();
        List<Track> tracks = playlist.getTracks();
        mAdapter.clear();
        mAdapter.setPlaylistId(playlist.getId());
        mAdapter.addAll(tracks);
        mAdapter.notifyDataSetChanged();
        mPlaylistTitleView.setText(playlist.getName());

        if (byUser && DbAccount.getUser(getBaseActivity()) != null) {
            refreshPlaylist(playlist, new OnPlaylistRefreshed() {

                @Override
                public void onRefreshed(Playlist playlist) {
                    if (playlist == null) {
                        loadPlaylists(null, true);
                        return;
                    }
                    if (mCurrPlaylistId != null && !mCurrPlaylistId.equals(playlist.getId())) {
                        return;
                    }
                    List<Track> tracks = playlist.getTracks();
                    if (tracks.size() == 0 && byUser) {
                        Toast.makeText(getBaseActivity().getApplicationContext(),
                                getString(R.string.desc_empty_playlist),
                                Toast.LENGTH_SHORT).show();
                    }
                    mAdapter.clear();
                    mAdapter.setPlaylistId(playlist.getId());
                    mAdapter.addAll(tracks);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void refreshPlaylist(Playlist playlist, final OnPlaylistRefreshed
            callback) {
        mPlaylistLoadProgress.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mPlaylistLoadProgress.setVisibility(View.GONE);
                if (!response.optBoolean("success")) {
                    String errorMsg = "Failed to sync playlist.";
                    Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                    callback.onRefreshed(null);
                    return;
                }
                try {
                    JSONObject playlistObj = response.getJSONObject("playlist");
                    String id = playlistObj.getString("id");
                    String name = playlistObj.getString("name");

                    Playlist temp = Playlist.fromJsonObject(id, name,
                            playlistObj.getJSONArray("data"));
                    Playlist p = PlayerContext.getPlaylist(id);
                    p.addAll(temp.getTracks(), true);
                    callback.onRefreshed(p);
                } catch (JSONException e) {
                    String errorMsg = "Failed to parse playlist info.";
                    Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                    callback.onRefreshed(null);
                }
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mPlaylistLoadProgress.setVisibility(View.GONE);
                String errorMsg = "Failed to sync playlist.";
                if (!TextUtils.isEmpty(msg)) {
                    errorMsg += " " + msg;
                }
                Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.getPlaylist(getBaseActivity(), playlist.getId(),
                responseListener, errorListener);
    }

    private void doPlay(final Track track, final String playlistId) {
        PlayerService.doPlay(getBaseActivity(), track, playlistId);
    }

    private void doPause() {
        PlayerService.doPause(getBaseActivity());
    }

    private void doSeek(int position) {
        PlayerService.doSeek(getBaseActivity(), position);
    }

    private void doBackward() {
        PlayerService.doBackward(getBaseActivity());
    }

    private void doForward() {
        PlayerService.doForward(getBaseActivity());
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!fromUser) {
            return;
        }

        float totalPlayTime = PlayerContext.totalPlayTime;
        if (totalPlayTime <= 0) {
            return;
        }
        float seekTime = totalPlayTime * progress / 100;

        int sec = (int) (seekTime / 1000);
        String time = sec / 60 >= 10 ? (sec / 60) + ":" :
                "0" + (sec / 60) + ":";
        time += (sec % 60) >= 10 ? (sec % 60) : "0" + (sec % 60);
        mCurrProgressTextView.setText(time);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        float totalPlayTime = PlayerContext.totalPlayTime;
        if (totalPlayTime <= 0) {
            return;
        }
        float seekTime = totalPlayTime * seekBar.getProgress() / 100;
        doSeek((int) seekTime);
    }

    @SuppressLint("NewApi")
    private void showPlaylistSelect() {
        if (mIsPlaylistSelect) {
            return;
        }
        mIsPlaylistSelect = true;
        mPlaylist.setVisibility(View.GONE);
        mPlaylistSelectList.setVisibility(View.VISIBLE);
        mPlaylistTitleContainerView.setBackgroundResource(R.drawable
                .playlist_title_bg_up_9);
        mPlaylistTitleContainerView.setPadding(0 ,0 ,
                ViewUtils.dpToPx(getBaseActivity(), 32), 0);
    }

    @SuppressLint("NewApi")
    private void hidePlaylistSelect() {
        if (!mIsPlaylistSelect) {
            return;
        }
        mIsPlaylistSelect = false;
        mPlaylist.setVisibility(View.VISIBLE);
        mPlaylistSelectList.setVisibility(View.GONE);
        mPlaylistTitleContainerView.setBackgroundResource(R.drawable.playlist_title_bg_down_9);
        mPlaylistTitleContainerView.setPadding(0, 0,
                ViewUtils.dpToPx(getBaseActivity(), 32), 0);
    }

    private void addTrackToPlaylist(final Track track, String section) {
        if (mCurrPlaylistId == null) {
            return;
        }

        final Playlist playlist = PlayerContext.getPlaylist(mCurrPlaylistId);
        if (playlist == null) {
            Toast.makeText(getBaseActivity().getApplicationContext(),
                    getString(R.string.error_playlist_not_initialized),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (DbAccount.getUser(getBaseActivity()) == null) {
            Intent intent = AuthActivity.createIntent(getBaseActivity(), true);
            startActivity(intent);
            return;
        }

        // Logging
        // to us
        Requests.logTrackAdd(getBaseActivity(), track.getTitle());
        // to GA
        DbApplication app = (DbApplication) getBaseActivity().getApplication();
        Tracker tracker = app.getAnalyticsTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("playlist-add-from-" + section)
                .setAction("add-" + track.getType())
                .setLabel(track.getTitle()).build());

        Playlist dummy = new Playlist();
        for (Track t : playlist.getTracks()) {
            dummy.addTrack(t);
        }
        dummy.addTrack(track);
        dummy.setId(playlist.getId());
        dummy.setName(playlist.getName());

        RobustResponseListener listener = new RobustResponseListener
                (getBaseActivity(), this) {
            @Override
            public void onSkipped(Object response) {
                playlist.addTrack(track);
                PlayerService.doUpdateRepeatState(getBaseActivity());
            }

            @Override
            public void onSecureResponse(Object response) {
                playlist.addTrack(track);
                Toast.makeText(getBaseActivity().getApplicationContext(),
                        getString(R.string.desc_added_playlist),
                        Toast.LENGTH_SHORT).show();
                mAdapter.clear();
                mAdapter.addAll(playlist.getTracks());
                mAdapter.notifyDataSetChanged();
                PlayerService.doUpdateRepeatState(getBaseActivity());
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {

            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                if (error instanceof NoConnectionError) {
                    Toast.makeText(getBaseActivity().getApplicationContext(), msg,
                            Toast.LENGTH_LONG).show();
                } else {
                    String errorMsg =
                            getString(R.string.desc_already_added_playlist);
                    Toast.makeText(getBaseActivity().getApplicationContext(), errorMsg,
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.setPlaylist(getBaseActivity(), dummy.getId(),
                dummy.toJsonArray(), listener, errorListener);
    }

    private void removeTrackFromPlaylist(final Track track) {
        if (mCurrPlaylistId == null) {
            return;
        }

        final Playlist playlist = PlayerContext.getPlaylist(mCurrPlaylistId);
        if (playlist == null) {
            Toast.makeText(getBaseActivity().getApplicationContext(),
                    getString(R.string.error_playlist_not_initialized),
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (DbAccount.getUser(getBaseActivity()) == null) {
            return;
        }

        List<Track> tracks = playlist.getTracks();
        int idx = -1;
        for (int i = 0; i < tracks.size(); i++) {
            if (track.getId().equals(tracks.get(i).getId())) {
                idx = i;
                break;
            }
        }
        if (idx == -1) {
            return;
        }

        Playlist dummy = new Playlist();
        for (Track t : playlist.getTracks()) {
            dummy.addTrack(t);
        }

        dummy.delTrack(idx);
        dummy.setId(playlist.getId());
        dummy.setName(playlist.getName());


        RobustResponseListener listener = new RobustResponseListener
                (getBaseActivity(), this) {
            @Override
            public void onSkipped(Object response) {
                synchronized (playlist) {
                    List<Track> tracks = playlist.getTracks();
                    int idx = -1;
                    for (int i = 0; i < tracks.size(); i++) {
                        if (track.getId().equals(tracks.get(i).getId())) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx != -1) {
                        playlist.delTrack(idx);
                    }
                    if (PlayerContext.playlistId != null &&
                            PlayerContext.playlistId.equals(playlist.getId())) {
                        Track curr = PlayerContext.currTrack;

                        List<Track> playlistTracks = playlist.getTracks();
                        PlayerContext.trackIdx = -1;

                        for (int i = 0; i < playlistTracks.size(); i++) {
                            Track t = playlistTracks.get(i);
                            if (t.getId().equals(curr.getId())) {
                                PlayerContext.trackIdx = i;
                            }
                        }
                    }
                }
                PlayerService.doUpdateRepeatState(getBaseActivity());
            }

            @Override
            public void onSecureResponse(Object response) {
                synchronized (playlist) {
                    List<Track> tracks = playlist.getTracks();
                    int idx = -1;
                    for (int i = 0; i < tracks.size(); i++) {
                        if (track.getId().equals(tracks.get(i).getId())) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx != -1) {
                        playlist.delTrack(idx);
                    }
                    if (PlayerContext.playlistId != null &&
                            PlayerContext.playlistId.equals(playlist.getId())) {
                        Track curr = PlayerContext.currTrack;

                        List<Track> playlistTracks = playlist.getTracks();
                        PlayerContext.trackIdx = -1;

                        for (int i = 0; i < playlistTracks.size(); i++) {
                            Track t = playlistTracks.get(i);
                            if (t.getId().equals(curr.getId())) {
                                PlayerContext.trackIdx = i;
                            }
                        }
                    }
                }
                PlayerService.doUpdateRepeatState(getBaseActivity());

                mAdapter.clear();
                mAdapter.addAll(playlist.getTracks());
                mAdapter.notifyDataSetChanged();

            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {

                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        String errorMsg = getString(R.string
                                .error_failed_to_save_playlist);
                        if (!TextUtils.isEmpty(msg)) {
                            errorMsg += "\n\n" + msg;
                        }
                        Toast.makeText(getBaseActivity().getApplicationContext(), errorMsg,
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.setPlaylist(getBaseActivity(), dummy.getId(),
                dummy.toJsonArray(), listener, errorListener);
    }

    private void showNewPlaylistDialog() {
        net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment fragment =
                net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment.getInstance();
        fragment.setTargetFragment(this, REQUEST_CODE__NEW_PLAYLIST);
        fragment.show(getFragmentManager(), "new_playlist");
    }

    private void showPlaylistNameChangeDialog(Playlist playlist) {
        net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment fragment =
                net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment.getInstance(playlist);
        fragment.setTargetFragment(this, REQUEST_CODE__PLAYLIST_NAME_CHANGE);
        fragment.show(getFragmentManager(), "playlist_name_change");
    }

    private void showPlaylistDeleteConfirmDialog(final Playlist playlist) {
        if (PlayerContext.getCopiedPlaylists().size() <= 1) {
            Toast.makeText(getBaseActivity().getApplicationContext(),
                    getString(R.string.desc_one_playlist_should_exist),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        String format = getString(R.string.desc_confirm_delete_playlist__format);
        String message = String.format(format, playlist.getName(),
                playlist.getTracks().size());

        final RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                loadPlaylistsWithCallback(null, true);
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                if (!response.optBoolean("success")) {
                    ProgressDialogFragment.hideAll(getBaseActivity());
                    String errorMsg = getString(
                            R.string.error_failed_to_delete_playlist);
                    if (!TextUtils.isEmpty(response.optString("error"))) {
                        errorMsg += "\n" + response.optString("error");
                    }
                    Toast.makeText(getBaseActivity().getApplicationContext(), errorMsg,
                            Toast.LENGTH_LONG).show();
                    return;
                }
                loadPlaylistsWithCallback(new Runnable() {
                    @Override
                    public void run() {
                        Playlist curr = PlayerContext.getPlaylist(mCurrPlaylistId);

                        // when current playing playlist deleted
                        if (PlayerContext.playlistId != null && PlayerContext.playlistId.equals(mCurrPlaylistId)) {
                            PlayerService.doStop(getBaseActivity());
                        }

                        // when current visible playlist deleted
                        if (curr == null) {
                            mAdapter.clear();
                            List<Playlist> playlists = PlayerContext.getCopiedPlaylists();
                            if (playlists.size() > 0) {
                                Playlist top = playlists.get(0);
                                mCurrPlaylistId = top.getId();
                                mPlaylistTitleView.setText(top.getName());
                                mPlaylistSelectAdapter.setSelctedId(mCurrPlaylistId);
                                mAdapter.addAll(top.getTracks());
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }, true);
            }
        };

        final RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                String errorMsg = getString(
                        R.string.error_failed_to_delete_playlist);
                if (!TextUtils.isEmpty(msg)) {
                    errorMsg += "\n" + msg;
                }
                Toast.makeText(getBaseActivity().getApplicationContext(), errorMsg,
                        Toast.LENGTH_LONG).show();
                ProgressDialogFragment.hideAll(getBaseActivity());
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        DialogInterface.OnClickListener listener =
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ProgressDialogFragment.show(getBaseActivity(),
                        "delete_progress", "Deleting..");
                Requests.deletePlaylist(getBaseActivity(), playlist.getId(),
                        responseListener, errorListener);
            }
        };

        AlertDialog dialog = new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.title_are_you_sure)
                .setMessage(message)
                .setPositiveButton(R.string.button_proceed, listener)
                .setNegativeButton(R.string.button_cancel, null)
                .create();
        dialog.show();
    }

    @Override
    public void onDialogResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__NEW_PLAYLIST && resultCode ==
                BaseDialogFragment.RESULT_OK) {
            if (isAdded()) {
                loadPlaylists(null, true);
            }
        }
        if (requestCode == REQUEST_CODE__PLAYLIST_NAME_CHANGE && resultCode ==
                BaseDialogFragment.RESULT_OK) {
            if (!isAdded()) {
                return;
            }
            String playlistId= data.getStringExtra
                    (net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment.PARAM_PLAYLIST_ID);
            String playlistName = data.getStringExtra
                    (net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment.PARAM_PLAYLIST_NAME);
            if (playlistId == null || playlistName == null) {
                return;
            }
            Playlist playlist = PlayerContext.getPlaylist(playlistId);
            if (playlist == null) {
                return;
            }
            playlist.setName(playlistName);
            mPlaylistSelectAdapter.notifyDataSetChanged();
            if (mCurrPlaylistId.equals(playlistId)) {
                mPlaylistTitleView.setText(playlistName);
            }
        }
    }
}

