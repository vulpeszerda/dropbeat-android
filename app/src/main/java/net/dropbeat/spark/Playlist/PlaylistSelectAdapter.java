package net.dropbeat.spark.playlist;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.model.Playlist;

/**
 * Created by vulpes on 15. 5. 1..
 */
public class PlaylistSelectAdapter extends ArrayAdapter<Playlist>{

    private static final int TYPE_SELECTED = 0;
    private static final int TYPE_UNSELECTED = 1;

    private static final int SELECTED_LAYOUT_ID = R.layout.playlist_select_item_selected;
    private static final int UNSELECTED_LAYOUT_ID = R.layout.playlist_select_item;

    public static interface OnElementClickListener {
        void onClick(View view, Playlist playlist, int position);
    }

    private String mSelectedId;
    private OnElementClickListener mListener;
    private boolean mIsAuth;

    public PlaylistSelectAdapter(Context context) {
        super(context);
        mIsAuth = DbAccount.getUser(getContext()) != null;
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        final Playlist playlist = getItem(position);
        ViewHolder viewHolder;
        if (row == null) {
            if (mSelectedId != null && mSelectedId.equals(playlist.getId())) {
                row = mInflater.inflate(SELECTED_LAYOUT_ID, null);
            } else {
                row = mInflater.inflate(UNSELECTED_LAYOUT_ID, null);
            }
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        viewHolder.getTitle().setText(playlist.getName());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, playlist, position);
                }
            }
        };

        row.setOnClickListener(listener);

        ImageButton menuView = viewHolder.getMenuBtn();

        if (mIsAuth) {
            menuView.setOnClickListener(listener);
            menuView.setVisibility(View.VISIBLE);
        } else {
            menuView.setOnClickListener(null);
            menuView.setVisibility(View.GONE);
        }

        return row;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return mSelectedId != null &&
                mSelectedId.equals(getItem(position).getId()) ?
                    TYPE_SELECTED : TYPE_UNSELECTED;
    }

    public void setSelctedId(String id) {
        mSelectedId = id;
    }

    public void setListener(OnElementClickListener listener) {
        mListener = listener;
    }

    @Override
    public void notifyDataSetChanged() {
        mIsAuth = DbAccount.getUser(getContext()) != null;
        super.notifyDataSetChanged();
    }

    private class ViewHolder {
        private TextView mTitleView;
        private ImageButton mMenuBtn;

        public ViewHolder(View view) {
            mTitleView = (TextView) view.findViewById(R.id.playlist_title);
            mMenuBtn = (ImageButton) view.findViewById(R.id.menu_button);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageButton getMenuBtn() {
            return mMenuBtn;
        }
    }
}
