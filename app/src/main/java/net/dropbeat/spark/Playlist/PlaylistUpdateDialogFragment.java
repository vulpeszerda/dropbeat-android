package net.dropbeat.spark.playlist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import net.dropbeat.spark.BaseDialogFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.network.Requests;

import org.json.JSONObject;

/**
 * Created by vulpes on 15. 5. 1..
 */
public class PlaylistUpdateDialogFragment extends BaseDialogFragment implements View.OnClickListener{

    private String mErrorMsg;
    private String mPlaylistName;
    private String mPlaylistId;
    private int mDescResId;
    private int mTitleResId;

    private TextView mDescView;
    private EditText mNameView;
    private boolean mIsChange;

    public static final String PARAM_PLAYLIST_NAME = "playlist_name";
    public static final String PARAM_PLAYLIST_ID = "playlist_id";
    private static final String PARAM_DESC_RES_ID = "desc_res_id";
    private static final String PARAM_TITLE_RES_ID = "title_res_id";
    private static final String PARAM_IS_CHANGE = "is_change";

    public static PlaylistUpdateDialogFragment getInstance() {
        return getInstance(null);
    }

    public static PlaylistUpdateDialogFragment getInstance(Playlist playlist) {
        PlaylistUpdateDialogFragment dialog = new PlaylistUpdateDialogFragment();
        Bundle args = new Bundle();
        if (playlist != null) {
            args.putString(PARAM_PLAYLIST_NAME, playlist.getName());
            args.putString(PARAM_PLAYLIST_ID, playlist.getId());
            args.putInt(PARAM_DESC_RES_ID, R.string.desc_change_playlist_name);
            args.putInt(PARAM_TITLE_RES_ID, R.string.title_playlist_name_change);
            args.putBoolean(PARAM_IS_CHANGE, true);
        } else {
            args.putInt(PARAM_DESC_RES_ID, R.string.desc_new_playlist);
            args.putInt(PARAM_TITLE_RES_ID, R.string.title_new_playlist);
            args.putBoolean(PARAM_IS_CHANGE, false);
        }
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle args = getArguments();
        if (args != null) {
            mPlaylistId = args.getString(PARAM_PLAYLIST_ID, null);
            mPlaylistName = args.getString(PARAM_PLAYLIST_NAME, null);
            mDescResId = args.getInt(PARAM_DESC_RES_ID);
            mTitleResId = args.getInt(PARAM_TITLE_RES_ID);
            mIsChange = args.getBoolean(PARAM_IS_CHANGE);
        }
        setResultCode(RESULT_CANCEL);
    }

    @SuppressLint("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_playlist_name, null);

        mDescView = (TextView) view.findViewById(R.id.description);
        mNameView = (EditText) view.findViewById(R.id.playlist_name_input);

        if (mIsChange && mPlaylistName != null) {
            mNameView.setText(mPlaylistName);
        }

        mDescView.setText(mDescResId);

        int positiveBtnResId = mIsChange ? R.string.button_change :
                R.string.button_create;

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(mTitleResId).setView(view)
                .setNegativeButton(R.string.button_cancel, null)
                .setPositiveButton(positiveBtnResId, null).create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button btn = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                btn.setOnClickListener(PlaylistUpdateDialogFragment.this);
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onClick(View view) {
        if (TextUtils.isEmpty(mNameView.getText())) {
            mNameView.setError(getString(R.string.error_value_required));
            return;
        }
        mPlaylistName = mNameView.getText().toString();
        Response.Listener<JSONObject> listener =
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    FragmentManager manager = getFragmentManager();
                    manager.popBackStack(null, 0);
                    dismiss(false);

                    Fragment target = getTargetFragment();
                    if (target != null && target instanceof DialogResultHandler) {
                        DialogResultHandler handler = (DialogResultHandler) target;
                        Intent data = new Intent();
                        data.putExtra(PARAM_PLAYLIST_ID, mPlaylistId);
                        data.putExtra(PARAM_PLAYLIST_NAME, mPlaylistName);
                        handler.onDialogResult(getTargetRequestCode(), RESULT_OK, data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    FragmentManager manager = getFragmentManager();
                    manager.popBackStack(null, 0);

                    String errorString = null;
                    if (error instanceof NoConnectionError) {
                        errorString =
                                getString(R.string.desc_failed_to_connect_internet);
                        Toast.makeText(getBaseActivity(), errorString,
                                Toast.LENGTH_LONG).show();
                        mErrorMsg = null;
                        return;
                    }
                    NetworkResponse response = error.networkResponse;
                    if (TextUtils.isEmpty(errorString) && response != null
                            && response.data != null && response.data.length > 0) {
                        errorString = new String(response.data);
                    }
                    if (TextUtils.isEmpty(errorString)) {
                        errorString = error.getMessage();
                    }
                    if (TextUtils.isEmpty(errorString)) {
                        errorString = getString(R.string.desc_undefined_error);
                    }

                    mErrorMsg = errorString;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        if (mIsChange) {
            Requests.changePlaylistName(getBaseActivity(), mPlaylistId,
                    mPlaylistName, listener, errorListener);
        } else {
            Requests.createPlaylist(getBaseActivity(), mPlaylistName,
                    listener, errorListener);
        }
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.addToBackStack(null);
        transaction.remove(PlaylistUpdateDialogFragment.this);

        ProgressDialogFragment progressDialog = new ProgressDialogFragment();
        progressDialog.show(transaction, "progress");
    }

    @Override
    public void onResume() {
        super.onResume();
        mNameView.setError(mErrorMsg);
    }

    public static class ProgressDialogFragment extends BaseDialogFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setCancelable(false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Saving..");
            dialog.setCanceledOnTouchOutside(false);
            return dialog;
        }
    }
}
