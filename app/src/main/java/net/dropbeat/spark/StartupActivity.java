package net.dropbeat.spark;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.IntentCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import net.dropbeat.spark.addon.ErrorDialogFragment;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.auth.EmailSubmitActivity;
import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.constants.ShuffleState;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.utils.DeviceUuidFactory;
import net.dropbeat.spark.utils.VersionComparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import main.java.com.mindscapehq.android.raygun4android.messages.RaygunUserInfo;
import roboguice.inject.ContentView;

@ContentView(R.layout.activity_startup)
public class StartupActivity extends BaseActivity {

    public static final String PARAM_SHARED_TRACK_UID = "shared_track_uid";
    public static final String PARAM_SHARED_PLAYLIST_UID = "shared_playlist_uid";
    public static final String PARAM_SHARED_TRACK_URL = "shared_track_url";

    public static Intent createIntent(Context context) {
        return createIntent(context, null);
    }

    public static Intent createIntent(Context context, Intent nextIntent) {
        Intent intentToBeStart = new Intent(context, StartupActivity.class);
        intentToBeStart.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        intentToBeStart.putExtra(PARAM_NEXT_INTENT, nextIntent);
        return intentToBeStart;
    }

    private static final String TAG = "StartupActivity";
    private static final int GOOGLE_PLAY_SERVICE_REQUEST = 9000;
    private static final String PARAM_NEXT_INTENT = "next_intent";
    private static final int REQUEST_CODE_EMAIL_SUBMIT = 400;


    private Intent mNextIntent;
    private StartupThread mThread;
    private Handler mResumeHandler;
    private Runnable mResumeRunnable;
    private AtomicBoolean mRunStartupThreadOnResume = new AtomicBoolean(true);
    private boolean mIsGeolocationReady = false;
    private boolean mGotFollowingRes = false;
    private boolean mGotLikeRes = false;
    private boolean mFollowingFetchSuccess = false;
    private boolean mLikeFetchSuccess = false;
    private Object mLock = new Object();

    private BroadcastReceiver mAccountSyncReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            boolean success = intent.getBooleanExtra(
                    DbAccount.PARAM_SYNC_SUCCESS, false);

            if (DbAccount.ACTION_FOLLOWING_SYNC.equals(action)) {
                mGotFollowingRes = true;
                mFollowingFetchSuccess = success;
                synchronized (mLock) {
                    mLock.notify();
                }
            } else if (DbAccount.ACTION_LIKE_SYNC.equals(action)) {
                mGotLikeRes = true;
                mLikeFetchSuccess = success;
                synchronized (mLock) {
                    mLock.notify();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        mNextIntent = intent.getParcelableExtra(PARAM_NEXT_INTENT);
        if (mNextIntent == null) {
            mNextIntent = NavigationDrawerActivity.createIntent(StartupActivity.this);
        }

        final String action = intent.getAction();

        if (action != null && action.equals(Intent.ACTION_VIEW)) {
            String dataString = intent.getDataString();
            Uri dataUri = Uri.parse(dataString);

            String path = dataUri.getPath();
            if (TextUtils.isEmpty(path) || path.equals("/")) {
                String sharedTrackUid = dataUri.getQueryParameter("track");
                String sharedPlaylistUid = dataUri.getQueryParameter("playlist");
                if (!TextUtils.isEmpty(sharedTrackUid)) {
                    mNextIntent.putExtra(PARAM_SHARED_TRACK_UID, sharedTrackUid);
                } else if (!TextUtils.isEmpty(sharedPlaylistUid)) {
                    mNextIntent.putExtra(PARAM_SHARED_PLAYLIST_UID, sharedPlaylistUid);
                }
            } else if (path.matches("^/r/[^/]+/[^/]+/?$")) {
                String sharedTrackUrl = "http://dropbeat.net" + path;
                mNextIntent.putExtra(PARAM_SHARED_TRACK_URL, sharedTrackUrl);
            }
        }

        if (!BuildConfig.DEBUG) {
            RaygunClient.Init(getApplicationContext());
            RaygunClient.AttachExceptionHandler();

            DeviceUuidFactory factory = new DeviceUuidFactory(StartupActivity
                    .this);
            RaygunUserInfo user = new RaygunUserInfo();
            DbAccount account = DbAccount.getUser(StartupActivity.this);
            if (account != null) {
                user.Identifier = account.getEmail();
                user.FullName = account.getFullName();
                user.Email = account.getEmail();
                user.Uuid = factory.getDeviceUuid().toString();
                user.IsAnonymous = false;

                // GA logging
                DbApplication app = (DbApplication) getApplication();
                Tracker tracker = app.getAnalyticsTracker();
                tracker.set("&uid", account.getUid());
            } else {
                user.Uuid = factory.getDeviceUuid().toString();
                user.IsAnonymous = true;
            }
            RaygunClient.SetUser(user);
        } else {
            Toast.makeText(this, "Developer mode enabled",
                    Toast.LENGTH_SHORT) .show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.unregisterReceiver(mAccountSyncReceiver);

        if (mResumeHandler != null && mResumeRunnable != null) {
            mResumeHandler.removeCallbacks(mResumeRunnable);
        }
        if (mThread != null && mThread.isAlive()) {
            mThread.setPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(DbAccount.ACTION_FOLLOWING_SYNC);
        filter.addAction(DbAccount.ACTION_LIKE_SYNC);
        manager.registerReceiver(mAccountSyncReceiver, filter);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (hasGooglePlayService()) {

            if (!isFinishing() && mRunStartupThreadOnResume.get()) {
                if (mResumeRunnable == null) {
                    mResumeRunnable = new Runnable() {

                        @Override
                        public void run() {
                            if (mThread == null || !mThread.isAlive()) {
                                mThread = new StartupThread();
                                mThread.start();
                            } else {
                                mThread.setResume();
                            }
                        }
                    };
                }
                if (mResumeHandler == null) {
                    mResumeHandler = new Handler();
                }
                mResumeHandler.postDelayed(mResumeRunnable, 1000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mThread != null && mThread.isAlive()) {
            mThread.interrupt();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EMAIL_SUBMIT) {
            if (resultCode == RESULT_OK && data != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mThread = new StartupThread();
                        mThread.start();
                    }
                }, 1000);
            } else {
                finish();
            }
        }
    }

    private class StartupThread extends Thread {
        private final AtomicBoolean mIsStarted = new AtomicBoolean(false);
        private Object mPauseLock;
        private boolean mPaused;

        public StartupThread() {
            mPauseLock = new Object();
            mPaused = false;
        }

        @Override
        public synchronized void start() {
            if (!mIsStarted.getAndSet(true)) {
                super.start();
            }
        }

        @Override
        public void run() {
            try {
                if ((!BuildConfig.DEBUG && !checkAppVersion()) ||
                        !getClientKeys()) {
                    return;
                }

//                String pushKey = null;
//                String exceptionMsg = null;
//                try {
//                    pushKey = getOrRegisterGcmKey(account);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                    VolleyError error = (VolleyError) e.getCause();
//                    if (error instanceof NoConnectionError) {
//                        exceptionMsg = getString(R.string.desc_failed_to_connect_internet);
//                    } else {
//                        NetworkResponse response = error.networkResponse;
//                        if (response != null && response.data != null
//                                && response.data.length > 0) {
//                            exceptionMsg = new String(response.data);
//                        }
//                        if (exceptionMsg == null) {
//                            exceptionMsg = error.getMessage();
//                        }
//                    }
//                }
//                if (pushKey == null) {
//                    String message = getString(R.string.failed_to_register_desc);
//                    if (exceptionMsg != null) {
//                        message += " (" + exceptionMsg + ")";
//                    }
//                    showRetryDialog(getString(R.string.failed_to_register),
//                            message);
//                    return;
//                }


                // do long time task
                getGeolocation();

                DbAccount account;
                if ((account = DbAccount.getUser(StartupActivity.this)) != null) {
                    String email = account.getEmail().toLowerCase();
                    if (email.contains("@dropbeat.net")) {
                        Bundle args = new Bundle();
                        Intent intent = EmailSubmitActivity.createIntent(
                                StartupActivity.this, args);
                        startActivityForResult(intent, REQUEST_CODE_EMAIL_SUBMIT);
                        mRunStartupThreadOnResume.set(false);
                        return;
                    }

                    account.syncFollowings(StartupActivity.this);
                    account.syncLike(StartupActivity.this);

                    synchronized (mLock) {
                        while (!mIsGeolocationReady ||
                                !mGotFollowingRes ||
                                !mGotLikeRes) {
                            mLock.wait();
                        }
                    }

                    if (!mLikeFetchSuccess) {
                        showRetryDialog(
                                getString(R.string.failed_to_fetch_like),
                                getString(R.string.failed_to_fetch_like_desc));
                        return;
                    }
                    if (!mFollowingFetchSuccess) {
                        showRetryDialog(
                                getString(R.string.failed_to_fetch_following),
                                getString(R.string.failed_to_fetch_following_desc));
                        return;
                    }
                } else {
                    synchronized (mLock) {
                        while(mIsGeolocationReady) {
                            mLock.wait();
                        }
                    }
                }


                SharedPreferences prefs = PreferenceManager
                        .getDefaultSharedPreferences(StartupActivity.this);
                PlayerContext.repeatState = prefs.getInt("repeat_state",
                        RepeatState.NOT_REPEAT);
                PlayerContext.shuffleState = prefs.getInt("shuffle_state",
                        ShuffleState.NOT_SHUFFLE);

                synchronized (mPauseLock) {
                    while (mPaused) {
                        mPauseLock.wait();
                    }
                }
                startNextActivity();
                finish();
            } catch (InterruptedException e) {
            }
        }

        public void setPause() {
            synchronized (mPauseLock) {
                mPaused = true;
            }
        }

        public void setResume() {
            synchronized (mPauseLock) {
                mPaused = false;
                mPauseLock.notifyAll();
            }
        }
    }

    private void startNextActivity() {
        startActivity(mNextIntent);
    }

    private String getOrRegisterGcmKey()
            throws IOException, InterruptedException, ExecutionException {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);

        String gcmPushIdKey = getString(R.string.preference_key__gcm);
        String appVersionKey = getString(R.string.preference_key__app_version);

        String gcmPushId = pref.getString(gcmPushIdKey, null);

        int currentAppVersion = getAppVersion(this);
        int appVersion = pref.getInt(appVersionKey, -1);
        boolean needServerPush = false;

        if (TextUtils.isEmpty(gcmPushId) || appVersion < 0
                || appVersion != currentAppVersion) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            gcmPushId = gcm.register(getString(R.string.gcm_sender_id));
            needServerPush = true;
        }

        if (needServerPush) {
//            RequestFuture<JSONObject> future = RequestFuture.newFuture();
//            ApiResources.registerPushKey(this, gcmPushId, future, future);
//            JSONObject result = future.get();

            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(appVersionKey, currentAppVersion)
                    .putString(gcmPushIdKey, gcmPushId).commit();

        }
        return gcmPushId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void showUpdateDialog() {
        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String appPackageName = getPackageName();
                Intent intent;
                try {
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appPackageName));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    intent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id="
                                    + appPackageName));
                    startActivity(intent);
                }
            }
        };
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(
                        StartupActivity.this)
                        .setTitle(R.string.get_new_version)
                        .setMessage(R.string.get_new_version_desc)
                        .setPositiveButton(R.string.button_get_new_version,
                                listener).create();

                ErrorDialogFragment fragment = new ErrorDialogFragment();
                fragment.setDialog(dialog);
                fragment.showAllowingStateLoss(getSupportFragmentManager(), "app_update");

            }

        });
    }

    private boolean getClientKeys() {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        Requests.getClientKey(this, future, future);
        try {
            JSONObject meta = future.get();
            DropbeatContext.soundcloudClientKey = meta.getString("soundcloud_key");
            return true;
        } catch (Exception e) {
            String text = getString(R.string.failed_to_fetch_client_key);
            String title = getString(R.string.failed_to_fetch_client_key_desc);
            showRetryDialog(title, text);
            return false;
        }
    }

    private void getGeolocation() {
        final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        String geolocation = prefs.getString("geolocation", null);
        if (geolocation != null) {
            mIsGeolocationReady = true;
            synchronized (mLock) {
                mLock.notify();
            }
            return;
        }

        final Response.ErrorListener errorListener =
                new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mIsGeolocationReady = true;
                synchronized (mLock) {
                    mLock.notify();
                }
            }
        };

        final Response.Listener<JSONObject> countryListener =
                new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {
                    if (!res.getString("status").toLowerCase().equals("ok")) {
                        return;
                    }

                    JSONArray results = res.getJSONArray("results");
                    JSONObject townObj = results.getJSONObject(0);
                    JSONObject countryObj =
                            results.getJSONObject(results.length() - 1);
                    JSONObject cityObj =
                            results.getJSONObject(results.length() - 2);

                    JSONArray addressComponents = countryObj
                            .getJSONArray("address_components");
                    String countryName = addressComponents.getJSONObject(0)
                            .getString("long_name");
                    String countryCode = addressComponents.getJSONObject(0)
                            .getString("short_name");

                    addressComponents = cityObj.getJSONArray("address_components");
                    String cityName = addressComponents.getJSONObject(0)
                            .getString("long_name");

                    JSONObject location = townObj.getJSONObject("geometry")
                            .getJSONObject("location");
                    double latitude = location.getDouble("lat");
                    double longitude = location.getDouble("lng");

                    JSONObject geoObj = new JSONObject();
                    geoObj.put("lat", Double.toString(latitude))
                            .put("lng", Double.toString(longitude))
                            .put("country_name", countryName)
                            .put("country_code", countryCode)
                            .put("city_name", cityName);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("geolocation", geoObj.toString()).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mIsGeolocationReady = true;
                synchronized (mLock) {
                    mLock.notify();
                }
            }
        };

        final Response.Listener<JSONObject> listener =
                new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {
                    double latitude = res.getDouble("latitude");
                    double longitude = res.getDouble("longitude");
                    Requests.getCountryInfo(
                            StartupActivity.this,
                            Double.toString(latitude),
                            Double.toString(longitude),
                            countryListener,
                            errorListener);

                } catch (Exception e) {
                    mIsGeolocationReady = true;
                    synchronized (mLock) {
                        mLock.notify();
                    }
                }
            }
        };

        Requests.getGeolocation(this, listener, errorListener);
    }

    private boolean checkAppVersion() {
        String message = null;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            String currentVersion = packageInfo.versionName;
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            Requests.getClientVersion(this, future, future);
            JSONObject response = future.get();
            String recentVersion = response.getString("android_version");
            if (!recentVersion.matches("\\d+\\.\\d+\\.\\d+")) {
                throw new Exception("version is not proper format");
            }
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            String remoteAppVersionKey = getString(R.string
                    .preference_key__remote_app_version);
            editor.putString(remoteAppVersionKey, recentVersion);
            editor.commit();

            int cmp = VersionComparator.compare(currentVersion, recentVersion);
            if (cmp < 0) {
                // older version
                showUpdateDialog();
                return false;
            }
            return true;
        } catch (ExecutionException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            VolleyError error = (VolleyError) e.getCause();
            if (error instanceof NoConnectionError) {
                message = getString(R.string.desc_failed_to_connect_internet);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            message = getString(R.string.desc_failed_to_parse_json);
        } catch (NameNotFoundException e) {
            // skip
        } catch (InterruptedException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            message = e.getMessage();
        }

        String text = getString(R.string.failed_to_version_compare_desc);
        if (message != null) {
            text += " (" + message + ")";
        }
        showRetryDialog(getString(R.string.failed_to_version_compare), text);
        return false;
    }

    public boolean hasGooglePlayService() {
        final int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                ErrorDialogFragment dialogFragment = null;
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, StartupActivity.this, GOOGLE_PLAY_SERVICE_REQUEST);
                    if (errorDialog != null) {
                        dialogFragment = new ErrorDialogFragment();
                        dialogFragment.setDialog(errorDialog);
                    }
                } else {
                    AlertDialog dialog = new AlertDialog.Builder(
                            StartupActivity.this)
                            .setTitle(R.string.critical_failure_on_start)
                            .setMessage(
                                    R.string.google_play_service_not_availiable)
                            .setPositiveButton(R.string.button_confirm,
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            finish();
                                        }
                                    }).create();
                    dialogFragment = new ErrorDialogFragment();
                    dialogFragment.setDialog(dialog);

                }
                if (!isFinishing() && dialogFragment != null) {
                    dialogFragment.showAllowingStateLoss(
                            getSupportFragmentManager(), "google playservice");
                }
            }
        });
        return false;
    }


    private void showRetryDialog(final String title, final String message) {

        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(
                        StartupActivity.this)
                        .setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(R.string.button_retry,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        if (mThread != null
                                                && mThread.isAlive()) {
                                            mThread.interrupt();
                                        }
                                        mThread = new StartupThread();
                                        mThread.start();
                                    }
                                })
                        .setNegativeButton(R.string.button_exit,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        finish();
                                    }
                                }).create();

                ErrorDialogFragment fragment = new ErrorDialogFragment();
                fragment.setDialog(dialog);
                if (!isFinishing()) {
                    fragment.showAllowingStateLoss(getSupportFragmentManager(), "retry_dialog");
                }
            }
        });
    }

}
