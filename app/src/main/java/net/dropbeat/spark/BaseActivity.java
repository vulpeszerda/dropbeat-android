package net.dropbeat.spark;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;

import com.facebook.appevents.AppEventsLogger;

import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;

import roboguice.activity.RoboActionBarActivity;

public class BaseActivity extends RoboActionBarActivity {

    private boolean mIsAlive;
    private Toolbar mToolbar;

    public boolean isAlive() {
        return mIsAlive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setElevation(0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsAlive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsAlive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        DbApplication app = (DbApplication)getApplication();
        app.stopActivityTransitionTimer();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DbApplication app = (DbApplication)getApplication();
        app.startActivityTransitionTimer();
        AppEventsLogger.deactivateApp(this);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}
