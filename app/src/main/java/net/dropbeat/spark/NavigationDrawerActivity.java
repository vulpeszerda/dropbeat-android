package net.dropbeat.spark;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.channel.ChannelFragment;
import net.dropbeat.spark.feed.FeedFragment;
import net.dropbeat.spark.search.SearchFragment;

import roboguice.inject.ContentView;

@ContentView(R.layout.activity_navi_drawer)
public class NavigationDrawerActivity extends PlayerActivity
        implements NavigationDrawerFragment.NavigationDrawerCallback {

    public static Intent createIntent(Context context) {
        return createIntent(context, -1);
    }

    public static Intent createIntent(Context context, int menu) {
        Intent intent = new Intent(context, NavigationDrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(PARAM_MENU, menu);
        return intent;
    }

    public static Intent createIntentWithAuthCheck(Context context, int position) {
        return StartupActivity.createIntent(context,
                createIntent(context, position));
    }

    private static final String PARAM_MENU = "menu";
    private static final String PARAM_TITLE = "title";
    private static final String TAG_CONTENT_FRAGMENT = "content_fragment";


    private FragmentManager mFragmentManager;
    private NavigationDrawerFragment mDrawerFragment;
    private Fragment mContentFragment;
    private CharSequence mTitle;
    private int mSelectedMenu;
    private Bundle mSelectedArgs;
    private boolean mNeedRefresh;

    private BroadcastReceiver mAuthReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isFinishing()) {
                return;
            }
            // force refresh current content fragment
            mNeedRefresh = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        mFragmentManager = getSupportFragmentManager();
        mDrawerFragment = (NavigationDrawerFragment)
                mFragmentManager.findFragmentById(R.id.navigation_drawer);


        if (savedInstanceState == null) {
            mTitle = ""; // getTitle();
            mSelectedMenu = getIntent().getIntExtra(PARAM_MENU, -1);
        } else {
            mTitle = savedInstanceState.getCharSequence(PARAM_TITLE);
            mSelectedMenu = savedInstanceState.getInt(PARAM_MENU, -1);

        }

        // Set up the drawer.
        mDrawerFragment.setUp(R.id.navigation_drawer, R.id.drawer_layout,
                mSelectedMenu);
        mDrawerFragment.initDrawer();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(DbAccount.ACTION_SIGNIN);
        manager.registerReceiver(mAuthReceiver, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mSelectedMenu = intent.getIntExtra(PARAM_MENU, -1);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PARAM_MENU, mSelectedMenu);
        outState.putCharSequence(PARAM_TITLE, mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        boolean handled = false;
        if (mPlaylistFragment != null) {
            handled = mPlaylistFragment.onBackPressed();
        }
        if (!handled && mContentFragment != null && mContentFragment instanceof
                BaseFragment) {
            handled = ((BaseFragment) mContentFragment).onBackPressed();
        }
        if (!handled) {
            super.onBackPressed();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int menuType, Bundle args) {
        onNavItemSelected(menuType, args, false);
    }

    private void onNavItemSelected(int menuType, Bundle args,
                                   boolean forceRefresh) {
        if (!forceRefresh && mSelectedMenu == menuType ) {
            if (mPlaylistFragment != null && mPlaylistFragment.isVisible()) {
                mFragmentManager.beginTransaction().hide(mPlaylistFragment).commit();
            }
            return;
        }
        mSelectedMenu = menuType;
        mSelectedArgs = args;

        Fragment f = null;
        boolean useFadeAnim = false;
        int titleResId = -1;

        switch (menuType) {
            case NavigationDrawerFragment.MenuType.FEED:
                f = new FeedFragment();
                titleResId = R.string.title_feed;
                break;
            case NavigationDrawerFragment.MenuType.CHANNEL:
                titleResId = R.string.title_channel;
                f = new ChannelFragment();
                break;
            case NavigationDrawerFragment.MenuType.SEARCH:
                f = new SearchFragment();
                titleResId = R.string.title_search;
                break;
            case NavigationDrawerFragment.MenuType.SETTINGS:
                titleResId = R.string.title_settings;
                f = new SettingsFragment();
                break;
        }

        if (f != null) {
            mContentFragment = f;
            mContentFragment.setArguments(args);
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            if (useFadeAnim) {
                ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            }
            ft.replace(R.id.container, f, TAG_CONTENT_FRAGMENT).commit();
            if (titleResId > -1) {
                mTitle = getString(titleResId);
            }
        }
        else {
            // handle for errors (or throw exception)
        }

        // Hide playlsit popup
        if (mPlaylistFragment != null && mPlaylistFragment.isVisible()) {
            mFragmentManager.beginTransaction().hide(mPlaylistFragment).commit();
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.unregisterReceiver(mAuthReceiver);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mNeedRefresh) {
            mNeedRefresh = false;
            onNavItemSelected(mSelectedMenu, mSelectedArgs, true);
        }
    }
}
