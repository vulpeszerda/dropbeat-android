package net.dropbeat.spark.constants;

/**
 * Created by vulpes on 15. 4. 30..
 */
public class PlayState {
    public static final int STOPPED = 0;
    public static final int LOADING = 1;
    public static final int PLAYING = 2;
    public static final int PAUSED = 3;
}
