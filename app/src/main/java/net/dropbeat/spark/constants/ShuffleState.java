package net.dropbeat.spark.constants;

/**
 * Created by parkilsu on 15. 4. 29..
 */
public class ShuffleState {
    public static final int NOT_SHUFFLE = 0;
    public static final int SHUFFLE = 1;
}
