package net.dropbeat.spark.constants;

/**
 * Created by parkilsu on 15. 4. 29..
 */
public class RepeatState {
    public static final int NOT_REPEAT = 0;
    public static final int REPEAT_PLAYLIST = 1;
    public static final int REPEAT_ONE = 2;
}
