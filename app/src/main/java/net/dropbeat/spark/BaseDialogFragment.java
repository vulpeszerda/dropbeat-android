package net.dropbeat.spark;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import roboguice.fragment.RoboDialogFragment;

public class BaseDialogFragment extends RoboDialogFragment {

    public static final int RESULT_CANCEL = 0;
    public static final int RESULT_OK = 1;

    public static interface DialogResultHandler {
        void onDialogResult(int requestCode, int resultCode, Intent data);
    }

    private int mResultCode;
    private Intent mResult;
    private boolean mNotifyResult = true;

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public void dismiss(boolean notifyOnDismiss) {
        mNotifyResult = notifyOnDismiss;
        dismiss();
    }

    public void dismissAllowingStateLoss(boolean notifyOnDismiss) {
        mNotifyResult = notifyOnDismiss;
        dismissAllowingStateLoss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mNotifyResult) {
            Fragment target = getTargetFragment();
            if (target != null && target instanceof DialogResultHandler) {
                DialogResultHandler handler = (DialogResultHandler) target;
                handler.onDialogResult(getTargetRequestCode(), mResultCode,
                        mResult);
            }
        }
    }

    public void showAllowingStateLoss(FragmentManager manager, String tag) {
        FragmentTransaction tr = manager.beginTransaction();
        tr.add(this, tag);
        tr.commitAllowingStateLoss();
    }

    public void setResultCode(int resultCode) {
        mResultCode = resultCode;
    }

    public void setResult(Intent result) {
        mResult = result;
    }

    /**
     * Bug fix in dismissing when screen rotate, although retainInstance set
     * true
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }
}
