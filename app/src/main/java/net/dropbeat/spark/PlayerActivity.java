package net.dropbeat.spark;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.UserTrack;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.playlist.PlaylistFragment;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.playlist.PlaylistUpdateDialogFragment;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 6. 12..
 */
public class PlayerActivity extends BaseActivity
        implements View.OnClickListener, View.OnKeyListener{
    @InjectView(R.id.playlist_button)
    protected ImageButton mPlaylistButton;

    @InjectView(R.id.player_info)
    protected View mPlayInfoView;

    protected PlaylistFragment mPlaylistFragment;
    private FragmentManager mFragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getSupportFragmentManager();
        mPlaylistButton.setOnClickListener(this);
        mPlaylistFragment = (PlaylistFragment)mFragmentManager
                .findFragmentById(R.id.playlist);
        if (mPlaylistFragment != null) {
            mFragmentManager.beginTransaction().hide(mPlaylistFragment).commit();
        }

        final Intent intent = getIntent();
        String sharedTrackUid = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_TRACK_UID);
        String sharedPlaylistUid = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_PLAYLIST_UID);
        String sharedTrackUrl = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_TRACK_URL);
        if (!TextUtils.isEmpty(sharedTrackUid)) {
            loadSharedTrack(sharedTrackUid);
        } else if (!TextUtils.isEmpty(sharedPlaylistUid)) {
            loadSharedPlaylist(sharedPlaylistUid);
        } else if (!TextUtils.isEmpty(sharedTrackUrl)) {
            loadSharedTrackWithURL(sharedTrackUrl);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String sharedTrackUid = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_TRACK_UID);
        String sharedTrackUrl = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_TRACK_URL);
        String sharedPlaylistUid = intent
                .getStringExtra(StartupActivity.PARAM_SHARED_PLAYLIST_UID);
        if (!TextUtils.isEmpty(sharedTrackUid)) {
            loadSharedTrack(sharedTrackUid);
        } else if (!TextUtils.isEmpty(sharedPlaylistUid)) {
            loadSharedPlaylist(sharedPlaylistUid);
        } else if (!TextUtils.isEmpty(sharedTrackUrl)) {
            loadSharedTrackWithURL(sharedTrackUrl);
        }
    }

    @Override
    public void onBackPressed() {
        boolean handled = false;
        if (mPlaylistFragment != null) {
            handled = mPlaylistFragment.onBackPressed();
        }
        if (!handled) {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.playlist_button) {
            mFragmentManager.beginTransaction().show(mPlaylistFragment)
                    .commit();
            mPlaylistFragment.onVisibilityChanged(true);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                PlayerService.doForward(this);
                return true;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                PlayerService.doBackward(this);
                return true;
            case KeyEvent.KEYCODE_HEADSETHOOK:
                if (PlayerContext.playState == PlayState.PLAYING) {
                    PlayerService.doPause(this);
                } else {
                    PlayerService.doPlay(this, null, PlayerContext.playlistId);
                }
                return true;
        }
        return false;
    }

    private void loadSharedTrack(final String uid) {
        ProgressDialogFragment.show(this, "shared_track",
                R.string.loading_shared_track);

        final Runnable retryRunnable = new Runnable() {
            @Override
            public void run() {
                loadSharedTrack(uid);
            }
        };

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(this) {
                    @Override
                    public void onSkipped(JSONObject response) {

                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        ProgressDialogFragment.hide(PlayerActivity.this, "shared_track");

                        if (!response.optBoolean("success")) {
                            showSharedContentsLoadFailreDialog(
                                    "Failed to load shared track.", retryRunnable);
                            return;
                        }
                        try {
                            Track track = null;

                            JSONObject data = response.optJSONObject("data");
                            if (data == null) {
                                data = response.getJSONObject("obj");
                            }

                            track = new Track();
                            track.setId(data.getString("ref"));
                            track.setTitle(data.getString("track_name"));
                            track.setType(data.getString("type"));

                            String thumbnailUrl = null;
                            String hqThumbnailUrl = null;

                            if (track.getType().equals("youtube")) {
                                thumbnailUrl = "http://img.youtube.com/vi/" +
                                        track.getId() +
                                        "/mqdefault.jpg";
                            }
                            if (track.getType().equals("youtube")) {
                                hqThumbnailUrl = "http://img.youtube.com/vi/" +
                                        track.getId() +
                                        "/hqdefault.jpg";
                            }
                            track.setThumbnailUrl(thumbnailUrl);
                            track.setHqThumbnailUrl(hqThumbnailUrl);

                            PlayerService.doPlay(PlayerActivity.this, track, null);
                        } catch(JSONException e) {
                            showSharedContentsLoadFailreDialog(
                                    "Failed to load shared track.", retryRunnable);
                            return;
                        }
                    }
                };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        ProgressDialogFragment.hide(PlayerActivity.this, "shared_track");
                        showSharedContentsLoadFailreDialog(
                                "Failed to load shared track.", retryRunnable);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.getSharedTrack(this, uid, listener, errorListener);
    }

    private void loadSharedTrackWithURL(final String url) {
        ProgressDialogFragment.show(this, "shared_track",
                R.string.loading_shared_track);

        final Runnable retryRunnable = new Runnable() {
            @Override
            public void run() {
                loadSharedTrackWithURL(url);
            }
        };

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hide(PlayerActivity.this, "shared_track");

                if (!response.optBoolean("success")) {
                    showSharedContentsLoadFailreDialog(
                            "Failed to load shared track.", retryRunnable);
                    return;
                }
                JSONObject data = response.optJSONObject("data");
                Track track = UserTrack.fromJsonObject(data);
                if (track == null) {
                    showSharedContentsLoadFailreDialog(
                            "Failed to load shared track.", retryRunnable);
                    return;
                }
                PlayerService.doPlay(PlayerActivity.this, track, null);
            }
        };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(PlayerActivity.this, "shared_track");
                showSharedContentsLoadFailreDialog(
                        "Failed to load shared track.", retryRunnable);
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.resolveResource(this, url, listener, errorListener);
    }

    private void loadSharedPlaylist(final String uid) {

        ProgressDialogFragment.show(this, "shared_playlist",
                R.string.loading_shared_track);

        final Runnable retryRunnable = new Runnable() {
            @Override
            public void run() {
                loadSharedPlaylist(uid);
            }
        };

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hide(
                        PlayerActivity.this, "shared_playlist");

                Playlist playlist = null;
                if (response.optBoolean("success")) {
                    try {
                        JSONObject obj = response.getJSONObject("playlist");
                        playlist = Playlist.fromJsonObject(
                                obj.getString("id"),
                                obj.getString("name"),
                                obj.getJSONArray("data"));
                        playlist.setId("shared_" + playlist.getId());
                    } catch(Exception e) {
                        playlist = null;
                    }
                }
                if (playlist == null) {
                    showSharedContentsLoadFailreDialog("Failed to load shared track.", retryRunnable);
                    return;
                }

                Intent intent = PlaylistActivity.createIntent(
                        PlayerActivity.this, playlist);
                startActivity(intent);
            }
        };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(PlayerActivity.this, "shared_playlist");
                showSharedContentsLoadFailreDialog("Failed to load shared track.", retryRunnable);
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.getSharedPlaylist(this, uid, listener, errorListener);
    }

    private void showSharedContentsLoadFailreDialog(String msg,
                                             final Runnable retryRunnable) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_failed_to_load))
                .setMessage(msg)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retryRunnable.run();
                    }
                })
                .setNegativeButton(R.string.button_cancel, null).show();
    }
}
