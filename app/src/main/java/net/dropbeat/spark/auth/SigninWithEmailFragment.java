package net.dropbeat.spark.auth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.addon.form.Form;
import net.dropbeat.spark.addon.form.SigninFormBuilder;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 31..
 */
public class SigninWithEmailFragment extends BaseFragment
        implements View.OnClickListener{

    @InjectView(R.id.input_email)
    private EditText mEmailInputView;

    @InjectView(R.id.input_password)
    private EditText mPasswordInputView;

    @InjectView(R.id.button_signin)
    private Button mSigninBtn;

    private Form mForm;
    private boolean isSubmitting = false;
    private CharSequence mPrevActionBarTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        mPrevActionBarTitle = actionbar.getTitle();
        actionbar.setTitle(R.string.title_signin_with_email);

        mSigninBtn.setOnClickListener(this);
        mForm = new SigninFormBuilder(getBaseActivity())
                .setEmailField(mEmailInputView)
                .setPasswdField(mPasswordInputView).build();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        actionbar.setTitle(mPrevActionBarTitle);
        hideKeyboard();
    }

    @Override
    public void onClick(View view) {
        hideKeyboard();
        if (isSubmitting) {
            return;
        }
        if (view.getId() != R.id.button_signin) {
            return;
        }
        mForm.clearError();
        if (!mForm.validate()) {
            return;
        }
        isSubmitting = true;
        ProgressDialogFragment.show(getBaseActivity(), "signin",
                R.string.desc_submiting);
        JSONObject params = mForm.getParamsAsJson();
        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                isSubmitting = false;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                isSubmitting = false;
                ProgressDialogFragment.hideAll(getBaseActivity());
                if (!response.optBoolean("success")) {
                    if (response.optString("error") != null) {
                        handleRemoteError(
                                Integer.parseInt(response.optString("error")));
                        return;
                    }
                    showError(null);
                    return;
                }
                Intent result = AuthActivity.handleSigninResponse(
                        getBaseActivity(), response);
                AuthActivity parent = (AuthActivity) getBaseActivity();
                parent.onAfterSignin(result);
            }
        };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                isSubmitting = false;
                ProgressDialogFragment.hideAll(getBaseActivity());
                showError(msg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                isSubmitting = false;
            }
        };

        Requests.userSigninWithEmail(getBaseActivity(), params, listener,
                errorListener);
    }

    private void showError(String msg) {
        String message = msg;
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.failed_to_signin);
        }
        new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.title_failed_to_signin)
                .setMessage(message)
                .setPositiveButton(R.string.button_confirm, null)
                .show();
    }

    private void handleRemoteError(int errorCode) {
        switch (errorCode) {
            case 1:
                showError(getString(R.string.error_email_not_found));
                break;
            case 2:
                showError(getString(R.string.error_invalid_password));
                break;
            default:
                showError(null);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getBaseActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEmailInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mPasswordInputView.getWindowToken(), 0);
    }
}
