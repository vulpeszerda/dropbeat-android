package net.dropbeat.spark.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.daftshady.superandroidkit.authentication.AccountConstants;
import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.model.User;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.utils.DbGson;
import net.dropbeat.spark.utils.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import main.java.com.mindscapehq.android.raygun4android.RaygunClient;

/**
 * Created by vulpes on 15. 4. 29..
 */
public class FacebookConnectCallback implements FacebookCallback<LoginResult>{

    public interface OnAfterConnect {
        void onSuccessFacebookConnect(Intent intent);
        void onErrorFacebookConnect(Exception e);
    }

    private OnAfterConnect mOnAfterConnect;
    private Context mContext;

    public FacebookConnectCallback(
            Context context, OnAfterConnect onAfterConnect) {

        mContext = context;
        mOnAfterConnect = onAfterConnect;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

        AccessToken accessToken = loginResult.getAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        FacebookRequestError error = response.getError();
                        if (error != null) {
                            LoginManager.getInstance().logOut();
                            mOnAfterConnect.onErrorFacebookConnect(error.getException());
                            error.getException().printStackTrace();
                            return;
                        }

                        String firstName = object.optString("first_name");
                        String lastName = object.optString("last_name");
                        String fbId = object.optString("id");
                        String email = object.optString("email");

                        if (TextUtils.isEmpty(email)) {
                            Random rand = new Random();
                            email = "user" + (rand.nextInt(89999999) +
                                    10000000) + "@dropbeat.net";
                        }

                        JSONObject param = new JSONObject();
                        try {
                            param.put("email", email);
                            param.put("fb_id", fbId);
                            param.put("first_name", firstName);
                            param.put("last_name", lastName);
                            requestFbSignin(mContext, param, mOnAfterConnect);
                        } catch(JSONException e) {
                            // not reach
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email, first_name, last_name, id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException e) {

    }

    public static void requestFbSignin(final Context context,
                                       final JSONObject params,
                                        final OnAfterConnect onAfterConnect) {
        ProgressDialogFragment.show((BaseActivity)context,
                "signin_fb", R.string.desc_submiting);
        Response.Listener<JSONObject> onResponseListener = new
                Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                BaseActivity parent = (BaseActivity) context;
                if (!parent.isFinishing()) {
                    ProgressDialogFragment.hide(parent, "signin_fb");
                }
                if (!response.optBoolean("success")) {
                    LoginManager.getInstance().logOut();
                    Exception e = new Exception(response.optString("error"));
                    onAfterConnect.onErrorFacebookConnect(e);
                    List<String> tags = new ArrayList<>();
                    tags.add(response.toString());
                    tags.add(params.toString());
                    RaygunClient.Send(e, tags);
                    return;
                }
                Intent result = AuthActivity.handleSigninResponse(
                        context, response);
                onAfterConnect.onSuccessFacebookConnect(result);
            }
        };

        Response.ErrorListener onErrorListener =
                new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                BaseActivity parent = (BaseActivity) context;
                if (!parent.isFinishing()) {
                    ProgressDialogFragment.hide(parent, "signin_fb");
                }
                LoginManager.getInstance().logOut();
                onAfterConnect.onErrorFacebookConnect(error);
            }
        };

        Requests.userSignin(context, params, onResponseListener,
                onErrorListener);
    }
}
