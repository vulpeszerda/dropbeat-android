package net.dropbeat.spark.auth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.inject.Inject;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.addon.form.Form;
import net.dropbeat.spark.addon.form.SignupFormBuilder;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONObject;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 31..
 */
public class SignupWithEmailFragment extends BaseFragment
        implements View.OnClickListener{

    @InjectView(R.id.input_email)
    private EditText mEmailInputView;

    @InjectView(R.id.input_nickname)
    private EditText mNicknameInputView;

    @InjectView(R.id.input_firstname)
    private EditText mFirstnameInputView;

    @InjectView(R.id.input_lastname)
    private EditText mLastnameInputView;

    @InjectView(R.id.input_password)
    private EditText mPasswordInputView;

    @InjectView(R.id.input_password_confirm)
    private EditText mPasswordConfirmInputView;

    @InjectView(R.id.button_signup)
    private Button mSignupBtn;

    private boolean mIsSubmitting = false;
    private Form mSignupForm;
    private CharSequence mPrevActionBarTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signup, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        mPrevActionBarTitle = actionbar.getTitle();
        actionbar.setTitle(R.string.title_signup_with_email);

        mSignupBtn.setOnClickListener(this);
        mSignupForm = new SignupFormBuilder(getBaseActivity())
                .setEmailField(mEmailInputView)
                .setNickNameField(mNicknameInputView)
                .setFirstNameField(mFirstnameInputView)
                .setLastNameField(mLastnameInputView)
                .setPasswdField(mPasswordInputView)
                .build();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        actionbar.setTitle(mPrevActionBarTitle);
        hideKeyboard();
    }

    @Override
    public void onClick(View view) {
        hideKeyboard();
        if (mIsSubmitting) {
            return;
        }
        mSignupForm.clearError();
        boolean isValid = mSignupForm.validate();
        isValid &= validatePasswdConfirm();
        isValid &= validateEmailDomain();
        if (!isValid) {
            return;
        }
        mIsSubmitting = true;

        ProgressDialogFragment.show(getBaseActivity(), "signup",
                R.string.desc_submiting);
        Response.Listener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsSubmitting = false;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hideAll(getBaseActivity());
                mIsSubmitting = false;
                if (!response.optBoolean("success")) {
                    // do remote validate
                    String errorStr;
                    if ((errorStr = response.optString("error")) != null) {
                        int errorCode = Integer.parseInt(errorStr);
                        handleRemoteError(errorCode);
                        return;
                    }
                    showError(null);
                }

                Intent result = AuthActivity.handleSigninResponse(
                        getBaseActivity(), response);
                AuthActivity parent = (AuthActivity) getBaseActivity();
                if (parent != null) {
                    parent.onAfterSignin(result);
                }
            }
        };

        RobustResponseErrorListener errorListener
                = new RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mIsSubmitting = false;
                ProgressDialogFragment.hideAll(getBaseActivity());
                showError(msg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                mIsSubmitting = false;
            }
        };

        Requests.userSignupWithEmail(getBaseActivity(),
                mSignupForm.getParamsAsJson(), listener, errorListener);
    }

    private boolean validatePasswdConfirm() {
        CharSequence seq = mPasswordConfirmInputView.getText();
        CharSequence passwdSeq = mPasswordInputView.getText();
        mPasswordConfirmInputView.setError(null);
        if (TextUtils.isEmpty(seq)) {
            mPasswordConfirmInputView.setError(
                    getString(R.string.error_value_required));
            return false;
        }
        if (!TextUtils.isEmpty(passwdSeq) &&
                !seq.toString().equals(passwdSeq.toString())) {
           mPasswordConfirmInputView.setError(
                   getString(R.string.error_password_confirm_not_match));
            return false;
        }
        return true;
    }

    private boolean validateEmailDomain() {
        CharSequence seq = mEmailInputView.getText();
        if (TextUtils.isEmpty(seq)) {
            return false;
        }
        if (seq.toString().toLowerCase().contains("@dropbeat.net")) {
            mEmailInputView.setError(getString(R.string.error_format__email));
            return false;
        }
        return true;
    }

    private void showError(String msg) {
        String message = msg;
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.failed_to_signin);
        }
        new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.title_failed_to_signin)
                .setMessage(message)
                .setPositiveButton(R.string.button_confirm, null)
                .show();
    }

    private void handleRemoteError(int errorCode) {
        switch (errorCode) {
            case 1:
                mEmailInputView.setError(
                        getString(R.string.error_email_already_exists));
                break;
            case 2:
                mNicknameInputView.setError(
                        getString(R.string.error_nickname_already_exists));
                break;
            default:
                showError(null);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getBaseActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEmailInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mPasswordInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mNicknameInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mPasswordConfirmInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mFirstnameInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mLastnameInputView.getWindowToken(), 0);
    }
}
