package net.dropbeat.spark.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.daftshady.superandroidkit.authentication.AccountConstants;
import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.inject.Inject;

import net.dropbeat.spark.BaseAccountAuthenticatorActivity;
import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.model.User;
import net.dropbeat.spark.utils.DbGson;
import net.dropbeat.spark.utils.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import roboguice.fragment.RoboFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


@ContentView(R.layout.activity_empty)
public class AuthActivity extends BaseAccountAuthenticatorActivity {

    public static final int REQUEST_CODE__AUTH = 7000;


    public static Intent createIntent(Context context) {
        return createIntent(context, false);
    }

    public static Intent createIntent(Context context, boolean isNeedAuth) {
        Intent intent = new Intent(context, AuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(PARAM_IS_NEED_AUTH, isNeedAuth);
        return intent;
    }

    private static final int EMAIL_SUBMIT_REQ_CODE = 400;
    private static final String PARAM_IS_NEED_AUTH = "is_need_auth";

    static Intent handleSigninResponse(Context context, JSONObject response) {
        JSONObject userJson = response.optJSONObject("user");
        Gson gson = DbGson.getInstance();
        User user = gson.fromJson(userJson.toString(), User.class);
        String token = response.optString("token");


        AccountManager manager = AccountManager.get(context);

        Account account = new Account(user.getEmail(), DbAccount.ACCOUNT_TYPE);
        if (AccountUtils.isThereAccount(context, DbAccount.ACCOUNT_TYPE)) {
            AccountUtils.removeAllAccount(context, DbAccount.ACCOUNT_TYPE);
        }
        Bundle userData = new Bundle();
        userData.putString(AccountConstants.ACCOUNT_SEQ_ID,
                AccountUtils.getNextAccountSeqId(context,
                        DbAccount.ACCOUNT_TYPE));
        userData.putString(DbAccount.KEY_FB_ID, user.getFbId());
        userData.putString(DbAccount.KEY_FIRST_NAME, user.getFirstName());
        userData.putString(DbAccount.KEY_LAST_NAME, user.getLastName());
        userData.putString(DbAccount.KEY_UID, user.getId());

        manager.addAccountExplicitly(account, null, userData);
        manager.setAuthToken(account, DbAccount.TOKEN_TYPE_FACEBOOK, token);

        Intent result = new Intent();
        result.putExtra(AccountManager.KEY_ACCOUNT_NAME, user.getEmail());
        result.putExtra(AccountManager.KEY_ACCOUNT_TYPE, DbAccount.ACCOUNT_TYPE);
        result.putExtra(AccountManager.KEY_AUTHTOKEN, token);
        result.putExtra(DbAccount.KEY_UID, user.getId());
        return result;
    }

    private @Inject AuthFragment mAuthFragment;
    private @Inject SigninWithEmailFragment mSigninFragment;
    private @Inject SignupWithEmailFragment mSignupFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        boolean isNeedAuth = false;
        if (intent != null) {
            isNeedAuth = intent.getBooleanExtra(PARAM_IS_NEED_AUTH, false);
        }
        if (isNeedAuth) {
            actionBar.setTitle(R.string.title_need_auth);
        } else {
            actionBar.setTitle(R.string.title_signin);
        }

        FragmentManager manager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            manager.beginTransaction()
                    .replace(R.id.container, mAuthFragment).commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EMAIL_SUBMIT_REQ_CODE)
        if (resultCode == RESULT_OK && data != null) {
            String email = data.getStringExtra("email");
            String token = data.getStringExtra("token");
            String uid = data.getStringExtra("uid");
            Intent result = new Intent();
            result.putExtra(AccountManager.KEY_ACCOUNT_NAME, email);
            result.putExtra(AccountManager.KEY_ACCOUNT_TYPE,
                    DbAccount.ACCOUNT_TYPE);
            result.putExtra(AccountManager.KEY_AUTHTOKEN, token);
            result.putExtra(DbAccount.KEY_UID, uid);
            onAfterSignin(result);
        } else {
            if (AccountUtils.isThereAccount(this, DbAccount.ACCOUNT_TYPE)) {
                AccountUtils.removeAllAccount(this, DbAccount.ACCOUNT_TYPE);
            }
        }
    }

    void onAfterSignin(Intent result) {
        String email = result.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
                .toLowerCase();
        if (email.contains("@dropbeat.net")) {
            Bundle args = new Bundle();
            args.putString("token", result.getStringExtra(
                    AccountManager.KEY_AUTHTOKEN));
            args.putString("uid", result.getStringExtra(DbAccount.KEY_UID));
            Intent intent = EmailSubmitActivity.createIntent(this, args);
            startActivityForResult(intent, EMAIL_SUBMIT_REQ_CODE);
            return;
        }

        Intent broadcast = new Intent(DbAccount.ACTION_SIGNIN);
        LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(this);
        manager.sendBroadcast(broadcast);

        DbApplication app = (DbApplication) getApplication();
        Tracker tracker = app.getAnalyticsTracker();
        tracker.set("&uid", result.getStringExtra(DbAccount.KEY_UID));

        setAccountAuthenticatorResult(result.getExtras());
        setResult(RESULT_OK, result);
        finish();
    }


    public static class AuthFragment extends BaseFragment implements
            OnClickListener, FacebookConnectCallback.OnAfterConnect{

        @InjectView(R.id.facebook_signin_button)
        private View mSigninBtn;

        @InjectView(R.id.email_signin_button)
        private View mEmailSigninBtn;

        @InjectView(R.id.email_signup_button)
        private View mEmailSignupBtn;

        private CallbackManager mFacebookCallbackManager;
        private @Inject SigninWithEmailFragment mSigninFragment;
        private @Inject SignupWithEmailFragment mSignupFragment;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_auth, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            mSigninBtn.setOnClickListener(this);
            mEmailSigninBtn.setOnClickListener(this);
            mEmailSignupBtn.setOnClickListener(this);

            mFacebookCallbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(
                    mFacebookCallbackManager,
                    new FacebookConnectCallback(getActivity(), this));
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.facebook_signin_button:
                    LoginManager.getInstance().logInWithReadPermissions(
                            this, Arrays.asList("public_profile", "email"));
                    break;
                case R.id.email_signin_button:
                    getFragmentManager().beginTransaction()
                        .replace(R.id.container, mSigninFragment)
                        .addToBackStack(null).commit();
                    break;
                case R.id.email_signup_button:
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, mSignupFragment)
                            .addToBackStack(null).commit();
                    break;
            }
        }

        @Override
        public void onSuccessFacebookConnect(Intent intent) {
            AuthActivity parent = (AuthActivity)getActivity();
            if (parent != null) {
                parent.onAfterSignin(intent);
            }
        }

        @Override
        public void onErrorFacebookConnect(Exception e) {
            String msg = null;
            if (e instanceof VolleyError) {
                NetworkResponse response = ((VolleyError) e).networkResponse;
                if (response != null && response.data != null && response.data
                        .length > 0) {
                    msg = new String(response.data);
                }
            }
            if (msg == null) {
                msg = e.getMessage();
            }
            if (msg == null) {
                msg = getString(R.string.desc_undefined_error);
            }
            if (getActivity() != null) {
                Toast.makeText(getActivity(), "Faled to signin (" + msg + ")",
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode,
                                     Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            mFacebookCallbackManager.onActivityResult(requestCode,
                    resultCode, data);
        }
    }

}
