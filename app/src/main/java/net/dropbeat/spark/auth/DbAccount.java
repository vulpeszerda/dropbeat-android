package net.dropbeat.spark.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Pair;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daftshady.superandroidkit.authentication.AbstractAccount;
import com.daftshady.superandroidkit.authentication.AccountConstants;
import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.daftshady.superandroidkit.exception.SuperAndroidKitException;
import com.facebook.login.LoginManager;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.model.Like;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.User;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Created by parkilsu on 15. 4. 28..
 */
public class DbAccount extends AbstractAccount {

    public enum FollowAction {
        FOLLOW,
        UNFOLLOW
    }

    public enum LikeAction {
        LIKE,
        DISLIKE
    }

    public static final String ACCOUNT_TYPE = "net.dropbeat.spark";
    public static final String TOKEN_TYPE_FACEBOOK = "dropbeat.token.facebook";
    public static final String KEY_FB_ID = "fb_id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_UID = "dropbeat_uid";


    public static final String ACTION_SIGNIN = "net.dropbeat.spark.SIGNIN";

    public static final String ACTION_SIGNOUT_SYNC = "net.dropbeat.spark.SIGNOUT_SYNC";
    public static final String ACTION_SIGNOUT_ASYNC = "net.dropbeat.spark.SIGNOUT_ASYNC";
    public static final String ACTION_FOLLOWING_CHANGED = "net.dropbeat.spark.following_changed";
    public static final String ACTION_FOLLOWING_SYNC = "net.dropbeat.spark.following_sync";
    public static final String ACTION_LIKE_CHANGED = "net.dropbeat.spark.like_changed";
    public static final String ACTION_LIKE_SYNC = "net.dropbeat.spark.like_sync";

    public static final String PARAM_FOLLOW_ACTION = "follow_action";
    public static final String PARAM_SYNC_SUCCESS = "sync_success";
    public static final String PARAM_LIKE_ACTION = "like_action";
    public static final String PARAM_USER = "user";
    public static final String PARAM_TRACK = "track";


    /**
     * Should return authenticated user.
     * @param context
     * @return
     */
    public static DbAccount getUser(Context context) {
        if (!AccountUtils.isThereAccount(context, ACCOUNT_TYPE)) {
            return null;
        }
        try {
            DbAccount account = new DbAccount(context, ACCOUNT_TYPE);
            String token = account.getCachedAuthToken();
            return account.getCachedAuthToken() == null ? null : account;
        } catch (SuperAndroidKitException e) {
            return null;
        }
    }

    public static interface OnSignoutFinishedListener {
        void onFinished();
    }

    /**
     * Sign out. Because android should notify 'user signed out' to server, this
     * method has OnSignoutFinishedListener callback (Blocking)
     *
     * @param context
     * @param listener
     */
    public static void signOut(final Context context,
                               final OnSignoutFinishedListener listener) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DbAccount account = getUser(context);
                if (account != null) {
                    // do all sign out notification jobs
                    sendBroadcast(context, ACTION_SIGNOUT_SYNC, true);
                    sendBroadcast(context, ACTION_SIGNOUT_ASYNC, false);
                    account.invalidateAuthToken();
                    AccountManager.get(context).setPassword(account.getAccount(), null);

                    AccountUtils.removeAllAccount(context, ACCOUNT_TYPE);
                    LoginManager manager = LoginManager.getInstance();
                    if (manager != null) {
                        manager.logOut();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                listener.onFinished();
            }
        }.execute();
    }

    private static void sendBroadcast(Context context, String action,
                                      boolean sync) {
        Intent intent = new Intent(action);
        LocalBroadcastManager broadcastManager = LocalBroadcastManager
                .getInstance(context);
        if (sync) {
            broadcastManager.sendBroadcastSync(intent);
        } else {
            broadcastManager.sendBroadcast(intent);
        }
    }

    private String mLastName;
    private String mFirstName;
    private String mFacebookId;
    private String mUid;
    private HashMap<String, User> mFollowings = new HashMap<>();
    private HashMap<String, Like> mLikes = new HashMap<>();

    private boolean mIsSyncFollowings = false;
    private boolean mIsSyncLikes = false;

    private List<FollowActionItem> mFollowWaiters = new ArrayList<>();
    private List<LikeActionItem> mLikeWaiters = new ArrayList<>();

    public DbAccount(Context context, String accountType) {
        super(context, accountType);
        Account account = getAccount();
        mLastName = getManager().getUserData(account, KEY_LAST_NAME);
        mFirstName = getManager().getUserData(account, KEY_FIRST_NAME);
        mFacebookId = getManager().getUserData(account, KEY_FB_ID);
        mUid = getManager().getUserData(account, KEY_UID);
    }

    @Override
    protected String accountType() {
        return ACCOUNT_TYPE;
    }

    @Override
    protected String tokenType() {
        return TOKEN_TYPE_FACEBOOK;
    }

    public String getFullName() {
        return mFirstName + " " + mLastName;
    }

    public String getEmail() {
        Account account = getAccount();
        return account.name;
    }

    public String getUid() {
        return mUid;
    }

    public String getProfileImageUrl() {
        if (TextUtils.isEmpty(mFacebookId)) {
            return null;
        }
        return "http://graph.facebook.com/" + mFacebookId +
                "/picture?type=square";
    }

    public boolean isLiked(Track track) {
        String key = track.getType() + "_" + track.getId();
        return mLikes.get(key) != null;
    }

    public void like(Context context, final Track track) {
        final LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(context);

        synchronized (mLikeWaiters) {
            int idx = -1;
            for (int i = 0; i <  mLikeWaiters.size(); i++) {
                LikeActionItem waiter = mLikeWaiters.get(i);
                if (waiter.track.getId().equals(track.getId()) &&
                        waiter.track.getType().equals(track.getType())) {
                    idx = i;
                }
            }
            if (idx > -1) {
                mLikeWaiters.remove(idx);
            }

            if (mIsSyncLikes) {
                mLikeWaiters.add(new LikeActionItem(track, LikeAction.LIKE));
                return;
            }

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    boolean success = response.optBoolean("success");
                    try {
                        if (success) {
                            JSONObject obj = response.getJSONObject("obj");
                            JSONObject likeObj = obj.getJSONObject("data");

                            Track t = Track.fromJsonObject(
                                    likeObj.getJSONObject("data"));
                            Like like = new Like(
                                    likeObj.getString("id"),
                                    likeObj.getString("type"),
                                    t);

                            String key = t.getType() + "_" + t.getId();
                            mLikes.put(key, like);
                        }
                    } catch (JSONException e) {
                        success = false;
                    }
                    Intent intent = new Intent(ACTION_LIKE_CHANGED);
                    intent.putExtra(PARAM_TRACK, track);
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);
                    intent.putExtra(PARAM_LIKE_ACTION, LikeAction.LIKE);
                    manager.sendBroadcastSync(intent);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_LIKE_CHANGED);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    intent.putExtra(PARAM_LIKE_ACTION, LikeAction.LIKE);
                    manager.sendBroadcastSync(intent);
                }
            };

            Requests.like(context, track, responseListener, errorListener);
        }
    }

    public void unlike(Context context, final Track track) {
        final LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(context);

        String key = track.getType() + "_" + track.getId();
        Like like = mLikes.get(key);
        if (like == null) {
            Intent intent = new Intent(ACTION_LIKE_CHANGED);
            intent.putExtra(PARAM_TRACK, track);
            intent.putExtra(PARAM_SYNC_SUCCESS, true);
            intent.putExtra(PARAM_LIKE_ACTION, LikeAction.LIKE);
            manager.sendBroadcastSync(intent);
            return;
        }

        synchronized (mLikeWaiters) {
            int idx = -1;
            for (int i = 0; i <  mLikeWaiters.size(); i++) {
                LikeActionItem waiter = mLikeWaiters.get(i);
                if (waiter.track.getId().equals(track.getId()) &&
                        waiter.track.getType().equals(track.getType())) {
                    idx = i;
                }
            }
            if (idx > -1) {
                mLikeWaiters.remove(idx);
            }

            if (mIsSyncLikes) {
                mLikeWaiters.add(new LikeActionItem(track, LikeAction.LIKE));
                return;
            }

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    boolean success = response.optBoolean("success");
                    if (success) {
                        String key = track.getType() + "_" + track.getId();
                        mLikes.remove(key);
                    }
                    Intent intent = new Intent(ACTION_LIKE_CHANGED);
                    intent.putExtra(PARAM_TRACK, track);
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);
                    intent.putExtra(PARAM_LIKE_ACTION, LikeAction.LIKE);
                    manager.sendBroadcastSync(intent);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_LIKE_CHANGED);
                    intent.putExtra(PARAM_TRACK, track);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    intent.putExtra(PARAM_LIKE_ACTION, LikeAction.DISLIKE);
                    manager.sendBroadcastSync(intent);
                }
            };

            Requests.unlike(context, like, responseListener,
                    errorListener);
        }
    }

    public void syncLike(final Context context) {
        synchronized (mLikeWaiters) {
            if (mIsSyncLikes) {
                return;
            }
            mIsSyncLikes = true;

            final LocalBroadcastManager manager =
                    LocalBroadcastManager.getInstance(context);

            Response.Listener<JSONObject> responseListener =
                    new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Intent intent = new Intent(ACTION_LIKE_SYNC);
                    boolean success = response.optBoolean("success") &&
                            response.optJSONArray("data") != null;
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);

                    if (success) {
                        mLikes.clear();
                        JSONArray data = response.optJSONArray("data");
                        for (int i = 0; i < data.length(); i ++) {
                            try {
                                JSONObject likeObj = data.getJSONObject(i);
                                Track t = Track.fromJsonObject(
                                        likeObj.getJSONObject("data"));
                                Like like = new Like(
                                        likeObj.getString("id"),
                                        likeObj.getString("type"),
                                        t);

                                String key = t.getType() + "_" + t.getId();
                                mLikes.put(key, like);
                            } catch (JSONException e) {

                            }
                        }
                    }

                    manager.sendBroadcastSync(intent);
                    synchronized (mLikeWaiters) {
                        mIsSyncLikes = true;
                        if (mLikeWaiters.size() == 0) {
                            return;
                        }
                        List<LikeActionItem> items =
                                new ArrayList<>(mLikeWaiters);
                        for (LikeActionItem item : items) {
                            String key = item.track.getType() + "_" + item.track.getId();
                            if (item.action == LikeAction.LIKE) {
                                like(context, item.track);
                            } else if (mLikes.get(key) != null){
                                unlike(context, mLikes.get(key).getTrack());
                            }
                        }
                    }
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_LIKE_SYNC);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    manager.sendBroadcastSync(intent);
                    synchronized (mLikeWaiters) {
                        mIsSyncLikes = false;
                    }
                }
            };

            Requests.getlikes(context, this.getUid(), responseListener,
                    errorListener);
        }
    }

    public boolean isFollowing(User user) {
        String key = user.getType() + "_" + user.getId();
        return mFollowings.get(key) != null;
    }

    public void unfollow(Context context, final User user) {
        final LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(context);

        synchronized (mFollowWaiters) {
            int idx = -1;
            for (int i = 0; i <  mFollowWaiters.size(); i++) {
                FollowActionItem waiter = mFollowWaiters.get(i);
                if (waiter.user.getId().equals(user.getId()) &&
                        waiter.user.getType().equals(user.getType())) {
                    idx = i;
                }
            }
            if (idx > -1) {
                mFollowWaiters.remove(idx);
            }

            if (mIsSyncFollowings) {
                mFollowWaiters.add(new FollowActionItem(user, FollowAction.UNFOLLOW));
                return;
            }

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    boolean success = response.optBoolean("success");

                    if (success) {
                        mFollowings.remove(user.getKey());
                    }
                    Intent intent = new Intent(ACTION_FOLLOWING_CHANGED);
                    intent.putExtra(PARAM_USER, user);
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);
                    intent.putExtra(PARAM_FOLLOW_ACTION, FollowAction.UNFOLLOW);
                    manager.sendBroadcastSync(intent);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_FOLLOWING_CHANGED);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    intent.putExtra(PARAM_FOLLOW_ACTION, FollowAction.UNFOLLOW);
                    manager.sendBroadcastSync(intent);
                }
            };

            Requests.unfollow(context, user.getId(), user.getType(),
                    responseListener, errorListener);
        }
    }

    public void follow(Context context, final User user) {
        final LocalBroadcastManager manager =
                LocalBroadcastManager.getInstance(context);

        synchronized (mFollowWaiters) {
            int idx = -1;
            for (int i = 0; i <  mFollowWaiters.size(); i++) {
                FollowActionItem waiter = mFollowWaiters.get(i);
                if (waiter.user.getId().equals(user.getId()) &&
                        waiter.user.getType().equals(user.getType())) {
                    idx = i;
                }
            }
            if (idx > -1) {
                mFollowWaiters.remove(idx);
            }

            if (mIsSyncFollowings) {
                mFollowWaiters.add(new FollowActionItem(user, FollowAction.FOLLOW));
                return;
            }

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    boolean success = response.optBoolean("success");
                    if (success) {
                        mFollowings.put(user.getKey(), user);
                    }
                    Intent intent = new Intent(ACTION_FOLLOWING_CHANGED);
                    intent.putExtra(PARAM_USER, user);
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);
                    intent.putExtra(PARAM_FOLLOW_ACTION, FollowAction.FOLLOW);
                    manager.sendBroadcastSync(intent);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_FOLLOWING_CHANGED);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    intent.putExtra(PARAM_FOLLOW_ACTION, FollowAction.FOLLOW);
                    manager.sendBroadcastSync(intent);
                }
            };

            Requests.follow(context, user.getId(), user.getType(), responseListener,
                    errorListener);
        }
    }

    public void syncFollowings(final Context context) {
        synchronized (mFollowWaiters) {
            if (mIsSyncFollowings) {
                return;
            }
            mIsSyncFollowings = true;

            final LocalBroadcastManager manager =
                    LocalBroadcastManager.getInstance(context);

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Intent intent = new Intent(ACTION_FOLLOWING_SYNC);
                    boolean success = response.optBoolean("success") &&
                            response.optJSONArray("data") != null;
                    intent.putExtra(PARAM_SYNC_SUCCESS, success);

                    if (success) {
                        mFollowings.clear();
                        JSONArray data = response.optJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            try {
                                JSONObject userObj = data.getJSONObject(i);
                                User followee = new User(
                                        userObj.getString("user_type"),
                                        userObj.getString("id"),
                                        userObj.getString("name"),
                                        userObj.getString("resource_name"),
                                        userObj.getString("image"));

                                mFollowings.put(followee.getKey(), followee);

                            } catch (JSONException e) {

                            }
                        }
                    }

                    manager.sendBroadcastSync(intent);
                    synchronized (mFollowWaiters) {
                        mIsSyncFollowings = true;
                        if (mFollowWaiters.size() == 0) {
                            return;
                        }
                        List<FollowActionItem> items =
                                new ArrayList<>(mFollowWaiters);
                        for (FollowActionItem item : items) {
                            if (item.action == FollowAction.FOLLOW) {
                                follow(context, item.user);
                            } else {
                                unfollow(context, item.user);
                            }
                        }
                    }
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Intent intent = new Intent(ACTION_FOLLOWING_SYNC);
                    intent.putExtra(PARAM_SYNC_SUCCESS, false);
                    manager.sendBroadcastSync(intent);
                    synchronized (mFollowWaiters) {
                        mIsSyncFollowings = false;
                    }
                }
            };

            Requests.getFollowings(context, getUid(), responseListener,
                    errorListener);
        }
    }

    private static class FollowActionItem {
        User user;
        FollowAction action;

        public FollowActionItem(User user, FollowAction action) {
            this.action = action;
            this.user = user;
        }
    }

    private static class LikeActionItem {
        Track track;
        LikeAction action;

        public LikeActionItem(Track track, LikeAction action) {
            this.action = action;
            this.track = track;
        }
    }
}
