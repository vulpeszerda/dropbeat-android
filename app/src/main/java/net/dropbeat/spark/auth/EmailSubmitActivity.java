package net.dropbeat.spark.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.daftshady.superandroidkit.authentication.AccountConstants;
import com.daftshady.superandroidkit.authentication.AccountUtils;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.addon.form.TextFormField;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 31..
 */
@ContentView(R.layout.activity_email_submit)
public class EmailSubmitActivity extends BaseActivity
        implements View.OnClickListener {

    public static Intent createIntent(Context context, Bundle extras) {
        Intent intent = new Intent(context, EmailSubmitActivity.class);
        intent.putExtras(extras);
        return intent;
    }


    @InjectView(R.id.input_email)
    private EditText mEmailInputView;

    @InjectView(R.id.button_submit)
    private Button mSubmitBtn;

    private TextFormField mEmailField;
    private boolean mIsSubmitting = false;
    private Bundle mExtras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubmitBtn.setOnClickListener(this);
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setRequired()
                .setFieldType(TextFormField.FieldType.EMAIL_FIELD).build();
        mEmailField = new TextFormField("email", mEmailInputView, options);
        mExtras = getIntent().getExtras();
        setResult(RESULT_CANCELED);
    }

    @Override
    public void onClick(View view) {
        hideKeyboard();
        final String email = (String)mEmailField.getValue();
        if (!mEmailField.validate(this)) {
            return;
        }
        if (email.toLowerCase().contains("@dropbeat.net")) {
            mEmailInputView.setError(getString(R.string.error_format__email));
            return;
        }
        mEmailInputView.setError(null);
        final Runnable callback = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.putExtras(mExtras);
                intent.putExtra("email", email);
                setResult(RESULT_OK, intent);
                finish();
            }
        };

        if (DbAccount.getUser(this) == null) {
            callback.run();
            return;
        }

        ProgressDialogFragment.show(this, "change_email", R.string.desc_submiting);
        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsSubmitting = false;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hide(EmailSubmitActivity.this, "change_email");
                mIsSubmitting = false;
                if (!response.optBoolean("success")) {
                    showError(getString(R.string.error_email_already_exists));
                    return;
                }
                changeAccountEmail(email);
                callback.run();
            }
        };

        RobustResponseErrorListener errorListener = new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mIsSubmitting = false;
                ProgressDialogFragment.hide(EmailSubmitActivity.this, "change_email");
                showError(msg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                mIsSubmitting = false;
            }
        };

        Requests.userChangeEmail(this, email, listener, errorListener);
    }

    private void changeAccountEmail(String email) {
        DbAccount account = DbAccount.getUser(this);
        if (account == null) {
            return;
        }
        Account coreAccount = account.getAccount();
        AccountManager manager = AccountManager.get(this);

        String token = account.getCachedAuthToken();
        String fbId = manager.getUserData(coreAccount, DbAccount.KEY_FB_ID);
        String lastName = manager.getUserData(coreAccount,
                DbAccount.KEY_LAST_NAME);
        String firstName = manager.getUserData(coreAccount,
                DbAccount.KEY_FIRST_NAME);
        String uid = manager.getUserData(coreAccount, DbAccount.KEY_UID);
        String seqId = manager.getUserData(coreAccount,
                AccountConstants.ACCOUNT_SEQ_ID);

        if (AccountUtils.isThereAccount(this, DbAccount.ACCOUNT_TYPE)) {
            AccountUtils.removeAllAccount(this, DbAccount.ACCOUNT_TYPE);
        }
        Account newCoreAccount = new Account(email, DbAccount.ACCOUNT_TYPE);

        Bundle userData = new Bundle();
        userData.putString(AccountConstants.ACCOUNT_SEQ_ID, seqId);
        userData.putString(DbAccount.KEY_FB_ID, fbId);
        userData.putString(DbAccount.KEY_FIRST_NAME, firstName);
        userData.putString(DbAccount.KEY_LAST_NAME, lastName);
        userData.putString(DbAccount.KEY_UID, uid);

        manager.addAccountExplicitly(newCoreAccount, null, userData);
        manager.setAuthToken(newCoreAccount,
                DbAccount.TOKEN_TYPE_FACEBOOK, token);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEmailInputView.getWindowToken(), 0);
    }

    private void showError(String msg) {
        String message = msg;
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.failed_to_change_email);
        }
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_failed_to_submit)
                .setMessage(message)
                .setPositiveButton(R.string.button_confirm, null)
                .show();
    }
}
