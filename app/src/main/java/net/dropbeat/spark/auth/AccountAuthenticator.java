package net.dropbeat.spark.auth;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.daftshady.superandroidkit.authentication.BaseAuthenticator;

import net.dropbeat.spark.R;

public class AccountAuthenticator extends BaseAuthenticator {

    private Context mContext;

    protected AccountAuthenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected String signIn(String userName, String userPass) {
        return null;
    }

    @Override
    protected Intent createActivityIntent(AccountAuthenticatorResponse response) {
        return AuthActivity.createIntent(mContext);
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response,
            String accountType, String authTokenType,
            String[] requiredFeatures, Bundle options)
            throws NetworkErrorException {
        if (AccountUtils.isThereAccount(mContext, accountType, authTokenType)) {
            final Bundle result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE,
                    AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION);
            result.putString(AccountManager.KEY_ERROR_MESSAGE,
                    mContext.getString(R.string.error_allow_only_one_account));
            Toast.makeText(mContext, R.string.error_allow_only_one_account,
                    Toast.LENGTH_LONG).show();
            return result;
        }
        return super.addAccount(response, accountType, authTokenType,
                requiredFeatures, options);
    }

    @Override
    public Bundle getAccountRemovalAllowed(
            AccountAuthenticatorResponse response, Account account)
            throws NetworkErrorException {
        // TODO
        // need to stop player
        return super.getAccountRemovalAllowed(response, account);
    }
}
