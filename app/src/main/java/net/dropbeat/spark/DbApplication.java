package net.dropbeat.spark;

import android.app.Application;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import net.dropbeat.spark.network.Requests;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class DbApplication extends Application {

    private static final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;
    private static final String TAG = "DropbeatApplication";

    private ImageLoader mImageLoader;

    private boolean mWasAppInBackground = true;
    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;

    // Google Analytics
    private Tracker mAnalyticsTracker;


    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public synchronized Tracker getAnalyticsTracker() {
        if (mAnalyticsTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mAnalyticsTracker = analytics.newTracker(R.xml.ga_tracker);
            mAnalyticsTracker.enableAdvertisingIdCollection(true);
        }
        return mAnalyticsTracker;
    }

    private ImageLoader createDefaultImageLoader() {
        File cacheDir;
        cacheDir = new File(android.os.Environment
                .getExternalStorageDirectory(),"thumbCache");
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        DisplayImageOptions imageLoaderOptions = new DisplayImageOptions.Builder()
//              .displayer(new FadeInBitmapDisplayer(500))
                .cacheInMemory(true)
                .resetViewBeforeLoading(true).build();

        ImageLoaderConfiguration.Builder configBuilder =
                new ImageLoaderConfiguration.Builder(this)
                        .defaultDisplayImageOptions(imageLoaderOptions)
                        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                        .tasksProcessingOrder(QueueProcessingType.LIFO);

        DiskCache diskCache;
        try {
            diskCache = new LruDiskCache(cacheDir,
                    new HashCodeFileNameGenerator(), 50 * 1024 * 1024);
            configBuilder.diskCache(diskCache);
        } catch (IOException e) {
        }

        ImageLoaderConfiguration config = configBuilder.build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        return imageLoader;
    }

    public synchronized ImageLoader getDefaultImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = createDefaultImageLoader();
        }
        return mImageLoader;
    }

    public void startActivityTransitionTimer() {
        mActivityTransitionTimer = new Timer();
        mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                if (!mWasAppInBackground) {
                    synchronized (this) {
                        if (!mWasAppInBackground) {
                            DbApplication.this.mWasAppInBackground = true;
                            onAppBecomeBackground();
                        }
                    }
                }
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        if (mWasAppInBackground) {
            synchronized (this) {
                if (mWasAppInBackground) {
                    mWasAppInBackground = false;
                    onAppBecomeForeground();
                }
            }
        }
    }

    public synchronized boolean wasAppInBackground() {
        return mWasAppInBackground;
    }

    // Send rcpr to all wakable devices,
    // and release cpr timer
    private void onAppBecomeBackground() {
        Log.d(TAG, "app is background");
    }

    // Send cpr to all wakable devices
    private void onAppBecomeForeground() {
        Log.d(TAG, "app is foreground");
        getClientKeys();
    }

    private void getClientKeys() {
        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                DropbeatContext.soundcloudClientKey =
                        response.optString("soundcloud_key");
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
            }
        };
        Requests.getClientKey(this, listener, errorListener);
    }

}
