package net.dropbeat.spark;

import net.dropbeat.spark.feed.BaseFeedFragment;

import java.util.Map;

/**
 * Created by vulpes on 15. 9. 26..
 */
public class DropbeatContext {

    public static String soundcloudClientKey;
    public static Map<String, BaseFeedFragment.FilterOption> genres;
}
