package net.dropbeat.spark.model;

/**
 * Created by parkilsu on 15. 5. 2..
 */
public class StreamSource {
    private String url;
    private String formatNote;
    private String type;

    public StreamSource(String url, String formatNote, String type) {
        this.url = url;
        this.formatNote = formatNote;
        this.type = type;
    }

    public String getFormatNote() {
        return formatNote;
    }

    public void setFormatNote(String formatNote) {
        this.formatNote = formatNote;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
