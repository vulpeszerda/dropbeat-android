package net.dropbeat.spark.model;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by vulpes on 15. 10. 9..
 */
public class UserTrack extends Track {
    private String uid;
    private String uniqueKey;
    private String streamUrl;
    private String trackType;
    private String userId;
    private int likeCount;
    private boolean downloadable;
    private int genreId;
    private String resourceName;
    private String waveformUrl;

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    public String getTrackType() {
        return trackType;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
    }

    public boolean getDownloadable() {
        return downloadable;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setWaveformUrl(String waveformUrl) {
        this.waveformUrl = waveformUrl;
    }

    @Override
    public void share(final BaseActivity activity) {
        DbApplication app = (DbApplication) activity.getApplication();
        Tracker tracker = app.getAnalyticsTracker();
        tracker.send(new HitBuilders.EventBuilder().setCategory("track-share")
                .setAction("from-android")
                .setLabel(title).setValue(1).build());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                title + " " + "http://dropbeat.net/r/" +
                        userResourceName + "/" + resourceName);
        sendIntent.setType("text/plain");
        activity.startActivity(sendIntent);
    }

    public static Track fromJsonObject(JSONObject obj) {
        try {
            UserTrack track = new UserTrack();
            track.setId(obj.getString("unique_key"));
            track.setUid(obj.getString("id"));
            track.setTrackName(obj.getString("name"));
            track.setUserId(obj.optString("user_id"));
            track.setUserName(obj.getString("user_name"));
            track.setUserResourceName(obj.getString("user_resource_name"));
            track.setUserProfileImage(obj.getString("user_profile_image"));
            track.setTrackType(obj.getString("track_type"));
            track.setDownloadable(obj.getBoolean("downloadable"));
            track.setDuration(obj.getInt("duration"));
            track.setDesc(obj.getString("description"));
            track.setThumbnailUrl(obj.getString("coverart_url"));
            track.setHqThumbnailUrl(obj.getString("coverart_url"));
            track.setGenreId(obj.getInt("genre_id"));
            track.setUniqueKey(obj.getString("unique_key"));
            track.setResourceName(obj.getString("resource_path"));
            track.setWaveformUrl(obj.getString("waveform_url"));
            track.setStreamUrl(obj.getString("stream_url"));
            track.setLikeCount(obj.optInt("like_count"));

            String createdAtStr = obj.getString("created_at");
            String dateStr = createdAtStr.substring(0, 10);
            SimpleDateFormat remoteFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = remoteFormat.parse(dateStr);
                track.setCreatedAt(date);
            } catch(ParseException e) {

            }

            track.setTitle(track.getUserName() + " - " + track.getTrackName());
            track.setType("dropbeat");

            String dropUrl = obj.getString("drop_url");
            Drop drop = new Drop(dropUrl, "dropbeat", 0);
            track.setDrop(drop);

            return track;
        } catch (JSONException e) {
            return null;
        }
    }
}
