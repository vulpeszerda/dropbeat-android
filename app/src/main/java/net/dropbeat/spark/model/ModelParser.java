package net.dropbeat.spark.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parkilsu on 15. 4. 29..
 */
public class ModelParser {
    /**
     * This method is used in `playlist/all`
     * @return
     */
    public static List<Playlist> parsePlaylists(JSONObject resp) {
        try {
            List<Playlist> playlists = new ArrayList<>();
            JSONArray array = (JSONArray) resp.get("data");
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject) array.get(i);
                playlists.add(
                        Playlist.fromJsonObject(
                                obj.getString("id"),
                                obj.getString("name"),
                                new JSONArray()));
            }
            return playlists;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Used in `playlist`, `playlist/initial` GET.
     * @param resp
     * @return
     */
    public static Playlist parsePlaylist(JSONObject resp) {
        try {
            JSONObject obj = (JSONObject) resp.get("playlist");
            return Playlist.fromJsonObject(
                    obj.getString("id"), obj.getString("name"), obj.get("data"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

}
