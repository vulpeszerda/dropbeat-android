package net.dropbeat.spark.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vulpes on 15. 6. 12..
 */
public class HotRelease implements Parcelable {
    private List<Track> playlist;
    private String img;
    private String name;
    private String shortdesc;
    private String description;

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public String getDescription() {
        return description;
    }

    public List<Track> getPlaylist() {
        return playlist;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(img);
        dest.writeString(shortdesc);
        dest.writeString(description);
        dest.writeInt(playlist.size());
        for (Track track : playlist) {
            dest.writeSerializable(track);
        }
    }

    public static final Parcelable.Creator<HotRelease> CREATOR =
            new Creator<HotRelease>() {

        @Override
        public HotRelease createFromParcel(Parcel source) {
            HotRelease release = new HotRelease();
            release.name = source.readString();
            release.img = source.readString();
            release.shortdesc = source.readString();
            release.description = source.readString();
            int size = source.readInt();
            List<Track> tracks = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Track track = (Track) source.readSerializable();
                tracks.add(track);
            }
            release.playlist = tracks;
            return release;
        }

        @Override
        public HotRelease[] newArray(int size) {
            return new HotRelease[size];
        }
    };
}
