package net.dropbeat.spark.model;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by parkilsu on 15. 4. 29..
 */
public class Playlist implements Serializable {
    private String id;
    private String name;
    private ArrayList<Track> data = new ArrayList<>();

    public Playlist() {

    }

    public Playlist(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Playlist(String id, String name, ArrayList<Track> tracks) {
        this.id = id;
        this.name = name;
        this.data = tracks;
    }

    private void showShareFailureDialog(final BaseActivity activity) {
        if (activity.isFinishing()) {
            return;
        }
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(
                        R.string.title_failed_to_load))
                .setMessage("Failed to share playlist")
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Playlist.this.share(activity);
                            }
                        })
                .setNegativeButton(R.string.button_cancel, null).show();
    }

    public void share(final BaseActivity activity) {
        ProgressDialogFragment.show(activity, "share", "Loading..");

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(activity) {
                    @Override
                    public void onSkipped(JSONObject response) {

                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        ProgressDialogFragment.hide(activity, "share");
                        if (!response.optBoolean("success")) {
                            Playlist.this.showShareFailureDialog(activity);
                            return;
                        }
                        try {
                            JSONObject obj = response.optJSONObject("obj");
                            if (obj == null) {
                                obj = response.getJSONObject("data");
                            }
                            String uid = obj.getString("uid");

                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, name + " " +
                                    "http://dropbeat.net/?playlist=" + uid);
                            sendIntent.setType("text/plain");
                            activity.startActivity(sendIntent);

                            DbApplication app = (DbApplication) activity.getApplication();
                            Tracker tracker = app.getAnalyticsTracker();
                            tracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("playlist-share")
                                    .setAction("from-android")
                                    .setLabel(name).setValue(1).build());
                        } catch (JSONException e) {
                            Playlist.this.showShareFailureDialog(activity);
                        }
                    }
                };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(activity) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        ProgressDialogFragment.hide(activity, "share");
                        Playlist.this.showShareFailureDialog(activity);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.sharePlaylist(activity, this, listener, errorListener);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public synchronized List<Track> getTracks() {
        List<Track> list = new ArrayList<>();
        for (Track track : data) {
            list.add(track);
        }
        return list;
    }

    public synchronized void addAll(List<Track> newTracks, boolean clear) {
        if (clear) {
            data.clear();
        }
        data.addAll(newTracks);
    }

    public synchronized void addTrack(Track track) {
        data.add(track);
    }

    public synchronized void delTrack(int idx) {
        data.remove(idx);
    }

    public static Playlist fromJsonObject(String id, String name, Object data) {
        try {
            Playlist playlist = new Playlist();
            playlist.setId(id);
            playlist.setName(name);
            JSONArray tracks = (JSONArray) data;
            for (int i = 0; i < tracks.length(); i++) {
                Track track = Track.fromJsonObject((JSONObject) tracks.get(i));
                if (track != null) {
                    playlist.addTrack(track);
                }
            }
            return playlist;
        } catch (JSONException e) {
            return null;
        }
    }

    public synchronized JSONArray toJsonArray() {
        try {
            JSONArray array = new JSONArray();
            for (Track track : data) {
                JSONObject obj = new JSONObject();
                if (track.getType().equals("dropbeat")) {
                    if (!(track instanceof UserTrack)) {
                        // broken case
                        continue;
                    }
                    UserTrack userTrack = (UserTrack)track;
                    obj.put("id", userTrack.getUid());
                    obj.put("type", userTrack.getType());
                } else {
                    obj.put("title", track.getTitle());
                    obj.put("id", track.getId());
                    obj.put("type", track.getType());
                }
                array.put(obj);
            }
            return array;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
