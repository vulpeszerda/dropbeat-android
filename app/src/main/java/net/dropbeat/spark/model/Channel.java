package net.dropbeat.spark.model;

/**
 * Created by vulpes on 15. 7. 13..
 */
public class Channel {

    public static final Channel EMPTY_CHANNEL = new Channel(false);

    private String uid;
    private String name;
    private String thumbnail;
    private int idx;
    private boolean isBookmarked;

    public Channel() {

    }

    public Channel(boolean isBookmarked) {
        this.isBookmarked = isBookmarked;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getIdx() {
        return idx;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(boolean isBookmarked) {
        this.isBookmarked = isBookmarked;
    }
}
