package net.dropbeat.spark.model;

/**
 * Created by vulpes on 15. 10. 26..
 */
public class Like {
    private String type;
    private String id;
    private Track data;

    public Like(String id, String type, Track track) {
        this.id = id;
        this.type = type;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Track getTrack() {
        return data;
    }
}
