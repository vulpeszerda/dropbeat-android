package net.dropbeat.spark.model;

import android.content.Context;

import net.dropbeat.spark.auth.DbAccount;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vulpes on 15. 4. 30..
 */
public class User implements Serializable{
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String fbId;
    private String profileImage;
    private String profileCoverImage;
    private String description;
    private int numFollowing;
    private int numFollowers;
    private int numTracks;
    private String nickname;
    private String resourceName;
    private String type = "user";

    public User(String userType, String id, String name, String resourceName,
                String image) {
        this.id = id;
        this.type = userType;
        this.nickname = name;
        this.resourceName = resourceName;
        this.profileImage = image;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFbId() {
        return fbId;
    }

    public String getDescription() {
        return description;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getProfileCoverImage() {
        return profileCoverImage;
    }

    public String getNickname() {
        return nickname;
    }

    public String getResourceName() {
        return resourceName;
    }

    public int getNumFollowing() {
        return numFollowing;
    }

    public int getNumFollowers() {
        return numFollowers;
    }

    public int getNumTracks() {
        return numTracks;
    }

    public String getType() {
        return type;
    }

    public String getKey() {
        String prefix = null;
        if ("user".equals(type)) {
            prefix = "u";
        } else if ("artist".equals(type)) {
            prefix = "a";
        } else if ("channel".equals(type)) {
            prefix = "c";
        }
        return prefix;
    }

    public boolean isFollowed(Context context) {
        DbAccount account = DbAccount.getUser(context);
        if (account == null) {
            return false;
        }

        return account.isFollowing(this);
    }
}
