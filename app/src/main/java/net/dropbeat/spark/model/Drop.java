package net.dropbeat.spark.model;

import java.io.Serializable;

/**
 * Created by vulpes on 15. 8. 13..
 */
public class Drop implements Serializable{
    private String type;
    private String dref;
    private int when;

    public Drop(String dref, String type, int when) {
        this.dref = dref;
        this.type = type;
        this.when = when;
    }

    public String getType() {
        return type;
    }

    public String getDref() {
        return dref;
    }

    public int getWhen() {
        return when;
    }
}
