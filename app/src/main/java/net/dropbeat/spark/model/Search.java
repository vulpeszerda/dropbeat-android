package net.dropbeat.spark.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Contains search result tracks.
 */
public class Search {

    private static final String KOREAN_REGEX = ".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*";

    private List<User> users = new ArrayList<>();
    private List<Track> tracks = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public static Search fromJsonObject(JSONObject data) {
        if (data == null || !data.optBoolean("success")) {
            return null;
        }
        Search search = new Search();
        JSONArray artistData = data.optJSONArray("artists");
        if (artistData != null) {
            for (int i = 0; i < artistData.length(); i++) {
                try {
                    JSONObject userObj = artistData.getJSONObject(i);
                    User user = new User(
                            userObj.getString("user_type"),
                            userObj.getString("id"),
                            userObj.getString("name"),
                            userObj.getString("resource_name"),
                            userObj.getString("image"));
                    search.users.add(user);
                } catch (JSONException e) {
                    continue;
                }
            }
        }

        JSONArray trackData = data.optJSONArray("tracks");
        if (trackData != null) {
            Pattern koreanPattern = Pattern.compile(KOREAN_REGEX);

            for (int i = 0; i < trackData.length(); i++) {
                JSONObject trackObj = trackData.optJSONObject(i);
                if (trackObj == null) {
                    continue;
                }
                Track track = Track.fromJsonObject(trackObj);
                if (track != null &&
                        ("dropbeat".equals(track.getType()) ||
                                !koreanPattern.matcher(track.getTitle()).matches())) {
                    search.tracks.add(track);
                }
            }
        }
        return search;
    }
}
