package net.dropbeat.spark.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;

import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.network.Path;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by parkilsu on 15. 4. 29..
 */

/**
 * NOTE that this class is used in two diffrent cases.
 * 1. As a playlist elem.
 * 2. As a search result elem.
 *
 * Track in case2 contains additional information. (drop, dref, tag)
 */
public class Track implements Serializable{
    protected String title;
    protected String trackName;
    protected String id;
    protected Drop drop;
    protected String type;
    protected String tag;
    protected String thumbnailUrl;
    protected String hqThumbnailUrl;
    protected String desc;
    protected String genre;
    protected String userName;
    protected String userResourceName;
    protected String userProfileImage;
    protected Date createdAt;
    protected int duration;
    protected boolean isTopMatch;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserResourceName() {
        return userResourceName;
    }

    public void setUserResourceName(String userResourceName) {
        this.userResourceName = userResourceName;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public Drop getDrop() {
        return drop;
    }

    public void setDrop(Drop drop) {
        this.drop = drop;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setHqThumbnailUrl(String url) {
        this.hqThumbnailUrl = url;
    }

    public String getHqThunbnailUrl() {
        return hqThumbnailUrl;
    }

    public void setThumbnailUrl(String url) {
        this.thumbnailUrl = url;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isTopMatch() {
        return isTopMatch;
    }

    public void setTopMatch(boolean isTopMatch) {
        this.isTopMatch = isTopMatch;
    }

    public boolean isLiked(Context context) {
        DbAccount account = DbAccount.getUser(context);

        if (account == null) {
            return false;
        }
        return account.isLiked(this);
    }

    protected void showShareFailureDialog(final BaseActivity activity) {
        if (activity.isFinishing()) {
            return;
        }
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(
                        R.string.title_failed_to_load))
                .setMessage("Failed to share track")
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Track.this.share(activity);
                            }
                        })
                .setNegativeButton(R.string.button_cancel, null).show();
    }

    public void share(final BaseActivity activity) {
        ProgressDialogFragment.show(activity, "share", "Loading..");

        DbApplication app = (DbApplication) activity.getApplication();
        Tracker tracker = app.getAnalyticsTracker();
        tracker.send(new HitBuilders.EventBuilder().setCategory("track-share")
                .setAction("from-android")
                .setLabel(title).setValue(1).build());

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(activity) {
                    @Override
                    public void onSkipped(JSONObject response) {

                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        ProgressDialogFragment.hide(activity, "share");
                        if (!response.optBoolean("success")) {
                            Track.this.showShareFailureDialog(activity);
                            return;
                        }
                        try {
                            JSONObject obj = response.optJSONObject("obj");
                            if (obj == null) {
                                obj = response.getJSONObject("data");
                            }
                            String uid = obj.getString("uid");

                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, title + " " +
                                    "http://dropbeat.net/?track=" + uid);
                            sendIntent.setType("text/plain");
                            activity.startActivity(sendIntent);

                        } catch (JSONException e) {
                            Track.this.showShareFailureDialog(activity);
                        }
                    }
                };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(activity) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        ProgressDialogFragment.hide(activity, "share");
                        Track.this.showShareFailureDialog(activity);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.shareTrack(activity, this, listener, errorListener);
    }

    public static Track fromJsonObject(JSONObject obj) {
        try {
            if (obj.getString("type").equals("dropbeat")) {
                return UserTrack.fromJsonObject(obj);
            }
            Track track = new Track();
            track.setId(obj.getString("id"));
            track.setTitle(obj.getString("title"));
            track.setType(obj.getString("type"));
            if (track.getType().equals("youtube")) {
                track.setThumbnailUrl(
                        "http://img.youtube.com/vi/" + track.getId() +
                                "/mqdefault.jpg");
                track.setHqThumbnailUrl(
                        "http://img.youtube.com/vi/" + track.getId() +
                                "/hqdefault.jpg");
            } else if (track.getType().equals("soundcloud")) {
                track.setThumbnailUrl(Path.soundcloudImage + "&id=" +
                        track.getId() + "&size=small");
                track.setHqThumbnailUrl(Path.soundcloudImage + "&id=" +
                        track.getId() + "&size=large");
            } else {
                track.setThumbnailUrl(obj.optString("artwork"));
                track.setHqThumbnailUrl(obj.optString("artwork"));
            }

            // Because `track` in search result contains these additional information.
            if (obj.has("drop")) {
                JSONObject dropObj = obj.getJSONObject("drop");
                Drop drop = new Drop(dropObj.getString("dref"),
                        dropObj.getString("type"),
                        dropObj.optInt("when", 0));
                track.setDrop(drop);
            }
            if (obj.has("tag")) {
                track.setTag(obj.getString("tag"));
            }
            if (obj.has("top_match")) {
                track.setTopMatch(obj.getBoolean("top_match"));
            }

            if (obj.has("published_at")) {
                String date = obj.getString("published_at").substring(0, 10);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date publishedAt = format.parse(date);
                    track.setCreatedAt(publishedAt);
                } catch(ParseException e) {
                    // do nothing
                }
            }

            if (obj.has("channel_image")) {
                track.setUserProfileImage(obj.getString("channel_image"));
            }

            if (obj.has("channel_title")) {
                track.setUserName(obj.getString("channel_title"));
            }

            if (obj.has("resource_name")) {
                track.setUserResourceName(obj.getString("resource_name"));
            }
            return track;
        } catch (JSONException e) {
            return null;
        }
    }
}
