package net.dropbeat.spark.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by vulpes on 15. 6. 11..
 */
public class BeatportTrack {
    private String artistName;
    private String category;
    private String genre;
    private String label;
    private String mixType;
    private String rank;
    private String released;
    private String thumbnail;
    private String trackName;
    private String youtubeUid;

    public String getArtistName() {
        return artistName;
    }

    public String getCategory() {
        return category;
    }

    public String getGenre() {
        return genre;
    }

    public String getLabel() {
        return label;
    }

    public String getMixType() {
        return mixType;
    }

    public String getRank() {
        return rank;
    }

    public String getReleased() {
        return released;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getYoutubeUid() {
        return youtubeUid;
    }


    public Track getTrack() {
        if (youtubeUid == null) {
            return null;
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put("id", youtubeUid);
            obj.put("title", artistName + " - " + trackName);
            obj.put("type", "youtube");
            return Track.fromJsonObject(obj);
        } catch(JSONException e) {
            // not reach
        }
        return null;
    }
}
