package net.dropbeat.spark.utils;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.ArrayList;

public class InternetConnectivityManager {

    public static interface Observer {
        void onInternetConnected();

        void onInternetDisconnected();

        void onInternetConnectTypeChanged(boolean isWifi);
    }

    public static InternetConnectivityManager getInstance(Context context) {
        if (manager == null) {
            if (context instanceof Activity) {
                context = ((Activity) context).getApplicationContext();
            } else if (context instanceof Service) {
                context = ((Service) context).getApplicationContext();
            }
            manager = new InternetConnectivityManager();
            updateNetworkState(context);
        }
        return manager;
    }

    private static final String TAG = "InternetManager";
    private static final int STATE_WIFI = 1;
    private static final int STATE_MOBILE = 2;
    private static InternetConnectivityManager manager;

    private ArrayList<Observer> mObservers;
    private int mConnectState;
    private Object mConnectStateLock;

    private InternetConnectivityManager() {
        mObservers = new ArrayList<Observer>();
        mConnectState = 0;
        mConnectStateLock = new Object();
    }

    public boolean addObserver(Observer ob) {
        synchronized (mObservers) {
            return mObservers.add(ob);
        }
    }

    public boolean removeObserver(Observer ob) {
        synchronized (mObservers) {
            return mObservers.remove(ob);
        }
    }

    public boolean isConnected() {
        synchronized (mConnectStateLock) {
            return mConnectState > 0;
        }
    }

    public boolean isWifiConnected() {
        synchronized (mConnectStateLock) {
            return (mConnectState & STATE_WIFI) > 0;
        }
    }

    public boolean isMobileConnected() {
        synchronized (mConnectStateLock) {
            return (mConnectState & STATE_MOBILE) > 0;
        }
    }

    private int getConnectState() {
        synchronized (mConnectStateLock) {
            return mConnectState;
        }
    }

    private void notifyConnected() {
        synchronized (mObservers) {
            Log.d(TAG, "notify internet connected");
            for (Observer ob : mObservers) {
                ob.onInternetConnected();
            }
        }
    }

    private void notifyDisconnected() {
        synchronized (mObservers) {
            Log.d(TAG, "notify internet disconnected");
            for (Observer ob : mObservers) {
                ob.onInternetDisconnected();
            }
        }
    }

    private void notifyConnectTypeChanged() {
        synchronized (mObservers) {
            Log.d(TAG, "notify internet connect type changed");
            for (Observer ob : mObservers) {
                ob.onInternetConnectTypeChanged(isWifiConnected());
            }
        }
    }

    private Object getConnectStateLock() {
        return mConnectStateLock;
    }

    private void setMobileConnected(boolean connected) {
        synchronized (mConnectStateLock) {
            boolean isMobilePrevConnected = isMobileConnected();
            if (isMobilePrevConnected && !connected) {
                mConnectState -= STATE_MOBILE;
            } else if (!isMobilePrevConnected && connected) {
                mConnectState += STATE_MOBILE;
            }
        }
    }

    private void setWifiConnected(boolean connected) {
        synchronized (mConnectStateLock) {
            boolean isWifiPrevConnected = isWifiConnected();
            if (isWifiPrevConnected && !connected) {
                mConnectState -= STATE_WIFI;
            } else if (!isWifiPrevConnected && connected) {
                mConnectState += STATE_WIFI;
            }
        }
    }

    private static void updateNetworkState(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfos = manager.getAllNetworkInfo();
        if (netInfos != null) {
            InternetConnectivityManager connManager = InternetConnectivityManager
                    .getInstance(context);

            synchronized (connManager.getConnectStateLock()) {
                int prevConnectedState = connManager.getConnectState();

                for (NetworkInfo netInfo : netInfos) {
                    switch (netInfo.getType()) {
                    case ConnectivityManager.TYPE_MOBILE:
                        connManager.setMobileConnected(netInfo.isConnected());
                        break;
                    case ConnectivityManager.TYPE_WIFI:
                        connManager.setWifiConnected(netInfo.isConnected());
                        break;
                    }
                }
                int currConnectedState = connManager.getConnectState();
                if (prevConnectedState != currConnectedState) {
                    if (currConnectedState == 0) {
                        connManager.notifyDisconnected();
                    } else {
                        if (prevConnectedState == 0) {
                            connManager.notifyConnected();
                        } else {
                            connManager.notifyConnectTypeChanged();
                        }
                    }
                }
            }
        }
    }

    public static class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateNetworkState(context);
        }
    }

}
