package net.dropbeat.spark.utils;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class FileUtils {

    public static boolean isSdcardAvailable() {
        return android.os.Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public static String readableFileSize(long size) {
        if (size <= 0)
            return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size
                / Math.pow(1024, digitGroups))
                + " " + units[digitGroups];
    }

    public static File getAlternativeFile(File file) {
        String name = file.getName();
        int dotIdx = name.lastIndexOf(".");
        String extension = "";
        if (dotIdx > 0 && name.length() > dotIdx) {
            extension = name.substring(dotIdx + 1);
            name = name.substring(0, dotIdx);
        }
        int count = 0;
        while (file.exists()) {
            ++count;
            file = new File(file.getParentFile(), name + "(" + count + ")."
                    + extension);
        }
        return file;
    }

    @SuppressLint("NewApi")
    public static Bitmap getThumbnail(Context context, String path)
            throws Exception {

        if (isVideoPath(path)) {
            return ThumbnailUtils.createVideoThumbnail(path,
                    MediaStore.Video.Thumbnails.MINI_KIND);
        } else if (isAudioPath(path) && android.os.Build.VERSION.SDK_INT >= 10) {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            byte[] rawArt;
            BitmapFactory.Options bfo = new BitmapFactory.Options();

            mmr.setDataSource(path);
            rawArt = mmr.getEmbeddedPicture();

            if (rawArt != null) {
                return BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length,
                        bfo);
            }
        } else if (isImagePath(path)) {
            ContentResolver resolver = context.getContentResolver();
            Cursor cursor = resolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    new String[] { MediaStore.MediaColumns._ID },
                    MediaStore.MediaColumns.DATA + "=?", new String[] { path },
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                int id = cursor.getInt(cursor
                        .getColumnIndex(MediaStore.MediaColumns._ID));
                cursor.close();

                ExifInterface exif = new ExifInterface(path);
                int exifOrientation = exif.getAttributeInt(ExifInterface
                        .TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int rotate = 0;
                switch (exifOrientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate += 90;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate += 90;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate += 90;
                }

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail
                        (resolver, id, MediaStore.Images.Thumbnails
                                .MINI_KIND, options);

                if (rotate > 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    return Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(), matrix,
                            true);
                }
                return bitmap;
            }

            cursor.close();
        }
        return null;
    }

    /**
     * Open file with any type of extension
     *
     * @see http
     *      ://www.androidsnippets.com/open-any-type-of-file-with-default-intent
     *
     * @param context
     * @param url
     * @throws java.io.IOException
     */
    public static Intent getOpenFileIntent(File url) throws IOException,
            ActivityNotFoundException {
        // Create URI
        File file = url;
        Uri uri = Uri.fromFile(file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        // Check what kind of file you are trying to open, by comparing the url
        // with extensions.
        // When the if condition is matched, plugin sets the correct intent
        // (mime) type,
        // so Android knew what application to use to open the file
        String path = url.toString().toLowerCase(Locale.ENGLISH);
        if (path.endsWith(".doc") || path.endsWith(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (path.endsWith(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (path.endsWith(".ppt") || path.endsWith(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (path.endsWith(".xls") || path.endsWith(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (path.endsWith(".zip") || path.endsWith(".rar")) {
            // ZIP file
            intent.setDataAndType(uri, "application/zip");
        } else if (path.endsWith(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (path.endsWith(".wav") || path.endsWith(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (path.endsWith(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (path.endsWith(".jpg") || path.endsWith(".jpeg")
                || path.endsWith(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (path.endsWith(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (path.endsWith(".3gp") || path.endsWith(".mpg")
                || path.endsWith(".mpeg") || path.endsWith(".mpe")
                || path.endsWith(".mp4") || path.endsWith(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            // if you want you can also define the intent type for any other
            // file

            // additionally use else clause below, to manage other unknown
            // extensions
            // in this case, Android will show all applications installed on the
            // device
            // so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static boolean isImagePath(String path) {
        String[] docExtensionList =
                new String[] {
                        "gif", "jpg", "jpeg", "png"
                };
        path = path.toLowerCase(Locale.ENGLISH);
        for (String ext : docExtensionList) {
            if (path.endsWith("." + ext)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isVideoPath(String path) {
        String[] docExtensionList =
                new String[] {
                        "mpg", "mpeg", "mpe", "mp4", "avi", "3gp"
                };
        path = path.toLowerCase(Locale.ENGLISH);
        for (String ext : docExtensionList) {
            if (path.endsWith("." + ext)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAudioPath(String path) {
        String[] docExtensionList =
                new String[] {
                        "mp3", "wav"
                };
        path = path.toLowerCase(Locale.ENGLISH);
        for (String ext : docExtensionList) {
            if (path.endsWith("." + ext)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDocumentPath(String path) {
        String[] docExtensionList =
                new String[] {
                        "txt", "doc", "docx", "docm", "hwp", "log", "pdf",
                        "rtf", "ppt", "pptx", "key", "keynote", "odp", "otp",
                        "pez", "xls", "xlsm", "xlsx"
                };
        path = path.toLowerCase(Locale.ENGLISH);
        for (String ext : docExtensionList) {
            if (path.endsWith("." + ext)) {
                return true;
            }
        }
        return false;
    }

    public static String getDefaultDownloadPath(Context context) {

        File syncportDir = new File(Environment.getExternalStorageDirectory()
                , "Syncport");
        File syncportDownloadDir = new File(syncportDir, "download");
        syncportDownloadDir.mkdirs();
        return syncportDownloadDir.getAbsolutePath();
    }

    public static boolean isSyncportLogPath(String path) {
        File syncportDir = new File(Environment.getExternalStorageDirectory()
                , "Syncport");
        File logFile = new File(syncportDir, "syncport.log");
        if (path.equals(logFile.getAbsolutePath())) {
            return true;
        }
        return false;
    }
}
