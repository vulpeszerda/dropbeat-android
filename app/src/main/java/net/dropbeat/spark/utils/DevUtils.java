package net.dropbeat.spark.utils;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vulpes on 15. 2. 28..
 */
public class DevUtils {

    private static Map<String, Long> sTime = new HashMap<>();
    private static Map<String, Long> sAggregatedTime = new HashMap<>();

    public static void pushTime(String key) {
        sTime.put(key, (new Date()).getTime());
    }

    public static long popTime(String key) {
        Long time = sTime.remove(key);
        Long atime = sAggregatedTime.get(key);
        if (time != null) {
            long curr = new Date().getTime();
            long period = curr - time;
            if (atime == null) {
                atime = 0L;
            }
            atime += period;
            sAggregatedTime.put(key, atime);
            return period;
        }
        return 0;
    }

    public static long popAggregatedTime(String key) {
        Long atime = sAggregatedTime.get(key);
        if (atime != null) {
            Log.d("DevUtils", key + "_aggregated:" + atime);
        }
        return atime;
    }
}
