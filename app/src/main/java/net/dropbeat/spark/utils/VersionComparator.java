package net.dropbeat.spark.utils;

import java.util.regex.Pattern;

/**
 * Created by vulpes on 15. 3. 17..
 */
public class VersionComparator {
    public static int compare(String v1, String v2) {
        String s1 = normalisedVersion(v1);
        String s2 = normalisedVersion(v2);
        return s1.compareTo(s2);
    }

    private static String normalisedVersion(String version) {
        return normalisedVersion(version, ".", 4);
    }

    private static String normalisedVersion(String version, String sep,
                                            int maxWidth) {
        String[] split = Pattern.compile(sep, Pattern.LITERAL).split(
                version);
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            sb.append(String.format("%" + maxWidth + 's', s));
        }
        return sb.toString();
    }
}
