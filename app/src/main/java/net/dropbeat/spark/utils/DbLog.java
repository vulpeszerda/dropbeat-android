package net.dropbeat.spark.utils;

import android.os.Environment;

import com.google.gson.annotations.JsonAdapter;

import net.dropbeat.spark.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vulpes on 15. 3. 14..
 */
public class DbLog {

    private static List<PlaybackAction> sPlaybackActions = new ArrayList<>();

    public static class PlaybackAction {
        String action;
        int when;

        public PlaybackAction(String action, int when) {
            this.action = action;
            this.when = when;
        }
    }

    public static JSONArray serializePlaybackActions() {
        JSONArray data = new JSONArray();
        for (PlaybackAction action : sPlaybackActions) {
            JSONObject actionObj = new JSONObject();
            try {
                actionObj.put("type", action.action);
                if (action.when > -1) {
                    actionObj.put("ts", action.when);
                }
                data.put(actionObj);
            } catch(JSONException e) {
            }
        }
        return data;
    }

    public static void clearPlaybackActions() {
        sPlaybackActions.clear();
    }

    public static List<PlaybackAction> getPlaybackActions() {
        return new ArrayList(sPlaybackActions);
    }

    public static void pushPlayback(String action, int when) {
        synchronized (sPlaybackActions) {
            sPlaybackActions.add(new PlaybackAction(action, when));
        }
    }
}
