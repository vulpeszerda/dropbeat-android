package net.dropbeat.spark.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.UUID;


/**
 * Getting UUID from ANDROID_ID or Random
 *
 * @see <a href="http://stackoverflow.com/questions/5088474/how-can-i-get-the-uuid-of-my-android-phone-in-an-application#answer-5628136">reference</a>
 *
 */
public class DeviceUuidFactory {
    protected static final String PREFS_FILE = "device_id.xml";
    protected static final String PREFS_DEVICE_ID = "device_id";

    protected static UUID uuid;

    public DeviceUuidFactory(Context context) {

        if (uuid == null) {
            synchronized (DeviceUuidFactory.class) {
                if (uuid == null) {
                    final SharedPreferences prefs = context
                            .getSharedPreferences(PREFS_FILE, 0);
                    String id = prefs.getString(PREFS_DEVICE_ID, null);
                    if (FileUtils.isSdcardAvailable()) {
                        String cachedId = null;
                        try {
                            cachedId = readDeviceIdFromFile();
                        } catch (IOException e) {
                            // do nothing
                        }
                        if (cachedId == null) {
                            if (id != null) {
                                try {
                                    writeDeviceIdToFile(id);
                                } catch (IOException e) {
                                    // do nothing
                                }
                            }
                        } else {
                            if (id == null) {
                                id = cachedId;
                            } else if (!id.equals(cachedId)) {
                                try {
                                    writeDeviceIdToFile(id);
                                } catch (IOException e) {
                                    // do nothing
                                }
                            }
                        }
                    }

                    if (id != null) {
                        // Use the ids previously computed and stored in the
                        // prefs file
                        uuid = UUID.fromString(id);

                    } else {

                        final String androidId = Secure
                                .getString(context.getContentResolver(),
                                        Secure.ANDROID_ID);

                        // Use the Android ID unless it's broken, in which case
                        // fallback on deviceId,
                        // unless it's not available, then fallback on a random
                        // number which we store
                        // to a prefs file
                        try {
                            if (!"9774d56d682e549c".equals(androidId)) {
                                uuid = UUID.nameUUIDFromBytes(androidId
                                        .getBytes("utf8"));
                            } else {
                                final String deviceId = ((TelephonyManager) context
                                        .getSystemService(Context.TELEPHONY_SERVICE))
                                        .getDeviceId();
                                uuid = deviceId != null ? UUID
                                        .nameUUIDFromBytes(deviceId
                                                .getBytes("utf8")) : UUID
                                        .randomUUID();
                            }
                        } catch (UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }

                        // Write the value out to the prefs file
                        final String deviceId = uuid.toString();
                        prefs.edit()
                                .putString(PREFS_DEVICE_ID, deviceId)
                                .commit();

                        try {
                            writeDeviceIdToFile(deviceId);
                        } catch (IOException e) {
                            // do nothing
                        }

                    }

                }
            }
        }

    }

    private String readDeviceIdFromFile() throws IOException{
        File syncportDir = new File(Environment
                .getExternalStorageDirectory(), "Syncport");
        if (!syncportDir.exists()) {
            syncportDir.mkdirs();
        }
        File cacheFile = new File(syncportDir, ".deviceId");
        if (cacheFile.exists()) {
            FileInputStream fis = new FileInputStream(cacheFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line = reader.readLine();
            fis.close();
            return line;
        }
        return null;
    }

    private void writeDeviceIdToFile(String deviceId) throws IOException{
        File syncportDir = new File(Environment
                .getExternalStorageDirectory(), "Syncport");
        if (!syncportDir.exists()) {
            syncportDir.mkdirs();
        }
        File cacheFile = new File(syncportDir, ".deviceId");
        if (cacheFile.exists()) {
            cacheFile.delete();
        }
        FileOutputStream fos = new FileOutputStream(cacheFile);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
        writer.write(deviceId);
        writer.flush();
        writer.close();
        fos.close();
    }

    /**
     * Returns a unique UUID for the current android device. As with all UUIDs,
     * this unique ID is "very highly likely" to be unique across all Android
     * devices. Much more so than ANDROID_ID is.
     *
     * The UUID is generated by using ANDROID_ID as the base key if appropriate,
     * falling back on TelephonyManager.getDeviceID() if ANDROID_ID is known to
     * be incorrect, and finally falling back on a random UUID that's persisted
     * to SharedPreferences if getDeviceID() does not return a usable value.
     *
     * In some rare circumstances, this ID may change. In particular, if the
     * device is factory reset a new device ID may be generated. In addition, if
     * a user upgrades their phone from certain buggy implementations of Android
     * 2.2 to a newer, non-buggy version of Android, the device ID may change.
     * Or, if a user uninstalls your app on a device that has neither a proper
     * Android ID nor a Device ID, this ID may change on reinstallation.
     *
     * Note that if the code falls back on using TelephonyManager.getDeviceId(),
     * the resulting ID will NOT change after a factory reset. Something to be
     * aware of.
     *
     * Works around a bug in Android 2.2 for many devices when using ANDROID_ID
     * directly.
     *
     * @see http://code.google.com/p/android/issues/detail?id=10603
     *
     * @return a UUID that may be used to uniquely identify your device for most
     *         purposes.
     */
    public UUID getDeviceUuid() {
        return uuid;
    }
}
