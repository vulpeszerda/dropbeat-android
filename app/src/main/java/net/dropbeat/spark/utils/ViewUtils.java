package net.dropbeat.spark.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by vulpes on 15. 5. 2..
 */
public class ViewUtils {

    public static int dpToPx(Context context, int dp) {
        if (context == null) {
            return -1;
        }
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        int px = Math.round(dp *
                (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(Context context, int px) {
        if (context == null) {
            return -1;
        }

        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }
}
