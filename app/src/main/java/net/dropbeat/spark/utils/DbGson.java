package net.dropbeat.spark.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DbGson {
    private static final String REMOTE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Gson getInstance() {
        return getInstance(REMOTE_DATE_FORMAT);
    }

    public static Gson getInstance(String dateFormat) {
        return new GsonBuilder()
                .setDateFormat(dateFormat)
                .setFieldNamingPolicy(
                        FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    }
}
