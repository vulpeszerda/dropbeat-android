package net.dropbeat.spark;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;

import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;

import org.json.JSONObject;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 14..
 */
@ContentView(R.layout.activity_feedback)
public class FeedbackActivity extends BaseActivity
        implements View.OnClickListener{

    public static Intent createIntent(Context context) {
        return new Intent(context, FeedbackActivity.class);
    }

    @InjectView(R.id.input_email)
    private EditText mEmailInput;

    @InjectView(R.id.input_contents)
    private EditText mContentsInput;

    @InjectView(R.id.submit_button)
    private Button mSubmitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            ActionBar actionbar = getSupportActionBar();
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }

        DbAccount account;
        if ((account = DbAccount.getUser(this)) != null) {
            mEmailInput.setText(account.getEmail());
            mContentsInput.requestFocus();
        } else {
            mEmailInput.requestFocus();
        }
        mSubmitBtn.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() != R.id.submit_button) {
            return;
        }

        boolean success = true;
        CharSequence email = mEmailInput.getText();
        CharSequence contents = mContentsInput.getText();
        mEmailInput.setError(null);
        mContentsInput.setError(null);

        if (TextUtils.isEmpty(email)) {
            mEmailInput.setError(getString(R.string.error_value_required));
            success = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailInput.setError(getString(R.string.error_format__email));
            success = false;
        }

        if (TextUtils.isEmpty(contents)) {
            mContentsInput.setError(getString(R.string.error_value_required));
            success = false;
        }

        if (!success) {
            return;
        }
        doSubmit(email.toString(), contents.toString());
    }

    private void doSubmit(final String email, final String contents) {
        ProgressDialogFragment.show(this, "submit", R.string.desc_submiting);
        Requests.sendFeedback(this, email, contents, new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                if (!response.optBoolean("success")) {
                    showFailureDialog(null, null);
                    return;
                }
                showSuccessDialog();
            }
        }, new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(FeedbackActivity.this, "submit");
                showFailureDialog(msg, new Runnable() {
                    @Override
                    public void run() {
                        doSubmit(email, contents);
                    }
                });
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        });
    }

    private void showSuccessDialog() {
        new AlertDialog.Builder(FeedbackActivity.this)
                .setTitle("Feedback submitted")
                .setMessage("Thank you for your feedback :D")
                .setPositiveButton(R.string.button_confirm,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }
                        }).show();
    }

    private void showFailureDialog(String msg, final Runnable retryRunnable) {
        String errorMsg = "Failed to submit feedback.";
        if (!TextUtils.isEmpty(msg)) {
            errorMsg += "\n" + msg;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this)
                .setTitle("Failed to submit")
                .setMessage(errorMsg)
                .setNegativeButton(R.string.button_cancel, null);
        if (retryRunnable != null) {
            builder.setPositiveButton(R.string.button_retry, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    retryRunnable.run();
                }
            });
        }

        builder.show();
    }
}
