package net.dropbeat.spark;

import java.util.ArrayList;
import java.util.List;


public class NavigationItem {

    public static List<NavigationItem> getNavigationItems() {
        return new ItemBuilder()
                .addItem(R.drawable.ic_home, R.string.drawer_menu__feed,
                        NavigationDrawerFragment.MenuType.FEED)
                .addItem(R.drawable.ic_channel, R.string.drawer_menu__channel,
                        NavigationDrawerFragment.MenuType.CHANNEL)
                .addItem(R.drawable.ic_seach,
                        R.string.drawer_menu__search,
                        NavigationDrawerFragment.MenuType.SEARCH)
                .addItem(R.drawable.ic_cogwheel,
                        R.string.drawer_menu__preference,
                        NavigationDrawerFragment.MenuType.SETTINGS)
            .build();
    }

    private Integer mImageResId;
    private Integer mTextResId;
    private Integer mMenuType;
    private boolean mIsSection;
    private boolean mIsNonSelectable;

    private NavigationItem(Integer imageResId, Integer textResId, int type) {
        this(imageResId, textResId, type, false);
    }
    private NavigationItem(Integer imageResId, Integer textResId, int type, boolean isNonSelectable) {
        mImageResId = imageResId;
        mTextResId = textResId;
        mMenuType = type;
        mIsSection = false;
        mIsNonSelectable = isNonSelectable;
    }

    private NavigationItem() {
        mIsSection = true;
    }

    public int getImageResId() {
        return mImageResId;
    }

    public int getTextResId() {
        return mTextResId;
    }

    public int getMenuType() {
        return mMenuType;
    }

    public boolean isSection() {
        return mIsSection;
    }

    public boolean isIsSelectable() {
        return !mIsNonSelectable;
    }

    public static class ItemBuilder {

        private List<NavigationItem> mItems;

        public ItemBuilder() {
            mItems = new ArrayList<>();
        }

        public List<NavigationItem> build() {
            // TODO check a menu added twice or more, then throw error
            return mItems;
        }

        public ItemBuilder addItem(int imgResId, int textResId, int type) {
            mItems.add(new NavigationItem(imgResId, textResId, type));
            return this;
        }

        public ItemBuilder addItem(int imgResId, int textResId, int type,
                                   boolean isSelectable) {
            mItems.add(new NavigationItem(imgResId, textResId, type,
                    isSelectable));
            return this;
        }

        public ItemBuilder addSection() {
            mItems.add(new NavigationItem());
            return this;
        }
    }
}
