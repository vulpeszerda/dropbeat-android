package net.dropbeat.spark;

import android.view.View;

import net.dropbeat.spark.model.Track;

/**
 * Created by vulpes on 15. 6. 12..
 */
public interface TrackClickListener {
    void onClick(View view, Track track, int position);
}
