package net.dropbeat.spark.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daftshady.superandroidkit.authentication.AbstractAccount;

import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.DbAccount;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parkilsu on 15. 4. 28..
 */
public class Adapter implements Response.ErrorListener {

    private static final int REQUEST_TIMEOUT_MS = 10000;
    private DbAccount mAccount;
    private boolean mRenew = true;
    private boolean mNeedAuth = false;

    private String mToken;
    private boolean mCache;

    // Request params
    private int mRequestMethod;
    private String mRequestPath;
    private String mUriQuery;
    private JSONObject mRequestParams;
    private Response.Listener<JSONObject> mResponseListener;
    private Response.ErrorListener mErrorListener;

    private RequestQueue mQueue;
    private Context mContext;

    public Adapter(Context context) {
        mAccount = DbAccount.getUser(context);
        mRequestMethod = Request.Method.GET;
        mRequestParams = null;
        mCache = true;
        mNeedAuth = true;
        mContext = context;
        mQueue = Rqueue.instance(context);
    }

    public Adapter setToken(String token) {
        mToken = token;
        return this;
    }

    public Adapter setNeedAuth(boolean needAuth) {
        mNeedAuth = needAuth;
        return this;
    }

    public Adapter setMethod(int requestMethod) {
        mRequestMethod = requestMethod;
        return this;
    }

    public Adapter setPath(String requestPath) {
        mRequestPath = requestPath;
        return this;
    }

    public Adapter setParams(JSONObject requestParams) {
        mRequestParams = requestParams;
        return this;
    }

    public Adapter setQuery(String k, String v) {
        if (mUriQuery == null) {
            mUriQuery = "?" + k + "=" + URLEncoder.encode(v);
        } else {
            mUriQuery += "&" + k + "=" + URLEncoder.encode(v);
        }
        return this;
    }

    public Adapter setShouldCache(boolean shouldCache) {
        mCache = shouldCache;
        return this;
    }

    public Adapter setResponseListener(
            Response.Listener<JSONObject> responseListener) {
        mResponseListener = responseListener;
        return this;
    }

    public Adapter setErrorListener(Response.ErrorListener errorListener) {
        mErrorListener = errorListener;
        return this;
    }

    public void sendRequest() {
        if (mNeedAuth && mToken == null && mAccount != null) {
            requestAuthToken();
            return;
        }

        JsonObjectRequest request = buildRequest(mToken);
        addToQueue(request);
    }

    private void addToQueue(Request req) {
        req.setShouldCache(mCache);
        req.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        mQueue.add(req);
    }

    private void requestAuthToken() {
        mAccount.getAuthToken(new AbstractAccount.OnGetAuthTokenListener() {
            @Override
            public void onGetAuthToken(String token) {
                JsonObjectRequest request = buildRequest(token);
                addToQueue(request);
            }

            @Override
            public void onFailure(Exception e) {
                mErrorListener.onErrorResponse(new VolleyError(
                        "failed to get auth token", e));
            }
        });
    }

    private JsonObjectRequest buildRequest(final String token) {
        if (mUriQuery != null) {
            mRequestPath += mUriQuery;
        }
        JsonObjectRequest request = new JsonObjectRequest(mRequestMethod,
                mRequestPath, mRequestParams, mResponseListener, this) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                if (token != null) {
                    headers.put("Cookie", String.format("sessionid=%s;", token));
                }
                headers.put("User-agent", "Android spark/net.dropbeat.prod " +
                        "Dbt/" + mContext.getString(R.string.app_version));
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    if (response.data.length == 0) {
                        byte[] responseData = "{}".getBytes("UTF8");
                        response = new NetworkResponse(response.statusCode,
                                responseData, response.headers,
                                response.notModified);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        return request;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        NetworkResponse response = error.networkResponse;
        boolean needAuth = response != null
                && response.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED;
        needAuth = needAuth
                || (error.getMessage() != null && error
                .getMessage()
                .equals("java.io.IOException: No authentication challenges found"));
        if (needAuth && mRenew && mAccount != null) {
            mRenew = false;
            mAccount.invalidateAuthToken();
            sendRequest();
            return;
        }
        mErrorListener.onErrorResponse(error);
    }
}

