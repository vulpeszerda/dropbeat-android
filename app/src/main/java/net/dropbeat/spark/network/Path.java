package net.dropbeat.spark.network;

/**
 * Created by parkilsu on 15. 4. 28..
 */
public class Path {
    public static final String apiPrefix = "api/v1/";
//     DEBUG
    public static final String apiHost = "http://spark.coroutine.io/";
//    public static final String streamResolveHost = "http://54.250.195.24:8028/";
//    public static final String streamResolveHost = "http://14.63.224.95:19001/";
//    public static final String coreHost = "http://core.coroutine.io/";

//     PROD
//    public static final String apiHost = "http://dropbeat.net/";
    public static final String streamResolveHost = "http://resolve.dropbeat.net/";
    public static final String coreHost = "http://core.dropbeat.net/";


    // api.user
    public static final String user = apiHost + apiPrefix + "user/";
    public static final String userSignIn = user + "signin_fb_android/";
    public static final String userSignInWithEmail = user + "email_signin/";
    public static final String userSignUpWithEmail = user + "email_signup/";
    public static final String userSelf = user + "self/";
    public static final String userSignout = user + "signout/";
    public static final String userUnlock = user + "unlock/";
    public static final String userChangeEmail = user + "change_email/";
    public static final String userFollow = user + "follow/";
    public static final String userUnfollow = user + "unfollow/";
    public static final String userFollowing = user + "following/";
    public static final String userLike = user + "like/";

    // api.feed
    public static final String feed = apiHost + apiPrefix + "feed/";
    public static final String feedChannel =  feed + "channel/";

    // api.playlist
    public static final String playlist = apiHost + apiPrefix + "playlist/";
    public static final String playlistAll = playlist + "all/";
    public static final String playlistList = playlist + "list/";
    public static final String playlistSet = playlist + "set/";
    public static final String playlistInitial = playlist + "initial/";
    public static final String playlistShared = playlist + "shared/";
    public static final String playlistImport = playlist + "import/";
    public static final String playlistDel = playlist + "del/";

    // api.track
    public static final String track = apiHost + apiPrefix + "track/";
    public static final String trackShared = track + "shared/";
    public static final String trackLike = track + "like/";
    public static final String trackDislike = track + "dislike/";

    // api.bookmark
    public static final String bookmark = apiHost + apiPrefix + "channelbookmark/";

    // api.log
    public static final String log = apiHost + apiPrefix + "log/";
    public static final String logSearch = log + "search/";
    public static final String logResolve = log + "resolve/";
    public static final String logTrackAdd = log + "trackadd/";
    public static final String logPlay = log + "play/";
    public static final String logPlayFailure = log + "playfailure/";
    public static final String logPlaybackDetail = log + "playback_detail/";

    // api.meta
    public static final String meta = apiHost + apiPrefix + "meta/";
    public static final String metaVersion = meta + "version/";
    public static final String metaKey = meta + "key/";

    // api.artist
    public static final String artist = apiHost + apiPrefix + "artist/";
    public static final String artistFollow = artist + "follow/";
    public static final String artistUnfollow = artist + "unfollow/";
    public static final String artistFollowing = artist + "following/";


    // api.stream
    public static final String streamFollowing = apiHost + apiPrefix + "stream/following/";

    // api.feedback
    public static final String feedback = apiHost + apiPrefix + "async/feedback/";

    // usertrack
    public static final String userTrack = apiHost + apiPrefix + "usertrack/";
    public static final String userTrackNewest = userTrack + "newest/";
    public static final String userTrackLike = userTrack + "like/";


    // resolve
    public static final String resourceResolve = apiHost + apiPrefix + "resolve/";

    public static final String corePrefix = "api/";

    // core.search
    public static final String search = coreHost + corePrefix + "v1/search/";
    public static final String searchRelated = search + "related/";
    public static final String searchOther = search + "other/";
    public static final String searchLiveset = search + "liveset/";
    public static final String searchArtist = search + "artist/";

    // core.podcast
    public static final String podcast = coreHost + corePrefix + "podcast/";

    // core.resolve
    public static final String resolve = coreHost + corePrefix + "resolve/";

    // core.related
    public static final String related = coreHost + corePrefix + "related/";

    // core.live
    public static final String live = coreHost + corePrefix + "live/";
    public static final String liveTracklist = live + "tracklist/";

    // core.trending
    public static final String trending = coreHost + corePrefix + "trending/";
    public static final String trendingChart = trending + "bpchart/";
    public static final String trendingHotRelease = trending + "hot_release/";
    public static final String trendingFeaturedPlaylist = trending + "featured_playlist/";

    // core.play
    public static final String play = coreHost + corePrefix + "play/";

    // core.trackadd
    public static final String trackadd = coreHost + corePrefix + "trackadd/";

    // core.drop
    public static final String drop = coreHost + corePrefix + "drop/";

    // core.genre
    public static final String genre = coreHost + corePrefix + "genre/";

    // core.channel
    public static final String channel = coreHost + corePrefix + "channel/";
    public static final String channelList = channel + "list/";
    public static final String channelDetail = channel + "detail/";
    public static final String channelGproxy = coreHost + corePrefix +
            "v1/channel/gproxy/";

    // core.stream
    public static final String stream = coreHost + corePrefix + "stream/";
    public static final String streamNewRelease = stream + "new/";
    public static final String streamTrending = stream + "trending/";

    // core.artist
    public static final String artistFilter = coreHost + corePrefix +
            "artist/filter/";

    // resolve
    public static final String streamResolve = streamResolveHost + "resolve/";
    public static final String decryptSignature = streamResolveHost + "decrypt_sig/";

    // google youtube api
    // XXX: we will use proxy from 0.0.10
//    public static final String googleApi = "https://www.googleapis.com/";
//    public static final String googleApiPlaylistItem =
//            googleApi + "youtube/v3/playlistItems/";


    // geolocation
    public static final String geolocation = "http://geo.ironbricks.com/json/";
    public static final String googleGeolocation = "http://maps.google.com/maps/api/geocode/json";

    // soundcloud images
    public static final String soundcloudImage = coreHost + corePrefix + "image/soundcloud/";
}
