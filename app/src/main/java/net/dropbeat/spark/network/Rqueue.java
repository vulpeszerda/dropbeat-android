package net.dropbeat.spark.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by parkilsu on 15. 4. 28..
 */
public class Rqueue {
    private static RequestQueue q;

    public static RequestQueue instance(Context context) {
        if (q == null) {
            q = Volley.newRequestQueue(context);
        }
        return q;
    }
}
