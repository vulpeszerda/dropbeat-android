package net.dropbeat.spark.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.model.Like;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.utils.DbGson;
import net.dropbeat.spark.utils.DbLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import main.java.com.mindscapehq.android.raygun4android.RaygunMessageBuilder;
import main.java.com.mindscapehq.android.raygun4android.messages.RaygunMessage;

/**
 * Created by parkilsu on 15. 4. 28..
 */
public class Requests {
    /**
     * Handles all HTTP method except GET.
     * @param context
     * @param params
     * @param url
     * @param listener
     * @param errorListener
     */
    private static void sendRequest(
            Context context, int method, String url, JSONObject params,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        sendRequest(context, method, url, true, params, listener, errorListener);
    }

    private static void sendRequest(
            Context context, int method, String url, boolean authRequired,
            JSONObject params, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {

        if (listener == null) {
            listener = EMPTY_RESPONSE_LISTENER;
        }
        if (errorListener == null) {
            errorListener = EMPTY_ERROR_RESPONSE_LISTENER;
        }

        Adapter adapter = new Adapter(context);
        adapter.setPath(url)
                .setParams(params)
                .setMethod(method)
                .setNeedAuth(authRequired)
                .setResponseListener(listener)
                .setErrorListener(errorListener);
        adapter.sendRequest();
    }

    private static void sendGet(
            Context context, String url, boolean authRequired,
            HashMap<String, String> params, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {

        if (listener == null) {
            listener = EMPTY_RESPONSE_LISTENER;
        }
        if (errorListener == null) {
            errorListener = EMPTY_ERROR_RESPONSE_LISTENER;
        }

        Adapter adapter = new Adapter(context);
        adapter.setPath(url)
                .setNeedAuth(authRequired)
                .setResponseListener(listener)
                .setErrorListener(errorListener);
        if (params != null) {
            for (String key : params.keySet()) {
                adapter.setQuery(key, params.get(key));
            }
        }
        adapter.sendRequest();
    }

    public static void getGeolocation(Context context,
                                      Response.Listener<JSONObject> listener,
                                      Response.ErrorListener errorListener) {
        sendGet(context, Path.geolocation, false, null, listener, errorListener);
    }

    public static void getCountryInfo(Context context,
                                      String lat, String lng,
                                      Response.Listener<JSONObject> listener,
                                      Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("latlng", lat + "," + lng);
        params.put("sensor", "false");
        params.put("language", "en");

        sendGet(context, Path.googleGeolocation, false, params,
                listener, errorListener);
    }

    /**
     * Used for self, signout, unlock
     * @param context
     * @param url
     * @param listener
     * @param errorListener
     */
    public static void userAction(
            Context context, String url, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        sendGet(context, url, true, null, listener, errorListener);
    }

    public static void userSignin(
            Context context, JSONObject params, Response.Listener<JSONObject>
            listener, Response.ErrorListener errorListener) {

        if (listener == null) {
            listener = EMPTY_RESPONSE_LISTENER;
        }
        if (errorListener == null) {
            errorListener = EMPTY_ERROR_RESPONSE_LISTENER;
        }

        Adapter adapter = new Adapter(context);
        adapter.setPath(Path.userSignIn)
                .setParams(params)
                .setMethod(Request.Method.POST)
                .setResponseListener(listener)
                .setNeedAuth(false)
                .setErrorListener(errorListener);
        adapter.sendRequest();
    }

    public static void userSigninWithEmail(
            Context context, JSONObject params, Response.Listener<JSONObject>
            listener, Response.ErrorListener errorListener) {

        Adapter adapter = new Adapter(context);
        adapter.setPath(Path.userSignInWithEmail)
                .setParams(params)
                .setMethod(Request.Method.POST)
                .setResponseListener(listener)
                .setNeedAuth(false)
                .setErrorListener(errorListener);
        adapter.sendRequest();
    }

    public static void userSignupWithEmail(
            Context context, JSONObject params, Response.Listener<JSONObject>
            listener, Response.ErrorListener errorListener) {

        Adapter adapter = new Adapter(context);
        adapter.setPath(Path.userSignUpWithEmail)
                .setParams(params)
                .setMethod(Request.Method.POST)
                .setResponseListener(listener)
                .setNeedAuth(false)
                .setErrorListener(errorListener);
        adapter.sendRequest();
    }

    public static void userChangeEmail(
            Context context, String email, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("email", email);
            sendRequest(
                    context, Request.Method.POST, Path.userChangeEmail,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void getPlaylist(
            Context context, String id, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        sendGet(context, Path.playlist, true, params, listener, errorListener);
    }

    public static void createPlaylist(
            Context context, String name, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("name", name);
            sendRequest(
                    context, Request.Method.POST, Path.playlist,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void deletePlaylist(
            Context context, String id, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("id", id);

            sendRequest(
                    context, Request.Method.POST, Path.playlistDel,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

     public static void changePlaylistName(
            Context context, String id, String name,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("id", id).put("name", name);
            sendRequest(
                    context, Request.Method.PUT, Path.playlist,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
     }

    public static void fetchPlaylistList(Context context,
                                         Response.Listener<JSONObject> listener,
                                         Response.ErrorListener errorListener) {
        sendGet(context, Path.playlistList, true, null, listener, errorListener);
    }

    public static void setPlaylist(
            Context context, String playlistId, JSONArray data,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().
                    put("playlist_id", playlistId).put("data", data);
            sendRequest(
                    context, Request.Method.POST, Path.playlistSet,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void fetchInitialPlaylist(
            Context context, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        sendGet(context, Path.playlistInitial, false, null, listener, errorListener);
    }

    public static void getSharedPlaylist(
            Context context, String uid, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {

        if (listener == null) {
            listener = EMPTY_RESPONSE_LISTENER;
        }
        if (errorListener == null) {
            errorListener = EMPTY_ERROR_RESPONSE_LISTENER;
        }

        Adapter adapter = new Adapter(context);
        adapter.setPath(Path.playlistShared)
                .setNeedAuth(false)
                .setQuery("uid", uid)
                .setResponseListener(listener)
                .setErrorListener(errorListener);
        adapter.sendRequest();
    }

    public static void sharePlaylist(
            Context context, Playlist playlist,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject()
                    .put("name", playlist.getName())
                    .put("data", playlist.toJsonArray());
            sendRequest(
                    context, Request.Method.POST, Path.playlistShared,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void importPlaylist(
            Context context, String uid,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("uid", uid);
            sendRequest(
                    context, Request.Method.POST, Path.playlistImport,
                    params, listener, errorListener
            );
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void search(
            Context context, String query, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("q", query);
        params.put("snippet", "1");
        sendGet(context, Path.search, false, params, listener, errorListener);
    }

    public static void searchOther(Context context, String artistName,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("q", artistName);

        sendGet(context, Path.searchOther, false, params, listener,
                errorListener);
    }

    public static void searchLiveset(Context context, String artistName,
                                   Response.Listener<JSONObject> listener,
                                   Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("q", artistName);

        sendGet(context, Path.searchLiveset, false, params, listener, errorListener);
    }

    public static void searchPodcast(Context context, String artistName,
                                     Response.Listener<JSONObject> listener,
                                     Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("q", artistName);
        params.put("p", "-1");
        sendGet(context, Path.podcast, false, params, listener, errorListener);
    }

    public static void searchArtist(Context context, String keyword,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("q", keyword);
        sendGet(context, Path.searchArtist, false, params, listener,
                errorListener);
    }

    public static void getRelatedKeywords(
            Context context, String query, Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("q", query);
        sendGet(context, Path.searchRelated, false, params, listener, errorListener);
    }

    public static void getRelatedTrack(
            Context context, String trackUid, String trackTitle,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("src_uid", trackUid);
        params.put("title", trackTitle);
        params.put("limit", "10");
        sendGet(context, Path.searchRelated, false, params, listener, errorListener);
    }

    public static void streamResolve(
            Context context, String uid,
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("uid", uid);
        sendGet(context, Path.streamResolve, false, params, listener, errorListener);
    }

    public static void fetchFeed(Context context,
                                 Response.Listener<JSONObject> listener,
                                 Response.ErrorListener errorListener) {

        DbAccount account = DbAccount.getUser(context);
        sendGet(context, Path.feed, account != null, null, listener,
                errorListener);
    }

    public static void logSearch(Context context, String keyword) {
        HashMap<String, String> params = new HashMap<>();
        params.put("q", keyword);
        params.put("device_type", "android");
        sendGet(context, Path.logSearch, true, params,
                EMPTY_RESPONSE_LISTENER,
                EMPTY_ERROR_RESPONSE_LISTENER);
    }

    public static void logTrackAdd(Context context, String title) {
        HashMap<String, String> params = new HashMap<>();
        params.put("t", title);
        params.put("device_type", "android");
        sendGet(context, Path.logTrackAdd, true, params,
                EMPTY_RESPONSE_LISTENER,
                EMPTY_ERROR_RESPONSE_LISTENER);
    }

    public static void logPlay(Context context, String title, String uid) {
        HashMap<String, String> params = new HashMap<>();
        params.put("t", title);
        params.put("uid", uid);

        params.put("device_type", "android");
        sendGet(context, Path.logPlay, DbAccount.getUser(context) != null,
                params, EMPTY_RESPONSE_LISTENER, EMPTY_ERROR_RESPONSE_LISTENER);
    }

    public static void logPlaybackDetail(Context context, String uid,
                                         List<DbLog.PlaybackAction> actions) {

        JSONArray data = DbLog.serializePlaybackActions();
        if (data == null || data.length() == 0) {
            return;
        }
        try {
            JSONObject params = new JSONObject();
            params.put("track_id", uid);
            params.put("data", data);

            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            String geolocation = prefs.getString("geolocation", null);
            if (geolocation == null) {
                return;
            }
            JSONObject locationObj = new JSONObject(geolocation);
            params.put("location", locationObj);

            sendRequest(context, Request.Method.POST, Path.logPlaybackDetail,
                    DbAccount.getUser(context) != null, params,
                    EMPTY_RESPONSE_LISTENER,
                    EMPTY_ERROR_RESPONSE_LISTENER);
        } catch (JSONException e) {

        }
    }

    public static void logPlayFailure(Context context, String tag,
                                      String title, String uid,
                                      List<String> urls,
                                      String stacktrace) {
        try {
            String email = "";
            DbAccount account = DbAccount.getUser(context);
            if (account != null) {
                email = account.getEmail();
            }
            JSONObject params = new JSONObject();
            params.put("email", email);
            params.put("title", title);
            params.put("uid", uid);
            JSONArray urlsArray = new JSONArray();
            if (urls != null) {
                for (String url : urls) {
                    urlsArray.put(url);
                }
            }
            params.put("stream_urls", urlsArray);
            JSONObject deviceInfo = getDeviceInfo(context);
            deviceInfo.put("tag", tag);
            if (stacktrace != null) {
                deviceInfo.put("stacktrace", stacktrace);
            }
            params.put("device_info", deviceInfo);
            sendRequest(context, Request.Method.POST,
                    Path.logPlayFailure, false, params,
                    EMPTY_RESPONSE_LISTENER, EMPTY_ERROR_RESPONSE_LISTENER);
        } catch (JSONException e) {
            // not reach
            e.printStackTrace();
        }
    }

    public static void getClientKey(Context context,
                                        Response.Listener<JSONObject> listener,
                                        Response.ErrorListener errorListener) {
        sendGet(context, Path.metaKey, false, null, listener, errorListener);
    }

    public static void getClientVersion(Context context,
                        Response.Listener<JSONObject> listener,
                        Response.ErrorListener errorListener) {
        sendGet(context, Path.metaVersion, false, null, listener, errorListener);
    }

    private static final Response.Listener<JSONObject> EMPTY_RESPONSE_LISTENER = new
            Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
        }
    };


    private static final Response.ErrorListener EMPTY_ERROR_RESPONSE_LISTENER
            = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    private static final JSONObject getDeviceInfo(Context context) throws JSONException{
        String version;
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "Not provided";
        }

        RaygunMessage msg =  RaygunMessageBuilder.New()
                .SetEnvironmentDetails(context)
                .SetMachineName(Build.MODEL)
                .SetClientDetails()
                .SetVersion(version)
                .SetNetworkInfo(context)
                .Build();
        Gson gson = DbGson.getInstance();

        JSONObject raygunMsg = new JSONObject(gson.toJson(msg));
        JSONObject detailObj = raygunMsg.getJSONObject("details");
        JSONObject envObj = detailObj.getJSONObject("environment");
        JSONObject requestObj = detailObj.getJSONObject("request");
        JSONObject info = new JSONObject();
        info.put("version", detailObj.getString("version"));
        info.put("network", requestObj.getString("network_connectivity_state"));
        info.put("machine_name", detailObj.getString("machine_name"));
        info.put("brand", envObj.getString("brand"));
        info.put("device_code", envObj.getString("device_code"));
        info.put("device_name", envObj.getString("device_name"));
        info.put("locale", envObj.getString("locale"));
        info.put("os_version", envObj.getString("o_s_version"));
        info.put("os_version", envObj.getString("o_s_version"));

        return info;
    }

    public static void getChartList(Context context, String genre,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        if (genre != null) {
            params.put("type", genre);
        }
        sendGet(context, Path.trendingChart, false, params, listener,
                errorListener);
    }

    public static void getFeaturedPlaylist(Context context, String index,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        if (index != null) {
            params.put("i", index);
        }
        sendGet(context, Path.trendingFeaturedPlaylist, false, params, listener,
                errorListener);
    }

    public static void getHotRelease(Context context,
                                           Response.Listener<JSONObject> listener,
                                           Response.ErrorListener errorListener) {

        sendGet(context, Path.trendingHotRelease, false, null, listener,
                errorListener);
    }

    public static void getChannelList(Context context, String genre,
                                      Response.Listener<JSONObject> listener,
                                      Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("genre", genre);
        sendGet(context, Path.channelList, false, params, listener,
                errorListener);
    }

    public static void getChannelDetail(Context context, String uid,
                                      Response.Listener<JSONObject> listener,
                                      Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("uid", uid);
        sendGet(context, Path.channelDetail, false, params, listener,
                errorListener);
    }

    public static void getBookmarks(Context context,
                                        Response.Listener<JSONObject> listener,
                                        Response.ErrorListener errorListener) {

        sendGet(context, Path.bookmark, true, null, listener,
                errorListener);
    }

    public static void putBookmarks(Context context, Set<String> ids,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        try {
            JSONArray array = new JSONArray();
            for (String id : ids) {
                array.put(id);
            }
            JSONObject params = new JSONObject();
            params.put("data", array);
            sendRequest(context, Request.Method.POST, Path.bookmark, params,
                    listener, errorListener);
        } catch (JSONException e) {

        }
    }

    public static void getChannelPlaylistItem(Context context,
                                          String playlistId,
                                          String pageToken,
                                          Response.Listener<JSONObject> listener,
                                          Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("uid", playlistId);
        if (!TextUtils.isEmpty(pageToken)) {
            params.put("pageToken", pageToken);
        }
        sendGet(context, Path.channelGproxy, false, params, listener,
                errorListener);
    }

    public static void follow(Context context, String type, String id,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            params.put(type + "_id", id);
            sendRequest(context, Request.Method.POST, Path.artistFollow, params,
                    listener, errorListener);
        } catch (JSONException e) {
        }
    }

    public static void unfollow(Context context, String type, String id,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            params.put(type + "_id", id);
            sendRequest(context, Request.Method.POST, Path.artistUnfollow,
                    params, listener, errorListener);
        } catch (JSONException e) {
        }
    }

    public static void getFollowings(Context context,
                                  String userId,
                                  Response.Listener<JSONObject> listener,
                                  Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", userId);
        sendGet(context, Path.userFollowing, true, params, listener, errorListener);
    }

    public static void getlikes(Context context,
                                String userId,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", userId);
        sendGet(context, Path.userLike, true, params, listener, errorListener);
    }

    public static void like(Context context, Track track,
                            Response.Listener<JSONObject> listener,
                            Response.ErrorListener errorListener) {
        try {
            String path;
            JSONObject params = new JSONObject();
            if ("dropbeat".equals(track.getType())) {
                params.put("track_id", track.getId());
                path = Path.userTrackLike;
            } else {
                params.put("id", track.getId())
                        .put("title", track.getTitle())
                        .put("type", track.getType());
                path = Path.trackLike;
            }
            sendRequest(context, Request.Method.POST, path, params,
                    listener, errorListener);
        } catch (JSONException e) {
        }
    }

    public static void unlike(Context context, Like like,
                            Response.Listener<JSONObject> listener,
                            Response.ErrorListener errorListener) {
        try {
            String path;
            JSONObject params = new JSONObject();

            if ("user_track".equals(like.getType())) {
                params.put("like_id", like.getId());
                path = Path.userTrackLike;
                sendRequest(context, Request.Method.DELETE, path, params,
                        listener, errorListener);
            } else {
                params.put("id", like.getId());
                path = Path.trackDislike;
                sendRequest(context, Request.Method.POST, path, params,
                        listener, errorListener);
            }
        } catch (JSONException e) {
        }
    }

    public static void shareTrack(Context context, Track track,
                                     Response.Listener<JSONObject> listener,
                                     Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            params.put("track_name", track.getTitle());
            params.put("ref", track.getId());
            sendRequest(context, Request.Method.POST, Path.trackShared, params,
                    listener, errorListener);
        } catch (JSONException e) {
        }
    }

    public static void getSharedTrack(Context context, String uid,
                                  Response.Listener<JSONObject> listener,
                                  Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("uid", uid);
        sendGet(context, Path.trackShared, false, params, listener, errorListener);
    }

    public static void sendFeedback(Context context, String email,
                                    String contents,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            params.put("sender", email);
            params.put("content", contents);
            sendRequest(context, Request.Method.POST, Path.feedback, params,
                    listener, errorListener);
        } catch(JSONException e) {

        }
    }

    public static void artistFilter(Context context, List<String> names,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            for (String name : names) {
                array.put(name);
            }
            params.put("q", array);
            sendRequest(context, Request.Method.POST, Path.artistFilter, false,
                    params, listener, errorListener);
        } catch (JSONException e) {
        }
    }

    public static void getFeedGenre(Context context,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
        sendGet(context, Path.genre, false, null, listener, errorListener);
    }

    public static void getStreamNewUpload(Context context, String order, int pageIdx,
                                     Response.Listener<JSONObject> listener,
                                     Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("order", order);
        if (!order.equals("popular")) {
            params.put("p", String.valueOf(pageIdx));
        }
        sendGet(context, Path.userTrackNewest, false, params, listener,
                errorListener);
    }

    public static void getStreamNewRelease(Context context, String genre, int pageIdx,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("p", String.valueOf(pageIdx));
        if (!TextUtils.isEmpty(genre)) {
            params.put("g", genre);
        }
        sendGet(context, Path.streamNewRelease, false, params, listener,
                errorListener);
    }

    public static void getStreamTrending(Context context, String genre, int pageIdx,
                                         Response.Listener<JSONObject> listener,
                                         Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("p", String.valueOf(pageIdx));
        if (!TextUtils.isEmpty(genre)) {
            params.put("g", genre);
        }
        sendGet(context, Path.streamTrending, false, params, listener,
                errorListener);
    }

    public static void getStreamFollowing(Context context, String order,
                                          int pageIdx,
                                          boolean forceRefresh,
                                      Response.Listener<JSONObject> listener,
                                         Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("p", String.valueOf(pageIdx));
        params.put("order", order);
        params.put("f", forceRefresh ? "1" : "0");
        boolean auth = DbAccount.getUser(context) != null;
        sendGet(context, Path.streamFollowing, auth, params, listener, errorListener);
    }

    public static void channelFeed(Context context, int pageIdx,
                                   Response.Listener<JSONObject> listener,
                                   Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put("p", String.valueOf(pageIdx));
        sendGet(context, Path.feedChannel, true, params, listener,
                errorListener);
    }

    public static void resolveResource(Context context, String url,
                                       Response.Listener<JSONObject> listener,
                                       Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("url", url);
        sendGet(context, Path.resourceResolve, false, params, listener, errorListener);
    }
}

