package net.dropbeat.spark.network;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import net.dropbeat.spark.BaseActivity;
import net.dropbeat.spark.R;


public abstract class RobustResponseErrorListener implements
        Response.ErrorListener {

    private Context mContext;
    private BaseActivity mActivity;
    private Fragment mFragment;

    public RobustResponseErrorListener(Context context) {
        mContext = context;
        mActivity = null;
        mFragment = null;
    }

    public RobustResponseErrorListener(BaseActivity activity) {
        this(activity, null);
    }

    public RobustResponseErrorListener(BaseActivity activity, Fragment fragment) {
        mContext = activity.getApplicationContext();
        mActivity = activity;
        mFragment = fragment;
    }

    public abstract void onErrorResponse(VolleyError error, String msg);

    @Override
    public void onErrorResponse(VolleyError error) {
        if ((mActivity != null && !mActivity.isAlive())
                || (mFragment != null && !mFragment.isAdded())) {
            try {
                onSkipped(error);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        boolean handled = false;
        if (error instanceof NoConnectionError) {
            handled = onNoConnectionError(error);
        }
        if (!handled) {
            onErrorResponse(error, getErrorMsg(mContext, error));
        }
    }

    public abstract void onSkipped(VolleyError error);

    protected boolean onNoConnectionError(VolleyError error) {
        return false;
    }

    private static String getErrorMsg(Context context, VolleyError error) {
        String errorString = null;
        boolean printStackTrace = false;
        if (error instanceof NoConnectionError) {
            errorString = context
                    .getString(R.string.desc_failed_to_connect_internet);
        }
        NetworkResponse response = error.networkResponse;
        if (TextUtils.isEmpty(errorString) && response != null
                && response.data != null && response.data.length > 0) {
            errorString = new String(response.data);
        }
        if (TextUtils.isEmpty(errorString)) {
            errorString = error.getMessage();
            printStackTrace = true;
        }
        if (TextUtils.isEmpty(errorString)) {
            errorString = context.getString(R.string.desc_undefined_error);
            printStackTrace = true;
        }

        if (printStackTrace) {
            error.printStackTrace();
        }

        return errorString;
    }

}
