package net.dropbeat.spark;

import roboguice.fragment.RoboFragment;

public class BaseFragment extends RoboFragment {
    private boolean mVisible;

    protected BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean lastVisibility = mVisible;
        mVisible = getUserVisibleHint();
        if (mVisible != lastVisibility) {
            onVisibilityChanged(mVisible);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        boolean lastVisibility = mVisible;
        mVisible = false;
        if (mVisible != lastVisibility) {
            onVisibilityChanged(mVisible);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        boolean lastVisibility = mVisible;
        mVisible = isVisibleToUser;
        if (mVisible != lastVisibility) {
            onVisibilityChanged(mVisible);
        }
    };

    public void onVisibilityChanged(boolean isVisible) {

    }
}
