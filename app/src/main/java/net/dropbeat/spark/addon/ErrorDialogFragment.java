package net.dropbeat.spark.addon;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import net.dropbeat.spark.BaseDialogFragment;

import java.util.List;


/**
 * Created by vulpes on 15. 2. 20..
 */
public class ErrorDialogFragment extends BaseDialogFragment {
    private Dialog mDialog;

    public ErrorDialogFragment() {
        super();
        mDialog = null;
    }

    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return mDialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        hideAllDialog();
        super.show(manager, tag);
    }

    @Override
    public void showAllowingStateLoss(FragmentManager manager, String tag) {
        hideAllDialog();
        super.showAllowingStateLoss(manager, tag);
    }

    public void hideAllDialog() {
        if (getActivity() == null) {
            return;
        }
        FragmentManager manager = getActivity().getSupportFragmentManager();
        List<Fragment> fragments = manager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof ErrorDialogFragment) {
                    ((ErrorDialogFragment) fragment)
                            .dismissAllowingStateLoss();
                }
            }
        }
    }

}
