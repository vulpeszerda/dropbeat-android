package net.dropbeat.spark.addon;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class ArrayAdapter<T> extends BaseAdapter {

    private Context mContext;
    private List<T> mItems;
    protected LayoutInflater mInflater;

    public ArrayAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = new ArrayList<>();
    }

    protected Context getContext() {
        return mContext;
    }

    protected LayoutInflater getLayoutInflater() {
        return mInflater;
    }

    public List<T> getItems() {
        return mItems;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean add(T item) {
        return mItems.add(item);
    }

    public void add(int position, T item) {
        mItems.add(position, item);
    }

    public boolean remove(T item) {
        return mItems.remove(item);
    }

    public T remove(int position) {
        return mItems.remove(position);
    }

    public boolean addAll(Collection<T> items) {
        return mItems.addAll(items);
    }

    public void clear() {
        mItems.clear();
    }
}
