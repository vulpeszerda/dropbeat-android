package net.dropbeat.spark.addon.form;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Form {
    private Map<String, FormFieldable> mFields;
    private Map<String, Object> mExtraFields;
    private Context mContext;

    public Form(Context context) {
        this(context, null, null);
    }

    public Form(Context context, Map<String, Object> extra) {
        this(context, null, extra);
    }

    public Form(Context context, List<FormFieldable> fields) {
        this(context, fields, null);
    }

    public Form(Context context, List<FormFieldable> fields,
            Map<String, Object> extra) {
        mContext = context;
        mFields = new HashMap<String, FormFieldable>();
        if (fields != null) {
            for (FormFieldable field : fields) {
                mFields.put(field.getName(), field);
            }
        }
        mExtraFields = extra == null ? new HashMap<String, Object>() : extra;
    }

    public void add(FormFieldable field) {
        mFields.put(field.getName(), field);
    }

    public FormFieldable getField(String key) {
        return mFields.get(key);
    }

    public Object getValue(String key) {
        FormFieldable field = mFields.get(key);
        return field == null ? mExtraFields.get(key) : field.getValue();
    }

    public Map<String, FormFieldable> getFields() {
        return mFields;
    }

    public boolean validate() {
        boolean success = true;
        for (FormFieldable field : mFields.values()) {
            field.clearError();
            success &= field.validate(mContext);
        }
        return success;
    }

    public void clearError() {
        for (FormFieldable field : mFields.values()) {
            field.clearError();
        }
    }

    public JSONObject getParamsAsJson() {
        JSONObject obj = null;
        try {
            obj = new JSONObject();
            for (FormFieldable field : mFields.values()) {
                obj.put(field.getName(), field.getValue());
            }
            for (String key : mExtraFields.keySet()) {
                obj.put(key, mExtraFields.get(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

}
