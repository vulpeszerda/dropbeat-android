package net.dropbeat.spark.addon.form;

import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SignupFormBuilder {
    public static final String FIELD_NAME_EMAIL = "email";
    public static final String FIELD_NAME_PASSWD = "password";
    public static final String FIELD_NAME_FIRSTNAME = "first_name";
    public static final String FIELD_NAME_LASTNAME = "last_name";
    public static final String FIELD_NAME_NICKNAME = "nickname";

    private Context mContext;
    private List<FormFieldable> mFields;

    public SignupFormBuilder(Context context) {
        mContext = context;
        mFields = new ArrayList<>();
    }

    public SignupFormBuilder setEmailField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setRequired()
                .setFieldType(TextFormField.FieldType.EMAIL_FIELD).build();
        mFields.add(new TextFormField(FIELD_NAME_EMAIL, view, options));
        return this;
    }

    public SignupFormBuilder setPasswdField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setMinLength(6)
                .setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_PASSWD, view, options));
        return this;
    }

    public SignupFormBuilder setFirstNameField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setMaxLength(20)
                .setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_FIRSTNAME, view, options));
        return this;
    }

    public SignupFormBuilder setLastNameField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setMaxLength(20)
                .setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_LASTNAME, view, options));
        return this;
    }

    public SignupFormBuilder setNickNameField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setMaxLength(25)
                .setMinLength(4)
                .setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_NICKNAME, view, options));
        return this;
    }

    public Form build() {
        return new Form(mContext, mFields);
    }
}
