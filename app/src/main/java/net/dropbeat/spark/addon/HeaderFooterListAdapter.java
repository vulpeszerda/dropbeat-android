package net.dropbeat.spark.addon;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.ListView.FixedViewInfo;

import java.util.ArrayList;

public class HeaderFooterListAdapter extends HeaderViewListAdapter {

    private final ListView mListView;
    private final BaseAdapter mWrappedAdapter;
    private final ArrayList<FixedViewInfo> mHeaderViewInfos;
    private final ArrayList<FixedViewInfo> mFooterViewInfos;

    public HeaderFooterListAdapter(ListView listView, BaseAdapter adapter) {
        this(new ArrayList<FixedViewInfo>(), new ArrayList<FixedViewInfo>(),
                listView, adapter);
    }

    public HeaderFooterListAdapter(ArrayList<FixedViewInfo> headerViewInfos,
            ArrayList<FixedViewInfo> footerViewInfos, ListView listView,
            BaseAdapter adapter) {
        super(headerViewInfos, footerViewInfos, adapter);
        mHeaderViewInfos = headerViewInfos;
        mFooterViewInfos = footerViewInfos;
        mListView = listView;
        mWrappedAdapter = adapter;
    }

    public HeaderFooterListAdapter addHeader(View v) {
        return addHeader(v, null, false);
    }

    public HeaderFooterListAdapter addHeader(View v, Object data, boolean isSelectable) {
        addViewInfo(mHeaderViewInfos, v, data, isSelectable);
        return this;
    }

    public HeaderFooterListAdapter addFooter(View v) {
        return addFooter(v, null, false);
    }

    public HeaderFooterListAdapter addFooter(View v, Object data, boolean isSelectable) {
        addViewInfo(mFooterViewInfos, v, data, isSelectable);
        return this;
    }

    @Override
    public boolean removeFooter(View v) {
        boolean removed = super.removeFooter(v);
        if (removed) {
            mWrappedAdapter.notifyDataSetChanged();
        }
        return removed;
    }

    @Override
    public boolean removeHeader(View v) {
        boolean removed = super.removeHeader(v);
        if (removed) {
            mWrappedAdapter.notifyDataSetChanged();
        }
        return removed;
    }

    public BaseAdapter getWrappedAdapter() {
        return mWrappedAdapter;
    }

    protected void addViewInfo(ArrayList<FixedViewInfo> infos, View view,
            Object data, boolean isSelectable) {
        FixedViewInfo info = mListView.new FixedViewInfo();
        info.view = view;
        info.data = data;
        info.isSelectable = isSelectable;
        infos.add(info);
        mWrappedAdapter.notifyDataSetChanged();
    }
}
