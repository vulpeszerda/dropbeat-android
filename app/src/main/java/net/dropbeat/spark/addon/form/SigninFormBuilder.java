package net.dropbeat.spark.addon.form;

import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SigninFormBuilder {
    public static final String FIELD_NAME_EMAIL = "email";
    public static final String FIELD_NAME_PASSWD = "password";

    private Context mContext;
    private List<FormFieldable> mFields;
    private Map<String, Object> mExtraFields;

    public SigninFormBuilder(Context context) {
        mContext = context;
        mFields = new ArrayList<>();
        mExtraFields = new HashMap<>();
    }

    public SigninFormBuilder setEmailField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setRequired()
                .setFieldType(TextFormField.FieldType.EMAIL_FIELD)
                .build();
        mFields.add(new TextFormField(FIELD_NAME_EMAIL, view, options));
        return this;
    }

    public SigninFormBuilder setPasswdField(TextView view) {
        TextFormField.Options options = new TextFormField.Options.Builder()
                .setRequired()
                .build();
        mFields.add(new TextFormField(FIELD_NAME_PASSWD, view, options));
        return this;
    }

    public SigninFormBuilder setPasswdValue(String value) {
        mExtraFields.put(FIELD_NAME_PASSWD, value);
        return this;
    }

    public Form build() {
        return new Form(mContext, mFields, mExtraFields);
    }
}
