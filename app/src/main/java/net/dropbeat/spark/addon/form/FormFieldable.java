package net.dropbeat.spark.addon.form;

import android.content.Context;

public interface FormFieldable {
    boolean validate(Context context);

    String getName();
    Object getValue();
    void setError(Context context, String error);
    void clearError();
}
