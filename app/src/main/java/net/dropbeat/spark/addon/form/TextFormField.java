package net.dropbeat.spark.addon.form;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import net.dropbeat.spark.R;

import java.util.ArrayList;
import java.util.List;

public class TextFormField implements FormFieldable {
    public static enum FieldType {
        EMAIL_FIELD, TEXT_FIELD, URL_FIELD, PHONE_FIELD, NUMBER_FIELD
    }

    private TextView mView;
    private Options mOptions;
    private String mName;

    public TextFormField(String name, TextView view, Options options) {
        mName = name;
        mView = view;
        mOptions = options;
    }

    public boolean validate(Context context) {
        List<String> errorStrings = new ArrayList<String>();

        String inputText = mView.getText().toString();

        if (mOptions.isRequired() && TextUtils.isEmpty(inputText)) {
            errorStrings.add(context.getString(R.string.error_value_required));
        }

        if (mOptions.getMaxLength() > 0 && inputText != null
                && inputText.length() > mOptions.getMaxLength()) {
            String msg = context.getString(R.string.error_maxlength_exceed);
            msg = String.format(msg, mOptions.getMaxLength(),
                    inputText.length());
            errorStrings.add(msg);
        }

        if (mOptions.getMinLength() > 0 && inputText != null
                && inputText.length() < mOptions.getMinLength()) {
            String msg = context.getString(R.string.error_minlength_under);
            msg = String.format(msg, mOptions.getMinLength(),
                    inputText.length());
            errorStrings.add(msg);
        }

        switch (mOptions.getFieldType()) {
        case EMAIL_FIELD:
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(inputText)
                    .matches()) {
                errorStrings.add(context
                        .getString(R.string.error_format__email));
            }
            break;
        case TEXT_FIELD:
            break;
        case URL_FIELD:
            if (!android.util.Patterns.WEB_URL.matcher(inputText).matches()) {
                errorStrings.add(context
                        .getString(R.string.error_format__url));
            }
            break;
        case PHONE_FIELD:
            if (!android.util.Patterns.PHONE.matcher(inputText).matches()) {
                errorStrings.add(context.getResources().getString(
                        R.string.error_format__phone));
            }
            break;
        case NUMBER_FIELD:
            if (!inputText.matches("^[0-9]+$")) {
                errorStrings.add(context.getResources().getString(
                        R.string.error_format__number));
            }
        default:
            ;
        }
        if (errorStrings.size() > 0) {
            setError(context, TextUtils.join("\n", errorStrings));
            return false;
        }
        return true;
    }

    // private String getPwdPattern() {
    // // return "^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{6,12}$";
    // return "^(?=.*[a-zA-Z0-9])(?=\\S+$).{6,12}$";
    // }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public void setError(Context context, String errorString) {
        mView.setError(errorString);
    }

    @Override
    public void clearError() {
        mView.setError(null);
    }

    @Override
    public Object getValue() {
        return mView.getText().toString();
    }

    public static class Options {
        private boolean mIsRequired;
        private FieldType mType;
        private int mMaxLength;
        private int mMinLength;

        public Options(FieldType type, boolean isRequired, int maxLength,
                int minLength) {
            mType = type;
            mIsRequired = isRequired;
            mMaxLength = maxLength;
            mMinLength = minLength;
        }

        public boolean isRequired() {
            return mIsRequired;
        }

        public FieldType getFieldType() {
            return mType;
        }

        public int getMaxLength() {
            return mMaxLength;
        }

        public int getMinLength() {
            return mMinLength;
        }

        public static class Builder {
            private boolean mIsRequired;
            private FieldType mType;
            private int mMaxLength;
            private int mMinLength;

            public Builder() {
                mIsRequired = false;
                mType = FieldType.TEXT_FIELD;
                mMaxLength = -1;
                mMinLength = 0;
            }

            public Builder setRequired() {
                mIsRequired = true;
                return this;
            }

            public Builder setRequired(boolean isRequired) {
                mIsRequired = isRequired;
                return this;
            }

            public Builder setFieldType(FieldType type) {
                mType = type;
                return this;
            }

            public Builder setMaxLength(int length) {
                mMaxLength = length;
                return this;
            }

            public Builder setMinLength(int length) {
                mMinLength = length;
                return this;
            }

            public Options build() {
                return new Options(mType, mIsRequired, mMaxLength, mMinLength);
            }
        }
    }
}
