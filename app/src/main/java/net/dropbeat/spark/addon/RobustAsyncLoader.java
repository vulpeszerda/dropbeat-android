package net.dropbeat.spark.addon;

import android.content.Context;

/**
 * Eat all exception and return default value
 * Initially thought by sean
 * @author vulpes
 *
 * @param <D>
 */
public abstract class RobustAsyncLoader<D>
    extends AsyncLoader<D> {

    /// Variables
    // (Private)
    private D mDefaultValue;
    //   :Default value will be returned whenever there exists exception
    private Exception mException;
    private Object mExceptionContext;

    public RobustAsyncLoader(final Context context) {
        this(context, null);
    }
    public RobustAsyncLoader(final Context context, D defaultValue) {
        super(context);
        mDefaultValue = defaultValue;
    }

    @Override
    public D loadInBackground() {
        D loadedValue = mDefaultValue;
        try {
            loadedValue = load();
        } catch (Exception e) {
            handleException(e);
        }
        return loadedValue;
    }

    /// Methods
    // (Public)
    public Exception getException() {
        return mException;
    }

    public Object getExceptionContext() {
        return mExceptionContext;
    }

    public void clearException() {
        mException = null;
        mExceptionContext = null;
    }

    // (Protected)
    abstract protected D load() throws Exception;
    protected void setException(Exception exception, Object exceptionContext) {
        mException = exception;
        mExceptionContext = exceptionContext;
    }
    protected void handleException(Exception exception) {

        // Set exception
        setException(exception, null);
    }
}

