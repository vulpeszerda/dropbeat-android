package net.dropbeat.spark.addon;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import net.dropbeat.spark.BaseActivity;

import java.util.List;

public class ProgressDialogFragment extends DialogFragment {
    private static interface OnProgressDialogCancelListener {
        void onCancelled(String tag);
    }

    private static final String PARAM_TAG = "tag";
    private static final String PARAM_MESSAGE = "message";
    private static final String PARAM_CANCELLABLE = "cancellable";


    private String mTag;
    private String mMessage;
    private boolean mCancellable;

    public ProgressDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle args = getArguments();
        mTag = args.getString(PARAM_TAG);
        mMessage = args.getString(PARAM_MESSAGE);
        mCancellable = args.getBoolean(PARAM_CANCELLABLE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Activity activity = getActivity();
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(mMessage);
        dialog.setCancelable(mCancellable);
        if (mCancellable && activity instanceof OnProgressDialogCancelListener) {
            dialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    ((OnProgressDialogCancelListener) activity)
                            .onCancelled(mTag);
                }
            });
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.setIndeterminate(true);
        setCancelable(mCancellable);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public static void show(BaseActivity activity, String tag, int msgResId) {
        show(activity, tag, activity.getString(msgResId), false);
    }

    public static void show(BaseActivity activity, String tag, int msgResId,
            boolean cancellable) {
        show(activity, tag, activity.getString(msgResId), cancellable);
    }

    public static void show(BaseActivity activity, String tag, String message) {
        show(activity, tag, message, false);
    }

    public static void show(BaseActivity activity, String tag, String message,
            boolean cancellable) {
        hideAll(activity);
        if (activity != null && activity.isAlive()) {
            FragmentManager manager = activity.getSupportFragmentManager();
            ProgressDialogFragment dialog = new ProgressDialogFragment();
            Bundle args = new Bundle();
            args.putString(PARAM_TAG, tag);
            args.putString(PARAM_MESSAGE, message);
            args.putBoolean(PARAM_CANCELLABLE, cancellable);
            dialog.setArguments(args);
            dialog.show(manager, tag);
        }
    }

    public static void hide(BaseActivity activity, String tag) {
        if (activity != null) {
            FragmentManager manager = activity.getSupportFragmentManager();
            Fragment dialog = manager.findFragmentByTag(tag);
            if (dialog != null && dialog instanceof ProgressDialogFragment) {
                ((DialogFragment) dialog).dismissAllowingStateLoss();
            }
        }
    }

    public static void hideAll(BaseActivity activity) {
        if (activity != null) {
            FragmentManager manager = activity.getSupportFragmentManager();
            List<Fragment> fragments = manager.getFragments();
            if (fragments != null) {
                for (Fragment fragment : fragments) {
                    if (fragment instanceof ProgressDialogFragment) {
                        ((ProgressDialogFragment) fragment).dismissAllowingStateLoss();
                    }
                }
            }
        }
    }
}
