package net.dropbeat.spark;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.dropbeat.spark.addon.ArrayAdapter;

public class NavigationAdapter extends ArrayAdapter<NavigationItem> {

    private static final int ITEM_LAYOUT_ID = R.layout.navigation_list__item;
    private static final int SECTION_LAYOUT_ID = R.layout.navigation_list__divider;


    public NavigationAdapter(Context context) {
        super(context);
    }

    public int getPosition(NavigationItem item) {
        return getItems().indexOf(item);
    }

    protected void update(ViewHolder holder, NavigationItem item) {
        holder.getImageView().setImageResource(item.getImageResId());
        holder.getTextView().setText(item.getTextResId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        NavigationItem item = getItem(position);

        if (convertView == null) {
            if (item.isSection()) {
                convertView = mInflater.inflate(SECTION_LAYOUT_ID, null);
                holder = new ViewHolder();
            } else {
                convertView = mInflater.inflate(ITEM_LAYOUT_ID, null);
                holder = new ViewHolder(
                        (ImageView) convertView.findViewById(R.id.img),
                        (TextView) convertView.findViewById(R.id.text));
            }
            if (convertView != null) {
                convertView.setTag(holder);
            }
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (holder != null && !item.isSection()) {
            update(holder, item);
        }

        return convertView;
    }

    public NavigationItem getItemFromMenu(int menu) {
        for (NavigationItem item: getItems()) {
            if (!item.isSection() && item.getMenuType() == menu) {
                    return item;
            }
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isSection() ? 1 : 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isEnabled(int position) {
        return !getItem(position).isSection();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    private class ViewHolder {
        private ImageView mImageView;
        private TextView mTextView;

        public ViewHolder() {
        }

        public ViewHolder(ImageView image, TextView text) {
            mImageView = image;
            mTextView = text;
        }

        public ImageView getImageView() {
            return mImageView;
        }

        public TextView getTextView() {
            return mTextView;
        }
    }
}
