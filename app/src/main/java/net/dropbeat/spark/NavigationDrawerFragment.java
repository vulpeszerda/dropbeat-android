package net.dropbeat.spark;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;

import roboguice.inject.InjectView;

public class NavigationDrawerFragment extends BaseFragment implements
        AdapterView.OnItemClickListener, View.OnClickListener{

    public static void changeMenu(Context context, int menuType) {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (context);
        Intent intent = new Intent(ACTION_MENU_CHANGE);
        intent.putExtra(PARAM_MENU_TYPE, menuType);
        manager.sendBroadcast(intent);
    }

    public static interface MenuType {
        static int HOME = 0;
        static int FEED = 1;
        static int SEARCH = 2;
        static int SETTINGS = 3;
        static int CHANNEL = 4;
    }


    /**
     * Remember the position of the selected item.
     */
    public static final String STATE_SELECTED_MENU = "state_selected_menu";
    public static final String TAG = "NavigationDrawerFragment";

    private static final String PARAM_MENU_TYPE = "menu_type";
    private static final String ACTION_MENU_CHANGE = "net.dropbeat.spark" +
            ".DRAWER_MENU_CHANGE";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallback mCallback;

    /**
     * Helper component that ties the action bar to the navigation mDrawerLayout.
     */
    private ActionBarDrawerToggle mDrawerToggle;
    private View mFragmentContainerView;
    private LinearLayout mHeaderContainerView;

    private @InjectView(android.R.id.list) ListView mDrawerListView;
    private DrawerLayout mDrawerLayout;

    private NavigationAdapter mNavigationAdapter;

    private int mCurrentSelectedMenu = -1;
    private View mHeaderView;
    private float mLastTranslate = 0.0f;

    private BroadcastReceiver mMenuChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int menu = intent.getIntExtra(PARAM_MENU_TYPE, -1);
            if (menu > -1) {
                remoteSelectMenu(menu);
            }
            NavigationDrawerActivity parentActivity =
                    (NavigationDrawerActivity) getBaseActivity();
            parentActivity.restoreActionBar();
        }
    };

    public NavigationDrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        manager.registerReceiver(mMenuChangeReceiver,
                new IntentFilter(ACTION_MENU_CHANGE));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        manager.unregisterReceiver(mMenuChangeReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_navigation_drawer, null,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mNavigationAdapter == null) {
            mNavigationAdapter = new NavigationAdapter(getActivity());
        }

        // set header container
        if (mHeaderContainerView == null) {
            mHeaderContainerView = new LinearLayout(getActivity());
            mHeaderContainerView.setOrientation(LinearLayout.VERTICAL);

            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);

            mHeaderContainerView.setLayoutParams(layoutParams);
        }
        mDrawerListView.addHeaderView(mHeaderContainerView, null, false);


        updateHeader();


        if (savedInstanceState == null) {
            // set data
            mNavigationAdapter.addAll(NavigationItem.getNavigationItems());
        }

        mDrawerListView.setOnItemClickListener(this);
        mDrawerListView.setAdapter(mNavigationAdapter);
    }

    private void updateHeader() {
        if (mHeaderContainerView == null) {
            return;
        }

        // remove preexsiting headers
        if (mHeaderView != null) {
            removeHeaderView(mHeaderView);
        }

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        DbAccount account = DbAccount.getUser(getBaseActivity());
        if (account == null) {
            mHeaderView = inflater.inflate(R.layout.header_navi_drawer_noauth, null, false);
            mHeaderView.setOnClickListener(this);
        } else {
            mHeaderView = inflater.inflate(R.layout.header_navi_drawer, null, false);
            ImageView profileView = (ImageView)mHeaderView.findViewById(R.id
                    .profile_photo);
            TextView userNameView = (TextView)mHeaderView.findViewById(R.id
                    .account_name);
            TextView userEmailView = (TextView)mHeaderView.findViewById(R.id
                    .account_email);

            userNameView.setText(account.getFullName());
            userEmailView.setText(account.getEmail());

            if (account.getProfileImageUrl() == null) {
                profileView.setImageResource(R.drawable.default_profile);
            } else {
                DbApplication app = (DbApplication)getBaseActivity()
                        .getApplication();

                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(new ColorDrawable(0x000000))
                        .displayer(new FadeInBitmapDisplayer(300))
                        .cacheOnDisk(true)
                        .cacheInMemory(true)
                        .showImageOnFail(R.drawable.default_profile)
                        .build();
                ImageLoader loader = app.getDefaultImageLoader();
                loader.displayImage(account.getProfileImageUrl(), profileView,
                        options);
            }
        }

        addHeaderView(mHeaderView);
    }

    private void addHeaderView(View view) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mHeaderContainerView.addView(view, params);
    }

    private void removeHeaderView(View view) {
        mHeaderContainerView.removeView(view);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void setUp(int fragmentId, int drawerLayoutResId, int menu) {
        mCurrentSelectedMenu = menu;
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = (DrawerLayout)getActivity().findViewById
                (drawerLayoutResId);
        final View frame = mDrawerLayout.findViewById(R.id.container_frame);

        mDrawerLayout.setScrimColor(Color.TRANSPARENT);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation mDrawerLayout and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                getBaseActivity().getToolbar(),             /* nav mDrawerLayout image to replace  'Up' caret */
                R.string.navigation_drawer_open,  /* "open mDrawerLayout" description for accessibility */
                R.string.navigation_drawer_close  /* "close mDrawerLayout" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View mDrawerLayoutView) {
                super.onDrawerClosed(mDrawerLayoutView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View mDrawerLayoutView) {
                super.onDrawerOpened(mDrawerLayoutView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mDrawerListView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    frame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(mLastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    frame.startAnimation(anim);

                    mLastTranslate = moveFactor;
                }
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void initDrawer() {
        NavigationItem item = mNavigationAdapter.getItemFromMenu
                (mCurrentSelectedMenu);
        if (item == null && mNavigationAdapter.getCount() > 0) {
            item = mNavigationAdapter.getItem(0);
        }
        if (item != null) {
            selectItem(item);
        }
    }

    public boolean remoteSelectMenu(int type) {
        NavigationItem foundMenu = null;
        for (NavigationItem item : mNavigationAdapter.getItems()) {
            if (item.getMenuType() == type) {
                foundMenu = item;
                break;
            }
        }

        if (foundMenu != null) {
            selectItem(foundMenu);
            return true;
        }
        return false;
    }

    private void selectItem(NavigationItem item) {
        mCurrentSelectedMenu = item.getMenuType();
        if (mCallback != null) {
            mCallback.onNavigationDrawerItemSelected(item.getMenuType(), null);
            int position = mDrawerListView.getHeaderViewsCount() +
                    mNavigationAdapter.getPosition(item);
            mDrawerListView.setItemChecked(position, true);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof NavigationDrawerCallback) {
            mCallback = (NavigationDrawerCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the mDrawerLayout toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the mDrawerLayout is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.menu_global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation mDrawerLayout design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
//        ActionBar actionBar = getActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallback {
        /**
         * Called when an item in the navigation mDrawerLayout is selected.
         */
        void onNavigationDrawerItemSelected(int menuType, Bundle args);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /**
         * called when item in listview is clicked
         * item has 2 cases, NavigationItem and Device
         */
        Object item = parent.getItemAtPosition(position);
        if (item == null) {
            return;
        }
        if (item instanceof NavigationItem) {
            NavigationItem navItem = (NavigationItem) item;
            selectItem(navItem);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook_signin_button:
                Intent intent = AuthActivity.createIntent(getBaseActivity());
                startActivityForResult(intent, AuthActivity.REQUEST_CODE__AUTH);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AuthActivity.REQUEST_CODE__AUTH && resultCode ==
                Activity.RESULT_OK) {
            updateHeader();
        }
    }

    @Override
    public void onVisibilityChanged(boolean isVisible) {
        super.onVisibilityChanged(isVisible);
        if (isVisible) {
            updateHeader();
        }
    }
}
