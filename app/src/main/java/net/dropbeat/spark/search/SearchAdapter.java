package net.dropbeat.spark.search;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.User;
import net.dropbeat.spark.play.PlayerContext;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by vulpes on 15. 4. 30..
 */
public class SearchAdapter extends ArrayAdapter<SearchAdapter.SearchItem>
        implements StickyListHeadersAdapter{

    public interface OnUserClickListener {
        void onClick(View v, User user, int position);
    }

    private static final int TRACK_LAYOUT_ID = R.layout.search_track_item;
    private static final int USER_LAYOUT_ID = R.layout.search_artist_item;
    private static final int HEADER_LAYOUT = R.layout.serach_section_header;

    private TrackClickListener mTrackListener;
    private OnUserClickListener mUserListener;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mTrackImageOptions;
    private DisplayImageOptions mProfileImageOptions;

    public SearchAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mTrackImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(new ColorDrawable(0x00000000))
                .showImageForEmptyUri(R.drawable.default_thumb)
//                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_thumb)
                .build();

        mProfileImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_profile)
                .showImageForEmptyUri(R.drawable.default_profile)
                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_profile)
                .build();
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        if (getItemViewType(position) == 0) {
            return getTrackView(position, convertView, parent);
        } else {
            return getArtistView(position, convertView, parent);
        }
    }

    private View getArtistView(final int position, View convertView,
                               ViewGroup parent) {
        View row = convertView;
        UserViewHolder userViewHolder;

        if (row == null) {
            row = mInflater.inflate(USER_LAYOUT_ID, null);
            userViewHolder = new UserViewHolder(row);
            row.setTag(userViewHolder);
        } else {
            userViewHolder = (UserViewHolder) row.getTag();
        }

        SearchItem item = getItem(position);
        final User user = item.user;

        mImageLoader.displayImage(user.getProfileImage(),
                userViewHolder.getThumb(), mProfileImageOptions);

        userViewHolder.getTitle().setText(user.getNickname());

        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mUserListener != null) {
                    mUserListener.onClick(view, user, position);
                }
            }
        };

        Button followBtn = userViewHolder.getFollowBtn();
        followBtn.setOnClickListener(listener);

        Context context = getContext();
        if (user.isFollowed(context)) {
            followBtn.setText(context.getString(R.string.button_unfollow));
            followBtn.setBackgroundResource(R.drawable.btn_unfollow);
            followBtn.setTextColor(context.getResources().getColor(
                    android.R.color.white));
        } else {
            followBtn.setText(context.getString(R.string.button_follow));
            followBtn.setBackgroundResource(R.drawable.btn_follow);
            followBtn.setTextColor(context.getResources().getColor(
                    R.color.color_primary));
        }

        return row;
    }

    private View getTrackView(final int position, View convertView,
                              ViewGroup parnet) {
        View row = convertView;
        TrackViewHolder trackViewHolder;

        if (row == null) {
            row = mInflater.inflate(TRACK_LAYOUT_ID, null);
            trackViewHolder = new TrackViewHolder(row);
            row.setTag(trackViewHolder);
        } else {
            trackViewHolder = (TrackViewHolder) row.getTag();
        }
        SearchItem item = getItem(position);
        final Track track = item.track;

        TextView titleView = trackViewHolder.getTitle();
        titleView.setText(track.getTitle());

        ImageButton menuBtn = trackViewHolder.getMenuBtn();
        ImageView hoverView = trackViewHolder.getThumbHover();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTrackListener != null) {
                    mTrackListener.onClick(v, track, position);
                }
            }
        };

        row.setOnClickListener(listener);
        menuBtn.setOnClickListener(listener);

        mImageLoader.displayImage(track.getThumbnailUrl(),
                trackViewHolder.getThumb(), mTrackImageOptions);

        boolean isPlaying;
        isPlaying = PlayerContext.playState == PlayState.LOADING ||
                PlayerContext.playState == PlayState.PLAYING ||
                PlayerContext.playState == PlayState.PAUSED;
        isPlaying &= PlayerContext.currTrack != null &&
                PlayerContext.currTrack.getId().equals(track.getId());
        isPlaying &= PlayerContext.trackIdx == -1;
        isPlaying &= PlayerContext.playlistId == null;

        if (isPlaying) {
            titleView.setTypeface(null, Typeface.BOLD);
            titleView.setTextColor(getContext().getResources().getColor
                    (android.R.color.white));
            hoverView.setVisibility(View.VISIBLE);
        } else {
            titleView.setTypeface(null, Typeface.NORMAL);
            titleView.setTextColor(getContext().getResources().getColor
                    (R.color.text_light_gray));
            hoverView.setVisibility(View.GONE);
        }
        return row;
    }

    public void setTrackClickListener(TrackClickListener listener) {
        mTrackListener = listener;
    }

    public void setUerClickListener(OnUserClickListener listener) {
        mUserListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        SearchItem item = getItem(position);
        if (item.type == SearchItemType.ARTIST) {
            return 1;
        }
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() == 0) {
            return 1;
        }
        SearchItem firstItem = getItem(0);
        SearchItem lastItem = getItem(getCount() - 1);
        if ((firstItem.type == SearchItemType.TOP_MATCH ||
                firstItem.type == SearchItemType.ARTIST) &&
                lastItem.type == SearchItemType.TRACK) {
            return 2;
        }
        return 1;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(HEADER_LAYOUT, null);
        }

        String title;
        switch ((int)getHeaderId(i)) {
            case 0:
                title = "PEOPLE";
                break;
            case 1:
                title = "TOP MATCH";
                break;
            case 2:
                title = "TRACKS";
                break;
            default:
                return null;
        }
        TextView titleView = (TextView)view.findViewById(R.id.title);
        titleView.setText(title);

        return view;
    }

    @Override
    public long getHeaderId(int i) {
        switch (getItem(i).type) {
            case TRACK:
                return 2;
            case TOP_MATCH:
                return 1;
            case ARTIST:
                return 0;
            default:
                return -1;
        }
    }

    private class TrackViewHolder {
        private TextView mTitleView;
        private ImageButton mMenuBtn;
        private ImageView mThumbView;
        private ImageView mThumbHoverView;

        public TrackViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.thumbnail);
            mThumbHoverView = (ImageView) view.findViewById(R.id
                    .thumbnail_hover);
            mTitleView = (TextView) view.findViewById(R.id.title);
            mMenuBtn = (ImageButton) view.findViewById(R.id.menu_button);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public ImageView getThumbHover() {
            return mThumbHoverView;
        }

        public ImageButton getMenuBtn() {
            return mMenuBtn;
        }
    }

    private class UserViewHolder {
        private TextView mTitleView;
        private ImageView mThumbView;
        private Button mFollowBtn;

        public UserViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.thumbnail);
            mTitleView = (TextView) view.findViewById(R.id.title);
            mFollowBtn = (Button) view.findViewById(R.id.follow_button);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public Button getFollowBtn() {
            return mFollowBtn;
        }
    }

    public enum  SearchItemType {
        TRACK,
        ARTIST,
        TOP_MATCH
    }

    public static class SearchItem {
        SearchItemType type;
        Track track;
        User user;

        public SearchItem(Track track) {
            this(track, false);
        }

        public SearchItem(Track track, boolean isTopMatch) {
            this.track = track;
            this.type = isTopMatch ? SearchItemType.TOP_MATCH :
                    SearchItemType.TRACK;
        }

        public SearchItem(User user) {
            this.user = user;
            this.type = SearchItemType.ARTIST;
        }
    }
}
