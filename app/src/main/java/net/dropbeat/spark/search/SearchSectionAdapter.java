package net.dropbeat.spark.search;

import android.content.Context;
import android.widget.BaseAdapter;

import net.dropbeat.spark.addon.Sectionizer;
import net.dropbeat.spark.addon.SimpleSectionAdapter;
import net.dropbeat.spark.model.Track;

/**
 * Created by vulpes on 15. 5. 3..
 */
public class SearchSectionAdapter extends SimpleSectionAdapter<Track> {

    // original simplesectionadapter's value
    // we declare agian because original VIEW_TYPE_SECTION_HEADER is private
    private static final int VIEW_TYPE_SECTION_HEADER = 0;

    /**
     * Constructs a {@linkplain com.mobsandgeeks.adapters.SimpleSectionAdapter}.
     *
     * @param context                The context for this adapter.
     * @param listAdapter            A {@link ListAdapter} that has to be sectioned.
     * @param sectionHeaderLayoutId  Layout Id of the layout that is to be used for the header.
     * @param sectionTitleTextViewId Id of a TextView present in the section header layout.
     * @param sectionizer            Sectionizer for sectioning the {@link ListView}.
     */
    public SearchSectionAdapter(Context context, BaseAdapter listAdapter,
                                int sectionHeaderLayoutId,
                                int sectionTitleTextViewId,
                                Sectionizer<Track> sectionizer) {
        super(context, listAdapter, sectionHeaderLayoutId,
                sectionTitleTextViewId, sectionizer);
    }

    @Override
    public int getIndexForPosition(int position) {
        if (getSectionCount() > 1) {
            return super.getIndexForPosition(position);
        }
        return position;
    }

    @Override
    public int getSectionCount() {
        int count = super.getSectionCount();
        return count > 1 ? count : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return getSectionCount() > 1 ? super.getItemViewType(position) : 1;
    }
}
