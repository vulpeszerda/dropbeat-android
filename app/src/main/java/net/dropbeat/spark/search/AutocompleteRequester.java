package net.dropbeat.spark.search;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.dropbeat.spark.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vulpes on 15. 5. 3..
 */
public class AutocompleteRequester implements Response.Listener<String>,
        Response.ErrorListener {

    public static interface OnResponseListener {
        void onResponse(List<String> keywords);
    }

    public static interface OnErrorListener {
        void onError(VolleyError error);
    }

    private static final String REQUEST_TAG = "autocomplete_req";
    private static final String YOUTUBE_API_PATH =
            "https://clients1.google.com/complete/search";

    private static final String FUNC_REGEX = "[a-zA-Z0-9\\.]+\\(([^\\)]+)\\)";
    private static final String KOREAN_REGEX = ".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*";

    private final Context mContext;
    private final Map<String, String> mDefaultParams;
    private final OnResponseListener mListener;
    private final OnErrorListener mErrorListener;
    private final RequestQueue mRequestQ;
    private String mPrevKeyword;

    private Pattern mFuncPattern;
    private Pattern mKoreanPattern;

    public AutocompleteRequester(Context context, OnResponseListener listener,
                                 OnErrorListener errorListener) {
        mContext = context;
        mRequestQ = Volley.newRequestQueue(context);
        mListener = listener;
        mErrorListener = errorListener;

        // set default params
        mDefaultParams = new HashMap<>();
        mDefaultParams.put("client", "youtube");
        mDefaultParams.put("hl", "en");
        mDefaultParams.put("gl", "us");
        mDefaultParams.put("gs_rn", "23");
        mDefaultParams.put("gs_ri", "youtube");
        mDefaultParams.put("tok", "I9KDmvOmJAg1Xq-coNjwGg");
        mDefaultParams.put("ds", "yt");
        mDefaultParams.put("cp", "3");
        mDefaultParams.put("gs_gbg", "K111AA607");

        mFuncPattern = Pattern.compile(FUNC_REGEX);
        mKoreanPattern = Pattern.compile(KOREAN_REGEX);
    }

    public synchronized void send(String keyword) {
        String q = keyword.trim();
        if (mPrevKeyword != null && mPrevKeyword.equals(q)) {
            return;
        }
        mPrevKeyword = q;

        String id = makeRandId();
        Map<String, String> params = new HashMap<>(mDefaultParams);
        params.put("q", keyword);
        params.put("gs_id", id);

        String url = null;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (url == null) {
                url = YOUTUBE_API_PATH + "?";
            } else {
                url += "&";
            }
            url += entry.getKey() + "=" + URLEncoder.encode(entry.getValue());
        }
        StringRequest request = new StringRequest(url, this, this);
        request.setTag(REQUEST_TAG);
        mRequestQ.add(request);
    }

    public void cancelAll() {
        mRequestQ.cancelAll(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String errorString = null;
        if (error instanceof NoConnectionError) {
            errorString = mContext
                    .getString(R.string.desc_failed_to_connect_internet);
        }
        NetworkResponse response = error.networkResponse;
        if (TextUtils.isEmpty(errorString) && response != null
                && response.data != null && response.data.length > 0) {
            errorString = new String(response.data);
        }
        if (TextUtils.isEmpty(errorString)) {
            errorString = error.getMessage();
        }
        if (TextUtils.isEmpty(errorString)) {
            errorString = mContext.getString(R.string.desc_undefined_error);
        }
    }

    @Override
    public void onResponse(String response) {
        Matcher m = mFuncPattern.matcher(response);
        if (!m.matches()) {
            return;
        }
        String args = m.group(1);
        try {
            JSONArray argArray = new JSONArray(args);
            String q = argArray.getString(0);
            JSONArray words = argArray.getJSONArray(1);
            JSONObject appendix = argArray.getJSONObject(2);
            String respId = appendix.getString("j");

            List<String> keywords = new ArrayList<>();
            for (int i = 0; i < words.length(); i++) {
                JSONArray wrapper = words.getJSONArray(i);
                String word = wrapper.getString(0);
                if (!mKoreanPattern.matcher(word).matches()) {
                    keywords.add(word);
                }
            }
            mListener.onResponse(keywords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String makeRandId() {
        String possible = "abcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(possible.charAt(
                    (int) Math.floor(Math.random() * possible.length())));
        }
        return builder.toString();
    }
}
