package net.dropbeat.spark.search;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.model.User;
import net.dropbeat.spark.playlist.PlaylistFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.Sectionizer;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Search;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.utils.ViewUtils;
import net.dropbeat.spark.view.MaterialProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import roboguice.inject.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by vulpes on 15. 4. 30..
 */
public class SearchFragment extends BaseFragment implements
        View.OnClickListener, TrackClickListener,
        SearchAdapter.OnUserClickListener, AdapterView.OnItemClickListener,
        TextWatcher, View.OnFocusChangeListener {

    public static Bundle getArguments(String keyword) {
        Bundle args = new Bundle();
        args.putString(PARAM_KEYWORD, keyword);
        return args;
    }

    private static final String PARAM_KEYWORD = "search";

    @InjectView(android.R.id.list)
    private StickyListHeadersListView mListView;

    @InjectView(R.id.autocomplete_list)
    private ListView mAutocompleteList;

    @InjectView(R.id.search_input)
    private EditText mSearchInputView;

    @InjectView(R.id.search_button)
    private Button mSearchButton;

    @InjectView(R.id.progress)
    private MaterialProgressBar mProgressBar;

    private View mFooterView;
    private LinearLayout mContainerHolder;

    private AutocompleteAdapter mAutocompleteAdapter;
    private boolean mIsKeyboardVisible = false;
    private Timer mFooterShowTimer;
    private AutocompleteRequester mAutocomRequester;

    private SearchAdapter mAdapter;
    private boolean mIsInitialized = false;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mProfileImageOptions;

    private Search mSearch;

    private BroadcastReceiver mPlayStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isAdded() && (PlayerContext.playState == PlayState.LOADING ||
                    PlayerContext.playState == PlayState.STOPPED) &&
                    mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    private BroadcastReceiver mLikeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isAdded() && mAdapter != null) {
                ProgressDialogFragment.hide(getBaseActivity(), "like");
                boolean success = intent.getBooleanExtra(
                        DbAccount.PARAM_SYNC_SUCCESS, false);
                int actionIdx;
                actionIdx = intent.getIntExtra(DbAccount.PARAM_LIKE_ACTION, -1);
                if (actionIdx == -1) {
                    return;
                }

                DbAccount.LikeAction action =
                        DbAccount.LikeAction.values()[actionIdx];
                String msg;
                if (success) {
                    msg = action == DbAccount.LikeAction.LIKE ?
                            "Liked" : "Unliked";
                } else {
                    msg = action == DbAccount.LikeAction.LIKE ?
                            "Failed to like" : "Failed to unlike";
                }
                Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT)
                        .show();
            }

        }
    };

    private BroadcastReceiver mFollowingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isAdded() && mAdapter != null) {
                ProgressDialogFragment.hide(getBaseActivity(), "follow");
                boolean success = intent.getBooleanExtra(
                        DbAccount.PARAM_SYNC_SUCCESS, false);
                int actionIdx;
                actionIdx = intent.getIntExtra(DbAccount.PARAM_FOLLOW_ACTION, -1);
                if (actionIdx == -1) {
                    return;
                }

                DbAccount.FollowAction action =
                        DbAccount.FollowAction.values()[actionIdx];
                String msg;
                if (!success) {
                    msg = action == DbAccount.FollowAction.FOLLOW ?
                            "Failed to follow" : "Failed to unfollow";
                    Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView.OnEditorActionListener listener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (TextUtils.isEmpty(mSearchInputView.getText())) {
                        return false;
                    }
                    String keyword = mSearchInputView.getText().toString().trim();
                    if (TextUtils.isEmpty(keyword)) {
                        return false;
                    }
                    performSearch(keyword);
                    return true;
                }
                return false;
            }
        };

        mSearchButton.setOnClickListener(this);
        mSearchInputView.setOnEditorActionListener(listener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!mIsInitialized) {
            mIsInitialized = true;
            DbApplication app = (DbApplication) getBaseActivity().getApplication();
            mImageLoader = app.getDefaultImageLoader();

            mAutocompleteAdapter = new AutocompleteAdapter(getBaseActivity());
            mAutocomRequester = new AutocompleteRequester(getBaseActivity(),
                    new AutocompleteRequester.OnResponseListener() {
                @Override
                public void onResponse(List<String> keywords) {
                    onAutocompleteResponse(keywords);
                }
            }, new AutocompleteRequester.OnErrorListener() {
                @Override
                public void onError(VolleyError error) {
                    onAutocompleteError(error);
                }
            });

            mAdapter = new SearchAdapter(getBaseActivity(), mImageLoader);
            mAdapter.setTrackClickListener(this);
            mAdapter.setUerClickListener(this);
        }

        mSearchInputView.addTextChangedListener(this);
        mSearchInputView.setOnFocusChangeListener(this);

        mListView.setAdapter(mAdapter);
        mAutocompleteList.setAdapter(mAutocompleteAdapter);
        mAutocompleteList.setOnItemClickListener(this);

        mFooterView = getBaseActivity().findViewById(R.id.footer);
        mContainerHolder = (LinearLayout)getBaseActivity().findViewById(R.id
                .container_holder);

        // Set keyboard showup observer
        final View activityRootView = getBaseActivity()
                .findViewById(R.id.container_frame);
        activityRootView.getViewTreeObserver()
                .addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                View rootView = activityRootView.getRootView();
                                int heightDiff = rootView.getHeight() - activityRootView.getHeight();
                                int heightDiffDp = ViewUtils.pxToDp(getBaseActivity(),
                                        heightDiff);
                                if (heightDiffDp < 0) {
                                    return;
                                }
                                if (heightDiffDp > 100) {
                                    if (!mIsKeyboardVisible) {
                                        mIsKeyboardVisible = true;
                                        onKeyboardVisibilityChange(true);
                                    }
                                } else {
                                    if (mIsKeyboardVisible) {
                                        mIsKeyboardVisible = false;
                                        onKeyboardVisibilityChange(false);
                                    }
                                }
                            }
                        });
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());

        manager.registerReceiver(mPlayStateChangeReceiver,
                new IntentFilter(PlayerService.ACTION_NOTIFY_PLAYSTATE_CHANGE));
        manager.registerReceiver(mFollowingReceiver,
                new IntentFilter(DbAccount.ACTION_FOLLOWING_CHANGED));
        manager.registerReceiver(mLikeReceiver,
                new IntentFilter(DbAccount.ACTION_LIKE_CHANGED));
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (getBaseActivity());
        manager.unregisterReceiver(mPlayStateChangeReceiver);
        manager.unregisterReceiver(mFollowingReceiver);
        manager.unregisterReceiver(mLikeReceiver);
    }

    private void performSearch(final String keyword) {
        hideAutocomplete();
        showProgress();

        InputMethodManager imm = (InputMethodManager) getBaseActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchInputView.getWindowToken(), 0);

        DbAccount account = DbAccount.getUser(getBaseActivity());
        if (account != null) {
            // Logging
            // to us
            Requests.logSearch(getBaseActivity(), keyword);
            // to GA
            DbApplication app = (DbApplication) getBaseActivity().getApplication();
            Tracker tracker = app.getAnalyticsTracker();
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory("search-section")
                    .setAction("search-with-keyword")
                    .setLabel(keyword).build());
        }
        final RobustResponseListener<JSONObject> listener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                hideProgress();

                JSONObject data = response.optJSONObject("data");
                mSearch = Search.fromJsonObject(data);
                if (mSearch == null) {
                    Toast.makeText(getBaseActivity(), "Failed to search",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                applyResults(mSearch);
            }
        };

        final RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                Toast.makeText(getBaseActivity(), "Failed to search (" + msg
                                + ")", Toast.LENGTH_LONG).show();
                hideProgress();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.search(getBaseActivity(), keyword, listener, errorListener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_button:
                if (TextUtils.isEmpty(mSearchInputView.getText())) {
                    return;
                }
                String keyword = mSearchInputView.getText().toString().trim();
                if (TextUtils.isEmpty(keyword)) {
                    return;
                }
                performSearch(keyword);
                break;
        }
    }

    @Override
    public void onClick(View view, final Track track, int position) {
        switch(view.getId()) {
            case R.id.track:
                if (track != null) {
                    PlayerService.doPlay(getBaseActivity(), track, null);
                }
                break;
            case R.id.menu_button:
                if (track != null) {
                    PopupMenu menu = new PopupMenu(getBaseActivity(), view);
                    menu.getMenuInflater().inflate(R.menu.menu_addable_track,
                            menu.getMenu());

                    DbAccount account = DbAccount.getUser(getBaseActivity());

                    MenuItem likeItem = menu.getMenu().findItem(R.id.action_like);
                    MenuItem unlikeItem = menu.getMenu().findItem(R.id.action_unlike);

                    if (track.isLiked(getBaseActivity())) {
                        likeItem.setVisible(false);
                        unlikeItem.setVisible(true);
                    } else {
                        likeItem.setVisible(true);
                        unlikeItem.setVisible(false);
                    }

                    menu.setOnMenuItemClickListener(
                            new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            return onTrackPopupMenuClicked(track, item);
                        }
                    });
                    menu.show();
                }
                break;
        }
    }

    @Override
    public void onClick(View v, User user, int position) {
        if (v.getId() != R.id.follow_button) {
            return;
        }

        DbAccount account = DbAccount.getUser(getBaseActivity());
        if (account == null) {
            startActivity(AuthActivity.createIntent(getBaseActivity(), true));
            return;
        }

        ProgressDialogFragment.show(getBaseActivity(), "follow", "");
        if (user.isFollowed(getBaseActivity())) {
            account.unfollow(getBaseActivity(), user);
        } else {
            account.follow(getBaseActivity(), user);
        }
    }

    protected boolean onTrackPopupMenuClicked (Track track, MenuItem item){
        DbAccount account = DbAccount.getUser(getBaseActivity());
        switch (item.getItemId()) {
            case R.id.action_add:
                PlaylistFragment.addTrack(getBaseActivity(), track, "search");
                return true;
            case R.id.action_share:
                track.share(getBaseActivity());
                return true;
            case R.id.action_like:
                if (account == null) {
                    startActivity(
                            AuthActivity.createIntent(getBaseActivity(), true));
                }
                ProgressDialogFragment.show(getBaseActivity(), "like", "");
                account.like(getBaseActivity(), track);
                return true;
            case R.id.action_unlike:
                if (account == null) {
                    startActivity(
                            AuthActivity.createIntent(getBaseActivity(), true));
                }
                ProgressDialogFragment.show(getBaseActivity(), "like", "");
                account.unlike(getBaseActivity(), track);
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick (AdapterView < ? > parent, View view,
    int position, long id){
        String keyword = mAutocompleteAdapter.getItem(position);
        if (TextUtils.isEmpty(keyword)) {
            return;
        }
        mSearchInputView.setText(keyword);
        performSearch(keyword);
    }

    private void hideAutocomplete() {
        mAutocompleteList.setVisibility(View.GONE);
    }

    private void showAutocomplete() {
        mAutocompleteList.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mSearchButton.setEnabled(false);
        mSearchInputView.setEnabled(false);
    }

    private void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
        mSearchButton.setEnabled(true);
        mSearchInputView.setEnabled(true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(mSearchInputView.getText())) {
            mAutocompleteAdapter.clear();
            hideAutocomplete();
            mAutocomRequester.cancelAll();
        } else {
            showAutocomplete();
            mAutocomRequester.send(s.toString());
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            showFooterView();
            InputMethodManager imm = (InputMethodManager) getBaseActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mSearchInputView.getWindowToken(), 0);
        }
    }

    private void showFooterView() {
        mFooterView.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout
                .LayoutParams)mContainerHolder.getLayoutParams();
        int marginBottom = ViewUtils.dpToPx(getBaseActivity(), 58);
        if (marginBottom < 0) {
            return;
        }
        params.setMargins(params.leftMargin, params.topMargin,
                params.rightMargin, marginBottom);
        mContainerHolder.setLayoutParams(params);
    }

    private void hideFooterView() {
        mFooterView.setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = (RelativeLayout
                .LayoutParams)mContainerHolder.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin,
                params.rightMargin, 0);
        mContainerHolder.setLayoutParams(params);
    }

    private synchronized void onKeyboardVisibilityChange(boolean isVisible) {
        if (isVisible) {
            if (mFooterShowTimer != null) {
                mFooterShowTimer.cancel();
                mFooterShowTimer = null;
            }
            hideFooterView();
            if (!TextUtils.isEmpty(mSearchInputView.getText())) {
                showAutocomplete();
            }
        } else {
            hideAutocomplete();
            if (mFooterShowTimer == null) {
                mFooterShowTimer = new Timer();
                mFooterShowTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mFooterView.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized (this) {
                                    mFooterShowTimer = null;
                                    if (isAdded()) {
                                        showFooterView();
                                    }
                                }
                            }
                        });
                    }
                }, 200);
            }
        }
    }

    private void onAutocompleteResponse(List<String> keywords) {
        mAutocompleteAdapter.clear();
        mAutocompleteAdapter.addAll(keywords);
        mAutocompleteAdapter.notifyDataSetChanged();
    }

    private void onAutocompleteError(VolleyError error) {

    }

    private void applyResults(Search search) {
        mAdapter.clear();

        if (search.getUsers().size() > 0) {
            List<User> users = search.getUsers();
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                mAdapter.add(new SearchAdapter.SearchItem(user));
            }
        }

        List<Track> tracks = search.getTracks();
        for (int i = 0; i < tracks.size(); i++) {
            Track track = tracks.get(i);
            mAdapter.add(new SearchAdapter.SearchItem(track,
                    mAdapter.getCount() == 0));
        }

        if (mAdapter.getCount() == 0) {
            Toast.makeText(getBaseActivity(), "No search result",
                    Toast.LENGTH_LONG).show();
        }
        mAdapter.notifyDataSetChanged();
    }

}
