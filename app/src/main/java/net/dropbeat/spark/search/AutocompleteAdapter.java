package net.dropbeat.spark.search;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ArrayAdapter;

/**
 * Created by vulpes on 15. 5. 3..
 */
public class AutocompleteAdapter extends ArrayAdapter<String> {

    private static final int LAYOUT_ID = R.layout.autocomplete_item;

    public AutocompleteAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }
        String text = getItem(position);
        TextView titleView = viewHolder.getText();
        titleView.setText(text);
        return row;
    }

    private class ViewHolder {
        private TextView mTextView;

        public ViewHolder(View view) {
            mTextView = (TextView) view.findViewById(R.id.text);
        }

        public TextView getText() {
            return mTextView;
        }
    }
}
