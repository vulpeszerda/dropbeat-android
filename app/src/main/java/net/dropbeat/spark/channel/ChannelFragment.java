package net.dropbeat.spark.channel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.VolleyError;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.feed.BaseFeedFragment;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.view.MaterialProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 7. 13..
 */
public class ChannelFragment extends BaseFragment implements View
        .OnClickListener, ViewPager.OnPageChangeListener{

    public static final int TAB_FEED = 0;
    public static final int TAB_CHANNELS = 1;

    public static void switchTab(Context context, int tabIdx) {
        Intent intent = new Intent(ACTION_SWITCH_TAB);
        intent.putExtra(PARAM_TAB_IDX, tabIdx);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static final String ACTION_SWITCH_TAB = "net.dropbeat.spark.SWITCH_TAB";
    private static final String PARAM_TAB_IDX = "tab_idx";

    @InjectView(R.id.pager)
    private ViewPager mPager;

    @InjectView(R.id.section_feed)
    private Button mFeedBtn;

    @InjectView(R.id.section_channels)
    private Button mChannelsBtn;

    @InjectView(R.id.channel_contents)
    private View mContentsView;

    @InjectView(R.id.genre_progress)
    private MaterialProgressBar mProgressBar;

    @InjectView(R.id.failure)
    private View mFailureView;

    @InjectView(R.id.failure_msg)
    private TextView mFailureMsgView;

    @InjectView(R.id.retry_button)
    private Button mRetryBtn;

    private PageAdapter mAdapter;
    private int mSelectedTabIdx;

    private boolean mIsInitialized = false;
    private ArrayList<BaseFeedFragment.FilterOption> mGenres;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int tabIdx = intent.getIntExtra(PARAM_TAB_IDX, -1);
            if (tabIdx > -1) {
                if (tabIdx == R.id.section_feed) {
                    mPager.setCurrentItem(TAB_FEED);
                    mSelectedTabIdx = TAB_FEED;
                } else {
                    mPager.setCurrentItem(TAB_CHANNELS);
                    mSelectedTabIdx = TAB_CHANNELS;
                }
                updateSectionTab();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_channel, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Runnable init = new Runnable() {
            @Override
            public void run() {
                mAdapter = new PageAdapter(getBaseActivity(),
                        getChildFragmentManager(), mGenres);
                mPager.setOnPageChangeListener(ChannelFragment.this);
                mPager.setAdapter(mAdapter);
                mSelectedTabIdx = TAB_FEED;
                if (DbAccount.getUser(getBaseActivity()) == null) {
                    mChannelsBtn.setSelected(true);
                    mPager.setCurrentItem(TAB_CHANNELS);
                } else {
                    mFeedBtn.setSelected(true);
                    mPager.setCurrentItem(TAB_FEED);
                }
                mFeedBtn.setOnClickListener(ChannelFragment.this);
                mChannelsBtn.setOnClickListener(ChannelFragment.this);
            }
        };

        if (!mIsInitialized) {
            mIsInitialized = true;
            loadGenres(init);
        } else {
            init.run();
        }

        mRetryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFailureView.setVisibility(View.GONE);
                loadGenres(init);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.isSelected()) {
            return;
        }
        if (v.getId() == R.id.section_feed) {
            mPager.setCurrentItem(TAB_FEED);
            mSelectedTabIdx = TAB_FEED;
        } else {
            mPager.setCurrentItem(TAB_CHANNELS);
            mSelectedTabIdx = TAB_CHANNELS;
        }
        updateSectionTab();
    }

    private void updateSectionTab() {
        if (mSelectedTabIdx == TAB_FEED) {
            mFeedBtn.setSelected(true);
            mChannelsBtn.setSelected(false);
        } else {
            mFeedBtn.setSelected(false);
            mChannelsBtn.setSelected(true);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseActivity())
                .unregisterReceiver(mReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getBaseActivity())
                .registerReceiver(mReceiver, new IntentFilter(ACTION_SWITCH_TAB));
    }

    @Override
    public void onPageSelected(int position) {
        if (DbAccount.getUser(getBaseActivity()) != null) {
            if (position == TAB_FEED) {
                mSelectedTabIdx = TAB_FEED;
            } else {
                mSelectedTabIdx = TAB_CHANNELS;
            }
        } else {
            mSelectedTabIdx = TAB_CHANNELS;
        }
        updateSectionTab();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void loadGenres(final Runnable callback) {
        mProgressBar.setVisibility(View.VISIBLE);
        hideFailure();

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(getBaseActivity(), this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressBar.setVisibility(View.GONE);
                        if (!response.optBoolean("success")) {
                            String errorMsg = "Failed to load genre data.";
                            showFailure(errorMsg);
                            return;
                        }

                        try {
                            mGenres = parseGenreResponse(response);

                            callback.run();
                            showContents();
                        } catch (JSONException e) {
                            String errorMsg = "Failed to load feed data.";
                            showFailure(errorMsg);
                        }
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressBar.setVisibility(View.GONE);
                        String errorMsg = "Failed to load genre data.";
                        if (!TextUtils.isEmpty(msg)) {
                            errorMsg += "\n" + msg;
                        }
                        showFailure(errorMsg);
                        hideContents();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.getFeedGenre(getBaseActivity(), listener, errorListener);
    }

    private void hideFailure() {
        mFailureView.setVisibility(View.GONE);
    }

    private void showFailure(String msg) {
        mFailureView.setVisibility(View.VISIBLE);
        mFailureMsgView.setText(msg);
    }

    private void hideContents() {
        mContentsView.setVisibility(View.GONE);
    }

    private void showContents() {
        mContentsView.setVisibility(View.VISIBLE);
    }

    private ArrayList<BaseFeedFragment.FilterOption> parseGenreResponse(JSONObject response)
            throws JSONException{

        ArrayList<BaseFeedFragment.FilterOption> genres = new ArrayList<>();

        JSONArray channelGenres = response.optJSONArray("channel");
        genres.add(new BaseFeedFragment.FilterOption("all", "ALL"));

        for (int i = 0; i < channelGenres.length(); i++) {
            JSONObject genreObj = channelGenres.getJSONObject(i);
            String name = genreObj.getString("name");
            genres.add(new BaseFeedFragment.FilterOption(name.toLowerCase(),
                    name));
        }

        return genres;
    }

    private static class PageAdapter extends FragmentStatePagerAdapter {

        private Context mContext;
        private ArrayList<BaseFeedFragment.FilterOption> mGenres;

        public PageAdapter(Context context, FragmentManager fm,
                           ArrayList<BaseFeedFragment.FilterOption> genres) {
            super(fm);
            mContext = context;
            mGenres = genres;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new ChannelFeedFragment();
            } else {
                return ChannelListFragment.newInstance(mGenres);
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
