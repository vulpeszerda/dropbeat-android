package net.dropbeat.spark.channel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import net.dropbeat.spark.PlayerActivity;
import net.dropbeat.spark.R;

import roboguice.inject.ContentView;

/**
 * Created by vulpes on 15. 8. 14..
 */
@ContentView(R.layout.activity_player_empty)
public class BookmarkActivity extends PlayerActivity {

    public static Intent createIntent(Context context) {
        return new Intent(context, BookmarkActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionbar = getSupportActionBar();
        if (!isTaskRoot()) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }

        BookmarkListFragment mBookmarkFragment = new BookmarkListFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.container,
                mBookmarkFragment, "bookmark").commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
