package net.dropbeat.spark.channel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.feed.BaseFeedFragment;
import net.dropbeat.spark.model.Channel;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 7. 13..
 */
public class ChannelListFragment extends BaseFragment implements
        ChannelAdapter.OnRowItemClickListener, AdapterView.OnItemSelectedListener{

    public static ChannelListFragment newInstance(
            ArrayList<BaseFeedFragment.FilterOption> options) {

        ChannelListFragment fragment = new ChannelListFragment();
        Bundle args = new Bundle();
        args.putSerializable(PARAM_OPTIONS, options);
        fragment.setArguments(args);

        return fragment;
    }

    private static final String PARAM_OPTIONS = "filter_options";

    @InjectView(android.R.id.list)
    private ListView mListView;

    @InjectView(R.id.progress)
    private View mProgressView;

    @InjectView(R.id.genre_select)
    private Spinner mGenreSelectView;

    @InjectView(R.id.empty)
    private View mEmptyView;

    private ChannelAdapter mChannelAdapter;
    private GenreAdapter mGenreAdapter;
    private Set<String> mBookmarkedIds = new HashSet<>();
    private List<Channel> mChannels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_channel_list, container,
                false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mChannelAdapter == null) {
            DbApplication app = (DbApplication) getBaseActivity()
                    .getApplication();
            mChannelAdapter = new ChannelAdapter(getBaseActivity(),
                    app.getDefaultImageLoader(), this);
        }
        if (mGenreAdapter == null) {
            mGenreAdapter = new GenreAdapter(getBaseActivity());
            Bundle args = getArguments();
            ArrayList<BaseFeedFragment.FilterOption> options =
                    (ArrayList<BaseFeedFragment.FilterOption>) args
                            .getSerializable(PARAM_OPTIONS);
            mGenreAdapter.addAll(options);
        }
        mGenreSelectView.setOnItemSelectedListener(this);
        mGenreSelectView.setAdapter(mGenreAdapter);
        mListView.setAdapter(mChannelAdapter);
        loadBookmarks(true);
    }

    private void loadBookmarks(final boolean loadChannels) {
        if (DbAccount.getUser(getBaseActivity()) == null) {
            return;
        }
        mProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        JSONArray array = response.optJSONArray("bookmark");
                        if (array == null) {
                            return;
                        }

                        mBookmarkedIds.clear();
                        for (int i = 0; i < array.length(); i++) {
                            mBookmarkedIds.add(array.optString(i));
                        }
                        renderChannels();
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (!response.optBoolean("success")) {
                            Toast.makeText(getBaseActivity(),
                                    "Failed to load bookmarks",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        JSONArray array = response.optJSONArray("bookmark");
                        if (array == null) {
                            Toast.makeText(getBaseActivity(),
                                    "Failed to load bookmarks",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mBookmarkedIds.clear();
                        for (int i = 0; i < array.length(); i++) {
                            mBookmarkedIds.add(array.optString(i));
                        }

                        renderChannels();

                        if (loadChannels) {
                            loadChannels("all");
                        }
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errorMsg = "Failed to fetch bookmarks ";
                        if (TextUtils.isEmpty(msg)) {
                            errorMsg += msg;
                        }
                        Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };


        Requests.getBookmarks(getBaseActivity(), responseListener, errorListener);
    }

    private void loadChannels(String genre) {
        mProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (!response.optBoolean("success")) {
                            return;
                        }
                        Gson gson = DbGson.getInstance();
                        Type listType = new TypeToken<List<Channel>>() {}.getType();
                        List<Channel> channels = gson.fromJson(response
                                .optJSONArray("data").toString(), listType);
                        mChannels.clear();
                        mChannels.addAll(channels);
                        renderChannels();
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (!response.optBoolean("success")) {
                            String errorMsg = "Failed to fetch channels ";
                            Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Gson gson = DbGson.getInstance();
                        Type listType = new TypeToken<List<Channel>>() {}.getType();
                        List<Channel> channels = gson.fromJson(response
                                .optJSONArray("data").toString(), listType);
                        mChannels.clear();
                        mChannels.addAll(channels);

                        renderChannels();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errorMsg = "Failed to fetch channels ";
                        if (TextUtils.isEmpty(msg)) {
                            errorMsg += msg;
                        }
                        Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.getChannelList(getBaseActivity(), genre, responseListener,
                errorListener);
    }

    @Override
    public void onClick(int position, View v, Channel item) {
        if (v.getId() == R.id.bookmark) {
            if (DbAccount.getUser(getBaseActivity()) == null) {
                Intent intent = AuthActivity.createIntent(getBaseActivity(), true);
                startActivity(intent);
                return;
            }
            if (item.isBookmarked()) {
                updateBookmark(item.getUid(), false);
            } else {
                updateBookmark(item.getUid(), true);
            }
            return;
        }
        if (v.getId() == R.id.channel) {
            startActivity(ChannelDetailActivity.createIntent(getBaseActivity(),
                    item.getUid(), item.getName()));
            return;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadChannels(mGenreAdapter.getItem(position).getKey());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onVisibilityChanged(boolean isVisible) {
        super.onVisibilityChanged(isVisible);
        if (isVisible && mListView != null && mChannelAdapter != null) {
            loadBookmarks(false);
        }
    }

    private void updateBookmark(final String uid, final boolean isAdd) {
        Set<String> uids = new HashSet<>(mBookmarkedIds);
        if ((isAdd && uids.contains(uid)) ||
                (!isAdd && !uid.contains(uid))) {
            return;
        }
        if (isAdd) {
            uids.add(uid);
        } else {
            uids.remove(uid);
        }

        mProgressView.setVisibility(View.VISIBLE);
        RobustResponseListener<JSONObject> responseListener =
                new RobustResponseListener<JSONObject>(getBaseActivity(),
                        this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (isAdd && !mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.add(uid);
                        } else if (!isAdd && mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.remove(uid);
                        }
                        renderChannels();
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (isAdd && !mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.add(uid);
                        } else if (!isAdd && mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.remove(uid);
                        }
                        renderChannels();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errMsg = "Failed to sync bookmark. ";
                        if (TextUtils.isEmpty(msg)) {
                            errMsg += msg;
                        }
                        Toast.makeText(getBaseActivity(), errMsg,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.putBookmarks(getBaseActivity(), uids,
                responseListener, errorListener);
    }


    private void renderChannels() {
        if (mChannelAdapter == null) {
            return;
        }
        mChannelAdapter.clear();
        for (Channel channel : mChannels) {
            if (mBookmarkedIds.contains(channel.getUid())) {
                channel.setBookmarked(true);
            } else {
                channel.setBookmarked(false);
            }
            mChannelAdapter.add(channel);
        }
        if (getBaseActivity() != null && !isDetached()) {
            mChannelAdapter.notifyDataSetChanged();
            if (mChannelAdapter.getItems().size() > 0) {
                mListView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
            } else {
                mEmptyView.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            }
        }
    }

    private static class GenreAdapter extends ArrayAdapter<BaseFeedFragment
            .FilterOption> {
        private Context mContext;

        public GenreAdapter(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = mInflater.inflate(R.layout.genre_item, parent, false);
            }
            ((TextView)row.findViewById(R.id.text)).setText(getItem(position).getName());
            return row;
        }
    }
}
