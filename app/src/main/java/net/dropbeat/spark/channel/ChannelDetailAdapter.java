package net.dropbeat.spark.channel;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.play.PlayerContext;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vulpes on 15. 5. 1..
 */
public class ChannelDetailAdapter extends ArrayAdapter<Track> {

    private static final int LAYOUT_ID = R.layout.channel_track;

    private TrackClickListener mListener;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mImageOptions;
    private String mPlaylistId;

    public ChannelDetailAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(new ColorDrawable(0x00000000))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_thumb)
                .build();
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }
        final Track track = getItem(position);
        TextView titleView = viewHolder.getTitle();
        titleView.setText(track.getTitle());

        TextView publishedAtView = viewHolder.getPublishedAtView();
        Date publishedAt = track.getCreatedAt();
        if (publishedAt != null) {
            SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
            publishedAtView.setText(formate.format(publishedAt));
            publishedAtView.setVisibility(View.VISIBLE);
        } else {
            publishedAtView.setVisibility(View.GONE);
        }

        viewHolder.getUserNameView().setText(track.getUserName());

        ImageButton menuBtn = viewHolder.getMenuBtn();
        ImageView hoverView = viewHolder.getThumbHover();

        View.OnClickListener listener =new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, track, position);
                }
            }
        };

        row.setOnClickListener(listener);
        menuBtn.setOnClickListener(listener);

        mImageLoader.displayImage(track.getThumbnailUrl(),
                viewHolder.getThumb(), mImageOptions);

        boolean isPlaying;
        isPlaying = PlayerContext.playState == PlayState.LOADING ||
                PlayerContext.playState == PlayState.PLAYING ||
                PlayerContext.playState == PlayState.PAUSED;
        isPlaying &= PlayerContext.currTrack != null &&
                PlayerContext.currTrack.getId().equals(track.getId());
        isPlaying &= PlayerContext.playlistId != null &&
                mPlaylistId != null &&
                PlayerContext.playlistId.equals(mPlaylistId);

        if (isPlaying) {
            titleView.setTypeface(null, Typeface.BOLD);
            titleView.setTextColor(getContext().getResources().getColor
                    (android.R.color.white));
            hoverView.setVisibility(View.VISIBLE);
        } else {
            titleView.setTypeface(null, Typeface.NORMAL);
            titleView.setTextColor(getContext().getResources().getColor
                    (R.color.text_light_gray));
            hoverView.setVisibility(View.GONE);
        }

        return row;
    }

    public void setPlaylistId(String playlistId) {
        mPlaylistId = playlistId;
    }

    public void setTrackClickListener(TrackClickListener listener) {
        mListener = listener;
    }

    private class ViewHolder {
        private TextView mTitleView;
        private TextView mPublishedAtView;
        private TextView mUserNameView;
        private ImageButton mMenuBtn;
        private ImageView mThumbView;
        private ImageView mThumbHoverView;

        public ViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.thumbnail);
            mThumbHoverView = (ImageView) view.findViewById(R.id
                    .thumbnail_hover);
            mTitleView = (TextView) view.findViewById(R.id.title);
            mPublishedAtView = (TextView) view.findViewById(R.id.description);
            mMenuBtn = (ImageButton) view.findViewById(R.id.menu_button);
            mUserNameView = (TextView) view.findViewById(R.id.channel_name);
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public ImageView getThumbHover() {
            return mThumbHoverView;
        }

        public ImageButton getMenuBtn() {
            return mMenuBtn;
        }

        public TextView getPublishedAtView() {
            return mPublishedAtView;
        }

        public TextView getUserNameView() {
            return mUserNameView;
        }
    }


}
