package net.dropbeat.spark.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.dropbeat.spark.BaseFragment;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.R;
import net.dropbeat.spark.StartupActivity;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.model.Channel;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 7. 13..
 */
public class BookmarkListFragment extends BaseFragment implements
        ChannelAdapter.OnRowItemClickListener, View.OnClickListener{

    @InjectView(android.R.id.list)
    private ListView mListView;

    @InjectView(R.id.progress)
    private View mProgressView;

    @InjectView(R.id.need_signin)
    private View mSigninView;

    @InjectView(R.id.facebook_signin_button)
    private View mSigninBtn;

    @InjectView(R.id.empty)
    private View mEmptyView;

    private Map<String, Channel> mChannels = new HashMap<>();
    private ChannelAdapter mChannelAdapter;
    private Set<String> mBookmarkIds = new HashSet<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bookmark_list, container,
                false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mChannelAdapter == null) {
            DbApplication app = (DbApplication) getBaseActivity()
                    .getApplication();
            mChannelAdapter = new ChannelAdapter(getBaseActivity(),
                    app.getDefaultImageLoader(), this);
        }
        mListView.setAdapter(mChannelAdapter);

        if (DbAccount.getUser(getBaseActivity()) != null) {
            mSigninView.setVisibility(View.GONE);
            loadChannels();
        } else {
            mSigninView.setVisibility(View.VISIBLE);
            mSigninBtn.setOnClickListener(this);
        }
    }

    private void loadChannels() {
        mProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (!response.optBoolean("success")) {
                            return;
                        }
                        Gson gson = DbGson.getInstance();
                        Type listType = new TypeToken<List<Channel>>() {}.getType();
                        List<Channel> channels = gson.fromJson(response
                                .optJSONArray("data").toString(), listType);
                        mChannels.clear();
                        for (int i = 0; i < channels.size(); i++) {
                            Channel channel = channels.get(i);
                            channel.setIdx(i);
                            mChannels.put(channel.getUid(), channel);
                        }
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (!response.optBoolean("success")) {
                            String errorMsg = "Failed to fetch channels ";
                            Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Gson gson = DbGson.getInstance();
                        Type listType = new TypeToken<List<Channel>>() {}.getType();
                        List<Channel> channels = gson.fromJson(response
                                .optJSONArray("data").toString(), listType);
                        mChannels.clear();
                        for (int i = 0; i < channels.size(); i++) {
                            Channel channel = channels.get(i);
                            channel.setIdx(i);
                            mChannels.put(channel.getUid(), channel);
                        }
                        loadBookmarks();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errorMsg = "Failed to fetch channels ";
                        if (TextUtils.isEmpty(msg)) {
                            errorMsg += msg;
                        }
                        Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                        mProgressView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.getChannelList(getBaseActivity(), "all", responseListener, errorListener);
    }

    private void loadBookmarks() {
        if (DbAccount.getUser(getBaseActivity()) == null) {
            return;
        }
        mProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                JSONArray array = response.optJSONArray("bookmark");
                if (array == null) {
                    return;
                }

                mBookmarkIds.clear();
                for (int i = 0; i < array.length(); i++) {
                    mBookmarkIds.add(array.optString(i));
                }

                renderChannels();
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mProgressView.setVisibility(View.GONE);
                if (!response.optBoolean("success")) {
                    Toast.makeText(getBaseActivity(),
                            "Failed to load bookmarks",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                JSONArray array = response.optJSONArray("bookmark");
                if (array == null) {
                    Toast.makeText(getBaseActivity(),
                            "Failed to load bookmarks",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                mBookmarkIds.clear();
                for (int i = 0; i < array.length(); i++) {
                    mBookmarkIds.add(array.optString(i));
                }
                renderChannels();
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mProgressView.setVisibility(View.GONE);
                String errorMsg = "Failed to fetch channels ";
                if (TextUtils.isEmpty(msg)) {
                    errorMsg += msg;
                }
                Toast.makeText(getBaseActivity(), errorMsg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };


        Requests.getBookmarks(getBaseActivity(), responseListener, errorListener);
    }

    @Override
    public void onClick(int position, View v, Channel item) {
        if (v.getId() == R.id.bookmark) {
            if (DbAccount.getUser(getBaseActivity()) == null) {
                Intent intent = AuthActivity.createIntent(getBaseActivity(), true);
                startActivity(intent);
                return;
            }
            if (item.isBookmarked()) {
                updateBookmark(item.getUid(), false);
            } else {
                updateBookmark(item.getUid(), true);
            }
            return;
        }
        if (v.getId() == R.id.channel) {
            startActivity(ChannelDetailActivity.createIntent(getBaseActivity(),
                    item.getUid(), item.getName()));
            return;
        }
    }

    private void renderChannels() {
        if (mChannelAdapter == null) {
            return;
        }

        List<Channel> channels = new ArrayList<>();
        for (String id : mBookmarkIds) {
            Channel channel = mChannels.get(id);
            if (channel != null) {
                channel.setBookmarked(true);
                channels.add(channel);
            }
        }

        Collections.sort(channels, new Comparator<Channel>() {
            @Override
            public int compare(Channel lhs, Channel rhs) {
                return lhs.getIdx() - rhs.getIdx();
            }
        });

        mChannelAdapter.clear();
        mChannelAdapter.addAll(channels);

        if (getBaseActivity() != null && !isDetached()) {
            mChannelAdapter.notifyDataSetChanged();
            mProgressView.setVisibility(View.GONE);
            if (channels.size() == 0) {
                mEmptyView.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            } else {
                mListView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
            }
        }
    }

    private void updateBookmark(final String uid, final boolean isAdd) {
        Set<String> uids = new HashSet<>(mBookmarkIds);
        if ((isAdd && uids.contains(uid)) ||
                (!isAdd && !uid.contains(uid))) {
            return;
        }
        if (isAdd) {
            uids.add(uid);
        } else {
            uids.remove(uid);
        }

        mProgressView.setVisibility(View.VISIBLE);
        RobustResponseListener<JSONObject> responseListener =
                new RobustResponseListener<JSONObject>(getBaseActivity(),
                        this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (isAdd && !mBookmarkIds.contains(uid)) {
                            mBookmarkIds.add(uid);
                        } else if (!isAdd && mBookmarkIds.contains(uid)) {
                            mBookmarkIds.remove(uid);
                        }
                        renderChannels();
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (isAdd && !mBookmarkIds.contains(uid)) {
                            mBookmarkIds.add(uid);
                        } else if (!isAdd && mBookmarkIds.contains(uid)) {
                            mBookmarkIds.remove(uid);
                        }
                        renderChannels();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        String errMsg = "Failed to sync bookmark. ";
                        if (TextUtils.isEmpty(msg)) {
                            errMsg += msg;
                        }
                        Toast.makeText(getBaseActivity(), errMsg,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.putBookmarks(getBaseActivity(), uids,
                responseListener, errorListener);
    }

    @Override
    public void onVisibilityChanged(boolean isVisible) {
        super.onVisibilityChanged(isVisible);
        if (isVisible && mListView != null && mChannelAdapter != null) {
            loadBookmarks();
        }
    }

    @Override
    public void onClick(View v) {
        startActivity(AuthActivity.createIntent(getBaseActivity()));
    }
}
