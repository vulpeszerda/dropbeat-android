package net.dropbeat.spark.channel;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.dropbeat.spark.R;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.model.Channel;

/**
 * Created by vulpes on 15. 7. 13..
 */
public class ChannelAdapter extends ArrayAdapter<Channel> {

    public static interface OnRowItemClickListener {
        void onClick(int position, View v, Channel item);
    }

    private static final int LAYOUT_ID = R.layout.channel_item;

    private ImageLoader mImageLoader;
    private DisplayImageOptions mImageOptions;
    private OnRowItemClickListener mListener;

    public ChannelAdapter(Context context, ImageLoader imageLoader,
                          OnRowItemClickListener listener) {
        super(context);
        mListener = listener;
        mImageLoader = imageLoader;
        mImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_thumb)
                .build();
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, parent, false);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        final Channel channel = getItem(position);

        viewHolder.getEmptyView().setVisibility(View.GONE);
        viewHolder.getNonEmptyView().setVisibility(View.VISIBLE);

        viewHolder.getTitle().setText(channel.getName());
        mImageLoader.displayImage(channel.getThumbnail(), viewHolder.getThumb(), mImageOptions);

        if (channel.isBookmarked()) {
            viewHolder.getBookmarkBtn().setImageResource(R.drawable.ic_star_filled);
        } else {
            viewHolder.getBookmarkBtn().setImageResource(R.drawable.ic_star);
        }

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position, v, channel);
                }
            }
        });

        viewHolder.getBookmarkBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position, v, channel);
                }
            }
        });

        return row;
    }

    private class ViewHolder {
        private TextView mTitleView;
        private ImageButton mBookmarkBtn;
        private ImageView mThumbView;
        private View mEmptyView;
        private View mNonEmptyView;

        public ViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.thumbnail);
            mTitleView = (TextView) view.findViewById(R.id.channel_name);
            mBookmarkBtn = (ImageButton) view.findViewById(R.id.bookmark);
            mEmptyView = view.findViewById(R.id.empty);
            mNonEmptyView = view.findViewById(R.id.nonempty);
        }

        public View getEmptyView() {
            return mEmptyView;
        }

        public View getNonEmptyView() {
            return mNonEmptyView;
        }

        public TextView getTitle() {
            return mTitleView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public ImageButton getBookmarkBtn() {
            return mBookmarkBtn;
        }
    }
}
