package net.dropbeat.spark.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.feed.BaseFeedFragment;
import net.dropbeat.spark.feed.FeedAdapter;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 14..
 */
public class ChannelFeedFragment extends BaseFeedFragment {

    @InjectView(R.id.need_signin)
    private View mNeedSigninView;

    @InjectView(R.id.no_bookmark)
    private View mNoBookmarkView;

    @InjectView(R.id.channel_feed)
    private View mChannelFeed;

    @InjectView(R.id.facebook_signin_button)
    private View mSigninBtn;

    @InjectView(R.id.channel_button)
    private View mChannelBtn;

    private List<String> mBookmarkIds = new ArrayList<>();
    private View mBookmarkHeader;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mChannelBtn.setOnClickListener(this);
        mSigninBtn.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_channel_feed, container,
                false);
    }

    @Override
    protected List<Track> parseResponse(JSONObject response) throws JSONException {
        Gson gson = DbGson.getInstance();
        if (!response.optBoolean("success")) {
            return null;
        }

        List<Track> tracks = new ArrayList<>();

        JSONArray data = response.getJSONArray("data");

        Type listType = new TypeToken<ArrayList<ChannelTrack>>() {}.getType();
        List<ChannelTrack> channelTracks = gson.fromJson(data.toString(),
                listType);

        for(ChannelTrack t : channelTracks) {
            tracks.add(t.toTrack());
        }
        return tracks;
    }

    @Override
    protected String getSectionKey() {
        return "channel_feed";
    }

    @Override
    protected void requestToApi(FilterOption option, int pageIdx,
                                boolean forceRefresh,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
        Requests.channelFeed(getBaseActivity(), pageIdx, listener, errorListener);
    }

    @Override
    protected void updateFeedTrackView(FeedAdapter.ViewHolder holder,
                                       Track track, int position) {
        super.updateFeedTrackView(holder, track, position);
        TextView channelView = holder.getChannelView();
        channelView.setVisibility(View.VISIBLE);
        channelView.setText(track.getUserName());

        TextView descView = holder.getDescView();
        descView.setVisibility(View.VISIBLE);
        descView.setText(track.getDesc());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Intent intent;
        switch(v.getId()) {
            case R.id.facebook_signin_button:
                intent = AuthActivity.createIntent(getBaseActivity(), false);
                startActivity(intent);
                break;
            case R.id.channel_button:
                ChannelFragment.switchTab(getBaseActivity(),
                        ChannelFragment.TAB_CHANNELS);
                break;
            case R.id.manage_bookmark_button:
                intent = BookmarkActivity.createIntent(getBaseActivity());
                startActivity(intent);
        }
    }

    @Override
    protected boolean hasPagenation() {
        return true;
    }

    @Override
    protected String getPlaylistName() {
        return "Channel Feed";
    }

    @Override
    protected int getTrackLayoutId() {
        return R.layout.channel_track;
    }

    @Override
    protected boolean hasFilterOptions() {
        return false;
    }

    @Override
    public void onVisibilityChanged(boolean isVisible) {
        super.onVisibilityChanged(isVisible);
        if (isVisible && mDidInitialLoad) {
            refresh();
        }
    }

    @Override
    protected void refresh() {
        if (DbAccount.getUser(getBaseActivity()) == null) {
            mNeedSigninView.setVisibility(View.VISIBLE);
            mChannelFeed.setVisibility(View.GONE);
            updateBookmarkHeader();
        } else {
            loadBookmark(new Runnable() {
                @Override
                public void run() {
                    updateBookmarkHeader();
                    ChannelFeedFragment.super.refresh();
                }
            });
        }
    }

    private void loadBookmark(final Runnable callback) {
        mFailureView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> listener =
                new RobustResponseListener<JSONObject>(getBaseActivity(), this) {
            @Override
            public void onSkipped(JSONObject response) {
                mDidInitialLoad = true;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mProgressBar.setVisibility(View.GONE);
                JSONArray array = response.optJSONArray("bookmark");
                if (array == null) {
                    showFailure("Failed to load bookmark info.");
                    return;
                }

                mBookmarkIds.clear();
                for (int i = 0; i < array.length(); i++) {
                    mBookmarkIds.add(array.optString(i));
                }
                if (mBookmarkIds.size() == 0) {
                    mNoBookmarkView.setVisibility(View.VISIBLE);
                    mChannelFeed.setVisibility(View.GONE);
                    mNeedSigninView.setVisibility(View.GONE);
                    mDidInitialLoad = true;
                    updateBookmarkHeader();
                    return;
                }
                mNoBookmarkView.setVisibility(View.GONE);
                mNeedSigninView.setVisibility(View.GONE);
                mChannelFeed.setVisibility(View.VISIBLE);
                callback.run();
            }
        };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mProgressBar.setVisibility(View.GONE);
                mDidInitialLoad = true;
                String errorMsg = "Failed to load bookmark info.";
                if (!TextUtils.isEmpty(msg)) {
                    errorMsg += "\n" + msg;
                }
                showFailure(errorMsg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                mDidInitialLoad = true;
            }
        };

        Requests.getBookmarks(getBaseActivity(), listener, errorListener);
    }

    private void showFailure(String msg) {
        mFailureView.setVisibility(View.VISIBLE);
        mNoBookmarkView.setVisibility(View.GONE);
        mNeedSigninView.setVisibility(View.GONE);
        mChannelFeed.setVisibility(View.GONE);
        mFailureMsgView.setText(msg);
    }

    private void updateBookmarkHeader() {
        if (mBookmarkIds.size() > 0) {
            if (mBookmarkHeader == null) {
                mBookmarkHeader = mLayoutInflater.inflate(R.layout.channel_feed_header, null, false);
                Button manageBtn = (Button) mBookmarkHeader.findViewById(R.id.manage_bookmark_button);
                manageBtn.setOnClickListener(this);
            }
            addHeaderView(mBookmarkHeader);
        } else {
            removeHeaderView(mBookmarkHeader);
        }
    }

    private static class ChannelTrack {
        String channelTitle;
        String publishedAt;
        String title;
        String videoId;

        public Track toTrack() {
            String desc = null;
            if (!TextUtils.isEmpty(publishedAt) && publishedAt.length() >= 10) {
                desc = publishedAt.substring(0, 10);
            }

            String thumbnailUrl = "http://img.youtube.com/vi/" + videoId +
                        "/mqdefault.jpg";
            String hqThumbnailUrl = "http://img.youtube.com/vi/" + videoId +
                        "/hqdefault.jpg";

            Track track = new Track();
            track.setTitle(title);
            track.setId(videoId);
            track.setType("youtube");
            track.setDesc(desc);
            track.setDrop(null);
            track.setThumbnailUrl(thumbnailUrl);
            track.setUserName(channelTitle);
            track.setHqThumbnailUrl(hqThumbnailUrl);
            return track;
        }
    }
}
