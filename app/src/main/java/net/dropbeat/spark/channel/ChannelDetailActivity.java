package net.dropbeat.spark.channel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.PlayerActivity;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.network.Path;
import net.dropbeat.spark.playlist.PlaylistFragment;
import net.dropbeat.spark.R;
import net.dropbeat.spark.TrackClickListener;
import net.dropbeat.spark.addon.ArrayAdapter;
import net.dropbeat.spark.addon.EndlessScrollListener;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.utils.DbGson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 7. 14..
 */
@ContentView(R.layout.activity_channel_detail)
public class ChannelDetailActivity extends PlayerActivity implements
        AdapterView.OnItemSelectedListener, TrackClickListener, View.OnClickListener {

    public static Intent createIntent(Context context, String uid,
                                      String name) {
        Intent intent = new Intent(context, ChannelDetailActivity.class);
        intent.putExtra(PARAM_UID, uid);
        intent.putExtra(PARAM_NAME, name);
        return intent;
    }

    private static final String PARAM_UID = "uid";
    private static final String PARAM_NAME = "name";
    private static final String GOOGLE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @InjectView(R.id.loading_progress)
    private View mProgressView;

    @InjectView(android.R.id.list)
    private ListView mListView;

    @InjectView(R.id.channel_name)
    private TextView mChannelNameView;

    @InjectView(R.id.genre)
    private TextView mGenreView;

    @InjectView(R.id.thumbnail)
    private ImageView mThumbnailView;

    @InjectView(R.id.bookmark)
    private ImageView mBookmarkView;

    @InjectView(R.id.playlist_select)
    private Spinner mPlaylistSelect;

    @InjectView(R.id.channel_detail)
    private View mChannelDetailView;

    private View mFooterProgressView;

    private ChannelDetail mChannel;
    private ChannelDetailAdapter mAdapter;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mImageOptions;
    private ChannelPlaylistAdapter mPlaylistAdapter;

    private String mPageToken = null;
    private ChannelPlaylist mCurrentPlaylist = null;
    private boolean mIsLoading = false;
    private boolean mIsListEnd = false;
    private String mPrevPageToken = null;
    private String mPrevPlaylistId = null;
    private View mFooterView;
    private Set<String> mBookmarkedIds = new HashSet<>();

    private BroadcastReceiver mPlayStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isFinishing() &&
                    (PlayerContext.playState == PlayState.LOADING ||
                    PlayerContext.playState == PlayState.STOPPED) &&
                    mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionbar = getSupportActionBar();
        if (!isTaskRoot()) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }

        actionbar.setTitle(getString(R.string.title_channel));

        DbApplication app = (DbApplication) getApplication();
        mImageLoader = app.getDefaultImageLoader();
        mImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.default_thumb)
                .build();

        loadBookmarks();
        initChannelView();
    }

    private void loadBookmarks() {
        if (DbAccount.getUser(this) == null) {
            return;
        }
        mProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        JSONArray array = response.optJSONArray("bookmark");
                        if (array == null) {
                            return;
                        }

                        mBookmarkedIds.clear();
                        for (int i = 0; i < array.length(); i++) {
                            mBookmarkedIds.add(array.optString(i));
                        }
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (!response.optBoolean("success")) {
                            Toast.makeText(ChannelDetailActivity.this,
                                    "Failed to load bookmarks",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        JSONArray array = response.optJSONArray("bookmark");
                        if (array == null) {
                            Toast.makeText(ChannelDetailActivity.this,
                                    "Failed to load bookmarks",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mBookmarkedIds.clear();
                        for (int i = 0; i < array.length(); i++) {
                            mBookmarkedIds.add(array.optString(i));
                        }
                        refreshBookmarkIcon();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errorMsg = "Failed to fetch channels ";
                        if (TextUtils.isEmpty(msg)) {
                            errorMsg += msg;
                        }
                        Toast.makeText(ChannelDetailActivity.this, errorMsg,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };


        Requests.getBookmarks(this, responseListener, errorListener);
    }

    private void initChannelView() {
        String uid = getIntent().getStringExtra(PARAM_UID);

        if (mChannel == null) {
            loadChannelInfo(uid);
        } else {
            initHeaderView(mChannel);
        }
        mAdapter = new ChannelDetailAdapter(this, mImageLoader);
        mAdapter.setTrackClickListener(this);

        mPlaylistAdapter = new ChannelPlaylistAdapter(this);
        mPlaylistSelect.setAdapter(mPlaylistAdapter);
        mPlaylistSelect.setOnItemSelectedListener(this);


        if (mFooterView == null) {
            mFooterView = LayoutInflater.from(this).inflate(R.layout
                    .loading_footer, null);
            mFooterProgressView = mFooterView.findViewById(R.id
                    .loading_progress_wrapper);
        }
        if (mListView.getFooterViewsCount() == 0) {
            mListView.addFooterView(mFooterView);
        }
        mFooterProgressView.setVisibility(View.GONE);

        mListView.setAdapter(mAdapter);
        mListView.setOnScrollListener(new EndlessScrollListener() {

            @Override
            public void onLoadMore(String page, int totalItemsCount) {
                if (page != null) {
                    loadPlaylist(mCurrentPlaylist, page);
                }
            }

            @Override
            public String getNextPage() {
                return mPageToken;
            }
        });

        mBookmarkView.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ChannelPlaylist playlist = mPlaylistAdapter.getItem(position);

        mPageToken = null;
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();

        mCurrentPlaylist = playlist;
        mPrevPageToken = null;
        mPrevPlaylistId = null;
        mIsListEnd = false;

        mAdapter.setPlaylistId(getPlaylistId());
        loadPlaylist(playlist);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View view, final Track track, int position) {
        switch(view.getId()) {
            case R.id.track:
                ArrayList<Track> tracks = new ArrayList<>(mAdapter.getItems());
                Playlist playlist = new Playlist(getPlaylistId(),
                        getPlaylistName(), tracks);
                PlayerContext.externalPlaylist = playlist;
                PlayerService.doPlay(this, track, playlist.getId());
                break;
            case R.id.menu_button:
                PopupMenu menu = new PopupMenu(this, view);
                menu.getMenuInflater().inflate(R.menu.menu_addable_track, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onTrackPopupMenuClicked(track, item);
                    }
                });
                menu.show();
                break;
        }
    }

    private String getPlaylistId() {
        if (mChannel != null && mCurrentPlaylist != null) {
            return "channel_feed_" + mChannel.uid + "_" + mCurrentPlaylist.uid;
        }
        return null;
    }

    private String getPlaylistName() {
        if (mChannel != null && mCurrentPlaylist != null) {
            return mChannel.channelName + " - " + mCurrentPlaylist.title;
        }
        return null;
    }

    private boolean onTrackPopupMenuClicked(Track track, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                PlaylistFragment.addTrack(this, track, "channel");
                return true;
            case R.id.action_share:
                track.share(this);
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.bookmark:
                if (DbAccount.getUser(this) == null) {
                    Intent intent = AuthActivity.createIntent(this, true);
                    startActivity(intent);
                    return;
                }
                if (mChannel == null) {
                    return;
                }
                if (mBookmarkedIds.contains(mChannel.uid)) {
                    updateBookmark(mChannel.uid, false);
                } else {
                    updateBookmark(mChannel.uid, true);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter =
                new IntentFilter(PlayerService.ACTION_NOTIFY_PLAYSTATE_CHANGE);
        manager.registerReceiver(mPlayStateChangeReceiver, filter);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        if (mChannel != null && !isFinishing()) {
            loadBookmarks();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.unregisterReceiver(mPlayStateChangeReceiver);
    }

    private void loadChannelInfo(final String uid) {
        mProgressView.setVisibility(View.VISIBLE);
        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mProgressView.setVisibility(View.GONE);
                if (!response.optBoolean("success")) {
                    String errorMsg = "Failed to fetch channel info";
                    Toast.makeText(ChannelDetailActivity.this, errorMsg,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = DbGson.getInstance();
                mChannel = gson.fromJson(
                        response.optJSONObject("data").toString(),
                        ChannelDetail.class);
                mChannel.uid = uid;
                initHeaderView(mChannel);
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mProgressView.setVisibility(View.GONE);
                String errorMsg = "Failed to fetch channel info";
                if (!TextUtils.isEmpty(msg)) {
                    errorMsg += " " + msg;
                }
                Toast.makeText(ChannelDetailActivity.this, errorMsg,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        Requests.getChannelDetail(this, uid, responseListener, errorListener);
    }

    private void initHeaderView(ChannelDetail channel) {
        mImageLoader.displayImage(channel.channelThumbnail, mThumbnailView, mImageOptions);
        mChannelNameView.setText(channel.channelName);
        mGenreView.setText(TextUtils.join(", ", channel.genre));
        mChannelDetailView.setVisibility(View.VISIBLE);
        mPlaylistSelect.setVisibility(View.VISIBLE);

        refreshBookmarkIcon();

        mPlaylistAdapter.clear();

        ChannelPlaylist uploads = new ChannelPlaylist();
        uploads.title = "RECENT";
        uploads.uid = channel.uploads;

        mPlaylistAdapter.add(uploads);
        mPlaylistAdapter.addAll(channel.playlist);
        mPlaylistAdapter.notifyDataSetChanged();
    }

    private void refreshBookmarkIcon() {
        if (mChannel == null || isFinishing()) {
            return;
        }
        if (mBookmarkedIds.contains(mChannel.uid)) {
            mBookmarkView.setImageResource(R.drawable.ic_star_filled);
        } else {
            mBookmarkView.setImageResource(R.drawable.ic_star);
        }
    }

    private void updateBookmark(final String uid, final boolean isAdd) {
        Set<String> uids = new HashSet<>(mBookmarkedIds);
        if ((isAdd && uids.contains(uid)) ||
                (!isAdd && !uid.contains(uid))) {
            return;
        }
        if (isAdd) {
            uids.add(uid);
        } else {
            uids.remove(uid);
        }

        mProgressView.setVisibility(View.VISIBLE);
        RobustResponseListener<JSONObject> responseListener =
                new RobustResponseListener<JSONObject>(this) {
                    @Override
                    public void onSkipped(JSONObject response) {
                        if (isAdd && !mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.add(uid);
                        } else if (!isAdd && mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.remove(uid);
                        }
                        refreshBookmarkIcon();
                    }

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mProgressView.setVisibility(View.GONE);
                        if (isAdd && !mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.add(uid);
                        } else if (!isAdd && mBookmarkedIds.contains(uid)) {
                            mBookmarkedIds.remove(uid);
                        }
                        refreshBookmarkIcon();
                    }
                };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mProgressView.setVisibility(View.GONE);
                        String errMsg = "Failed to sync bookmark. ";
                        if (TextUtils.isEmpty(msg)) {
                            errMsg += msg;
                        }
                        Toast.makeText(ChannelDetailActivity.this, errMsg,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                };

        Requests.putBookmarks(this, uids, responseListener, errorListener);
    }

    private void loadPlaylist(ChannelPlaylist playlist) {
        loadPlaylist(playlist, mPageToken);
    }

    private void loadPlaylist(ChannelPlaylist playlist, String pageToken) {
        String uid = playlist.uid;
        if (mIsLoading || mIsListEnd ||
                (mPrevPlaylistId != null &&
                 uid.equals(mPrevPlaylistId) &&
                    ((mPrevPageToken == null && pageToken == null) ||
                    (mPrevPageToken != null && mPrevPageToken.equals(pageToken))))) {
            return;
        }
        mIsLoading = true;
        mPrevPageToken = pageToken;
        mPrevPlaylistId = uid;

        mFooterProgressView.setVisibility(View.VISIBLE);

        RobustResponseListener<JSONObject> responseListener = new
                RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsLoading = false;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mIsLoading = false;
                JSONArray items = response.optJSONArray("items");
                if (items == null) {
                    mFooterProgressView.setVisibility(View.GONE);
                    String errMsg = "Failed to load channel info.";
                    Toast.makeText(ChannelDetailActivity.this, errMsg,
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                mPageToken = response.optString("next_page_token");
                if (TextUtils.isEmpty(mPageToken)) {
                    mIsListEnd = true;
                    mPageToken = null;
                    mFooterProgressView.setVisibility(View.GONE);
                }

                List<Track> tracks = new ArrayList<>();
                for (int i = 0; i < items.length(); i++) {
                    JSONObject item = items.optJSONObject(i);
                    if (item == null) {
                        continue;
                    }
                    Track track = Track.fromJsonObject(item);
                    if (track != null) {
                        tracks.add(track);
                    }
                }

                Collections.sort(tracks, new Comparator<Track>() {
                    @Override
                    public int compare(Track lhs, Track rhs) {
                        Date lhsPublishedAt = lhs.getCreatedAt();
                        Date rhsPublishedAt = rhs.getCreatedAt();
                        if (lhsPublishedAt == null && rhsPublishedAt == null) {
                            return 0;
                        }
                        if (lhsPublishedAt == null && rhsPublishedAt != null) {
                            return 1;
                        }
                        if (lhsPublishedAt != null && rhsPublishedAt == null) {
                            return -1;
                        }
                        return rhsPublishedAt.compareTo(lhsPublishedAt);
                    }
                });

                mAdapter.addAll(tracks);
                mAdapter.notifyDataSetChanged();

                if (PlayerContext.externalPlaylist != null && PlayerContext
                        .externalPlaylist.getId().equals(getPlaylistId())) {

                    Playlist playlist = PlayerContext.externalPlaylist;
                    playlist.addAll(new ArrayList<>(mAdapter.getItems()), true);

                    if (PlayerContext.playlistId != null &&
                            PlayerContext.playlistId.equals(getPlaylistId()) &&
                            PlayerContext.currTrack != null) {

                        Track currTrack = PlayerContext.currTrack;
                        List<Track> allTracks = playlist.getTracks();
                        for (int i = 0; i < allTracks.size(); i++) {
                            if (allTracks.get(i).getId().equals(currTrack
                                    .getId())) {
                                PlayerContext.trackIdx = i;
                                break;
                            }
                        }
                    }
                }
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                mFooterProgressView.setVisibility(View.GONE);
                mIsLoading = false;
                String errMsg = "Failed to load channel info.";
                if (!TextUtils.isEmpty(msg)) {
                    errMsg += " " + msg;
                }
                Toast.makeText(ChannelDetailActivity.this, errMsg,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {
                mIsLoading = false;
            }
        };

        Requests.getChannelPlaylistItem(this, uid, pageToken, responseListener,
                errorListener);
    }

    private static class ChannelDetail {
        String uid;
        String channelName;
        String channelThumbnail;
        List<String> genre;
        String uploads;
        List<ChannelPlaylist> playlist;
    }

    private static class ChannelPlaylist {
        String uid;
        String title;
    }

    private static class ChannelPlaylistAdapter extends
            ArrayAdapter<ChannelPlaylist> {

        public ChannelPlaylistAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = mInflater.inflate(R.layout.genre_item, parent, false);
            }
            ((TextView)row.findViewById(R.id.text)).setText(getItem(position).title);
            return row;
        }
    }

}
