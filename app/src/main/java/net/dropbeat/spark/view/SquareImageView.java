package net.dropbeat.spark.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import net.dropbeat.spark.R;


public class SquareImageView extends ImageView {
    public static enum FixedAlong {
        width, height
    }

    private FixedAlong fixedAlong = FixedAlong.width;

    public SquareImageView(Context context) {
        this(context, null);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.SquareImageView, defStyle, 0);
        String fixedAlongAttr = array
                .getString(R.styleable.SquareImageView_fixedAlong);
        fixedAlong = fixedAlongAttr != null ? FixedAlong
                .valueOf(fixedAlongAttr) : FixedAlong.width;
        array.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int h = this.getMeasuredHeight();
        int w = this.getMeasuredWidth();
        int curSquareDim = fixedAlong == FixedAlong.width ? w : h;

        setMeasuredDimension(curSquareDim, curSquareDim);
    }
}
