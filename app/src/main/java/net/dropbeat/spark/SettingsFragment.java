package net.dropbeat.spark;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;
import android.support.v4.preference.PreferenceFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.play.PlayerContext;

/**
 * Created by vulpes on 15. 5. 2..
 */
public class SettingsFragment extends PreferenceFragment {

    private static final String TAG_PROGRESS_DIALOG_SIGNOUT = "signout";

    private DbAccount mAccount;

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);

        setRetainInstance(true);

        mAccount = DbAccount.getUser(getActivity());
        if (mAccount != null) {
            addPreferencesFromResource(R.xml.preferences);
        } else {
            addPreferencesFromResource(R.xml.preferences_nonauth);
        }

        // set version code
        Preference versionPref = findPreferenceByKeyResId(R.string
                .preference_key__version);
        String version = getString(R.string.app_version);
        if (BuildConfig.DEBUG) {
            version += "(DEBUG)";
        }
        versionPref.setSummary(version);

        // set account row info
        mAccount = DbAccount.getUser(getActivity());
        if (mAccount != null) {
            Preference accountPref = findPreferenceByKeyResId(R.string
                    .preference_key__email);
            accountPref.setSummary(mAccount.getEmail());
        }
    }

    @Override
    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        View view = super.onCreateView(paramLayoutInflater, paramViewGroup,
                paramBundle);
        view.setBackgroundColor(getResources().getColor(android.R.color.white));
        return view;
    }

    private Preference findPreferenceByKeyResId(int resId) {
        return getPreferenceManager().findPreference(getString(resId));
    }

    private boolean isEqualKey(String key, int resourceId) {
        return TextUtils.equals(key, getString(resourceId));
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                                         Preference preference) {
        String key = preference.getKey();
        if (isEqualKey(key, R.string.preference_key__signout)) {
            showSignoutConfirmDialog();
            return true;
        } else if (isEqualKey(key, R.string.preference_key__copyright)) {
            startActivity(CopyrightActivity.createIntent(getActivity()));
            return true;
        } else if (isEqualKey(key, R.string.preference_key__feedback)) {
            startActivity(FeedbackActivity.createIntent(getActivity()));
            return true;
        }
        if (preference instanceof PreferenceScreen) {
            initializeActionBar((PreferenceScreen) preference);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @SuppressLint("NewApi")
    private void initializeActionBar(PreferenceScreen screen) {
        final Dialog dialog = screen.getDialog();
        if (dialog == null
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return;
        }

        dialog.getActionBar().setDisplayHomeAsUpEnabled(true);
        View homeBtn = dialog.findViewById(android.R.id.home);
        if (homeBtn != null) {
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            };
            ViewParent homeBtnContainer = homeBtn.getParent();
            if (homeBtnContainer instanceof FrameLayout) {
                ViewGroup containerParent = (ViewGroup) homeBtnContainer
                        .getParent();

                if (containerParent instanceof LinearLayout) {
                    ((LinearLayout) containerParent)
                            .setOnClickListener(listener);
                } else {
                    ((FrameLayout) homeBtnContainer)
                            .setOnClickListener(listener);
                }
            } else {
                homeBtn.setOnClickListener(listener);
            }
        }
    }

    private void showSignoutConfirmDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_are_you_sure)
                .setMessage(R.string.desc_confirm_signout)
                .setNegativeButton(R.string.button_no, null)
                .setPositiveButton(R.string.button_yes,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                processSignout();
                            }
                        }).show();
    }

    private void processSignout() {
        final BaseActivity activity = (BaseActivity) getActivity();
        ProgressDialogFragment.show((BaseActivity) getActivity(),
                TAG_PROGRESS_DIALOG_SIGNOUT, R.string.desc_wait_signout);
        DbAccount.signOut(getActivity(),
                new DbAccount.OnSignoutFinishedListener() {

                    @Override
                    public void onFinished() {
                        PlayerContext.reset();
                        ProgressDialogFragment.hide(
                                (BaseActivity) getActivity(),
                                TAG_PROGRESS_DIALOG_SIGNOUT);
                        activity.finish();
                        startActivity(new Intent(StartupActivity
                                .createIntent(getActivity())));

                    }
                });
    }
}
