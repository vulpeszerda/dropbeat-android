package net.dropbeat.spark.play;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;

import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;

/**
 * Created by vulpes on 15. 5. 1..
 */

public class RemoteControlReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_MEDIA_BUTTON.equals(action)) {
            KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return;
            }
            switch(event.getKeyCode()) {
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    if (PlayerContext.playState == PlayState.PLAYING) {
                        PlayerService.doPause(context);
                    } else {
                        PlayerService.doPlay(context, PlayerContext.currTrack, PlayerContext.playlistId);
                    }
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    PlayerService.doPause(context);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    PlayerService.doPlay(context, PlayerContext.currTrack, PlayerContext.playlistId);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    PlayerService.doBackward(context);
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    PlayerService.doForward(context);
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    PlayerService.doStop(context);
                    break;
            }
        } else if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(action)) {
            // pause when app become noisy
            PlayerService.doPause(context);
        }
    }
}
