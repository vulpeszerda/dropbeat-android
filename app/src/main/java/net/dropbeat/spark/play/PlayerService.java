package net.dropbeat.spark.play;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer.extractor.webm.WebmExtractor;
import com.google.android.exoplayer.util.Util;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.dropbeat.spark.BuildConfig;
import net.dropbeat.spark.DbApplication;
import net.dropbeat.spark.NavigationDrawerActivity;
import net.dropbeat.spark.R;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.StreamSource;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.UserTrack;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.utils.DbLog;
import net.dropbeat.spark.utils.DeviceUuidFactory;

import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import main.java.com.mindscapehq.android.raygun4android.messages.RaygunUserInfo;

/**
 * Created by parkilsu on 15. 4. 28..
 */
public class PlayerService extends Service implements AtomicMediaPlayer.Listener,
        AudioFocusHelper.MusicFocusable, AudioCapabilitiesReceiver.Listener {


    private static final String TAG = "PlayerService";

    // Broadcasting actions
    public static final String ACTION_NOTIFY_PLAYSTATE_CHANGE =
            "net.dropbeat.spark.NOTIFY_PLAYSTATE_CHANGE";
    public static final String ACTION_NOTIFY_PROGRESS_CHANGE =
            "net.dropbeat.spark.NOTIFY_PROGRESS_CHANGE";

    public static final String PARAM_TRACK = "track";
    public static final String PARAM_PLAYLIST_ID = "playlist_id";
    public static final String PARAM_SEEK_POSITION = "seek_position";
    public static final String PARAM_CANCEL_NOTI = "cancel_noti";

    // Receiving actions
    public static final String ACTION_PLAY = "net.dropbeat.spark.PLAY";
    public static final String ACTION_PAUSE = "net.dropbeat.spark.PAUSE";
    public static final String ACTION_SEEK = "net.dropbeat.spark.SEEK";
    public static final String ACTION_FORWARD = "net.dropbeat.spark.FORWARD";
    public static final String ACTION_BACKWARD = "net.dropbeat.spark.BACKWARD";
    public static final String ACTION_STOP = "net.dropbeat.spark.STOP";
    public static final String ACTION_UPDATE_REPEAT_STATE =
            "net.dropbeat.spark.UPDATE_REPEAT_STATE";


    private static final long PROGRESS_UPDATE_PERIOD = 500;

    public static boolean isRunning(Context context) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (PlayerService.class.getName().equals(
                    service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void doPlay(Context context, Track track, String playlistId) {
        Intent playIntent = new Intent(context, PlayerService.class);
        playIntent.setAction(PlayerService.ACTION_PLAY);
        playIntent.putExtra(PlayerService.PARAM_PLAYLIST_ID, playlistId);
        if (track != null) {
            playIntent.putExtra(PlayerService.PARAM_TRACK, track);
        } else {
            playIntent.putExtra(PlayerService.PARAM_TRACK, PlayerContext.currTrack);
        }
        context.startService(playIntent);
    }

    public static void doPause(Context context) {
        Intent pauseIntent = new Intent(context, PlayerService.class);
        pauseIntent.setAction(PlayerService.ACTION_PAUSE);
        context.startService(pauseIntent);
    }

    public static void doSeek(Context context, int position) {
        Intent seekIntent = new Intent(context, PlayerService.class);
        seekIntent.setAction(PlayerService.ACTION_SEEK);
        seekIntent.putExtra(PARAM_SEEK_POSITION, position);
        context.startService(seekIntent);
    }

    public static void doBackward(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(PlayerService.ACTION_BACKWARD);
        context.startService(intent);
    }

    public static void doForward(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(PlayerService.ACTION_FORWARD);
        context.startService(intent);
    }

    public static void doStop(Context context) {
        doStop(context, false);
    }

    public static void doStop(Context context, boolean cancelNoti) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(PlayerService.ACTION_STOP);
        intent.putExtra(PARAM_CANCEL_NOTI, cancelNoti);
        context.startService(intent);
    }

    public static void doUpdateRepeatState(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(PlayerService.ACTION_UPDATE_REPEAT_STATE);
        context.startService(intent);
    }

    private BroadcastReceiver mAuthReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!BuildConfig.DEBUG || true) {
                RaygunClient.Init(getApplicationContext());
                RaygunClient.AttachExceptionHandler();

                DeviceUuidFactory factory = new DeviceUuidFactory(context);
                RaygunUserInfo user = new RaygunUserInfo();
                DbAccount account = DbAccount.getUser(context);
                if (account != null) {
                    user.Identifier = account.getEmail();
                    user.FullName = account.getFullName();
                    user.Email = account.getEmail();
                    user.Uuid = factory.getDeviceUuid().toString();
                    user.IsAnonymous = false;
                } else {
                    user.Uuid = factory.getDeviceUuid().toString();
                    user.IsAnonymous = true;
                }
                RaygunClient.SetUser(user);
            }
        }
    };

    private AtomicMediaPlayer mPlayer;

    // Player states.
    private String mCurrentUid;
    private StreamSource mCurrStreamSource;
    private AtomicBoolean mIsFirstProgress = new AtomicBoolean(true);
    private HandlerThread mPlayerHandlerThread;
    private Handler mPlayerHandler;
    private Handler mUiHandler;
    private AudioFocusHelper mAudioFocusHelper;
    private AudioCapabilities mAudioCapabilities;
    private AudioCapabilitiesReceiver mAudioCapabilitiesReceiver;
    private int mLastPlayerPosition = 0;

    private LocalBroadcastManager mBroadcastManager;
    private Timer mProgressUpdateTimer;
    private Object mProgressPublishLock = new Object();
    private int mPrevPlayState = -1;
    private List<StreamSource> mStreamCandidateUrls = new ArrayList<>();

    private boolean mAuthReceiverRegistered = false;

    private MediaSession mMediaSession;
    private ComponentName mMediaButtonReceiverComponent;
    private RemoteControlClient mRemoteControlClient;
    private boolean mMediaButtonReceiverBound;
    private boolean mRemoteControlClientBound;


    private void loadMusic(final Track track, final StreamSource source) {
        mCurrStreamSource = source;
        mPlayerHandler.post(new Runnable() {
            @Override
            public void run() {

                try {
                    // Play a new track.
                    mPlayer.load(track, mCurrStreamSource);
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // Logging music play
                            // to our server
                            Requests.logPlay(PlayerService.this,
                                    track.getTitle(),
                                    track.getId());

                            if (DbAccount.getUser(PlayerService.this) == null) {
                                return;
                            }

                            // to GA
                            DbApplication app = (DbApplication)getApplication();
                            Tracker tracker = app.getAnalyticsTracker();
                            tracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("player-play-from-android")
                                    .setAction("play-" + track.getType())
                                    .setLabel(track.getTitle()).build());
                        }
                    });
                } catch (IllegalStateException e) {
                    // Unrecoverable
                    e.printStackTrace();
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updatePlayStateChange(PlayState.STOPPED);
                        }
                    });
                } catch (IOException e) {
                    // Unrecoverable
                    e.printStackTrace();
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updatePlayStateChange(PlayState.STOPPED);
                        }
                    });
                }
            }
        });
    }

    private void handlePlay(final Track track, String playlistId) {
        boolean isTokenExpired = false;
        if (track == null) {
            return;
        }

        if (mPlayer == null) {
            return;
        }

        if (PlayerContext.currTrack != null &&
                track.getId().equals(PlayerContext.currTrack.getId()) &&
                PlayerContext.playState == PlayState.LOADING) {
            // In case that same track is clicked multiple times while loading.
            return;
        }

        Track prevTrack = PlayerContext.currTrack;
        PlayerContext.currTrack = track;
        PlayerContext.playlistId = playlistId;
        PlayerContext.trackIdx = -1;
        if (PlayerContext.playlistId != null) {
            Playlist playlist =
                    PlayerContext.getPlaylist(PlayerContext.playlistId);
            if (playlist != null) {
                synchronized (playlist) {
                    List<Track> tracks = playlist.getTracks();

                    for (int i = 0; i < tracks.size(); i++) {
                        Track t = tracks.get(i);
                        if (t == null || t.getId() == null) {
                            continue;
                        }
                        if (t.getId().equals(track.getId())) {
                            PlayerContext.trackIdx = i;
                            break;
                        }
                    }
                }
            }
        }

        // TEMPERAL TOAST
        if (!track.getType().equals("youtube") &&
                !track.getType().equals("soundcloud") &&
                !track.getType().equals("dropbeat")) {
            Toast.makeText(this, getString(R.string.not_supported_track_type),
                    Toast.LENGTH_LONG).show();
            if (!handleForward()) {
                handleStop();
            }
            return;
        }


        if (!mAudioFocusHelper.requestFocus()) {
            // do not play music when failed to acquire audio focus
            return;
        }

        if (track.getId().equals(mCurrentUid) && !isTokenExpired &&
                mCurrStreamSource != null) {
            // Resume.
            if (mPlayer.isPaused()) {
                mPlayer.start();
                bindProgressObserver(mPlayer);
                return;
            } else if (!mPlayer.isPlaying()) {
                DbLog.clearPlaybackActions();
                DbLog.pushPlayback("start", -1);

                updatePlayStateChange(PlayState.LOADING);
                loadMusic(track, mCurrStreamSource);
            }
            // Ignore if it already is playing music.
            // (In case that same music is clicked when it is being played)
            return;
        }

        if (DbLog.getPlaybackActions().size() > 0 &&
                prevTrack != null &&
                prevTrack.getType().equals("dropbeat") &&
                !prevTrack.getId().equals(track.getId()) &&
                prevTrack instanceof UserTrack &&
                PlayerContext.currPlayTime > 0) {
            UserTrack userTrack = (UserTrack) prevTrack;
            DbLog.pushPlayback("exit", (int)(PlayerContext.currPlayTime / 1000));

            Requests.logPlaybackDetail(this, userTrack.getUid(),
                    DbLog.getPlaybackActions());
        }

        DbLog.clearPlaybackActions();
        DbLog.pushPlayback("start", -1);

        mCurrentUid = track.getId();
        updatePlayStateChange(PlayState.LOADING);

        final Response.Listener<JSONObject> listener =
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (!mAudioFocusHelper.requestFocus()) {
                    // double check request focus
                    return;
                }
                if (PlayerContext.playState != PlayState.LOADING) {
                    return;
                }
                if (!track.getId().equals(mCurrentUid)) {
                    // Skip
                    return;
                }

                PlayerContext.currStreamUrls =
                        PlayerResolve.getStreamUrls(response, false);
                if (PlayerContext.currStreamUrls.isEmpty()) {
                    if (PlayerContext.currTrack != null) {
                        // No stream available.
                        Toast.makeText(PlayerService.this,
                                "This track is not streamable\n'" +
                                PlayerContext.currTrack.getTitle() + "'",
                                Toast.LENGTH_LONG).show();

                        // Log stream failure
                        Requests.logPlayFailure(PlayerService.this,
                                "empty_stream_url",
                                PlayerContext.currTrack.getTitle(),
                                PlayerContext.currTrack.getId(), null, null);
                    } else {
                        Toast.makeText(PlayerService.this,
                                "This track is not streamable",
                                Toast.LENGTH_LONG).show();
                    }

                    if (!handleForward()) {
                        handleStop();
                    }
                    return;
                }
                mStreamCandidateUrls.clear();
                mStreamCandidateUrls.addAll(PlayerContext.currStreamUrls);
                PlayerContext.currStreamCandidate = PlayerContext.currStreamUrls.get(0);
                StreamSource cand = PlayerContext.currStreamCandidate;
                PlayerContext.currStreamUrls.remove(0);

                if (cand != null) {
                    loadMusic(track, cand);
                } else if (!handleForward()) {
                    handleStop();
                }
            }
        };

        final Response.ErrorListener errorListener =
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                String errorString;
                if (error instanceof NoConnectionError) {
                    errorString = "Failed to play";
                    errorString += "\n\n" + getString(
                            R.string.desc_failed_to_connect_internet);
                } else {
                    errorString = "This track is currently unavailable";
                    if (error.getCause() instanceof YoutubeIE
                            .IeIntegrityException) {
                        YoutubeIE.IeIntegrityException e = (YoutubeIE
                                .IeIntegrityException)error.getCause();
                        String integrityMsg = e.getCleanMessage();
                        if (!TextUtils.isEmpty(integrityMsg)) {
                            errorString += ":\n" + e.getCleanMessage();
                        }
                    }
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    error.printStackTrace(pw);
                    String stacktrace = sw.toString();

                    if (PlayerContext.currTrack != null) {
                        Requests.logPlayFailure(PlayerService.this,
                                "failed_to_resolve",
                                PlayerContext.currTrack.getTitle(),
                                PlayerContext.currTrack.getId(),
                                null, stacktrace);
                    } else {
                        Requests.logPlayFailure(PlayerService.this,
                                "failed_to_resolve",
                                "undefined",
                                "undefined",
                                null, stacktrace);
                    }
                }

                Toast.makeText(PlayerService.this, errorString,
                        Toast.LENGTH_LONG).show();

                if (!(error instanceof NoConnectionError)) {
                    if (!handleForward()) {
                        handleStop();
                    }
                } else {
                    handleStop();
                }
            }
        };

        mPlayerHandler.post(new Runnable() {
            @Override
            public void run() {
                mPlayer.stop();
                PlayerResolve.resolve(
                        PlayerService.this, track, listener, errorListener);
            }
        });
    }

    private void handlePause() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
            updatePlayStateChange(PlayState.PAUSED);
            unbindProgressObserver();
        }
    }

    private void handleSeek(final int position) {
        SharedPreferences s = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        StreamSource streamCand = PlayerContext.currStreamCandidate;
        if (streamCand != null &&
                "DASH audio".equals(streamCand.getFormatNote()) && "webm".equals(streamCand.getType())) {
            // Workaround for samsung phone.
            if (!s.getBoolean("DASH", true)) {
                return;
            }
        }

        if (PlayerContext.currTrack.getType().equals("dropbeat")) {
            DbLog.pushPlayback("seek_from",
                    (int) (PlayerContext.currPlayTime / 1000));
            DbLog.pushPlayback("seek_to", position / 1000);
        }

        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayerHandler.post(new Runnable() {
                @Override
                public void run() {
                    unbindProgressObserver();
                    mPlayer.seekTo(position);
                }
            });
            updateProgressChange(mPlayer.getCurrentPosition(), mPlayer.getDuration());
        }
    }

    private boolean handleForward() {
       Track track = PlayerContext.pickNextTrack();
        if (track != null) {
            handlePlay(track, PlayerContext.playlistId);
            return true;
        }
        return false;
    }

    private boolean handleBackward() {
        Track track = PlayerContext.pickPrevTrack();
        if (track != null) {
            handlePlay(track, PlayerContext.playlistId);
            return true;
        }
        return false;
    }

    private void handleStop() {
        handleStop(false);
    }

    private void handleStop(final boolean cancelNoti) {
        mPlayerHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mPlayer != null) {
                    mPlayer.stop();
                }
                mUiHandler.post(new Runnable() {
                    @SuppressLint("NewApi")
                    @Override
                    public void run() {

                        updatePlayStateChange(PlayState.STOPPED);
                        if (cancelNoti) {
                            PlayerNotification.cancel(PlayerService.this);
                        }
                        if (Build.VERSION.SDK_INT >= 21 && mMediaSession != null) {
                            mMediaSession.setActive(false);
                        }
                        mAudioFocusHelper.abandonFocus();
                    }
                });
            }
        });
    }

    private void handleUpdateRepeatState() {
        if (mPlayer != null) {
            mPlayer.setLooping(PlayerContext.repeatState == RepeatState.REPEAT_ONE);
        }
        if (PlayerNotification.isVisible()) {
            PlayerNotification.notify(this, PlayerContext.currTrack);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void updateMetaInfoAfterLollipop(int state) {
        long action = 0;
        int playState = PlaybackState.STATE_NONE;
        boolean isSessionActive = false;
        switch (state) {
            case PlayState.LOADING:
                playState = PlaybackState.STATE_CONNECTING;
                Track track = PlayerContext.currTrack;

                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.mipmap.ic_launcher);
                MediaMetadata.Builder metaBuilder = new MediaMetadata.Builder()
                        .putString(MediaMetadata.METADATA_KEY_MEDIA_ID,
                                track.getId())
                        .putString(MediaMetadata.METADATA_KEY_DISPLAY_TITLE,
                                track.getTitle())
                        .putString(MediaMetadata.METADATA_KEY_TITLE, track.getTitle())
                        .putBitmap(MediaMetadata.METADATA_KEY_DISPLAY_ICON, icon);
//                Bitmap cover = BitmapFactory.decodeResource(getResources(),
//                        R.drawable.default_cover);
//                metaBuilder.putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART,
//                        cover);
                mMediaSession.setMetadata(metaBuilder.build());
                break;

            case PlayState.STOPPED:
                action |= PlaybackState.ACTION_PLAY;
                playState = PlaybackState.STATE_STOPPED;
                break;

            case PlayState.PAUSED:
                action |= PlaybackState.ACTION_PLAY;
                if (PlayerContext.pickNextTrack() != null) {
                    action |= PlaybackState.ACTION_SKIP_TO_NEXT;
                }
                if (PlayerContext.pickPrevTrack() != null) {
                    action |= PlaybackState.ACTION_SKIP_TO_PREVIOUS;
                }
                playState = PlaybackState.STATE_PAUSED;
                break;

            case PlayState.PLAYING:
                action |= PlaybackState.ACTION_PAUSE;
                if (PlayerContext.pickNextTrack() != null) {
                    action |= PlaybackState.ACTION_SKIP_TO_NEXT;
                }
                if (PlayerContext.pickPrevTrack() != null) {
                    action |= PlaybackState.ACTION_SKIP_TO_PREVIOUS;
                }
                playState = PlaybackState.STATE_PLAYING;
                isSessionActive = true;
                break;
        }
        PlaybackState.Builder stateBuilder = new PlaybackState.Builder();
        stateBuilder.setActions(action);
        stateBuilder.setState(playState, 0, 1.0f);
        mMediaSession.setPlaybackState(stateBuilder.build());
        mMediaSession.setActive(isSessionActive);

    }

    private synchronized void updateMetaInfoUnderLollipop(int state) {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        String playState = null;
        switch (state) {
            case PlayState.LOADING:
                if (!mMediaButtonReceiverBound) {
                    mMediaButtonReceiverBound = true;
                    MediaButtonHelper.registerMediaButtonEventReceiverCompat(
                            audioManager, mMediaButtonReceiverComponent);
                }
                if (!mRemoteControlClientBound) {
                    mRemoteControlClientBound = true;
                    audioManager.registerRemoteControlClient(mRemoteControlClient);
                }
                mRemoteControlClient.setPlaybackState(
                        RemoteControlClient.PLAYSTATE_BUFFERING);
                mRemoteControlClient.setTransportControlFlags(0);
                playState = "LOADING";
                break;
            case PlayState.PLAYING:
                if (mRemoteControlClient!= null) {
                    mRemoteControlClient
                            .setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);

                    mRemoteControlClient.editMetadata(true).putString
                            (MediaMetadataRetriever.METADATA_KEY_ALBUM,
                                    "PLAYING").apply();
                    int actions = RemoteControlClient.FLAG_KEY_MEDIA_STOP |
                            RemoteControlClient.FLAG_KEY_MEDIA_PAUSE;
                    if (PlayerContext.pickPrevTrack() != null) {
                        actions |= RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS;
                    }
                    if (PlayerContext.pickNextTrack() != null) {
                        actions |= RemoteControlClient.FLAG_KEY_MEDIA_NEXT;
                    }
                    mRemoteControlClient.setTransportControlFlags(actions);
                    playState = "PLAYING";
                }
                break;
            case PlayState.PAUSED:
                if (mRemoteControlClient != null) {
                    mRemoteControlClient.setPlaybackState(RemoteControlClient
                                    .PLAYSTATE_PAUSED);
                    int actions = RemoteControlClient.FLAG_KEY_MEDIA_STOP |
                            RemoteControlClient.FLAG_KEY_MEDIA_PLAY;
                    if (PlayerContext.pickPrevTrack() != null) {
                        actions |= RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS;
                    }
                    if (PlayerContext.pickNextTrack() != null) {
                        actions |= RemoteControlClient.FLAG_KEY_MEDIA_NEXT;
                    }
                    mRemoteControlClient.setTransportControlFlags
                            (actions);
                    playState = "PAUSED";
                }
                break;
            case PlayState.STOPPED:
                if (mRemoteControlClient != null) {
                    mRemoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_STOPPED);
                }
                mRemoteControlClient.setPlaybackState(RemoteControlClient
                                .PLAYSTATE_PAUSED);
                int actions = 0;
                if (PlayerContext.currTrack != null) {
                    actions |= RemoteControlClient.FLAG_KEY_MEDIA_PLAY;
                }
                mRemoteControlClient.setTransportControlFlags(actions);
                audioManager.unregisterRemoteControlClient(mRemoteControlClient);
                mRemoteControlClientBound = false;
                MediaButtonHelper.unregisterMediaButtonEventReceiverCompat(audioManager, mMediaButtonReceiverComponent);
                mMediaButtonReceiverBound = false;
                break;
        }
        if (mRemoteControlClient != null && state != PlayState.STOPPED &&
                PlayerContext.currTrack != null) {
//            Bitmap cover = BitmapFactory.decodeResource(getResources(),
//                    R.drawable.default_cover);
            mRemoteControlClient.editMetadata(true)
                    .putString(MediaMetadataRetriever.METADATA_KEY_ALBUM, playState)
                    .putString(MediaMetadataRetriever.METADATA_KEY_TITLE,
                            PlayerContext.currTrack.getTitle())
//                    .putBitmap(RemoteControlClientCompat.MetadataEditorCompat
//                            .METADATA_KEY_ARTWORK, cover)
                    .apply();
        }
    }

    private synchronized void updatePlayStateChange(int state) {
        PlayerContext.playState = state;
        PlayerContext.playState = state;
        if (state == PlayState.STOPPED || state == PlayState.LOADING) {
            PlayerContext.currPlayTime= -1;
            PlayerContext.totalPlayTime = -1;
            if (state == PlayState.STOPPED) {
                PlayerContext.currTrack = null;
                PlayerContext.trackIdx = -1;
                PlayerContext.playlistId = null;
            }
        }

        if (Build.VERSION.SDK_INT >= 21) {
            updateMetaInfoAfterLollipop(state);
        } else {
            updateMetaInfoUnderLollipop(state);
        }

        Intent intent = new Intent(ACTION_NOTIFY_PLAYSTATE_CHANGE);
        mBroadcastManager.sendBroadcast(intent);
        PlayerNotification.notify(this, PlayerContext.currTrack);
    }

    private void updateProgressChange(long curr, long total) {
        synchronized (mProgressPublishLock) {
            if (curr > 0 && mIsFirstProgress.get() && curr < total) {
                // If Stream source is valid, empty candidate queue.
                PlayerContext.currStreamUrls = new ArrayList<>();
                updatePlayStateChange(PlayState.PLAYING);
                mIsFirstProgress.set(false);
            }

            if (curr > total) {
                curr = total;
            }

            PlayerContext.currPlayTime = curr;
            PlayerContext.totalPlayTime = total;

            Intent intent = new Intent(ACTION_NOTIFY_PROGRESS_CHANGE);
            mBroadcastManager.sendBroadcast(intent);
        }
    }

    private synchronized void unbindProgressObserver() {
        if (mProgressUpdateTimer != null) {
            mProgressUpdateTimer.cancel();
            mProgressUpdateTimer = null;
        }
    }

    private synchronized void bindProgressObserver(final AtomicMediaPlayer mp) {

        if (mProgressUpdateTimer != null) {
            mProgressUpdateTimer.cancel();
            mProgressUpdateTimer = null;
        }
        mProgressUpdateTimer = new Timer();
        mIsFirstProgress.set(true);
        mProgressUpdateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (PlayerService.this) {
                            mProgressUpdateTimer = null;
                        }
                        try {
                            int total = mp.getDuration();
                            int curr = mp.getCurrentPosition();
                            updateProgressChange(curr, total);
                        } catch (IllegalStateException e) {

                        }
                    }
                });
            }
        }, 0, PROGRESS_UPDATE_PERIOD);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private RendererBuilderSelector getRendererBuilder() {
        return new RendererBuilderSelector();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 21) {
            mMediaSession = new MediaSession(this, "DropbeatPlayer");
            mMediaSession.setCallback(new MediaSession.Callback() {
                @Override
                public boolean onMediaButtonEvent(final Intent mediaButtonIntent) {
                    return super.onMediaButtonEvent(mediaButtonIntent);
                }

                @Override
                public void onPlay() {
                    super.onPlay();
                    doPlay(PlayerService.this, PlayerContext.currTrack, PlayerContext.playlistId);
                }

                @Override
                public void onPause() {
                    super.onPause();
                    doPause(PlayerService.this);
                }

                @Override
                public void onSkipToPrevious() {
                    super.onSkipToPrevious();
                    doBackward(PlayerService.this);
                }

                @Override
                public void onSkipToNext() {
                    super.onSkipToNext();
                    doForward(PlayerService.this);
                }

                @Override
                public void onStop() {
                    super.onStop();
                    doStop(PlayerService.this);
                }
            });
            mMediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                    MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
            PlaybackState state = new PlaybackState.Builder()
                    .setActions(0)
                    .setState(PlaybackState.STATE_NONE, 0, 1.0f,
                            SystemClock.elapsedRealtime())
                    .build();
            mMediaSession.setPlaybackState(state);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    NavigationDrawerActivity.createIntent(this),
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mMediaSession.setSessionActivity(contentIntent);
        } else {
            mMediaButtonReceiverComponent = new ComponentName(getPackageName(),
                    RemoteControlReceiver.class.getName());

            Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
            intent.setComponent(mMediaButtonReceiverComponent);
            mRemoteControlClient = new RemoteControlClient(
                    PendingIntent.getBroadcast(this, 0, intent, 0));
        }
        mPlayerHandlerThread = new HandlerThread("mediaPlayerHandlerThread");
        mPlayerHandlerThread.start();

        mPlayerHandler = new Handler(mPlayerHandlerThread.getLooper());
        mUiHandler = new Handler();

        mAudioFocusHelper = new AudioFocusHelper(getApplicationContext(), this);
        mAudioCapabilitiesReceiver = new AudioCapabilitiesReceiver
                (getApplicationContext(), this);
        mAudioCapabilitiesReceiver.register();

        mBroadcastManager = LocalBroadcastManager.getInstance(this);

        if (!BuildConfig.DEBUG) {
            RaygunClient.Init(getApplicationContext());
            RaygunClient.AttachExceptionHandler();

            DeviceUuidFactory factory = new DeviceUuidFactory(this);
            RaygunUserInfo user = new RaygunUserInfo();
            DbAccount account = DbAccount.getUser(this);
            if (account != null) {
                user.Identifier = account.getEmail();
                user.FullName = account.getFullName();
                user.Email = account.getEmail();
                user.Uuid = factory.getDeviceUuid().toString();
                user.IsAnonymous = false;
            } else {
                user.Uuid = factory.getDeviceUuid().toString();
                user.IsAnonymous = true;
            }
            RaygunClient.SetUser(user);
        }

        IntentFilter authFilter = new IntentFilter(DbAccount.ACTION_SIGNIN);
        authFilter.addAction(DbAccount.ACTION_SIGNOUT_ASYNC);

        mBroadcastManager.registerReceiver(mAuthReceiver, authFilter);
        mAuthReceiverRegistered = true;

        preparePlayer();
    }

    @SuppressLint("NewApi")
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediaSession != null && Build.VERSION.SDK_INT >= 21) {
            mMediaSession.release();
        }
        mPlayerHandlerThread.quit();
        mAudioCapabilitiesReceiver.unregister();
        if (mAuthReceiverRegistered) {
            unregisterReceiver(mAuthReceiver);
        }
    }

    @Override
    public void onGainAudioFocus(AudioManager am) {
        if (mPrevPlayState > -1 && mPrevPlayState == PlayState.PLAYING) {
            doPlay(this, PlayerContext.currTrack, PlayerContext.playlistId);
        }
        if (mPlayer != null) {
            mPlayer.setVolume(1.0f);
        }
    }

    @Override
    public void onLoseAudioFocus(AudioManager am, AudioFocusHelper.LoseState state) {
        mPrevPlayState = PlayerContext.playState;
        switch (state) {
            case LOSE:
                doPause(this);
                break;
            case LOSE_TRANSIENT:
                doPause(this);
                break;
            case LOSE_TRANSIENT_CAN_DUCK:
                if (mPlayer != null) {
                    mPlayer.setVolume(0.1f);
                }
                break;
        }
    }

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
        boolean audioCapabilitiesChanged = !audioCapabilities.equals(this.mAudioCapabilities);
        if (mPlayer == null || audioCapabilitiesChanged) {
            this.mAudioCapabilities = audioCapabilities;
            releasePlayer();
            preparePlayer();
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || TextUtils.isEmpty(intent.getAction())) {
            return START_STICKY;
        }
        String action = intent.getAction();
        if (action.equals(ACTION_PLAY)) {
            Track track = (Track) intent.getSerializableExtra(PARAM_TRACK);
            String playlistId = intent.getStringExtra(PARAM_PLAYLIST_ID);
            handlePlay(track, playlistId);
        } else if (action.equals(ACTION_PAUSE)) {
            handlePause();
        } else if (action.equals(ACTION_SEEK)) {
            int position = intent.getIntExtra(PARAM_SEEK_POSITION, -1);
            if (position > -1) {
                handleSeek(position);
            }
        } else if (action.equals(ACTION_FORWARD)) {
            handleForward();
        } else if (action.equals(ACTION_BACKWARD)) {
            handleBackward();
        } else if (action.equals(ACTION_STOP)) {
            boolean cancelNoti = intent.getBooleanExtra(PARAM_CANCEL_NOTI, false);

            if (DbLog.getPlaybackActions().size() > 0 &&
                    PlayerContext.currTrack != null &&
                    PlayerContext.currTrack.getType().equals("dropbeat") &&
                    PlayerContext.currTrack instanceof UserTrack &&
                    PlayerContext.currPlayTime > 0) {
                UserTrack userTrack = (UserTrack) PlayerContext.currTrack;
                DbLog.pushPlayback("exit", (int)(PlayerContext.currPlayTime / 1000));

                Requests.logPlaybackDetail(this, userTrack.getUid(),
                        DbLog.getPlaybackActions());
            }
            DbLog.clearPlaybackActions();
            handleStop(cancelNoti);
        } else if (action.equals(ACTION_UPDATE_REPEAT_STATE)) {
            handleUpdateRepeatState();
        }
        return START_STICKY;
    }

    @Override
    public void onStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case AtomicMediaPlayer.STATE_IDLE:
                break;
            case AtomicMediaPlayer.STATE_PREPARING:
                Log.d("STATE", "Player is initialized and preparing now");
                // Do nothing.
                // It seems to some kind of `mp.onPrepared()` but it just initialize player.
                bindProgressObserver(mPlayer);
                break;
            case AtomicMediaPlayer.STATE_BUFFERING:
                Log.d("STATE", "Music buffered for playing");
                // STATE_BUFFERING can be occurred via 2 ways
                // 1. Firstly play music,
                // 2. Seeking (seekTo)
                break;
            case AtomicMediaPlayer.STATE_READY:
                // STATE_READY has status, playing and paused
                // determined by getPlayWhenReady(); (true:playing, false:paused)
                if (playWhenReady) {
                    // start
                    Log.d("STATE", "Music started or resumed");
                    bindProgressObserver(mPlayer);
                } else {
                    // paused
                    Log.d("STATE", "Music paused");
                    unbindProgressObserver();
                }
                break;
            case AtomicMediaPlayer.STATE_ENDED:
                Log.d("STATE", "Music ended");
                // Music completed. (originally it is CompleteListener.onCompleted)
                if (!PlayerContext.currStreamUrls.isEmpty()) {
                    // In this case, this is invoked by invalid stream url condition.
                    PlayerContext.currStreamCandidate = PlayerContext.currStreamUrls.get(0);
                    loadMusic(PlayerContext.currTrack, PlayerContext.currStreamCandidate);
                    PlayerContext.currStreamUrls.remove(0);
                    return;
                }

                if (DbLog.getPlaybackActions().size() > 0 &&
                        PlayerContext.currTrack != null &&
                        PlayerContext.currTrack.getType().equals("dropbeat") &&
                        PlayerContext.currTrack instanceof UserTrack) {
                    UserTrack userTrack = (UserTrack) PlayerContext.currTrack;
                    DbLog.pushPlayback("end", -1);
                    Requests.logPlaybackDetail(this, userTrack.getUid(),
                            DbLog.getPlaybackActions());
                }
                DbLog.clearPlaybackActions();

                if (mPlayer != null) {
                    Log.d(TAG, "Player's looping state: " + mPlayer.getLooping());
                    if (mPlayer.getLooping()) {
                        // Loop one song.
                        DbLog.pushPlayback("start", -1);
                        mPlayer.seekTo(0);
                        return;
                    }
                }

                unbindProgressObserver();
                if (!handleForward()) {
                    handleStop();
                }
                break;
        }
    }

    @Override
    public void onError(Exception e) {
        // Error from renderers
        StreamSource cand = PlayerContext.currStreamCandidate;
        if (cand != null &&
                "DASH audio".equals(cand.getFormatNote()) && "webm".equals(cand.getType())) {
            // Workaround for samsung phone which cannot stream DASH audio.
            SharedPreferences.Editor editor =
                    PreferenceManager.
                            getDefaultSharedPreferences(getApplicationContext()).edit();
            editor.putBoolean("DASH", false);
            editor.commit();
        }
        if (!PlayerContext.currStreamUrls.isEmpty()) {
            PlayerContext.currStreamCandidate = PlayerContext.currStreamUrls.get(0);
            loadMusic(PlayerContext.currTrack, PlayerContext.currStreamCandidate);
            PlayerContext.currStreamUrls.remove(0);
            return;
        }

        if (PlayerContext.currTrack != null) {
            Toast.makeText(PlayerService.this, "This track is not streamable\n'" +
                    PlayerContext.currTrack.getTitle() + "'", Toast.LENGTH_LONG).show();
        }

        // Log stream failure
        List<String> streamCandidateUrls = new ArrayList<>();
        for (StreamSource source : mStreamCandidateUrls) {
            streamCandidateUrls.add(source.getUrl());
        }

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        Requests.logPlayFailure(PlayerService.this,
                "failed_to_play",
                PlayerContext.currTrack == null ? "undefined" :
                        PlayerContext.currTrack.getTitle(),
                PlayerContext.currTrack == null ? "undefined" :
                        PlayerContext.currTrack.getId(),
                streamCandidateUrls,
                stacktrace);

        unbindProgressObserver();

        if (!handleForward()) {
            handleStop();
        }
    }

    private void preparePlayer() {
        if (mPlayer == null) {
            mPlayer = new AtomicMediaPlayer(getRendererBuilder());
            mPlayer.addListener(this);
            mPlayer.seekTo(mLastPlayerPosition);
        }
    }

    private void releasePlayer() {
        if (mPlayer != null) {
            mLastPlayerPosition = mPlayer.getCurrentPosition();
            mPlayer.stop();
            mPlayer = null;
        }
    }

    class RendererBuilderSelector {

        public AtomicMediaPlayer.RendererBuilder get(Track track, StreamSource source) {
            String userAgent = Util.getUserAgent(getApplicationContext(), "Dropbeat");
            Context context = getApplicationContext();

            if (source.getFormatNote().contains("DASH")) {
                // Adaptive streaming.
                String manifestUrl = source.getUrl();
                return new DashRendererBuilder(
                        context, userAgent, manifestUrl,
                        null, mAudioCapabilities);
            } else if (track.getType().equals("youtube")) {
                if (source.getType().equals("mp4") || source.getType().equals("m4a")) {
                    // mp4 and m4a has same encodings for audio.
                    return new DefaultRendererBuilder(
                            context, userAgent, Uri.parse(source.getUrl()), new Mp4Extractor());
                } else if (source.getType().equals("webm")) {
                    return new DefaultRendererBuilder(
                            context, userAgent, Uri.parse(source.getUrl()), new WebmExtractor());
                }
            } else if (track.getType().equals("soundcloud")) {
                // soundcloud
                return new DefaultRendererBuilder(
                        context, userAgent, Uri.parse(source.getUrl()), new Mp3Extractor());
            } else {
                String url = source.getUrl();
                if (url.endsWith(".mp4") || url.endsWith(".m4a")) {
                    return new DefaultRendererBuilder(
                            context, userAgent, Uri.parse(source.getUrl()), new Mp4Extractor());
                } else if (url.endsWith(".webm")) {
                    return new DefaultRendererBuilder(
                            context, userAgent, Uri.parse(source.getUrl()),
                            new WebmExtractor());
                } else {
                    return new DefaultRendererBuilder(
                            context, userAgent, Uri.parse(source.getUrl()), new Mp3Extractor());
                }
            }

            return null;
        }
    }
}

