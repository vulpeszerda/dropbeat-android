package net.dropbeat.spark.play;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Patterns;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import net.dropbeat.spark.DropbeatContext;
import net.dropbeat.spark.R;
import net.dropbeat.spark.model.StreamSource;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.model.UserTrack;
import net.dropbeat.spark.network.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by parkilsu on 15. 4. 30..
 */
public class PlayerResolve {
    /**
     *
     * @param context
     * @param uid
     *      ex) wKES3IIbTl4, 164138555
     * @param listener
     * @param errorListener
     */
    public static void resolve(
            final Context context, final Track track,
            final Response.Listener<JSONObject> listener,
            final Response.ErrorListener errorListener) {

        String uid = track.getId();
        String type = track.getType();

        if (type.equals("youtube")) {
            // Youtube.
//            Requests.streamResolve(context, uid, listener, errorListener);
            AsyncYoutubeResolver.resolve(context, uid, listener, errorListener);
        } else {
            String url = null;
            if (type.equals("soundcloud")) {
                // Soundcloud.
                if (DropbeatContext.soundcloudClientKey == null) {
                    getClientKeys(context, new Runnable() {

                        @Override
                        public void run() {
                            resolve(context, track, listener, errorListener);
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {
                            String msg = context.getResources().getString(
                                    R.string.failed_to_fetch_client_key);
                            errorListener.onErrorResponse(new VolleyError(msg));
                            return;
                        }
                    });
                    return;
                }
                // Mocks HTTP resp aiming for same resolve interface in both streaming sources.
                url = String.format(
                        "https://api.soundcloud.com/tracks/%s/stream?client_id=%s",
                        uid, DropbeatContext.soundcloudClientKey);

            } else if (type.equals("dropbeat")) {
                // Dropbeat.
                // Mocks HTTP resp aiming for same resolve interface in both streaming sources.
                UserTrack userTrack = (UserTrack) track;
                url = userTrack.getStreamUrl();
            } else {
                String cand = URLDecoder.decode(uid);
                if (Patterns.WEB_URL.matcher(cand).matches()) {
                    url = cand;
                }
            }
            JSONObject obj = new JSONObject();
            try {
                JSONArray urls = new JSONArray();
                if (url != null) {
                    JSONObject urlObj = new JSONObject();
                    urlObj.put("url", url);
                    urls.put(urlObj);
                }
                obj.put("urls", urls);
            } catch (JSONException e) {
                // 100% Unreachable.
                errorListener.onErrorResponse(new VolleyError("Why should i handle this"));
                return;
            }
            listener.onResponse(obj);
        }
    }

    public static void getClientKeys(Context context,
                                       final Runnable onSuccess, final Runnable onFailure) {
        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.optBoolean("success") ||
                        TextUtils.isEmpty(response.optString("soundcloud_key"))) {
                    onFailure.run();
                    return;
                }
                DropbeatContext.soundcloudClientKey =
                        response.optString("soundcloud_key");
                onSuccess.run();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
                onFailure.run();
            }
        };

        Requests.getClientKey(context, listener, errorListener);
    }

    public static List<StreamSource> getStreamUrls(JSONObject resp, boolean audioOnly) {
        List<StreamSource> urls = new ArrayList<StreamSource>();
        try {
            JSONArray array = resp.getJSONArray("urls");
            for(int i=0; i<array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                if (audioOnly) {
                    if (!obj.has("format_note")) {
                        continue;
                    }
                    if (!"DASH audio".equals(obj.getString("format_note"))) {
                        continue;
                    }
                }

                if (obj.has("format_id") && "mpd".equals(obj.getString("format_id"))) {
                    urls.add(new StreamSource(obj.getString("url"), "DASH MPD", "DASH"));
                } else {
                    String formatNote = obj.has("format_note") ? obj.getString("format_note") : "";
                    String type = obj.has("ext") ? obj.getString("ext") : "";
                    urls.add(new StreamSource(obj.getString("url"), formatNote, type));
                }
            }
            return urls;
        } catch (JSONException e) {
            e.printStackTrace();
            return urls;
        }
    }

    private static class Result {
        boolean success;
        JSONObject response;
        Exception e;
    }

    private static class AsyncYoutubeResolver extends
            AsyncTask<Void, Void, Result> {

        private static RequestQueue sResolveReqQ;
        private static AsyncYoutubeResolver sCurrResolver;

        private Context mContext;
        private String mUid;
        private Response.Listener mListener;
        private Response.ErrorListener mErrorListener;

        public static void resolve(Context context, String uid,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
            if (sCurrResolver != null) {
                sCurrResolver.cancel(true);
            }
            if (sResolveReqQ == null) {
                sResolveReqQ = Volley.newRequestQueue(context);
            } else {
                sResolveReqQ.cancelAll(new RequestQueue.RequestFilter() {
                    @Override
                    public boolean apply(Request<?> request) {
                        return true;
                    }
                });
            }
            sCurrResolver = new AsyncYoutubeResolver(context,
                    uid, listener, errorListener);
            sCurrResolver.execute();
        }

        public AsyncYoutubeResolver(Context context, String uid,
                                    Response.Listener<JSONObject> listener,
                                    Response.ErrorListener errorListener) {
            mContext = context;
            mUid = uid;
            mListener = listener;
            mErrorListener = errorListener;
        }

        @Override
        protected Result doInBackground(Void... params) {
            YoutubeIE ie = new YoutubeIE(mContext, sResolveReqQ);
            Result result = new Result();
            try {
                JSONArray urls = ie.extractAudio(mUid);
                result.success = true;
                result.response = new JSONObject();
                result.response.put("urls", urls);
            } catch (Exception e) {
                result.success = false;
                result.e = e;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Result result) {
            super.onPostExecute(result);
            if (result.success) {
                mListener.onResponse(result.response);
            } else {
                if (result.e instanceof VolleyError) {
                    mErrorListener.onErrorResponse((VolleyError) result.e);
                } else {
                    mErrorListener.onErrorResponse(new VolleyError(result.e));
                }
            }
        }
    }
}
