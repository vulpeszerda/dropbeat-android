package net.dropbeat.spark.play;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.session.MediaSession;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;

import net.dropbeat.spark.NavigationDrawerActivity;
import net.dropbeat.spark.R;
import net.dropbeat.spark.StartupActivity;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.model.Track;

/**
 * Created by vulpes on 15. 5. 1..
 */
public class PlayerNotification {

    private static final int PLAYER_NOTIFICATION_ID = 548850;
    private static boolean sIsVisible = false;

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public static void nofityLollipop(Service service, Track track,
//                                            MediaSession session) {
//        if (track == null) {
//            return;
//        }
//        Context context = service;
//
//        Notification.MediaStyle style = new Notification.MediaStyle();
//        style.setMediaSession(session.getSessionToken());
//        style.setShowActionsInCompactView(0, 1, 2);
//
//        Intent close = new Intent(context, PlayerService.class);
//        close.setAction(PlayerService.ACTION_STOP);
//        close.putExtra(PlayerService.PARAM_CANCEL_NOTI, true);
//        PendingIntent pClose = PendingIntent.getService(context, 1, close,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//        Notification.Builder builder = new Notification.Builder(service)
//                .setSmallIcon(R.drawable.ic_launcher)
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources
//                        (), R.drawable.default_albumcover1))
//                .setDeleteIntent(pClose)
//                .setColor(0x000000)
//                .setVisibility(Notification.VISIBILITY_PUBLIC)
//                .setStyle(style);
//
//
//        builder.setContentTitle(track.getTitle());
//        builder.addAction(createAction(context, android.R.drawable
//                        .ic_media_previous,
//                "Previous", RemoteControlReceiver.ACTION_BACKWARD ) );
//
//
//        String playStatus = "READY";
//        switch (PlayerContext.playState) {
//            case PlayState.LOADING:
//                playStatus = "LOADING";
//                contentView.setViewVisibility(R.id.play_button, View.GONE);
//                contentView.setViewVisibility(R.id.pause_button, View.GONE);
//                contentView.setViewVisibility(R.id.play_loader_wheel, View.VISIBLE);
//                builder.addAction(createAction(context,
//                        android.R.drawable.ic_media_pause, "play",
//                        Player.ACTION_PLAY));
//                break;
//            case PlayState.PLAYING:
//                playStatus = "PLAYING";
//                contentView.setViewVisibility(R.id.play_button, View.GONE);
//                contentView.setViewVisibility(R.id.pause_button, View.VISIBLE);
//                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
//                break;
//            case PlayState.PAUSED:
//                playStatus = "PAUSED";
//                contentView.setViewVisibility(R.id.play_button, View.VISIBLE);
//                contentView.setViewVisibility(R.id.pause_button, View.GONE);
//                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
//                break;
//            case PlayState.STOPPED:
//                playStatus = "READY";
//                contentView.setViewVisibility(R.id.play_button, View.VISIBLE);
//                contentView.setViewVisibility(R.id.pause_button, View.GONE);
//                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
//                break;
//        }
//
//        builder.setContentText(playStatus);
//        builder.addAction(createAction(context,
//                android.R.drawable.ic_media_next,
//                "Next", RemoteControlReceiver.ACTION_FORWARD));
//
//        NotificationManager notificationManager = (NotificationManager)
//                service.getSystemService( Context.NOTIFICATION_SERVICE );
//        notificationManager.notify(PLAYER_NOTIFICATION_ID, builder.build());
//        sIsVisible = true;
//    }
//
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    private static Notification.Action createAction(Context context, int icon,
//                                             String title, String intentAction ) {
//        Intent intent = new Intent(context, PlayerService.class);
//        intent.setAction( intentAction );
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, 0);
//        return new Notification.Action.Builder(icon, title, pendingIntent).build();
//    }

    public static void notify(Service service, Track track){
        if (track == null) {
            return;
        }
        Context context = service;
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context);

        Intent close = new Intent(context, PlayerService.class);
        close.setAction(PlayerService.ACTION_STOP);
        close.putExtra(PlayerService.PARAM_CANCEL_NOTI, true);
        PendingIntent pClose = PendingIntent.getService(context, 1, close,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_statusbar)
                .setDeleteIntent(pClose)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);


        RemoteViews contentView = new RemoteViews(context.getPackageName(),
                R.layout.player_notification);

        String playStatus = "READY";
        switch (PlayerContext.playState) {
            case PlayState.LOADING:
                playStatus = "LOADING";
                contentView.setViewVisibility(R.id.play_button, View.GONE);
                contentView.setViewVisibility(R.id.pause_button, View.GONE);
                contentView.setViewVisibility(R.id.play_loader_wheel, View.VISIBLE);
                break;
            case PlayState.PLAYING:
                playStatus = "PLAYING";
                contentView.setViewVisibility(R.id.play_button, View.GONE);
                contentView.setViewVisibility(R.id.pause_button, View.VISIBLE);
                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
                break;
            case PlayState.PAUSED:
                playStatus = "PAUSED";
                contentView.setViewVisibility(R.id.play_button, View.VISIBLE);
                contentView.setViewVisibility(R.id.pause_button, View.GONE);
                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
                break;
            case PlayState.STOPPED:
                playStatus = "READY";
                contentView.setViewVisibility(R.id.play_button, View.VISIBLE);
                contentView.setViewVisibility(R.id.pause_button, View.GONE);
                contentView.setViewVisibility(R.id.play_loader_wheel, View.GONE);
                break;
        }

        if (PlayerContext.pickNextTrack() == null ||
                PlayerContext.repeatState == RepeatState.REPEAT_ONE) {
            contentView.setBoolean(R.id.forward_button, "setEnabled", false);
            contentView.setInt(R.id.forward_button, "setImageResource", R.drawable.btn_forward_disabled);
        } else {
            contentView.setBoolean(R.id.forward_button, "setEnabled", true);
            contentView.setInt(R.id.forward_button, "setImageResource", R.drawable.btn_forward_normal);
        }
        if (PlayerContext.pickPrevTrack() == null ||
                PlayerContext.repeatState == RepeatState.REPEAT_ONE) {
            contentView.setBoolean(R.id.backward_button, "setEnabled", false);
            contentView.setInt(R.id.backward_button, "setImageResource", R.drawable.btn_backward_disabled);
        } else {
            contentView.setBoolean(R.id.backward_button, "setEnabled", true);
            contentView.setInt(R.id.backward_button, "setImageResource", R.drawable.btn_backward_normal);
        }

        contentView.setTextViewText(R.id.music_title, track.getTitle());
        contentView.setTextViewText(R.id.player_status, playStatus);
        builder.setContent(contentView);

        PendingIntent contentIntent = PendingIntent.getActivity(
                context, 0, NavigationDrawerActivity.createIntent(context),
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        //set the button listeners
        bindListeners(context, contentView);
        sIsVisible = true;

        service.startForeground(PLAYER_NOTIFICATION_ID, builder.build());
    }

    public static boolean isVisible() {
        return sIsVisible;
    }

    public static void cancel(Service service) {
        service.stopForeground(true);
        sIsVisible = false;
    }

    private static void bindListeners(Context context, RemoteViews view){

        // play listener
        Intent play = new Intent(context, PlayerService.class);
        play.setAction(PlayerService.ACTION_PLAY);
        play.putExtra(PlayerService.PARAM_TRACK, PlayerContext.currTrack);
        play.putExtra(PlayerService.PARAM_PLAYLIST_ID,
                PlayerContext.playlistId);
        PendingIntent pPlay = PendingIntent.getService(context, 1, play,
                PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.play_button, pPlay);

        // paust listener
        Intent pause = new Intent(context, PlayerService.class);
        pause.setAction(PlayerService.ACTION_PAUSE);
        PendingIntent pPause = PendingIntent.getService(context, 1, pause,
                PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.pause_button, pPause);

        //prev listener
        Intent prev = new Intent(context, PlayerService.class);
        prev.setAction(PlayerService.ACTION_BACKWARD);
        PendingIntent pPrev = PendingIntent.getService(context, 1, prev,
                PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.backward_button, pPrev);

        //next listener
        Intent next = new Intent(context, PlayerService.class);
        next.setAction(PlayerService.ACTION_FORWARD);
        PendingIntent pNext = PendingIntent.getService(context, 1, next,
                PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.forward_button, pNext);

        //exit listener
        Intent close = new Intent(context, PlayerService.class);
        close.setAction(PlayerService.ACTION_STOP);
        close.putExtra(PlayerService.PARAM_CANCEL_NOTI, true);
        PendingIntent pClose = PendingIntent.getService(context, 1, close,
                PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.close_button, pClose);
    }

}
