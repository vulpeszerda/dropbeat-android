package net.dropbeat.spark.play;

import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.constants.ShuffleState;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.StreamSource;
import net.dropbeat.spark.model.Track;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * `trackIdx` track index which is currently playing in playlist.
 * `playlistIdx` playlist index current track belongs to.
 * `playlists` all playlists of signed user.
 */
public class PlayerContext {
    public static int trackIdx;
    public static String playlistId;
    public static int repeatState = RepeatState.NOT_REPEAT;
    public static int shuffleState = ShuffleState.NOT_SHUFFLE;
    public static int playState = PlayState.STOPPED;
    public static float currPlayTime = -1;
    public static float totalPlayTime = -1;
    public static Track currTrack;
    public static List<StreamSource> currStreamUrls = new ArrayList<StreamSource>();
    public static StreamSource currStreamCandidate;
    public static Playlist externalPlaylist;

    private static List<Playlist> sPlaylists = new ArrayList<>();

    public static void reset() {
        synchronized (sPlaylists) {
            sPlaylists.clear();
        }
        playlistId = null;
    }

    public static List<Playlist> getCopiedPlaylists() {
        return new ArrayList<>(sPlaylists);
    }

    public static void resetPlaylists(List<Playlist> playlists) {
        synchronized (sPlaylists) {
            sPlaylists.clear();
            sPlaylists.addAll(playlists);
        }
    }

    public static Track pickNextTrack() {
        Track nextTrack = null;
        Playlist playlist = getPlaylist(PlayerContext.playlistId);
        if (playlist == null) {
            if (trackIdx == -1 &&
                    repeatState != RepeatState.NOT_REPEAT) {
                return currTrack;
            } else {
                return null;
            }
        }

        synchronized (playlist) {
            int playlistSize = playlist.getTracks().size();

            if (trackIdx < 0) {
                return null;
            }

            if (shuffleState == ShuffleState.SHUFFLE && repeatState !=
                    RepeatState.REPEAT_ONE) {
                if (playlistSize == 0) {
                    return null;
                }
                // Music never stops in shuffle state.
                nextTrack = playlist.getTracks().get(new Random().nextInt((playlistSize)));
            } else {
                int next;

                if (repeatState == RepeatState.NOT_REPEAT) {
                    next = trackIdx + 1;
                    // If not, we reached at last track.
                    if (next < playlistSize) {
                        nextTrack = playlist.getTracks().get(next);
                    }
                } else if (repeatState == RepeatState.REPEAT_PLAYLIST) {
                    if (playlistSize == 0) {
                        return null;
                    }
                    next = (trackIdx + 1) % playlistSize;
                    nextTrack = playlist.getTracks().get(next);
                } else if (repeatState == RepeatState.REPEAT_ONE) {
                    if (playlistSize == 0) {
                        return null;
                    }
                    if (trackIdx < playlistSize) {
                        nextTrack = playlist.getTracks().get(trackIdx);
                    }
                }
            }
        }
        return nextTrack;
    }

    public static Track pickPrevTrack() {
        Track prevTrack = null;
        Playlist playlist = getPlaylist(PlayerContext.playlistId);
        if (playlist == null) {
            if (trackIdx == -1 &&
                    repeatState != RepeatState.NOT_REPEAT) {
                return currTrack;
            } else {
                return null;
            }
        }

        synchronized (playlist) {
            int playlistSize = playlist.getTracks().size();

            if (trackIdx < 0) {
                return null;
            }

            if (shuffleState == ShuffleState.SHUFFLE &&
                    repeatState != RepeatState.REPEAT_ONE) {
                if (playlistSize == 0) {
                    return null;
                }
                // Music never stops in shuffle state.
                return playlist.getTracks().get(new Random().nextInt((playlistSize)));
            } else {
                int prev;

                if (repeatState == RepeatState.NOT_REPEAT) {
                    if (playlistSize == 0) {
                        return null;
                    }
                    prev = trackIdx - 1;
                    // If not, we reached at first track.
                    if (prev >= 0 && prev < playlistSize) {
                        prevTrack = playlist.getTracks().get(prev);
                    }
                } else if (repeatState == RepeatState.REPEAT_PLAYLIST) {
                    if (playlistSize == 0) {
                        return null;
                    }
                    prev = trackIdx - 1;
                    if (prev <= 0) {
                        prev = playlistSize - 1;
                    }
                    prevTrack = playlist.getTracks().get(prev);
                } else if (repeatState == RepeatState.REPEAT_ONE) {
                    if (playlistSize == 0) {
                        return null;
                    }
                    if (trackIdx >= 0 && trackIdx < playlistSize) {
                        prevTrack = playlist.getTracks().get(trackIdx);
                    }
                }
            }
        }
        return prevTrack;
    }

    public static Playlist getPlaylist(String playlistId) {
        Playlist playlist = null;
        List<Playlist> playlists = getCopiedPlaylists();
        for (Playlist p : playlists) {
            if (p.getId().equals(playlistId)) {
                playlist = p;
            }
        }

        if (playlist == null &&
                PlayerContext.externalPlaylist != null &&
                PlayerContext.externalPlaylist.getId().equals(playlistId)) {
            return PlayerContext.externalPlaylist;
        }
        return playlist;
    }

    public static void changeRepeatState() {
        PlayerContext.repeatState = (PlayerContext.repeatState + 1) % 3;
    }

    public static void changeshuffleState() {
        PlayerContext.shuffleState = (PlayerContext.shuffleState + 1) % 2;
    }

}
