package net.dropbeat.spark.play;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.extractor.Extractor;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;

public class DefaultRendererBuilder implements AtomicMediaPlayer.RendererBuilder {

    private static final int BUFFER_SIZE = 10 * 1024 * 1024;

    private final Context context;
    private final String userAgent;
    private final Uri uri;
    private final Extractor extractor;

    public DefaultRendererBuilder(Context context, String userAgent, Uri uri,
                                    Extractor extractor) {
        this.context = context;
        this.userAgent = userAgent;
        this.uri = uri;
        this.extractor = extractor;
    }

    @Override
    public void buildRenderers(AtomicMediaPlayer player, AtomicMediaPlayer.RendererBuilderCallback callback) {
        // Build the video and audio renderers.
        DataSource dataSource = new DefaultUriDataSource(context, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(uri,
                dataSource, extractor, 1, BUFFER_SIZE);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource,
                null, true, player.getMainHandler(), null);

        // Invoke the callback.
        TrackRenderer[] renderers = new TrackRenderer[AtomicMediaPlayer.RENDERER_COUNT];
        renderers[AtomicMediaPlayer.TYPE_AUDIO] = audioRenderer;
        callback.onRenderers(null, null, renderers);
    }
}
