package net.dropbeat.spark.play;

import android.content.Context;
import android.media.AudioManager;

/**
 * Originally thought by goole reference code
 * Created by vulpes on 15. 5. 5..
 */
public class AudioFocusHelper implements AudioManager.OnAudioFocusChangeListener{

    public static interface MusicFocusable {
        void onGainAudioFocus(AudioManager am);
        void onLoseAudioFocus(AudioManager am, LoseState state);
    }

    public enum LoseState {
        LOSE,
        LOSE_TRANSIENT,
        LOSE_TRANSIENT_CAN_DUCK
    }


    private AudioManager mManager;
    private MusicFocusable mFocusable;

    public AudioFocusHelper(Context ctx, MusicFocusable focusable) {
        mManager = (AudioManager) ctx.getSystemService(Context.AUDIO_SERVICE);
        mFocusable = focusable;
    }

    /** Requests audio focus. Returns whether request was successful or not. */
    public boolean requestFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                mManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN);
    }

    /** Abandons audio focus. Returns whether request was successful or not. */
    public boolean abandonFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                mManager.abandonAudioFocus(this);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                mFocusable.onGainAudioFocus(mManager);
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                mFocusable.onLoseAudioFocus(mManager, LoseState.LOSE);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                mFocusable.onLoseAudioFocus(mManager, LoseState.LOSE_TRANSIENT);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                mFocusable.onLoseAudioFocus(mManager, LoseState.LOSE_TRANSIENT_CAN_DUCK);
                break;
        }
    }
}
