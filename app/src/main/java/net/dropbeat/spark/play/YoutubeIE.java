package net.dropbeat.spark.play;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;

import net.dropbeat.spark.network.Path;
import net.dropbeat.spark.network.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class YoutubeIE {
    private RequestQueue mRequestQ;

    public class IeException extends Exception {
        public IeException(String msg) {
            super(msg);
        }
        public IeException(Throwable t) {
            super(t);
        }
    }

    public class IeIntegrityException extends IeException {
        private String mExceptionMsg;
        public IeIntegrityException(String msg) {
            super(msg);
            mExceptionMsg = msg;
        }
        public String getCleanMessage() {
            return mExceptionMsg;
        }
    }

    public static final String[] TESTS = {
            // Tests of youtube_dl.extractor.YoutubeIE
            "BaW_jenozKc", "UxxajLWwzqY", "07FYdnEawAQ", "yZIXLfi8CZQ", "a9LDPn-MO4I",
            "IB3lcPjvWLA", "nfWlot6h_JM", "T4XJQO3qol8", "HtVdAasjOgU", "6kLq3WMV1nU",
            "__2ABJjxzNo", "lqQg6PlCWgI", "_b-2C3KPAM0", "qEJwOuvDf7I",

            // Some tests of issues from https://github.com/rg3/youtube-dl/issues
            "a9OEBH4HmzY", "gTMnJNEWip4", "InxwGZ_PgC4", "6PGKODQcOw8", "aM9WwHp0x6M",
            "sc03SlMY3vA", "8MMnbeGDzdw", "RaqyRrvGUnE", "e-ORhEE9VVg", "6SFNW5F8K9Y",
            "wnuCSmqC7tI", "Bb4TPuXCd8U", "j8mW06gHL6k",
    };

    private static final Map<String, JSONObject> FORMATS;
    static {
        HashMap<String, JSONObject> _formats = new HashMap<>();

        try {
            _formats.put("5 ", new JSONObject("{'ext': 'flv', 'width': 400, 'height': 240}"));
            _formats.put("6 ", new JSONObject("{'ext': 'flv', 'width': 450, 'height': 270}"));
            _formats.put("13", new JSONObject("{'ext': '3gp'}"));
            _formats.put("17", new JSONObject("{'ext': '3gp', 'width': 176, 'height': 144}"));
            _formats.put("18", new JSONObject("{'ext': 'mp4', 'width': 640, 'height': 360}"));
            _formats.put("22", new JSONObject("{'ext': 'mp4', 'width': 1280, 'height': 720}"));
            _formats.put("34", new JSONObject("{'ext': 'flv', 'width': 640, 'height': 360}"));
            _formats.put("35", new JSONObject("{'ext': 'flv', 'width': 854, 'height': 480}"));
            _formats.put("36", new JSONObject("{'ext': '3gp', 'width': 320, 'height': 240}"));
            _formats.put("37", new JSONObject("{'ext': 'mp4', 'width': 1920, 'height': 1080}"));
            _formats.put("38", new JSONObject("{'ext': 'mp4', 'width': 4096, 'height': 3072}"));
            _formats.put("43", new JSONObject("{'ext': 'webm', 'width': 640, 'height': 360}"));
            _formats.put("44", new JSONObject("{'ext': 'webm', 'width': 854, 'height': 480}"));
            _formats.put("45", new JSONObject("{'ext': 'webm', 'width': 1280, 'height': 720}"));
            _formats.put("46", new JSONObject("{'ext': 'webm', 'width': 1920, 'height': 1080}"));


            // 3d videos
            _formats.put("82", new JSONObject("{'ext': 'mp4', 'height': 360, 'format_note': '3D', 'preference': -20}"));
            _formats.put("83", new JSONObject("{'ext': 'mp4', 'height': 480, 'format_note': '3D', 'preference': -20}"));
            _formats.put("84", new JSONObject("{'ext': 'mp4', 'height': 720, 'format_note': '3D', 'preference': -20}"));
            _formats.put("85", new JSONObject("{'ext': 'mp4', 'height': 1080, 'format_note': '3D', 'preference': -20}"));
            _formats.put("100", new JSONObject("{'ext': 'webm', 'height': 360, 'format_note': '3D', 'preference': -20}"));
            _formats.put("101", new JSONObject("{'ext': 'webm', 'height': 480, 'format_note': '3D', 'preference': -20}"));
            _formats.put("102", new JSONObject("{'ext': 'webm', 'height': 720, 'format_note': '3D', 'preference': -20}"));

            // Apple HTTP Live Streaming
            _formats.put("92", new JSONObject("{'ext': 'mp4', 'height': 240, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("93", new JSONObject("{'ext': 'mp4', 'height': 360, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("94", new JSONObject("{'ext': 'mp4', 'height': 480, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("95", new JSONObject("{'ext': 'mp4', 'height': 720, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("96", new JSONObject("{'ext': 'mp4', 'height': 1080, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("132", new JSONObject("{'ext': 'mp4', 'height': 240, 'format_note': 'HLS', 'preference': -10}"));
            _formats.put("151", new JSONObject("{'ext': 'mp4', 'height': 72, 'format_note': 'HLS', 'preference': -10}"));

            // DASH mp4 video
            _formats.put("133", new JSONObject("{'ext': 'mp4', 'height': 240, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("134", new JSONObject("{'ext': 'mp4', 'height': 360, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("135", new JSONObject("{'ext': 'mp4', 'height': 480, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("136", new JSONObject("{'ext': 'mp4', 'height': 720, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("137", new JSONObject("{'ext': 'mp4', 'height': 1080, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("138", new JSONObject("{'ext': 'mp4', 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("160", new JSONObject("{'ext': 'mp4', 'height': 144, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("264", new JSONObject("{'ext': 'mp4', 'height': 1440, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("298", new JSONObject("{'ext': 'mp4', 'height': 720, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'h264'}"));
            _formats.put("299", new JSONObject("{'ext': 'mp4', 'height': 1080, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'h264'}"));
            _formats.put("266", new JSONObject("{'ext': 'mp4', 'height': 2160, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'vcodec': 'h264'}"));

            // Dash mp4 audio
            _formats.put("139", new JSONObject("{'ext': 'm4a', 'format_note': 'DASH audio', 'acodec': 'aac', 'vcodec': 'none', 'abr': 48, 'preference': -50, 'container': 'm4a_dash'}"));
            _formats.put("140", new JSONObject("{'ext': 'm4a', 'format_note': 'DASH audio', 'acodec': 'aac', 'vcodec': 'none', 'abr': 128, 'preference': -50, 'container': 'm4a_dash'}"));
            _formats.put("141", new JSONObject("{'ext': 'm4a', 'format_note': 'DASH audio', 'acodec': 'aac', 'vcodec': 'none', 'abr': 256, 'preference': -50, 'container': 'm4a_dash'}"));

            // Dash webm
            _formats.put("167", new JSONObject("{'ext': 'webm', 'height': 360, 'width': 640, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("168", new JSONObject("{'ext': 'webm', 'height': 480, 'width': 854, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("169", new JSONObject("{'ext': 'webm', 'height': 720, 'width': 1280, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("170", new JSONObject("{'ext': 'webm', 'height': 1080, 'width': 1920, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("218", new JSONObject("{'ext': 'webm', 'height': 480, 'width': 854, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("219", new JSONObject("{'ext': 'webm', 'height': 480, 'width': 854, 'format_note': 'DASH video', 'acodec': 'none', 'container': 'webm', 'vcodec': 'VP8', 'preference': -40}"));
            _formats.put("278", new JSONObject("{'ext': 'webm', 'height': 144, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'container': 'webm', 'vcodec': 'VP9'}"));
            _formats.put("242", new JSONObject("{'ext': 'webm', 'height': 240, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("243", new JSONObject("{'ext': 'webm', 'height': 360, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("244", new JSONObject("{'ext': 'webm', 'height': 480, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("245", new JSONObject("{'ext': 'webm', 'height': 480, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("246", new JSONObject("{'ext': 'webm', 'height': 480, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("247", new JSONObject("{'ext': 'webm', 'height': 720, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("248", new JSONObject("{'ext': 'webm', 'height': 1080, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("271", new JSONObject("{'ext': 'webm', 'height': 1440, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("272", new JSONObject("{'ext': 'webm', 'height': 2160, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40}"));
            _formats.put("302", new JSONObject("{'ext': 'webm', 'height': 720, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'VP9'}"));
            _formats.put("303", new JSONObject("{'ext': 'webm', 'height': 1080, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'VP9'}"));
            _formats.put("308", new JSONObject("{'ext': 'webm', 'height': 1440, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'VP9'}"));
            _formats.put("313", new JSONObject("{'ext': 'webm', 'height': 2160, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'vcodec': 'VP9'}"));
            _formats.put("315", new JSONObject("{'ext': 'webm', 'height': 2160, 'format_note': 'DASH video', 'acodec': 'none', 'preference': -40, 'fps': 60, 'vcodec': 'VP9'}"));

            // Dash webm audio
            _formats.put("171", new JSONObject("{'ext': 'webm', 'vcodec': 'none', 'format_note': 'DASH audio', 'abr': 128, 'preference': -50}"));
            _formats.put("172", new JSONObject("{'ext': 'webm', 'vcodec': 'none', 'format_note': 'DASH audio', 'abr': 256, 'preference': -50}"));

            // Dash webm audio"with opus inside
            _formats.put("249", new JSONObject("{'ext': 'webm', 'vcodec': 'none', 'format_note': 'DASH audio', 'acodec': 'opus', 'abr': 50, 'preference': -50}"));
            _formats.put("250", new JSONObject("{'ext': 'webm', 'vcodec': 'none', 'format_note': 'DASH audio', 'acodec': 'opus', 'abr': 70, 'preference': -50}"));
            _formats.put("251", new JSONObject("{'ext': 'webm', 'vcodec': 'none', 'format_note': 'DASH audio', 'acodec': 'opus', 'abr': 160, 'preference': -50}"));

            // RTMP (unnamed)
            _formats.put("_rtmp", new JSONObject("{'protocol': 'rtmp'}"));

            _formats.put("mpd", new JSONObject("{'format_note': 'DASH MPD'}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        FORMATS = Collections.unmodifiableMap(_formats);
    }

    private static final String TAG = "YoutubeIe";
    private static String sProtocol = "https";
    private Context mContext;

    public YoutubeIE(Context context, RequestQueue rq) {
        mRequestQ = rq;
        mContext = context;
    }

    public JSONArray extractAudio(String uid) throws
            IeException, JSONException, ExecutionException, InterruptedException {
        ArrayList<JSONObject> tmp = new ArrayList<>();
        JSONArray audioResult = new JSONArray();

        // Extract audio files
        JSONArray fullResult = extract(uid);
        for (int i=0, length=fullResult.length(); i < length; i++) {
            JSONObject result = fullResult.getJSONObject(i);

            String formatNote = "";
            if (result.has("format_note")) {
                formatNote = result.getString("format_note");
            }
            if (result.has("format_id") && "mpd".equals(result.get("format_id"))) {
                tmp.add(result);
                continue;
            }
            if (result.has("ext") && !formatNote.equals("DASH video")) {
                String ext = result.getString("ext");
                if (ext.equals("m4a") || ext.equals("webm")) {
                    tmp.add(result);
                }
            }
        }

        if (!tmp.isEmpty()) {
            // Sort
            Collections.sort(tmp, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject lhs, JSONObject rhs) {
                    try {
                        if ("mpd".equals(lhs.getString("format_id")) ||
                            "mpd".equals(rhs.getString("format_id"))) {
                            return "mpd".equals(lhs.getString("format_id")) ? -1 : 1;
                        }
                        String extLeft = lhs.getString("ext"), extRight = rhs.getString("ext");
                        if (extLeft.equals("webm") && extRight.equals("m4a")) {
                            return -1;
                        } else if (extLeft.equals("m4a") && extRight.equals("webm")) {
                            return 1;
                        }

                        if (lhs.has("abr") && rhs.has("abr")) {
                            Integer abrLeft = lhs.getInt("abr"), abrRight = rhs.getInt("abr");
                            if (abrLeft > abrRight) {
                                return -1;
                            } else if (abrLeft < abrRight) {
                                return 1;
                            }
                        } else if (!lhs.has("abr") && rhs.has("abr")) {
                            return 1;
                        } else if (lhs.has("abr") && !rhs.has("abr")) {
                            return -1;
                        }

                        return 0;
                    } catch (JSONException e) {
                        Log.e(TAG, "Failed when sorting");
                        Log.e(TAG, "Blame me.");
                        return 0;
                    }
                }
            });
        } else {
            // Is there are not any audio files, return all mp4 files
            for (int i=0, length=fullResult.length(); i < length; i++) {
                JSONObject result = fullResult.getJSONObject(i);
                if (result.has("ext")) {
                    if (result.getString("ext").equals("mp4")) {
                        tmp.add(result);
                    }
                }
            }
        }

        for (int i=0, length=tmp.size(); i < length; i++) {
            audioResult.put(tmp.get(i));
        }

        Log.d(TAG, "Selected streaming source count: " + audioResult.length());
        return audioResult;
    }

    public JSONArray extract(String videoId) throws
            IeException, JSONException, ExecutionException, InterruptedException {
        Log.d(TAG, "Start extract: " + videoId);

        RequestFuture<String> videoPageFuture = getVideoPageFuture(videoId);
        RequestFuture<String> embedPageFuture= getEmbedPageFuture(videoId);
        String videoPage = videoPageFuture.get();
        String embedPage = embedPageFuture.get();
        JSONObject videoInfo = getVideoInfo(videoId, videoPage, embedPage);
        checkIntegrity(videoInfo);

        String playerUrl = "";
        Matcher playerUrlMatcher = Pattern
                .compile("swfConfig.*?\"(https?:\\\\/\\\\/.*?watch.*?-.*?\\.swf)")
                .matcher(videoPage);

        if (playerUrlMatcher.find()) {
            playerUrl = playerUrlMatcher.group(1).replaceAll("\\(.\\)", "\1");
            Log.d(TAG, "Player url fetched, " + playerUrl);
        }
        String encodedUrlKey = "";
        if (videoInfo.has("url_encoded_fmt_stream_map")) {
            encodedUrlKey = videoInfo.getString("url_encoded_fmt_stream_map");
        }

        String encodedUrlVal = "";
        if (videoInfo.has("adaptive_fmts")) {
            encodedUrlVal = videoInfo.getString("adaptive_fmts");
        }

        JSONArray formats = new JSONArray();
        RequestFuture<JSONObject> decryptSigFuture = null;
        RequestFuture<String> m3u8Future = null;
        String dashManifestUrl = null;
        HashMap<String, String> urlMap = new HashMap<>();
        List<DecryptReq> reqs = new ArrayList<>();

        if (videoInfo.has("conn")) {
            if (videoInfo.getJSONArray("conn").get(0).toString().startsWith("rtmp")) {
                JSONObject format = new JSONObject();

                format.put("format_id", "_rtmp");
                format.put("protocol", "rtmp");
                format.put("url", videoInfo.getJSONArray("conn").get(0).toString());
                format.put("player_url", playerUrl);

                formats.put(format);
            } else {
                Log.w(TAG, "Expected supporting rtmp, but NOT support");
            }
        } else if (encodedUrlKey.length() > 0 || encodedUrlVal.length() > 0) {

            String encodedUrlMap = encodedUrlKey + "," + encodedUrlVal;
            if (encodedUrlMap.contains("rtmpe%3Dyes")) {
                Log.w(TAG, "rtmpe downloads are not supported, see https,//github" +
                        ".com/rg3/youtube-dl/issues/343 for more information.");
            }

            for (String urlDataStr : encodedUrlMap.split(",")) {
                JSONObject urlData = parseQS(urlDataStr);

                if (!urlData.has("itag") || !urlData.has("url")) {
                    continue;
                }
                String formatId = urlData.getString("itag");
                String realPath = urlData.getString("url");
                realPath = unquote(realPath.replaceAll("\\\\", ""));

                if (urlData.has("sig")) {
                    realPath += "&signature=" + urlData.getString("sig");
                } else if (urlData.has("s")) {
                    boolean ageGate = videoInfo.getBoolean("ageGate");
                    String encryptedSig = urlData.getString("s");
                    String playerContainer = ageGate ? embedPage : videoPage;
                    String jsPlayerUrl = null;

                    Pattern assetRE = Pattern.compile("\"assets\":.+?\"js\":\\s*(\"[^\"]+\")");

                    Matcher jsPlayerMatcher = assetRE.matcher(playerContainer);
                    if (jsPlayerMatcher.find()) {
                        jsPlayerUrl = jsPlayerMatcher.group(1);
                    }

                    if (jsPlayerUrl == null || !ageGate) {
                        Matcher embedPageMatcher = assetRE.matcher(embedPage);
                        if (embedPageMatcher.find()) {
                            jsPlayerUrl = embedPageMatcher.group(1);
                        }

                    }

                    if (jsPlayerUrl == null) {
                        Matcher jsPlayerUrlMatcher = Pattern
                                .compile("ytplayer\\.config.*?\"url\"\\s*:\\s*(\"[^\"]+\")")
                                .matcher(videoPage);
                        if (jsPlayerUrlMatcher.find()) {
                            jsPlayerUrl = jsPlayerUrlMatcher.group(1);
                        }
                    }

                    jsPlayerUrl = unquote(jsPlayerUrl.replaceAll("\\\\", ""));

                    if (jsPlayerUrl.startsWith("//")) {
                        jsPlayerUrl = sProtocol + ":" + jsPlayerUrl;
                    }

                    playerUrl = jsPlayerUrl;

                    RequestFuture<JSONObject> future = lazyDecrypt(encryptedSig,
                            jsPlayerUrl);
                    if (future != null) {
                        DecryptReq req = new DecryptReq(formatId, realPath, future);
                        reqs.add(req);
                    }
                    continue;
                }
                if (!realPath.contains("ratebypass")) {
                    realPath += "&ratebypass=yes";
                }
                urlMap.put(formatId, realPath);
            }
        } else if (videoInfo.has("hlsvp")) {
            m3u8Future = simpleGetFuture(videoInfo.getString("hlsvp"));
        } else {
            String errorMsg = "Youtube video page does not have enough " +
                    "information for extraction";
            Log.e(TAG, errorMsg);
            throw new IeException(errorMsg);
        }

        if (videoInfo.has("dashmpd")) {
            dashManifestUrl = videoInfo.getString("dashmpd");
            Matcher matcher = Pattern.compile("/s/(\\w+\\.\\w+)").matcher
                    (dashManifestUrl);

            if (matcher.find()) {
                // We will not handle futre response here
                // because it blocks thread
                String encryptedSig = matcher.group(1);
                String url = unquote(playerUrl.replaceAll("\\\\", ""));

                if (url.startsWith("//")) {
                    url = sProtocol + ":" + url;
                }
                decryptSigFuture = lazyDecrypt(encryptedSig, url);
            } else {
                JSONObject dashFormat = new JSONObject();
                dashFormat.put("format_id", "mpd");
                dashFormat.put("url", dashManifestUrl);
                formats.put(dashFormat);
            }
        }



        // handle all sent request results
        for (DecryptReq req : reqs) {
            String formatId = req.formatId;
            String realPath = req.realPath;
            RequestFuture<JSONObject> future = req.future;

            try {
                JSONObject decrypted = future.get();
                String sig = decrypted.getString("signature");

                if (!realPath.contains("ratebypass")) {
                    realPath += "&ratebypass=yes";
                }

                urlMap.put(formatId, realPath + "&signature=" + sig);
            } catch (ExecutionException | JSONException e) {
                // skip this request
                logException(e, videoId);
            }
        }

        if (m3u8Future != null) {
            try {
                urlMap.putAll(parseM3U8(m3u8Future.get()));
            } catch (ExecutionException e) {
                // skip this request
                logException(e, videoId);
            }
        }

        if (urlMap.size() > 0) {
            JSONArray mapData = mapToFormatList(urlMap);
            for (int i = 0; i < mapData.length(); i++) {
                formats.put(mapData.get(i));
            }
        }

        if (decryptSigFuture != null) {
            try {
                String sig = decryptSigFuture.get().getString("signature");
                String signaturedDashUrl = dashManifestUrl
                        .replaceAll("/s/(\\w+\\.\\w+)", "/signature/" + sig);
                JSONObject format =new JSONObject();
                format.put("format_id", "mpd");
                format.put("url", signaturedDashUrl);
                formats.put(format);
            } catch (ExecutionException | JSONException e) {
                // skip this request
                logException(e, videoId);
            }
        }

        Log.d(TAG, "Extract fin.");
        Log.d(TAG, "result count: " + formats.length());
        return formats;
    }

    private String simpleGet(String url) throws InterruptedException, ExecutionException {
        return simpleGetFuture(url).get();
    }

    private RequestFuture<String> simpleGetFuture(String url) throws InterruptedException, ExecutionException {
        RequestFuture<String> future = RequestFuture.newFuture();
        SimpleStringRequest req =
                new SimpleStringRequest(StringRequest.Method.GET, url, future, future);
        mRequestQ.add(req);

        return future;
    }

    private static JSONObject parseQS(String str) throws IeException, JSONException{
        JSONObject queryPairs = new JSONObject();

        String[] pairs = str.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            try {
                queryPairs.put(
                    URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                    URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
                );
            } catch(UnsupportedEncodingException e) {
                queryPairs.put(
                    pair.substring(0, idx),
                    pair.substring(idx + 1)
                );
                e.printStackTrace();
            }
        }

        return queryPairs;
    }

    private RequestFuture<JSONObject> lazyDecrypt(String sig, String playerUrl) {
        if (TextUtils.isEmpty(sig) || TextUtils.isEmpty(playerUrl)) {
            return null;
        }
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        String url = Path.decryptSignature + "?s=" + sig + "&p=" + playerUrl;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url,
                future, future);
        mRequestQ.add(req);

        return future;
    }


    public static String unquote(String s) {
        if (s != null &&
            ((s.startsWith("\"") && s.endsWith("\"")) ||
            (s.startsWith("'") && s.endsWith("'")))) {

            s = s.substring(1, s.length() - 1);
        }
        return s;
    }

    private JSONArray mapToFormatList(Map<String, String> urlMap) throws JSONException {
        JSONArray formats = new JSONArray();

        for (HashMap.Entry<String, String> entry :  urlMap.entrySet()) {
            JSONObject format = new JSONObject();

            String itag = entry.getKey();
            String realPath = entry.getValue();
            format.put("format_id", itag);
            format.put("url", realPath);

            if (FORMATS.keySet().contains(itag)) {
                JSONObject _format = FORMATS.get(itag);

                Iterator<String> keys = _format.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    format.put(key, _format.get(key));
                }
            }
            formats.put(format);
        }
        return formats;
    }

    private HashMap<String, String> parseM3U8(String response) {
        HashMap<String, String> formats = new HashMap<>();

        String[] formatUrls = getUrls(response);

        for (String realPath : formatUrls) {
            String itag;
            Matcher itagMatcher = Pattern.compile("itag/(\\d+?)/").matcher(realPath);
            if (itagMatcher.find()) {
                itag = itagMatcher.group(1);
                formats.put(itag, realPath);
            }
        }

        return formats;
    }

    private String[] getUrls(String manifest) {
        ArrayList<String> urls = new ArrayList<>();
        String[] lines = manifest.split("\n");
        for (String line : lines) {
            if (!line.isEmpty() && !line.startsWith("#")) {
                urls.add(line);
            }
        }

        String[] result = new String [urls.size()];
        return urls.toArray(result);
    }

    private JSONObject getVideoInfo(String videoId, String videoPage, String embedPage)
            throws IeException, ExecutionException, InterruptedException, JSONException {
        boolean ageGate;
        JSONObject videoInfo = new JSONObject();
        if (Pattern.compile("player-age-gate-content\">").matcher(videoPage).find()) {
            ageGate = true;
            String sts = "";
            Matcher stsMatcher = Pattern.compile("\"sts\"\\s*:\\s*(\\d+)").matcher(embedPage);
            if (stsMatcher.find()) {
                sts = stsMatcher.group(1);
            }

            String query =
                    "video_id=" + videoId + "&" +
                            "eurl=" + "https://youtube.googleapis.com/v/" + videoId + "&" +
                            "sts=" + sts;
            String videoInfoUrl = sProtocol + "://www.youtube.com/get_video_info?" + query;
            String videoInfoPage = simpleGet(videoInfoUrl);

            videoInfo = parseQS(videoInfoPage);
        } else {
            ageGate = false;
            boolean isValidVideoInfo = false;
            Matcher videoPageMatcher = Pattern
                    .compile(";ytplayer\\.config\\s*=\\s*(\\{.*\\});")
                    .matcher(videoPage);

            if (videoPageMatcher.find()) {
                videoInfo = new JSONObject(videoPageMatcher.group(1)).getJSONObject("args");

                if (videoInfo.has("url_encoded_fmt_stream_map")) {
                    isValidVideoInfo = true;
                }
            }
            if (!isValidVideoInfo) {
                String[] elTypes = {"&el=embedded", "&el=detailpage", "&el=vevo", ""};
                for (String elType: elTypes) {
                    String videoInfoUrl =
                            sProtocol + "://www.youtube.com/get_video_info?&video_id=" +
                                    videoId + elType + "&ps=default&eurl=&gl=US&hl=en";

                    String videoInfoPage = simpleGet(videoInfoUrl);
                    videoInfo = parseQS(videoInfoPage);
                    if (videoInfo.has("token")) {
                        break;
                    }
                }
            }
        }
        videoInfo.put("ageGate", ageGate);
        return videoInfo;
    }

    private void checkIntegrity(JSONObject videoInfo) throws JSONException, IeException {
        if (!videoInfo.has("token")) {
            String errorMsg = "undefined error";
            if (videoInfo.has("reason")) {
                String reason = videoInfo.getString("reason");
                errorMsg = "Youtube says, " + reason;
                Log.e(TAG, errorMsg);
                throw new IeIntegrityException(reason);
            }
            throw new IeException(errorMsg);
        }
        if (videoInfo.has("ypc_video_rental_bar_text") && !videoInfo.has("author")) {
            String errorMsg = "'rental' video not supported";
            Log.e(TAG, errorMsg);
            throw new IeException(errorMsg);
        }
    }

    private RequestFuture<String> getVideoPageFuture(String videoId) throws
            ExecutionException, InterruptedException {
        String webpageUrl = sProtocol +
                "://www.youtube.com/watch?v=" + videoId +
                "&gl=US&hl=en&has_verified=1&bpctr=9999999999";
        return simpleGetFuture(webpageUrl);
    }

    private RequestFuture<String> getEmbedPageFuture(String videoId) throws
            ExecutionException, InterruptedException {
        String embedUrl = sProtocol + "://www.youtube.com/embed/" + videoId;
        return simpleGetFuture(embedUrl);
    }

    private void logException(Exception e, String videoId) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        Requests.logPlayFailure(mContext, "decrypt_failure", "-",
                videoId, null, stacktrace);
    }

    private class SimpleStringRequest extends StringRequest {
        int TIMEOUT = 15000; // Timeout 10 secs
        Map<String, String> body;
        public SimpleStringRequest(
                int method,
                String url,
                Response.Listener<String> listener,
                Response.ErrorListener errorListener) {
            this(method, url, null, listener, errorListener);
        }
        public SimpleStringRequest(
                int method,
                String url,
                Map<String, String> body_,
                Response.Listener<String> listener,
                Response.ErrorListener errorListener) {
            super(method, url, listener, errorListener);
            this.setRetryPolicy(new DefaultRetryPolicy(
                    TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            body = body_;
        }
        @Override
        public Map<String, String> getHeaders() {
            HashMap<String, String> header = new HashMap<>();
            header.put("Connection", "keep-alive");
            String locale = Locale.getDefault().toString();
            String lang = Locale.getDefault().getLanguage();
            String format = "%s,%s;q=0.8,en-US;q=0.6,en;q=0.4";
            String langHeader = String.format(format, locale, lang);
            header.put("Accept-Language", langHeader);
            header.put("Accept", "application/xhtml+xml,application/xml,text/html;q=0.9,image/webp,*/*;q=0.8");
            header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            return header;
        }

        @Override
        public byte[] getBody() {
            Map<String, String> params = body;

            if (params != null && params.size() > 0) {
                return encodeParameters(params, getParamsEncoding());
            }
            return null;
        }

        private byte[] encodeParameters(Map<String, String> params, String paramsEncoding) {
            StringBuilder encodedParams = new StringBuilder();
            try {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
                    encodedParams.append('=');
                    encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
                    encodedParams.append('&');
                }
                return encodedParams.toString().getBytes(paramsEncoding);
            } catch (UnsupportedEncodingException uee) {
                throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
            }
        }
    }

    private static class DecryptReq {

        String formatId;
        String realPath;
        RequestFuture<JSONObject> future;
        public DecryptReq(String formatId, String realPath,
                          RequestFuture<JSONObject> future) {
            this.formatId = formatId;
            this.realPath = realPath;
            this.future = future;
        }
    }
}
