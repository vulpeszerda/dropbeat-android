package net.dropbeat.spark.play;

public final class UnsupportedDrmException extends Exception {

    public static final int REASON_NO_DRM = 0;
    public static final int REASON_UNSUPPORTED_SCHEME = 1;
    public static final int REASON_UNKNOWN = 2;

    public final int reason;

    public UnsupportedDrmException(int reason) {
        this.reason = reason;
    }

    public UnsupportedDrmException(int reason, Exception cause) {
        super(cause);
        this.reason = reason;
    }

}
