package net.dropbeat.spark.play;

import android.media.MediaCodec;
import android.os.Handler;
import android.os.Looper;

import com.google.android.exoplayer.DummyTrackRenderer;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.audio.AudioTrack;
import com.google.android.exoplayer.chunk.ChunkSampleSource;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.chunk.MultiTrackChunkSource;
import com.google.android.exoplayer.drm.StreamingDrmSessionManager;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.util.PlayerControl;

import net.dropbeat.spark.constants.RepeatState;
import net.dropbeat.spark.model.StreamSource;
import net.dropbeat.spark.model.Track;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

public class AtomicMediaPlayer implements
        ExoPlayer.Listener {

    /**
     * Builds renderers for the player.
     */
    public interface RendererBuilder {
        /**
         * Constructs the necessary components for playback.
         *
         * @param player The parent player.
         * @param callback The callback to invoke with the constructed components.
         */
        void buildRenderers(AtomicMediaPlayer player, RendererBuilderCallback callback);
    }

    /**
     * A callback invoked by a {@link RendererBuilder}.
     */
    public interface RendererBuilderCallback {
        /**
         * Invoked with the results from a {@link RendererBuilder}.
         *
         * @param trackNames The names of the available tracks, indexed by {@link AtomicMediaPlayer}
         *     TYPE_* constants. May be null if the track names are unknown. An individual element
         *     may be null if the track names are unknown for the corresponding type.
         * @param multiTrackSources Sources capable of switching between multiple available tracks,
         *     indexed by {@link AtomicMediaPlayer} TYPE_* constants.
         *     May be null if there are no types with multiple tracks. An individual element may be
         *     null if it does not have multiple tracks.
         * @param renderers Renderers indexed by {@link AtomicMediaPlayer} TYPE_* constants.
         *     An individual element may be null if there do not exist tracks of the corresponding type.
         */
        void onRenderers(String[][] trackNames, MultiTrackChunkSource[] multiTrackSources,
                         TrackRenderer[] renderers);
        /**
         * Invoked if a {@link RendererBuilder} encounters an error.
         *
         * @param e Describes the error.
         */
        void onRenderersError(Exception e);
    }

    /**
     * A listener for core events.
     */
    public interface Listener {
        void onStateChanged(boolean playWhenReady, int playbackState);
        void onError(Exception e);
    }

    // Constants pulled into this class for convenience.
    public static final int STATE_IDLE = ExoPlayer.STATE_IDLE;
    public static final int STATE_PREPARING = ExoPlayer.STATE_PREPARING;
    public static final int STATE_BUFFERING = ExoPlayer.STATE_BUFFERING;
    public static final int STATE_READY = ExoPlayer.STATE_READY;
    public static final int STATE_ENDED = ExoPlayer.STATE_ENDED;

    public static final int DISABLED_TRACK = -1;

    public static final int RENDERER_COUNT = 1;
    public static final int TYPE_AUDIO = 0;

    private static final int RENDERER_BUILDING_STATE_IDLE = 1;
    private static final int RENDERER_BUILDING_STATE_BUILDING = 2;
    private static final int RENDERER_BUILDING_STATE_BUILT = 3;

    private RendererBuilder mRendererBuilder;
    private PlayerService.RendererBuilderSelector mRendererBuilderSelector;
    private ExoPlayer mPlayer;
    private PlayerControl mPlayerControl;
    private Handler mMainHandler;
    private boolean mLooping = false;
    private CopyOnWriteArrayList<Listener> mListeners;

    private InternalRendererBuilderCallback mBuilderCallback;

    private MultiTrackChunkSource[] mMultiTrackSources;
    private String[][] mTrackNames;
    private int[] mSelectedTracks;

    private MediaCodecAudioTrackRenderer mAudioRenderer;
    private boolean mLastReportedPlayWhenReady;
    private int mLastReportedPlaybackState;
    private int mRendererBuildingState;

    public AtomicMediaPlayer(PlayerService.RendererBuilderSelector selector) {
        this.mRendererBuilderSelector = selector;
        mPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT, 1000, 5000);
        mPlayer.addListener(this);
        mPlayerControl = new PlayerControl(mPlayer);
        mMainHandler = new Handler(Looper.getMainLooper());
        mListeners = new CopyOnWriteArrayList<>();
        mLastReportedPlaybackState = STATE_IDLE;
        mRendererBuildingState = RENDERER_BUILDING_STATE_IDLE;
        mSelectedTracks = new int[RENDERER_COUNT];
    }

    public void addListener(Listener listener) {
        mListeners.add(listener);
    }

    public void removeListener(Listener listener) {
        mListeners.remove(listener);
    }

    void onRenderers(String[][] trackNames,
            MultiTrackChunkSource[] multiTrackSources, TrackRenderer[] renderers) {
        mBuilderCallback = null;
        mAudioRenderer = (MediaCodecAudioTrackRenderer) renderers[TYPE_AUDIO];
        // Normalize the results.
        if (trackNames == null) {
            trackNames = new String[RENDERER_COUNT][];
        }
        if (multiTrackSources == null) {
            multiTrackSources = new MultiTrackChunkSource[RENDERER_COUNT];
        }
        for (int i = 0; i < RENDERER_COUNT; i++) {
            if (renderers[i] == null) {
                // Convert a null renderer to a dummy renderer.
                renderers[i] = new DummyTrackRenderer();
            } else if (trackNames[i] == null) {
                // We have a renderer so we must have at least one track, but the names are unknown.
                // Initialize the correct number of null track names.
                int trackCount = multiTrackSources[i] == null ? 1 : multiTrackSources[i].getTrackCount();
                trackNames[i] = new String[trackCount];
            }
        }
        // Complete preparation.
        this.mTrackNames = trackNames;
        this.mMultiTrackSources = multiTrackSources;
        pushTrackSelection(TYPE_AUDIO, true);
        mPlayer.setPlayWhenReady(true);
        mPlayer.prepare(renderers);
        mRendererBuildingState = RENDERER_BUILDING_STATE_BUILT;
    }

    // Because onRenderersError called from player initailize thread,
    // we have to post message with main handler
    private void onRenderersError(final Exception e) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mBuilderCallback = null;
                for (Listener listener : mListeners) {
                    listener.onError(e);
                }
                mRendererBuildingState = RENDERER_BUILDING_STATE_IDLE;
                maybeReportPlayerState();
            }
        });
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        mPlayer.setPlayWhenReady(playWhenReady);
    }

    public void release() {
        if (mBuilderCallback != null) {
            mBuilderCallback.cancel();
            mBuilderCallback = null;
        }
        mRendererBuildingState = RENDERER_BUILDING_STATE_IDLE;
        mPlayer.release();
    }

    public int getPlaybackState() {
        if (mRendererBuildingState == RENDERER_BUILDING_STATE_BUILDING) {
            return STATE_PREPARING;
        }
        return mPlayer.getPlaybackState();
    }

    public boolean getPlayWhenReady() {
        return mPlayer.getPlayWhenReady();
    }

    public Looper getPlaybackLooper() {
        return mPlayer.getPlaybackLooper();
    }

    public Handler getMainHandler() {
        return mMainHandler;
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int state) {
        maybeReportPlayerState();
    }

    @Override
    public void onPlayerError(final ExoPlaybackException exception) {
        mRendererBuildingState = RENDERER_BUILDING_STATE_IDLE;
        for (Listener listener : mListeners) {
            listener.onError(exception);
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {
        // Do nothing.
    }

    private void maybeReportPlayerState() {
        final boolean playWhenReady = mPlayer.getPlayWhenReady();
        final int playbackState = getPlaybackState();
        if (mLastReportedPlayWhenReady != playWhenReady || mLastReportedPlaybackState !=
                playbackState) {
            for (Listener listener : mListeners) {
                listener.onStateChanged(playWhenReady, playbackState);
            }
            mLastReportedPlayWhenReady = playWhenReady;
            mLastReportedPlaybackState = playbackState;
        }
    }

    private void pushTrackSelection(int type, boolean allowRendererEnable) {
        if (mMultiTrackSources == null) {
            return;
        }

        int trackIndex = mSelectedTracks[type];
        if (trackIndex == DISABLED_TRACK) {
            mPlayer.setRendererEnabled(type, false);
        } else if (mMultiTrackSources[type] == null) {
            mPlayer.setRendererEnabled(type, allowRendererEnable);
        } else {
            boolean playWhenReady = mPlayer.getPlayWhenReady();
            mPlayer.setPlayWhenReady(false);
            mPlayer.setRendererEnabled(type, false);
            mPlayer.sendMessage(mMultiTrackSources[type], MultiTrackChunkSource.MSG_SELECT_TRACK,
                    trackIndex);
            mPlayer.setRendererEnabled(type, allowRendererEnable);
            mPlayer.setPlayWhenReady(playWhenReady);
        }
    }

    public synchronized void start() {
        if (mPlayer != null &&
                mPlayer.getPlaybackState() != STATE_IDLE &&
                mPlayer.getPlaybackState() != STATE_PREPARING &&
                mPlayer.getPlaybackState() != STATE_BUFFERING) {
            if (!getPlayWhenReady()) {
                setPlayWhenReady(true);
            }
            mPlayerControl.start();
        }
    }

    public synchronized void stop() {
        onPlayerStateChanged(false, STATE_ENDED);
        release();
    }

    public synchronized void pause() {
        if (mPlayer != null &&
                mPlayer.getPlaybackState() != STATE_IDLE &&
                mPlayer.getPlaybackState() != STATE_PREPARING &&
                mPlayer.getPlaybackState() != STATE_ENDED &&
                mPlayer.getPlaybackState() != STATE_BUFFERING && mPlayerControl.isPlaying()) {
            if (getPlayWhenReady()) {
                setPlayWhenReady(false);
            }
            mPlayerControl.pause();
        }
    }

    public synchronized int getCurrentPosition() {
        if (mPlayer != null) {
            return (int) mPlayer.getCurrentPosition();
        }
        return 0;
    }

    public synchronized int getDuration() {
        if (mPlayer != null) {
            return (int) mPlayer.getDuration();
        }
        return 0;
    }

    public synchronized void seekTo(int position) {
        if (mPlayer != null &&
                mPlayer.getPlaybackState() != STATE_IDLE &&
                mPlayer.getPlaybackState() != STATE_PREPARING &&
                mPlayer.getPlaybackState() != STATE_BUFFERING) {
            mPlayer.seekTo(position);
        }
    }
    public synchronized boolean isPlaying() {
        if (mPlayer != null) {
            return (getPlaybackState() == STATE_READY && getPlayWhenReady());
        }
        return false;
    }

    public synchronized boolean isPaused() {
        return (mPlayer != null && mPlayer.getPlaybackState() == STATE_READY &&
                !mPlayer.getPlayWhenReady());
    }

    public synchronized boolean isStopped() {
        return (mPlayer.getPlaybackState() == STATE_ENDED ||
                mPlayer.getPlaybackState() == STATE_IDLE);
    }

    public synchronized void setVolume(float volume) {
        if (mPlayer != null) {
            mPlayer.sendMessage(mAudioRenderer,
                    MediaCodecAudioTrackRenderer.MSG_SET_VOLUME, volume);
        }
    }

    public synchronized void setLooping(boolean looping) {
        mLooping = looping;
    }

    public synchronized boolean getLooping() {
        return mLooping;
    }

    public synchronized void load(Track track, StreamSource source) throws IOException {
        reset();
        if (mRendererBuildingState == RENDERER_BUILDING_STATE_BUILT) {
            mPlayer.stop();
        }

        if (mBuilderCallback != null) {
            mBuilderCallback.cancel();
        }

        mMultiTrackSources = null;
        mRendererBuildingState = RENDERER_BUILDING_STATE_BUILDING;
        maybeReportPlayerState();
        mBuilderCallback = new InternalRendererBuilderCallback();
        mRendererBuilder = mRendererBuilderSelector.get(track, source);
        if (mRendererBuilder == null) {
            onPlayerError(new ExoPlaybackException(
                    "Failed to find proper rendererBuilder"));
            return;
        }
        mRendererBuilder.buildRenderers(this, mBuilderCallback);
    }

    private synchronized void reset() {
        stop();
        mPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
        mPlayerControl = new PlayerControl(mPlayer);
        mPlayer.addListener(this);
        setLooping(PlayerContext.repeatState == RepeatState.REPEAT_ONE);
    }

    private class InternalRendererBuilderCallback implements RendererBuilderCallback {

        private boolean mCanceled;

        public void cancel() {
            mCanceled = true;
        }

        @Override
        public void onRenderers(String[][] trackNames, MultiTrackChunkSource[] multiTrackSources,
                                TrackRenderer[] renderers) {
            if (!mCanceled) {
                AtomicMediaPlayer.this.onRenderers(trackNames, multiTrackSources, renderers);
            }
        }

        @Override
        public void onRenderersError(Exception e) {
            if (!mCanceled) {
                AtomicMediaPlayer.this.onRenderersError(e);
            }
        }
    }

}
