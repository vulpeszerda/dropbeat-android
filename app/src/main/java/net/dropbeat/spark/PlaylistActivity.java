package net.dropbeat.spark;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.volley.VolleyError;

import net.dropbeat.spark.addon.ProgressDialogFragment;
import net.dropbeat.spark.auth.AuthActivity;
import net.dropbeat.spark.auth.DbAccount;
import net.dropbeat.spark.constants.PlayState;
import net.dropbeat.spark.model.Playlist;
import net.dropbeat.spark.model.Track;
import net.dropbeat.spark.network.Requests;
import net.dropbeat.spark.network.RobustResponseErrorListener;
import net.dropbeat.spark.network.RobustResponseListener;
import net.dropbeat.spark.play.PlayerContext;
import net.dropbeat.spark.play.PlayerService;
import net.dropbeat.spark.playlist.PlaylistAdapter;
import net.dropbeat.spark.playlist.PlaylistFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 8. 14..
 */

@ContentView(R.layout.activity_shared_playlist)
public class PlaylistActivity extends PlayerActivity
        implements TrackClickListener{

    public static Intent createIntent(Context context, Playlist playlist) {
        Intent intent = new Intent(context, PlaylistActivity.class);
        intent.putExtra(PARAM_PLAYLIST, playlist);
        return intent;
    }

    private static final String PARAM_PLAYLIST = "playlist";

    @InjectView(android.R.id.list)
    private ListView mListView;

    private PlaylistAdapter mAdapter;
    private Playlist mPlaylist;
    private boolean mIsImported;

    private BroadcastReceiver mPlayStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isFinishing() && (PlayerContext.playState == PlayState.LOADING ||
                    PlayerContext.playState == PlayState.STOPPED) &&
                    mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mPlaylist = (Playlist)intent.getSerializableExtra(PARAM_PLAYLIST);


        ActionBar actionbar = getSupportActionBar();
        if (!isTaskRoot()) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }

        actionbar.setSubtitle(mPlaylist.getName());
        mAdapter = new PlaylistAdapter(this);
        mAdapter.setPlaylistId(mPlaylist.getId());
        mAdapter.addAll(mPlaylist.getTracks());
        mAdapter.setTrackClickListener(this);

        mListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_shared_playlist, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mIsImported) {
            MenuItem item = menu.findItem(R.id.action_import);
            if (mIsImported) {
                item.setIcon(R.drawable.ic_actionbar_heart_fill);
            } else {
                item.setIcon(R.drawable.ic_actionbar_heart);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_import:
                if (mIsImported) {
                    return true;
                }
                if (DbAccount.getUser(this) == null) {
                    startActivity(AuthActivity.createIntent(this, true));
                } else {
                    importPlaylist();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.unregisterReceiver(mPlayStateChangeReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(
                PlayerService.ACTION_NOTIFY_PLAYSTATE_CHANGE);
        manager.registerReceiver(mPlayStateChangeReceiver, filter);
    }

    @Override
    public void onClick(View view, final Track track, int position) {
        switch(view.getId()) {
            case R.id.track:
                PlayerContext.externalPlaylist = mPlaylist;
                PlayerService.doPlay(this, track, mPlaylist.getId());
                break;
            case R.id.menu_button:
                PopupMenu menu = new PopupMenu(this, view);

                menu.getMenuInflater().inflate(
                        R.menu.menu_shared_playlist_track, menu.getMenu());
                menu.setOnMenuItemClickListener(
                        new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onTrackPopupMenuClicked(track, item);
                    }
                });
                menu.show();
                break;
        }
    }

    private boolean onTrackPopupMenuClicked(Track track, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                PlaylistFragment.addTrack(this, track, "shared_playlist");
                return true;
            case R.id.action_share:
                track.share(this);
                return true;
        }
        return false;
    }

    private void updatePlaylist(final String id) {

        final RobustResponseListener<JSONObject> updateListener =
                new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {
                mIsImported = true;
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hide(PlaylistActivity.this, "import");
                if (!response.optBoolean("success")) {
                    Requests.deletePlaylist(PlaylistActivity.this, id, null, null);
                    Toast.makeText(PlaylistActivity.this,
                            "Failed to save playlist", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                mIsImported = true;
                invalidateOptionsMenu();
            }
        };

        final RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(PlaylistActivity.this, "import");
                Requests.deletePlaylist(PlaylistActivity.this, id, null, null);
                Toast.makeText(PlaylistActivity.this, "Failed to save playlist",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {
                Requests.deletePlaylist(PlaylistActivity.this, id, null, null);
            }
        };

        JSONArray data = mPlaylist.toJsonArray();
        Requests.setPlaylist(PlaylistActivity.this, id, data, updateListener,
                errorListener);
    }

    private void importPlaylist() {
        ProgressDialogFragment.show(this, "import", "Saving..");


        final RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(PlaylistActivity.this, "import");
                Toast.makeText(PlaylistActivity.this, "Failed to save playlist",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        final RobustResponseListener<JSONObject> createListener =
                new RobustResponseListener<JSONObject>(this) {
            @Override
            public void onSkipped(JSONObject response) {

            }

            @Override
            public void onSecureResponse(JSONObject response) {
                ProgressDialogFragment.hide(PlaylistActivity.this, "import");
                if (!response.optBoolean("success")) {
                    Toast.makeText(PlaylistActivity.this, "Failed to save playlist",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    JSONObject obj = response.getJSONObject("obj");
                    String playlistId = obj.getString("id");
                    updatePlaylist(playlistId);
                } catch (JSONException e) {
                    Toast.makeText(PlaylistActivity.this, "Failed to save playlist",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        };


        Requests.createPlaylist(this, mPlaylist.getName(),
                createListener, errorListener);
    }
}
